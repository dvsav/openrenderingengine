/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "UniformBlockBuffer.h"

#include "EngineTypedefs.h"

namespace engine
{
    struct Material
    {
        color_rgba32f EmissiveColor;
        color_rgba32f DiffuseColor;
        color_rgba32f SpecularColor;
        GLfloat Shininess; // Typical value ranges from 1 to 128.
    };

    class UniformBlock_Material : public opengl::UniformBlockStructure<Material>
    {
        #pragma region Type Aliases

        public:
            using TEmissiveColor = decltype(Material::EmissiveColor);
            using TDiffuseColor = decltype(Material::DiffuseColor);
            using TSpecularColor = decltype(Material::SpecularColor);
            using TShininess = decltype(Material::Shininess);

        #pragma endregion

        #pragma region Constructors

        public:
            UniformBlock_Material() :
                opengl::UniformBlockStructure<Material>(
                    Material
                    {
                        make_Color_RGBA32F_Black(),
                        make_Color_RGBA32F_Gray(0.5f),
                        make_Color_RGBA32F_Gray(0.5f),
                        1.0f
                    },
                    opengl::Buffer::DRAW_USAGE::STATIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void setEmissiveColor(const TEmissiveColor& value)
            {
                this->setData(
                    /*offset*/ offsetof(Material, Material::EmissiveColor),
                    /*size*/ sizeof(TEmissiveColor),
                    /*data*/ &value);
            }

            TEmissiveColor getEmissiveColor() const
            {
                TEmissiveColor value;
                this->getData(
                    /*offset*/ offsetof(Material, Material::EmissiveColor),
                    /*size*/ sizeof(TEmissiveColor),
                    /*data*/ &value);
                return value;
            }

            void setDiffuseColor(const TDiffuseColor& value)
            {
                this->setData(
                    /*offset*/ offsetof(Material, Material::DiffuseColor),
                    /*size*/ sizeof(TDiffuseColor),
                    /*data*/ &value);
            }

            TDiffuseColor getDiffuseColor() const
            {
                TDiffuseColor value;
                this->getData(
                    /*offset*/ offsetof(Material, Material::DiffuseColor),
                    /*size*/ sizeof(TDiffuseColor),
                    /*data*/ &value);
                return value;
            }

            void setSpecularColor(const TSpecularColor& value)
            {
                this->setData(
                    /*offset*/ offsetof(Material, Material::SpecularColor),
                    /*size*/ sizeof(TSpecularColor),
                    /*data*/ &value);
            }

            TSpecularColor getSpecularColor() const
            {
                TSpecularColor value;
                this->getData(
                    /*offset*/ offsetof(Material, Material::SpecularColor),
                    /*size*/ sizeof(TSpecularColor),
                    /*data*/ &value);
                return value;
            }

            void setShininess(const TShininess& value)
            {
                this->setData(
                    /*offset*/ offsetof(Material, Material::Shininess),
                    /*size*/ sizeof(TShininess),
                    /*data*/ &value);
            }

            TShininess getShininess() const
            {
                TShininess value;
                this->getData(
                    /*offset*/ offsetof(Material, Material::Shininess),
                    /*size*/ sizeof(TShininess),
                    /*data*/ &value);
                return value;
            }

        #pragma endregion
    };
}
