/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "OpenGLUtility.h"
#include "Texture2D.h"
#include "TextureCubemap.h"
#include "FramebufferObject.h"
#include "BMP.h"
#include "Image.h"
#include "Requires.h"

namespace engine
{
    inline opengl::Texture2D CreateTextureFromFile(
        const std::string& fileName)
    {
        util::Image img(fileName, 4);

        return opengl::make_Texture2D_RGBA32F(
            /*width*/  img.width(),
            /*height*/ img.height(),
            /*format*/ opengl::PIXEL_FORMAT::RGBA,
            /*level_zero_data*/ reinterpret_cast<const GLubyte*>(img.data()));
    }

    template <typename TColor>
    inline opengl::Texture2D make_Texture2dMonochrome(
        const TColor& color);

    template<>
    inline opengl::Texture2D make_Texture2dMonochrome<color_rgba>(
        const color_rgba& color)
    {
        return opengl::make_Texture2D_RGBA32F(1, 1, opengl::PIXEL_FORMAT::RGBA, color.data());
    }

    template<>
    inline opengl::Texture2D make_Texture2dMonochrome<color_rgba32f>(
        const color_rgba32f& color)
    {
        color_rgba value
        {
            static_cast<color_rgba::value_type>(255.0f * color.R),
            static_cast<color_rgba::value_type>(255.0f * color.G),
            static_cast<color_rgba::value_type>(255.0f * color.B),
            static_cast<color_rgba::value_type>(255.0f * color.A)
        };
        return opengl::make_Texture2D_RGBA32F(1, 1, opengl::PIXEL_FORMAT::RGBA, value.data());
    }

    inline opengl::TextureCubemap CreateTextureCubemapFromFiles(
        const std::string& pos_x,
        const std::string& neg_x,
        const std::string& pos_y,
        const std::string& neg_y,
        const std::string& pos_z,
        const std::string& neg_z)
    {
        util::Image pos_x_texture(pos_x, 4);
        util::Image neg_x_texture(neg_x, 4);
        util::Image pos_y_texture(pos_y, 4);
        util::Image neg_y_texture(neg_y, 4);
        util::Image pos_z_texture(pos_z, 4);
        util::Image neg_z_texture(neg_z, 4);

        util::Requires::That(
            pos_x_texture.width() == neg_x_texture.width() &&
            pos_x_texture.width() == pos_y_texture.width() &&
            pos_x_texture.width() == neg_y_texture.width() &&
            pos_x_texture.width() == pos_z_texture.width() &&
            pos_x_texture.width() == neg_z_texture.width() &&
            pos_x_texture.height() == neg_x_texture.height() &&
            pos_x_texture.height() == pos_y_texture.height() &&
            pos_x_texture.height() == neg_y_texture.height() &&
            pos_x_texture.height() == pos_z_texture.height() &&
            pos_x_texture.height() == neg_z_texture.height(),
            FUNCTION_INFO);

        return opengl::make_TextureCubemap_RGBA32F(
            /*width*/  pos_x_texture.width(),
            /*height*/ pos_x_texture.height(),
            /*format*/ opengl::PIXEL_FORMAT::RGBA,
            reinterpret_cast<const GLubyte*>(pos_x_texture.data()),
            reinterpret_cast<const GLubyte*>(neg_x_texture.data()),
            reinterpret_cast<const GLubyte*>(pos_y_texture.data()),
            reinterpret_cast<const GLubyte*>(neg_y_texture.data()),
            reinterpret_cast<const GLubyte*>(pos_z_texture.data()),
            reinterpret_cast<const GLubyte*>(neg_z_texture.data()));
    }

    inline void SaveScreenshot(
        const std::string& filename,
        int width,
        int height)
    {
        if(width % 4 != 0)
            width += (4 - (width % 4));

        std::unique_ptr<char[]> pixels(new char[width * height * 3]);

        opengl::ReadPixels(
            /*color_buffer*/ opengl::COLOR_BUFFER::BACK_LEFT,
            /*x*/ 0,
            /*y*/ 0,
            /*width*/ width,
            /*height*/ height,
            /*format*/ opengl::PIXEL_FORMAT::BGR,
            /*component_type*/ opengl::TYPE::UNSIGNED_BYTE,
            /*data*/ pixels.get());

        win::WriteBMP24File(
            /*filename*/ filename,
            /*width*/ width,
            /*height*/ height,
            /*pixels*/ pixels.get());
    }

    inline void SaveScreenshot(
        opengl::FramebufferObject& fbo,
        opengl::FramebufferObject::COLOR_ATTACHMENT attachment,
        const std::string& filename,
        int width,
        int height)
    {
        if (width % 4 != 0)
            width += (4 - (width % 4));

        std::unique_ptr<char[]> pixels(new char[width * height * 3]);

        fbo.ReadPixels(
            /*attachment*/ attachment,
            /*x*/ 0,
            /*y*/ 0,
            /*width*/ width,
            /*height*/ height,
            /*format*/ opengl::PIXEL_FORMAT::BGR,
            /*component_type*/ opengl::TYPE::UNSIGNED_BYTE,
            /*data*/ pixels.get());

        win::WriteBMP24File(
            /*filename*/ filename,
            /*width*/ width,
            /*height*/ height,
            /*pixels*/ pixels.get());
    }

    inline void SaveTextureImage(
        const std::string& filename,
        opengl::Texture2D& texture,
        GLint mipmap_level = 0)
    {
        util::Requires::That(
            texture.getPixelFormat() == opengl::PIXEL_FORMAT::BGR &&
            texture.getType() == opengl::TYPE::UNSIGNED_BYTE,
            FUNCTION_INFO);

        auto width = texture.getWidth();
        auto height = texture.getHeight();
        std::unique_ptr<char[]> pixels(new char[width * height * 3]);

        texture.getImage(mipmap_level, pixels.get());

        win::WriteBMP24File(
            /*filename*/ filename,
            /*width*/ width,
            /*height*/ height,
            /*pixels*/ pixels.get());
    }
}