/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "GLEWHeaders.h"

namespace opengl
{
    // Initializes GLEW library
    void Initialize_GLEW_Library();
}
