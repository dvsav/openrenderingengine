/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <string>
#include <vector>
#include <algorithm>
#include <sstream>

#include "File.h"

namespace util
{
    /*
    Class able to read files containig INCLUDE directive that
    includes other files (like header files in C++).
    Example:
    util::TextPreprocessor preprocessor("#include", '\"', '\"');
    std::cout << preprocessor.ReadAllText("sourcefile.txt");
    or
    std::cout << util::TextPreprocessor::default().ReadAllText("sourcefile.txt");
    */
    class TextPreprocessor
    {
    private:
        std::string m_IncludeDirective;
        char m_LeftBracket;
        char m_RightBracket;

    public:
        TextPreprocessor(
            const std::string& includeDirective,
            char leftBracket,
            char rightBracket) noexcept :
            m_IncludeDirective(includeDirective),
            m_LeftBracket(leftBracket),
            m_RightBracket(rightBracket)
        { }

    private:
        void ReadAllTextInternal(
            std::ostream& os,
            const std::string& filename,
            std::vector<fs::path>& headers) const
        {
            std::ifstream fileStream(filename);
            if (!fileStream)
                throw std::runtime_error("util::TextPreprocessor::ReadAllTextInternal() -> Cannot open file: " + filename);

            std::string codeline;
            while (std::getline(fileStream, codeline))
            {
                size_t include = std::string::npos;
                if ((include = codeline.find(m_IncludeDirective)) != std::string::npos)
                {
                    size_t leftBracket = codeline.find(m_LeftBracket, include + m_IncludeDirective.size());
                    size_t rightBracket = codeline.find(m_RightBracket, leftBracket + 1);
                    std::string header_filename = codeline.substr(leftBracket + 1, rightBracket - leftBracket - 1);
                    fs::path header_path = file::getDirectory(filename) / header_filename;

                    if (std::find_if(headers.begin(), headers.end(), [&header_path](const fs::path& path) { return file::equivalent(header_path, path); }) == headers.end())
                    {
                        headers.push_back(header_path);
                        ReadAllTextInternal(os, header_path.generic_string(), headers);
                    }
                }
                else
                    os << codeline << std::endl;
            }
        }

    public:
        std::string ReadAllText(
            const std::string& filename) const
        {
            std::vector<fs::path> headers;
            std::stringstream os(std::ios_base::out);
            ReadAllTextInternal(os, filename, headers);
            return os.str();
        }

        std::stringstream& ReadAllText(
            std::stringstream& os,
            const std::string& filename) const
        {
            std::vector<fs::path> headers;
            ReadAllTextInternal(os, filename, headers);
            return os;
        }

        static const TextPreprocessor& default()
        {
            static util::TextPreprocessor preprocessor("#include", '\"', '\"');
            return preprocessor;
        }
    };

}
