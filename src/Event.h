/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <vector>

#include "FastDelegate/FastDelegate.h"

namespace util
{
    /*
    Emulates the behavior of C# delegates i. e. contains a collection of
    function objects which can be member functions as well as free functions.
    Type Parameters:
    TArg - event handler's argument type
    Example of usage:
    class A
    {
        private:
            util::Event<int> m_MyEvent;

        public:
            util::Event<int>& MyEvent() { return m_MyEvent; }
            void RaiseMyEvent(int eventArg) { m_MyEvent(eventArg); }
    }

    class B
    {
        public:
            void MyEventHandler(int eventArg) { std::cout << "In B::MyEventHandler; eventArg = " << eventArg; }
    }

    void main()
    {
        A a;
        B b;

        a.MyEvent() += fastdelegate::MakeDelegate(&b, &B::MyEventHandler);
        a.RaiseMyEvent(1234);
    }
    */
    template <typename TArg>
    class Event final
    {
        #pragma region Typedefs

        public:
            using EventHandler = fastdelegate::FastDelegate1<TArg>;

        #pragma endregion

        #pragma region Fields

        private:
            std::vector<EventHandler> m_HandlersCollection;

        #pragma endregion

        #pragma region Constructors

        public:
            Event() :
                m_HandlersCollection()
            { }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            Event(const Event&) = delete;
            Event& operator=(const Event&) = delete;

        #pragma endregion

        #pragma region Methods

        public:
            // Adds new event handler to the event handler's collection.
            Event& operator+=(const EventHandler& eventHadler)
            {
                m_HandlersCollection.push_back(eventHadler);
                return *this;
            }

            // Removes event handler from the event handler's collection.
            Event& operator-=(const EventHandler& eventHadler)
            {
                auto iter = std::find(m_HandlersCollection.begin(), m_HandlersCollection.end(), eventHadler);
                m_HandlersCollection.erase(iter);
                return *this;
            }

            // Calls all event handlers.
            void operator()(TArg arg)
            {
                for (EventHandler& f : m_HandlersCollection)
                    f(arg);
            }

        #pragma endregion
    };
}