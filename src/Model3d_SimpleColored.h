/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <memory>

#include "Model3d_Base.h"
#include "WorldPositionAndOrientation.h"

namespace engine
{
    class Model3d_SimpleColored : public Model3d_Base
    {
        #pragma region Fields

        private:
            mat4 m_WorldToScreen;

        #pragma endregion

        #pragma region Constructors

        public:
            Model3d_SimpleColored(
                const std::shared_ptr<MetaProgram>& program,
                const std::shared_ptr<MeshBase>& mesh) :
                Model3d_Base(program, mesh),
                m_WorldToScreen(vmath::make_IdentityMatrix<GLfloat, 4>())
            {
                setUniformBlock<UniformBlockEnum::ModelViewPerspective>(
                    std::shared_ptr<UniformBlock_ModelViewPerspective>(new UniformBlock_ModelViewPerspective()));
            }

            Model3d_SimpleColored(
                const std::shared_ptr<MetaProgram>& program,
                const std::shared_ptr<MeshBase>& mesh,
                const std::shared_ptr<UniformBlock_ModelViewPerspective>& uniformBlock_ModelViewPerspective) :
                Model3d_Base(program, mesh),
                m_WorldToScreen(vmath::make_IdentityMatrix<GLfloat, 4>())
            {
                setUniformBlock<UniformBlockEnum::ModelViewPerspective>(uniformBlock_ModelViewPerspective);
            }

            // Move constructor.
            Model3d_SimpleColored(Model3d_SimpleColored&& value) noexcept :
                Model3d_Base(std::move(value)),
                m_WorldToScreen(std::move(value.m_WorldToScreen))
            { }

        #pragma endregion

        #pragma region Methods

        protected:
            void PrepareForRendering(RenderPassEnum renderPass) override
            {
                if (m_WorldPositionAndOrientation_Invalid)
                {
                    getUniformBlock<UniformBlockEnum::ModelViewPerspective>().setValue(
                        m_WorldToScreen * m_WorldPositionAndOrientation.ModelToWorldMatrix());

                    m_WorldPositionAndOrientation_Invalid = false;
                }
            }

        public:
            // Sets up all state variables dependent on the camera.
            void SetupCamera(const Camera& cam)
            {
                m_WorldToScreen = cam.getCameraToScreenMatrix() * cam.getWorldToCameraMatrix();
                m_WorldPositionAndOrientation_Invalid = true;
            }
        };

        #pragma endregion
}