/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include "Texture.h"

namespace opengl
{
    GLuint Texture::getCurrentlyBoundTextureHandle(TARGET target)
    {
        GLint current_texture;

        switch (target)
        {
            case TARGET::TEXTURE_1D:
                glGetIntegerv(GL_TEXTURE_BINDING_1D, &current_texture);
                break;
            case TARGET::TEXTURE_2D:
                glGetIntegerv(GL_TEXTURE_BINDING_2D, &current_texture);
                break;
            case TARGET::TEXTURE_3D:
                glGetIntegerv(GL_TEXTURE_BINDING_3D, &current_texture);
                break;
            case TARGET::TEXTURE_1D_ARRAY:
                glGetIntegerv(GL_TEXTURE_BINDING_1D_ARRAY, &current_texture);
                break;
            case TARGET::TEXTURE_2D_ARRAY:
                glGetIntegerv(GL_TEXTURE_BINDING_2D_ARRAY, &current_texture);
                break;
            case TARGET::TEXTURE_RECTANGLE:
                glGetIntegerv(GL_TEXTURE_BINDING_RECTANGLE, &current_texture);
                break;
            case TARGET::TEXTURE_CUBE_MAP:
                glGetIntegerv(GL_TEXTURE_BINDING_CUBE_MAP, &current_texture);
                break;
            case TARGET::TEXTURE_CUBE_MAP_ARRAY:
                glGetIntegerv(GL_TEXTURE_BINDING_CUBE_MAP_ARRAY, &current_texture);
                break;
            case TARGET::TEXTURE_BUFFER:
                glGetIntegerv(GL_TEXTURE_BINDING_BUFFER, &current_texture);
                break;
            case TARGET::TEXTURE_2D_MULTISAMPLE:
                glGetIntegerv(GL_TEXTURE_2D_MULTISAMPLE, &current_texture);
                break;
            case TARGET::TEXTURE_2D_MULTISAMPLE_ARRAY:
                glGetIntegerv(GL_TEXTURE_2D_MULTISAMPLE_ARRAY, &current_texture);
                break;
            default:
                throw std::runtime_error("opengl::Texture.getCurrentlyBoundTextureHandle()");
        }

        return current_texture;
    }

    void Texture::setWrapMode(
        GLuint textureUnit,
        TARGET target,
        WRAP_MODE mode)
    {
        glActiveTexture(GL_TEXTURE0 + textureUnit);

        glTexParameteri(static_cast<GLenum>(target), GL_TEXTURE_WRAP_S, static_cast<GLint>(mode));
        glTexParameteri(static_cast<GLenum>(target), GL_TEXTURE_WRAP_T, static_cast<GLint>(mode));
        glTexParameteri(static_cast<GLenum>(target), GL_TEXTURE_WRAP_R, static_cast<GLint>(mode));
    }

    void Texture::setWrapMode(
        GLuint textureUnit,
        TARGET target,
        TEXTURE_COORDINATE coord,
        WRAP_MODE mode)
    {
        glActiveTexture(GL_TEXTURE0 + textureUnit);

        switch (coord)
        {
            case TEXTURE_COORDINATE::S:
                glTexParameteri(static_cast<GLenum>(target), GL_TEXTURE_WRAP_S, static_cast<GLint>(mode));
                break;

            case TEXTURE_COORDINATE::T:
                glTexParameteri(static_cast<GLenum>(target), GL_TEXTURE_WRAP_T, static_cast<GLint>(mode));
                break;

            case TEXTURE_COORDINATE::R:
                glTexParameteri(static_cast<GLenum>(target), GL_TEXTURE_WRAP_R, static_cast<GLint>(mode));
                break;
        }
    }
}