#pragma once

#include "ConditionalCompilation.h"

#include <stdexcept>

#include "Scene.h"
#include "MeshIndexed.h"

#pragma comment(lib, "assimp-vc141-mt.lib")
#pragma comment(lib, "IrrXML.lib")
#pragma comment(lib, "zlibstatic.lib")
#include "assimp/Importer.hpp"
#include "assimp/scene.h"
#include "assimp/postprocess.h"

namespace engine
{
    inline MeshIndexed import_mesh(
        const std::string& filename,
        const vec3& scale)
    {
        // Create an instance of the Importer class
        Assimp::Importer importer;

        // And have it read the given file with some example postprocessing
        // Usually - if speed is not the most important aspect for you - you'll
        // probably to request more postprocessing than we do in this example.
        const aiScene* scene = importer.ReadFile(
            filename,
            aiProcess_MakeLeftHanded |
            aiProcess_CalcTangentSpace |
            aiProcess_Triangulate |
            aiProcess_JoinIdenticalVertices |
            aiProcess_GenNormals |
            aiProcess_FlipUVs |
            aiProcess_SortByPType);

        // If the import failed
        if (!scene)
            throw std::runtime_error("engine::import_model: " + filename);

        // Now we can access the file's contents.
        unsigned int iMesh = 0;
        {
            auto& mesh = *scene->mMeshes[iMesh];

            std::vector<VertexPNTT> vertices;
            vertices.reserve(mesh.mNumVertices);
            for (unsigned int iVertex = 0; iVertex < mesh.mNumVertices; iVertex++)
            {
                vertices.push_back(
                    VertexPNTT
                    {
                        vec3{ mesh.mVertices[iVertex].x, mesh.mVertices[iVertex].y, mesh.mVertices[iVertex].z } * scale,
                        vec3{ mesh.mNormals[iVertex].x, mesh.mNormals[iVertex].y, mesh.mNormals[iVertex].z },
                        vec2{ mesh.mTextureCoords[0][iVertex].x, mesh.mTextureCoords[0][iVertex].y },
                        vec4{ mesh.mTangents[iVertex].x, mesh.mTangents[iVertex].y, mesh.mTangents[iVertex].z, -1.0f }
                    });
            }

            std::vector<GLuint> indices;
            indices.reserve(mesh.mNumFaces * 3);
            for (unsigned int iFace = 0; iFace < mesh.mNumFaces; iFace++)
            {
                indices.insert(
                    indices.end(),
                    &mesh.mFaces[iFace].mIndices[0],
                    &mesh.mFaces[iFace].mIndices[mesh.mFaces[iFace].mNumIndices]);
            }

            return MeshIndexed(
                /*vertexBuffer*/ opengl::VertexBuffer<VertexPNTT>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
                /*indexBuffer*/ opengl::IndexBuffer(indices),
                /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
        }
    }
}