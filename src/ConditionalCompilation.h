/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

// Put here any macro definitions for conditional compilation.
// You must include this file to any hearder or source files using conditional compilation.

#define USE_SSE

#ifdef USE_SSE
    #define _ENABLE_EXTENDED_ALIGNED_STORAGE
#endif

#define OPENGL_4
//#define OPENGL_4_5
//#define _DEBUG

// This is to resolve the conflict between max() macro defined in <windows.h>
// and STL functions having the same name - max.
#define NOMINMAX

#ifdef _DEBUG
    #define USE_REQUIRES
#endif
