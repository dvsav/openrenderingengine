/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "GLEWHeaders.h"
#include "OpenGLTypes.h"
#include "GLenums.h"

#include "Requires.h"

namespace opengl
{
    class Texture
    {
        #pragma region Nested Types

        public:
            enum class TARGET : GLenum
            {
                TEXTURE_1D = GL_TEXTURE_1D,
                TEXTURE_2D = GL_TEXTURE_2D,
                TEXTURE_3D = GL_TEXTURE_3D,
                TEXTURE_1D_ARRAY = GL_TEXTURE_1D_ARRAY,
                TEXTURE_2D_ARRAY = GL_TEXTURE_2D_ARRAY,
                TEXTURE_RECTANGLE = GL_TEXTURE_RECTANGLE,
                TEXTURE_CUBE_MAP = GL_TEXTURE_CUBE_MAP,
                TEXTURE_CUBE_MAP_ARRAY = GL_TEXTURE_CUBE_MAP_ARRAY,
                TEXTURE_BUFFER = GL_TEXTURE_BUFFER,
                TEXTURE_2D_MULTISAMPLE = GL_TEXTURE_2D_MULTISAMPLE,
                TEXTURE_2D_MULTISAMPLE_ARRAY = GL_TEXTURE_2D_MULTISAMPLE_ARRAY
            };

            enum class TEXTURE_FILTER : GLenum
            {
                NEAREST = GL_NEAREST,
                LINEAR = GL_LINEAR,
                NEAREST_MIPMAP_NEAREST = GL_NEAREST_MIPMAP_NEAREST,
                LINEAR_MIPMAP_NEAREST = GL_LINEAR_MIPMAP_NEAREST,
                NEAREST_MIPMAP_LINEAR = GL_NEAREST_MIPMAP_LINEAR,
                LINEAR_MIPMAP_LINEAR = GL_LINEAR_MIPMAP_LINEAR
            };

            enum class TEXTURE_COORDINATE
            {
                S,
                T,
                R
            };

            enum class WRAP_MODE : GLint
            {
                REPEAT = GL_REPEAT,
                CLAMP = GL_CLAMP,
                CLAMP_TO_EDGE = GL_CLAMP_TO_EDGE,
                CLAMP_TO_BORDER = GL_CLAMP_TO_BORDER,
                MIRRORED_REPEAT = GL_MIRRORED_REPEAT,
                MIRROR_CLAMP_TO_EDGE = GL_MIRROR_CLAMP_TO_EDGE
            };

            enum class TEXTURE_COMPRESSION_HINT : GLenum
            {
                FASTEST = GL_FASTEST,
                NICEST = GL_NICEST,
                DONT_CARE = GL_DONT_CARE
            };

            enum class COMPARE_MODE : GLenum
            {
                COMPARE_REF_TO_TEXTURE = GL_COMPARE_REF_TO_TEXTURE,
                NONE = GL_NONE
            };

            /*
            Provides more safe binding of a Texture in RAII fashion.
            Example:
            {
            Texture::Binding binding(texture); // here we bind the texture
            // do some work
            } // here the texture is unbound automatically
            */
            class Binding final
            {
                private:
                    TARGET m_Target;
                    GLuint m_PreviousTexture = 0;

                private:
                    Binding(const Binding&) = delete;
                    Binding& operator=(const Binding&) = delete;

                public:
                    Binding(Texture& texture) :
                        m_Target(texture.getTarget()),
                        m_PreviousTexture(Texture::getCurrentlyBoundTextureHandle(texture.getTarget()))
                    {
                        texture.Bind();
                    }

                    Binding(Texture& texture, TARGET target) :
                        m_Target(target),
                        m_PreviousTexture(Texture::getCurrentlyBoundTextureHandle(target))
                    {
                        texture.Bind(target);
                    }

                    ~Binding() { glBindTexture(static_cast<GLenum>(m_Target), m_PreviousTexture); }

                    TARGET getTarget() const { return m_Target; }
            };

        #pragma endregion

        #pragma region Fields

        private:
            GLuint m_Handle;
            TARGET m_Target;
            INTERNAL_FORMAT m_InternalFormat;
            PIXEL_FORMAT m_Format;
            TYPE m_Type;

            GLint m_MipmapLevelsNumber;
            GLsizei m_Width;
            GLsizei m_Height;

        #pragma endregion

        #pragma region Constructors

        /*
        Creates a texture object.
        Parameters:
        target - the type of texture.
        internalFormat - the format that OpenGL will use to store the texture's data (in GPU memory).
        format - the format of user data in application's memory which are to be uploaded to the GPU memory.
        type - the type of color component of a single texel.
        */
        protected:
            Texture(
                TARGET target,
                INTERNAL_FORMAT internalFormat,
                PIXEL_FORMAT format,
                TYPE type,
                GLint mipmap_levels_number,
                GLsizei width,
                GLsizei height) :

                m_Handle(0),
                m_Target(target),
                m_InternalFormat(internalFormat),
                m_Format(format),
                m_Type(type),
                m_MipmapLevelsNumber(mipmap_levels_number),
                m_Width(width),
                m_Height(height)
            {
                util::Requires::ArgumentNotNegative(mipmap_levels_number, "mipmap_levels_number", FUNCTION_INFO);
                util::Requires::ArgumentPositive(width, "width", FUNCTION_INFO);
                util::Requires::ArgumentPositive(height, "height", FUNCTION_INFO);

                glGenTextures(1, &m_Handle);

                Binding binding(*this, target);
            }

            // Move constructor.
            Texture(Texture&& value) noexcept :
                m_Handle(value.m_Handle),
                m_Target(value.m_Target),
                m_InternalFormat(value.m_InternalFormat),
                m_Format(value.m_Format),
                m_Type(value.m_Type),
                m_MipmapLevelsNumber(value.m_MipmapLevelsNumber),
                m_Width(value.m_Width),
                m_Height(value.m_Height)
            {
                value.m_Handle = 0;
                value.m_MipmapLevelsNumber = 0;
                value.m_Width = 0;
                value.m_Height = 0;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~Texture()
            {
                glDeleteTextures(1, &m_Handle);
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            Texture(const Texture&) = delete;
            Texture& operator=(const Texture&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            // Gets handle to a texture currently bound to the specified target.
            static GLuint getCurrentlyBoundTextureHandle(TARGET target);

            static GLuint getActiveTextureUnit()
            {
                GLint unit;
                glGetIntegerv(GL_ACTIVE_TEXTURE, &unit);
                return unit;
            }

            static GLuint getMaxTextureUnits()
            {
                GLint iUnits;
                glGetIntegerv(GL_MAX_TEXTURE_UNITS, &iUnits);
                return iUnits;
            }

            static GLfloat getMaximumAnisortopy()
            {
                GLfloat fLargest = 1.0f;
                glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
                return fLargest;
            }

            GLint getMipmapLevelsNumber() const
            {
                return m_MipmapLevelsNumber;
            }

            GLsizei getWidth() const
            {
                return m_Width;
            }

            GLsizei getHeight() const
            {
                return m_Height;
            }

            // Gets the handle of the texture.
            GLuint getHandle()
            {
                return m_Handle;
            }

            TARGET getTarget() const
            {
                return m_Target;
            }

            INTERNAL_FORMAT getInternalFormat() const
            {
                return m_InternalFormat;
            }

            PIXEL_FORMAT getPixelFormat() const
            {
                return m_Format;
            }

            // Gets the type of color component of a texel.
            TYPE getType() const
            {
                return m_Type;
            }

            // The texture magnification function is used when the pixel being textured maps to an area less than or equal to one texture element.
            static void setMagnificationFilter(
                GLuint textureUnit,
                TARGET target,
                TEXTURE_FILTER filter)
            {
                glActiveTexture(GL_TEXTURE0 + textureUnit);

                glTexParameteri(
                    static_cast<GLenum>(target),
                    GL_TEXTURE_MAG_FILTER,
                    static_cast<GLint>(filter));
            }

            // The texture minifying function is used whenever the pixel being textured maps to an area greater than one texture element.
            static void setMinificationFilter(
                GLuint textureUnit,
                TARGET target,
                TEXTURE_FILTER filter)
            {
                glActiveTexture(GL_TEXTURE0 + textureUnit);

                glTexParameteri(
                    static_cast<GLenum>(target),
                    GL_TEXTURE_MIN_FILTER,
                    static_cast<GLint>(filter));
            }

            // Defines what happens when texture coordinates fall outside the range 0...1.
            static void setWrapMode(
                GLuint textureUnit,
                TARGET target,
                WRAP_MODE mode);

            // Defines what happens when particular texture coordinate falls outside the range 0...1.
            static void setWrapMode(
                GLuint textureUnit,
                TARGET target,
                TEXTURE_COORDINATE coord,
                WRAP_MODE mode);

            // When texture is used as depth buffer (for example for shadow mapping) defines depth test behavior.
            static void setupDepthTest(
                GLuint textureUnit,
                TARGET target,
                COMPARE_MODE compareMode,
                COMPARISON_FUNCTION compareFunc)
            {
                glActiveTexture(GL_TEXTURE0 + textureUnit);

                glTexParameteri(
                    static_cast<GLenum>(target),
                    GL_TEXTURE_COMPARE_MODE,
                    static_cast<GLint>(compareMode));

                glTexParameteri(
                    static_cast<GLenum>(target),
                    GL_TEXTURE_COMPARE_FUNC,
                    static_cast<GLint>(compareFunc));
            }

            /*
            Sets border color for the texture.
            If the texture contains depth components parameter 'r' is interpreted as a depth value.
            */
            static void setBorderColor(
                GLuint textureUnit,
                TARGET target, 
                GLfloat r, GLfloat g, GLfloat b, GLfloat a)
            {
                glActiveTexture(GL_TEXTURE0 + textureUnit);
                GLfloat border[]{ r, g, b, a };
                glTexParameterfv(
                    static_cast<GLenum>(target),
                    GL_TEXTURE_BORDER_COLOR,
                    border);
            }

            /*
            Sets the amount of anisotropy in anisotropy filtering.
            Parameters:
            value - amount of anisotropy.
            1.0f is the minimum allowable value and means no anisotropy (isotropic filtering).
            You can retrieve the maximum allowable value through the method getMaximumAnisortopy().
            */
            static void setAnisotropyAmount(
                GLuint textureUnit,
                TARGET target,
                GLfloat value)
            {
                util::Requires::That(
                    value >= 1.0f && value <= getMaximumAnisortopy(),
                    FUNCTION_INFO);

                glActiveTexture(GL_TEXTURE0 + textureUnit);

                glTexParameterf(
                    static_cast<GLenum>(target),
                    GL_TEXTURE_MAX_ANISOTROPY_EXT,
                    value);
            }

            // Sets the compression hint to OpenGL (nicest, fastest or don't care).
            void setCompressionHint(TEXTURE_COMPRESSION_HINT hint)
            {
                glHint(GL_TEXTURE_COMPRESSION_HINT, static_cast<GLenum>(hint));
            }

            // Gets texture image.
            void getImage(
                GLint mipmapLevel,
                GLvoid* buffer) const
            {
                Binding binding(const_cast<Texture&>(*this));

                glGetTexImage(
                    static_cast<GLenum>(m_Target),
                    mipmapLevel,
                    static_cast<GLenum>(m_Format),
                    static_cast<GLenum>(m_Type),
                    buffer);
            }

            // Gets whether the texture was successfully compressed.
            bool getIsCompressed() const
            {
                Binding binding(const_cast<Texture&>(*this));

                GLint compFlag;
                glGetTexLevelParameteriv(
                    /*target*/ static_cast<GLenum>(m_Target),
                    /*mipmap_level*/ 0,
                    /*parameter_name*/ GL_TEXTURE_COMPRESSED,
                    /*parameter_value*/ &compFlag);

                return compFlag != 0.0f;
            }

            // Gets the size in bytes of compressed image.
            GLint getCompressedImageSize(GLint mipmap_level) const
            {
                Binding binding(const_cast<Texture&>(*this));

                GLint imageSizeInBytes;
                glGetTexLevelParameteriv(
                    /*target*/ static_cast<GLenum>(m_Target),
                    /*mipmap_level*/ mipmap_level,
                    /*parameter_name*/ GL_TEXTURE_COMPRESSED_IMAGE_SIZE,
                    /*parameter_value*/ &imageSizeInBytes);

                return imageSizeInBytes;
            }

            // Gets compressed texture image.
            void getCompressedImage(
                GLint mipmap_level,
                GLvoid* buffer) const
            {
                Binding binding(const_cast<Texture&>(*this));

                glGetCompressedTexImage(
                    static_cast<GLenum>(m_Target),
                    mipmap_level,
                    buffer);
            }

        #pragma endregion

        #pragma region Methods

        public:
            // Binds the texture to a texturing target.
            void Bind()
            {
                glBindTexture(static_cast<GLenum>(m_Target), m_Handle);
            }

            void Unbind()
            {
                glBindTexture(static_cast<GLenum>(m_Target), 0);
            }

            /*
            Binds the texture to a texture target.
            Parameters:
            target - the target to which the texture object will be bound.
            */
            void Bind(TARGET target)
            {
                glBindTexture(static_cast<GLenum>(target), m_Handle);
            }

            void BindToTextureUnit(GLuint unit)
            {
                util::Requires::That(
                    unit >= 0 && unit < getMaxTextureUnits(),
                    FUNCTION_INFO);

                GLuint prevUnit = getActiveTextureUnit();
                glActiveTexture(GL_TEXTURE0 + unit);
                Bind();
                glActiveTexture(prevUnit);
            }

            // If the image for mipmap level 0 has been loaded generates images for all other mipmap levels.
            void GenerateMipmap()
            {
                Binding binding(*this);
                glGenerateMipmap(static_cast<GLenum>(m_Target));
            }

        #pragma endregion
    };
}