/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "MeshBase.h"

namespace engine
{
    class MeshIndexed : public MeshBase
    {
        #pragma region Fields

        protected:
            opengl::IndexBuffer m_IndexBuffer;

        #pragma endregion

        #pragma region Constructors

        public:
            template<typename TVertex>
            MeshIndexed(
                opengl::VertexBuffer<TVertex>&& vertexBuffer,
                opengl::IndexBuffer&& indexBuffer,
                opengl::DRAWING_MODE drawing_mode) :

                MeshBase(std::move(vertexBuffer), drawing_mode),
                m_IndexBuffer(std::move(indexBuffer))
            { }

            // Move constructor.
            MeshIndexed(MeshIndexed&& value) noexcept :
                MeshBase(std::move(value)),
                m_IndexBuffer(std::move(value.m_IndexBuffer))
            { }

        #pragma endregion

        #pragma region Methods

        public:
            void BindIndexBuffer() override
            {
                m_IndexBuffer.Bind(opengl::Buffer::TARGET::ELEMENT_ARRAY);
            }

            void Render() override
            {
                glDrawElements(
                    /*mode*/    m_DrawingMode,
                    /*count*/   m_IndexBuffer.getIndexCount(),
                    /*type*/    m_IndexBuffer.getIndexType(),
                    /*indices*/ 0);
            }

            void Render(
                GLsizei instance_count,
                GLuint base_instance = 0) override
            {
                glDrawElementsInstancedBaseInstance(
                    /*mode*/    m_DrawingMode,
                    /*count*/   m_IndexBuffer.getIndexCount(),
                    /*type*/    m_IndexBuffer.getIndexType(),
                    /*indices*/ 0,
                    /*primcount*/ instance_count,
                    /*baseinstance*/ base_instance);
            }

        #pragma endregion
    };

    // Creates a mesh for cube.
    inline MeshIndexed makeCubeP(
        GLfloat radius)
    {
        std::vector<VertexP> vertices
        {
            // -Z
            vec3{ -radius, -radius, -radius },
            vec3{ radius, -radius, -radius },
            vec3{ radius, radius, -radius },
            vec3{ -radius,  radius, -radius },

            // +Z
            vec3{ -radius, -radius, radius },
            vec3{ radius, -radius, radius },
            vec3{ radius, radius, radius },
            vec3{ -radius,  radius, radius }
        };

        std::vector<GLuint> indices
        {
            // -Z (+0)
            0, 1, 2,
            0, 2, 3,

            // +Z (+4)
            4, 7, 6,
            4, 6, 5,

            // -Y (+8)
            1, 0, 4,
            1, 4, 5,

            // +Y (+12)
            3, 2, 6,
            3, 6, 7,

            // -X (+16)
            0, 3, 7,
            0, 7, 4,

            // +X (+20)
            1, 5, 6,
            1, 6, 2
        };

        return MeshIndexed(
            /*vertexBuffer*/ opengl::VertexBuffer<VertexP>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
            /*indexBuffer*/ opengl::IndexBuffer(indices),
            /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
    }

    /*
    Creates a mesh for cube.
    Faces of the cube are facing inside the cube.
    This is needed when the viewer (the camera) is inside the cube.
    */
    inline MeshIndexed makeCubeP_FacingIncide(
        GLfloat radius)
    {
        std::vector<VertexP> vertices
        {
            // -Z
            vec3{ -radius, -radius, -radius },
            vec3{ -radius, radius, -radius },
            vec3{ radius, radius, -radius },
            vec3{ radius, -radius, -radius },

            // +Z
            vec3{ -radius, -radius, radius },
            vec3{ -radius, radius, radius },
            vec3{ radius, radius, radius },
            vec3{ radius, -radius, radius }
        };

        std::vector<GLuint> indices
        {
            // -Z (+0)
            0, 1, 2,
            0, 2, 3,

            // +Z (+4)
            4, 7, 6,
            4, 6, 5,

            // -Y (+8)
            1, 0, 4,
            1, 4, 5,

            // +Y (+12)
            3, 2, 6,
            3, 6, 7,

            // -X (+16)
            0, 3, 7,
            0, 7, 4,

            // +X (+20)
            1, 5, 6,
            1, 6, 2
        };

        return MeshIndexed(
            /*vertexBuffer*/ opengl::VertexBuffer<VertexP>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
            /*indexBuffer*/ opengl::IndexBuffer(indices),
            /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
    }

    // Creates a mesh for cube.
    inline MeshIndexed makeCubePN(
        GLfloat radius)
    {
        std::vector<VertexPN> vertices
        {
            // -Z
            VertexPN
            {
                vec3{ -radius, -radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f }
            },
            VertexPN
            {
                vec3{ radius, -radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f }
            },
            VertexPN
            {
                vec3{ radius, radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f }
            },
            VertexPN
            {
                vec3{ -radius,  radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f }
            },

            // +Z
            VertexPN
            {
                vec3{ -radius, -radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f }
            },
            VertexPN
            {
                vec3{ radius, -radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f }
            },
            VertexPN
            {
                vec3{ radius, radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f }
            },
            VertexPN
            {
                vec3{ -radius,  radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f }
            },

            // -Y
            VertexPN
            {
                vec3{ -radius, -radius, radius },
                vec3{ 0.0f, -1.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ radius, -radius, radius },
                vec3{ 0.0f, -1.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ radius, -radius, -radius },
                vec3{ 0.0f, -1.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ -radius, -radius, -radius },
                vec3{ 0.0f, -1.0f, 0.0f }
            },

            // +Y
            VertexPN
            {
                vec3{ -radius, radius, radius },
                vec3{ 0.0f, 1.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ radius, radius, radius },
                vec3{ 0.0f, 1.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ radius, radius, -radius },
                vec3{ 0.0f, 1.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ -radius, radius, -radius },
                vec3{ 0.0f, 1.0f, 0.0f }
            },

            // -X
            VertexPN
            {
                vec3{ -radius, -radius, radius },
                vec3{ -1.0f, 0.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ -radius, -radius, -radius },
                vec3{ -1.0f, 0.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ -radius, radius, -radius },
                vec3{ -1.0f, 0.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ -radius, radius, radius },
                vec3{ -1.0f, 0.0f, 0.0f }
            },

            // +X
            VertexPN
            {
                vec3{ radius, -radius, radius },
                vec3{ 1.0f, 0.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ radius, -radius, -radius },
                vec3{ 1.0f, 0.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ radius, radius, -radius },
                vec3{ 1.0f, 0.0f, 0.0f }
            },
            VertexPN
            {
                vec3{ radius, radius, radius },
                vec3{ 1.0f, 0.0f, 0.0f }
            }
        };

        std::vector<GLuint> indices
        {
            // -Z (+0)
            0, 1, 2,
            0, 2, 3,

            // +Z (+4)
            2 + 4, 1 + 4, 0 + 4,
            3 + 4, 2 + 4, 0 + 4,

            // -Y (+8)
            0 + 8, 1 + 8, 2 + 8,
            0 + 8, 2 + 8, 3 + 8,

            // +Y (+12)
            2 + 12, 1 + 12, 0 + 12,
            3 + 12, 2 + 12, 0 + 12,

            // -X (+16)
            0 + 16, 1 + 16, 2 + 16,
            0 + 16, 2 + 16, 3 + 16,

            // +X (+20)
            2 + 20, 1 + 20, 0 + 20,
            3 + 20, 2 + 20, 0 + 20
        };

        return MeshIndexed(
            /*vertexBuffer*/ opengl::VertexBuffer<VertexPN>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
            /*indexBuffer*/ opengl::IndexBuffer(indices),
            /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
    }

    // Creates a mesh for cube.
    inline MeshIndexed makeCubePNT(
        GLfloat radius)
    {
        std::vector<VertexPNT> vertices
        {
            // -Z
            VertexPNT
            {
                vec3{ -radius, -radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.50f, 1.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ radius, -radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.75f, 1.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ radius, radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.75f, 2.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ -radius,  radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.50f, 2.0f / 3.0f }
            },

            // +Z
            VertexPNT
            {
                vec3{ -radius, -radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f },
                vec2{ 0.25f, 1.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ radius, -radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f },
                vec2{ 0.00f, 1.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ radius, radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f },
                vec2{ 0.00f, 2.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ -radius,  radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f },
                vec2{ 0.25f, 2.0f / 3.0f }
            },

            // -Y
            VertexPNT
            {
                vec3{ -radius, -radius, radius },
                vec3{ 0.0f, -1.0f, 0.0f },
                vec2{ 0.50f, 0.00f }
            },
            VertexPNT
            {
                vec3{ radius, -radius, radius },
                vec3{ 0.0f, -1.0f, 0.0f },
                vec2{ 0.75f, 0.00f }
            },
            VertexPNT
            {
                vec3{ radius, -radius, -radius },
                vec3{ 0.0f, -1.0f, 0.0f },
                vec2{ 0.75f, 1.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ -radius, -radius, -radius },
                vec3{ 0.0f, -1.0f, 0.0f },
                vec2{ 0.50f, 1.0f / 3.0f }
            },

            // +Y
            VertexPNT
            {
                vec3{ -radius, radius, radius },
                vec3{ 0.0f, 1.0f, 0.0f },
                vec2{ 0.50f, 1.00f }
            },
            VertexPNT
            {
                vec3{ radius, radius, radius },
                vec3{ 0.0f, 1.0f, 0.0f },
                vec2{ 0.75f, 1.00f }
            },
            VertexPNT
            {
                vec3{ radius, radius, -radius },
                vec3{ 0.0f, 1.0f, 0.0f },
                vec2{ 0.75f, 2.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ -radius, radius, -radius },
                vec3{ 0.0f, 1.0f, 0.0f },
                vec2{ 0.50f, 2.0f / 3.0f }
            },

            // -X
            VertexPNT
            {
                vec3{ -radius, -radius, radius },
                vec3{ -1.0f, 0.0f, 0.0f },
                vec2{ 0.25f, 1.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ -radius, -radius, -radius },
                vec3{ -1.0f, 0.0f, 0.0f },
                vec2{ 0.50f, 1.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ -radius, radius, -radius },
                vec3{ -1.0f, 0.0f, 0.0f },
                vec2{ 0.50f, 2.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ -radius, radius, radius },
                vec3{ -1.0f, 0.0f, 0.0f },
                vec2{ 0.25f, 2.0f / 3.0f }
            },

            // +X
            VertexPNT
            {
                vec3{ radius, -radius, radius },
                vec3{ 1.0f, 0.0f, 0.0f },
                vec2{ 1.00f, 1.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ radius, -radius, -radius },
                vec3{ 1.0f, 0.0f, 0.0f },
                vec2{ 0.75f, 1.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ radius, radius, -radius },
                vec3{ 1.0f, 0.0f, 0.0f },
                vec2{ 0.75f, 2.0f / 3.0f }
            },
            VertexPNT
            {
                vec3{ radius, radius, radius },
                vec3{ 1.0f, 0.0f, 0.0f },
                vec2{ 1.00f, 2.0f / 3.0f }
            }
        };

        std::vector<GLuint> indices
        {
            // -Z (+0)
            0, 1, 2,
            0, 2, 3,

            // +Z (+4)
            2 + 4, 1 + 4, 0 + 4,
            3 + 4, 2 + 4, 0 + 4,

            // -Y (+8)
            0 + 8, 1 + 8, 2 + 8,
            0 + 8, 2 + 8, 3 + 8,

            // +Y (+12)
            2 + 12, 1 + 12, 0 + 12,
            3 + 12, 2 + 12, 0 + 12,

            // -X (+16)
            0 + 16, 1 + 16, 2 + 16,
            0 + 16, 2 + 16, 3 + 16,

            // +X (+20)
            2 + 20, 1 + 20, 0 + 20,
            3 + 20, 2 + 20, 0 + 20
        };

        return MeshIndexed(
            /*vertexBuffer*/ opengl::VertexBuffer<VertexPNT>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
            /*indexBuffer*/ opengl::IndexBuffer(indices),
            /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
    }

    // Creates a mesh for cube.
    inline MeshIndexed makeCubePNTT(
        GLfloat radius)
    {
        std::vector<VertexPNTT> vertices
        {
            // -Z
            VertexPNTT
            {
                vec3{ -radius, -radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.50f, 1.0f / 3.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, -radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.75f, 1.0f / 3.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.75f, 2.0f / 3.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ -radius,  radius, -radius },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.50f, 2.0f / 3.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },

            // +Z
            VertexPNTT
            {
                vec3{ -radius, -radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f },
                vec2{ 0.25f, 1.0f / 3.0f },
                vec4{ -1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, -radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f },
                vec2{ 0.00f, 1.0f / 3.0f },
                vec4{ -1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f },
                vec2{ 0.00f, 2.0f / 3.0f },
                vec4{ -1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ -radius,  radius, radius },
                vec3{ 0.0f, 0.0f, 1.0f },
                vec2{ 0.25f, 2.0f / 3.0f },
                vec4{ -1.0f, 0.0f, 0.0f, -1.0f }
            },

            // -Y
            VertexPNTT
            {
                vec3{ -radius, -radius, radius },
                vec3{ 0.0f, -1.0f, 0.0f },
                vec2{ 0.50f, 0.00f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, -radius, radius },
                vec3{ 0.0f, -1.0f, 0.0f },
                vec2{ 0.75f, 0.00f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, -radius, -radius },
                vec3{ 0.0f, -1.0f, 0.0f },
                vec2{ 0.75f, 1.0f / 3.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ -radius, -radius, -radius },
                vec3{ 0.0f, -1.0f, 0.0f },
                vec2{ 0.50f, 1.0f / 3.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },

            // +Y
            VertexPNTT
            {
                vec3{ -radius, radius, radius },
                vec3{ 0.0f, 1.0f, 0.0f },
                vec2{ 0.50f, 1.00f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, radius, radius },
                vec3{ 0.0f, 1.0f, 0.0f },
                vec2{ 0.75f, 1.00f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, radius, -radius },
                vec3{ 0.0f, 1.0f, 0.0f },
                vec2{ 0.75f, 2.0f / 3.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ -radius, radius, -radius },
                vec3{ 0.0f, 1.0f, 0.0f },
                vec2{ 0.50f, 2.0f / 3.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },

            // -X
            VertexPNTT
            {
                vec3{ -radius, -radius, radius },
                vec3{ -1.0f, 0.0f, 0.0f },
                vec2{ 0.25f, 1.0f / 3.0f },
                vec4{ 0.0f, 0.0f, -1.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ -radius, -radius, -radius },
                vec3{ -1.0f, 0.0f, 0.0f },
                vec2{ 0.50f, 1.0f / 3.0f },
                vec4{ 0.0f, 0.0f, -1.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ -radius, radius, -radius },
                vec3{ -1.0f, 0.0f, 0.0f },
                vec2{ 0.50f, 2.0f / 3.0f },
                vec4{ 0.0f, 0.0f, -1.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ -radius, radius, radius },
                vec3{ -1.0f, 0.0f, 0.0f },
                vec2{ 0.25f, 2.0f / 3.0f },
                vec4{ 0.0f, 0.0f, -1.0f, -1.0f }
            },

            // +X
            VertexPNTT
            {
                vec3{ radius, -radius, radius },
                vec3{ 1.0f, 0.0f, 0.0f },
                vec2{ 1.00f, 1.0f / 3.0f },
                vec4{ 0.0f, 0.0f, 1.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, -radius, -radius },
                vec3{ 1.0f, 0.0f, 0.0f },
                vec2{ 0.75f, 1.0f / 3.0f },
                vec4{ 0.0f, 0.0f, 1.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, radius, -radius },
                vec3{ 1.0f, 0.0f, 0.0f },
                vec2{ 0.75f, 2.0f / 3.0f },
                vec4{ 0.0f, 0.0f, 1.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ radius, radius, radius },
                vec3{ 1.0f, 0.0f, 0.0f },
                vec2{ 1.00f, 2.0f / 3.0f },
                vec4{ 0.0f, 0.0f, 1.0f, -1.0f }
            }
        };

        std::vector<GLuint> indices
        {
            // -Z (+0)
            0, 1, 2,
            0, 2, 3,

            // +Z (+4)
            2 + 4, 1 + 4, 0 + 4,
            3 + 4, 2 + 4, 0 + 4,

            // -Y (+8)
            0 + 8, 1 + 8, 2 + 8,
            0 + 8, 2 + 8, 3 + 8,

            // +Y (+12)
            2 + 12, 1 + 12, 0 + 12,
            3 + 12, 2 + 12, 0 + 12,

            // -X (+16)
            0 + 16, 1 + 16, 2 + 16,
            0 + 16, 2 + 16, 3 + 16,

            // +X (+20)
            2 + 20, 1 + 20, 0 + 20,
            3 + 20, 2 + 20, 0 + 20
        };

        return MeshIndexed(
            /*vertexBuffer*/ opengl::VertexBuffer<VertexPNTT>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
            /*indexBuffer*/ opengl::IndexBuffer(indices),
            /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
    }

    // Creates a mesh for rectangle.
    inline MeshIndexed makeRectanglePN(
        GLfloat width,
        GLfloat height)
    {
        std::vector<VertexPN> vertices
        {
            VertexPN
            {
                vec3{ -width / 2.0f, -height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f }
            },
            VertexPN
            {
                vec3{ width / 2.0f, -height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f }
            },
            VertexPN
            {
                vec3{ width / 2.0f, height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f }
            },
            VertexPN
            {
                vec3{ -width / 2.0f,  height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f }
            }
        };

        std::vector<GLuint> indices
        {
            0, 1, 2,
            0, 2, 3
        };

        return MeshIndexed(
            /*vertexBuffer*/ opengl::VertexBuffer<VertexPN>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
            /*indexBuffer*/ opengl::IndexBuffer(indices),
            /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
    }

    // Creates a mesh for rectangle.
    inline MeshIndexed makeRectanglePNT(
        GLfloat width,
        GLfloat height,
        GLfloat umax = 1.0f,
        GLfloat vmax = 1.0f)
    {
        std::vector<VertexPNT> vertices
        {
            VertexPNT
            {
                vec3{ -width / 2.0f, -height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.0f, 0.0f }
            },
            VertexPNT
            {
                vec3{ width / 2.0f, -height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ umax, 0.0f }
            },
            VertexPNT
            {
                vec3{ width / 2.0f, height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ umax, vmax }
            },
            VertexPNT
            {
                vec3{ -width / 2.0f,  height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.0f, vmax }
            }
        };

        std::vector<GLuint> indices
        {
            0, 1, 2,
            0, 2, 3
        };

        return MeshIndexed(
            /*vertexBuffer*/ opengl::VertexBuffer<VertexPNT>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
            /*indexBuffer*/ opengl::IndexBuffer(indices),
            /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
    }

    // Creates a mesh for rectangle.
    inline MeshIndexed makeRectanglePNTT(
        GLfloat width,
        GLfloat height,
        GLfloat umax = 1.0f,
        GLfloat vmax = 1.0f)
    {
        std::vector<VertexPNTT> vertices
        {
            VertexPNTT
            {
                vec3{ -width / 2.0f, -height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.0f, 0.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ width / 2.0f, -height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ umax, 0.0f },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ width / 2.0f, height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ umax, vmax },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            },
            VertexPNTT
            {
                vec3{ -width / 2.0f,  height / 2.0f, 0.0f },
                vec3{ 0.0f, 0.0f, -1.0f },
                vec2{ 0.0f, vmax },
                vec4{ 1.0f, 0.0f, 0.0f, -1.0f }
            }
        };

        std::vector<GLuint> indices
        {
            0, 1, 2,
            0, 2, 3
        };

        return MeshIndexed(
            /*vertexBuffer*/ opengl::VertexBuffer<VertexPNTT>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
            /*indexBuffer*/ opengl::IndexBuffer(indices),
            /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
    }

    // Takes vertices collection and indices collection and calculates a normal vector for each vertex.
    // == Assumptions ==
    // Initial vertices[i]::Normal = {0, 0, 0}
    // TVertex is inherited from VertexPN
    // Coordinate system is left-handed
    template <typename TVertex>
    inline std::vector<TVertex>& GenerateNormals(
        std::vector<TVertex>& vertices,
        const std::vector<GLuint>& indices,
        opengl::DRAWING_MODE drawing_mode)
    {
        static_assert(std::is_base_of<typename VertexPN, TVertex>::value, "Unsupported vertex type");

        switch (drawing_mode)
        {
        case opengl::DRAWING_MODE::TRIANGLES:
            for (size_t i = 0; i < indices.size(); i += 3)
            {
                GLuint i1 = indices[i + 0];
                GLuint i2 = indices[i + 1];
                GLuint i3 = indices[i + 2];

                vec3 v1 = vertices[i2].Position - vertices[i1].Position;
                vec3 v2 = vertices[i3].Position - vertices[i1].Position;
                vec3 normal = vmath::cross_product(v1, v2);

                vertices[i1].Normal -= normal;
                vertices[i2].Normal -= normal;
                vertices[i3].Normal -= normal;
            }

            for (size_t i = 0; i < vertices.size(); i++)
                vertices[i].Normal = vmath::normalize(vertices[i].Normal);

            break;

        default:
            throw std::invalid_argument("Unsupported drawing mode");
        }

        return vertices;
    }
}