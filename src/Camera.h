/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "OpenGLUtility.h"
#include "WorldPositionAndOrientation.h"
#include "utility.h"

namespace engine
{
    // Abstract camera class
    class Camera
    {
        #pragma region Fields

        private:
            WorldPositionAndOrientation m_WorldPositionAndOrientation;
            GLfloat m_NearPlane;
            GLfloat m_FarPlane;

        protected:
            mat4 m_CameraToScreenMatrix;

        #pragma endregion

        #pragma region Constructors

        public:
            Camera() :
                m_WorldPositionAndOrientation(),
                m_NearPlane(1.0f),
                m_FarPlane(100.0f),
                m_CameraToScreenMatrix()
            { }

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~Camera() { }

        #pragma endregion

        #pragma region Properties

        protected:
            virtual void CalcCameraToScreenMatrix() = 0;

        public:
            WorldPositionAndOrientation& PositionAndOrientation() { return m_WorldPositionAndOrientation; }
            const WorldPositionAndOrientation& PositionAndOrientation() const { return m_WorldPositionAndOrientation; }

            GLfloat getNearPlane() const { return m_NearPlane; }
            GLfloat getFarPlane() const { return m_FarPlane; }

            void setNearPlane(GLfloat value) { m_NearPlane = value; CalcCameraToScreenMatrix(); }
            void setFarPlane(GLfloat value) { m_FarPlane = value; CalcCameraToScreenMatrix(); }

            mat4 getWorldToCameraMatrix() const
            {
                return vmath::make_WorldToCameraMatrix<GLfloat>(
                    m_WorldPositionAndOrientation.Position().vec3(),
                    m_WorldPositionAndOrientation.getForwardDirection().vec3(),
                    m_WorldPositionAndOrientation.getUpDirection().vec3());
            }

            const mat4& getCameraToScreenMatrix() const { return m_CameraToScreenMatrix; }

        #pragma endregion
    };

    // Perspective projection camera
    class PerspectiveCamera final : public Camera
    {
        #pragma region Fields

        private:
            GLfloat m_ViewAngle;
            GLfloat m_AspectRatio;

        #pragma endregion

        #pragma region Constructors

        public:
            PerspectiveCamera() :
                m_ViewAngle(M_PI / 3.0f),
                m_AspectRatio(16.0f / 9.0f)
            {
                CalcCameraToScreenMatrix();
            }

            PerspectiveCamera(
                GLfloat viewAngle,
                GLfloat aaspectRatio) :
                m_ViewAngle(viewAngle),
                m_AspectRatio(aaspectRatio)
            {
                CalcCameraToScreenMatrix();
            }

        #pragma endregion

        #pragma region Properties

        public:
            GLfloat getViewAngle() const { return m_ViewAngle; }
            GLfloat getAspectRatio() const { return m_AspectRatio; }

            void setViewAngle(GLfloat value) { m_ViewAngle = value; CalcCameraToScreenMatrix(); }
            void setAspectRatio(GLfloat value) { m_AspectRatio = value; CalcCameraToScreenMatrix(); }

        #pragma endregion

        #pragma region Methods

        protected:
            void CalcCameraToScreenMatrix() override
            {
                m_CameraToScreenMatrix = vmath::make_PerspectiveProjectionMatrix(
                    m_ViewAngle,
                    m_AspectRatio,
                    getNearPlane(),
                    getFarPlane());
            }

        #pragma endregion
    };

    // Orthographic projection camera
    class OrthographicCamera final : public Camera
    {
        #pragma region Fields

        private:
            GLfloat m_Width;
            GLfloat m_Height;

        #pragma endregion

        #pragma region Constructors

        public:
            OrthographicCamera() :
                m_Width(1.0f),
                m_Height(1.0f)
            {
                CalcCameraToScreenMatrix();
            }

        #pragma endregion

        #pragma region Properties

        public:
            GLfloat getWidth() const { return m_Width; }
            GLfloat getHeight() const { return m_Height; }

            void setWidth(GLfloat value) { m_Width = value; CalcCameraToScreenMatrix(); }
            void setHeight(GLfloat value) { m_Height = value; CalcCameraToScreenMatrix(); }

        #pragma endregion

        #pragma region Methods

        private:
            void CalcCameraToScreenMatrix() override
            {
                m_CameraToScreenMatrix = vmath::make_OrthographicProjectionMatrix(
                    /*width*/ m_Width,
                    /*height*/ m_Height,
                    /*zNear*/ getNearPlane(),
                    /*zFar*/ getFarPlane());
            }

        #pragma endregion
    };

    /*
    Fits viewport into the specified width and height so as to preserve the specified aspect ratio.
    Parameters:
    width - screen (window) width in pixels
    heigth - screen (window) heigth in pixels
    aspect_ratio = aspect ratio of the viewport
    */
    inline void setViewport(GLint width, GLint height, GLfloat aspect_ratio)
    {
        if (width / height < aspect_ratio)
        {
            glViewport(
                0,
                static_cast<GLint>((height - width / aspect_ratio) / 2.0f),
                width,
                static_cast<GLsizei>(width / aspect_ratio));
        }
        else
        {
            glViewport(
                static_cast<GLint>((width - height * aspect_ratio) / 2.0f),
                0,
                static_cast<GLsizei>(height * aspect_ratio),
                height);
        }
    }

    inline void setViewport(GLint width, GLint height)
    {
        glViewport(
            0, 0,
            width, height);
    }

    inline void setViewport(const opengl::Rect& viewport)
    {
        glViewport(
            viewport.x, viewport.y,
            viewport.width, viewport.height);
    }
}
