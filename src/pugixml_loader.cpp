#include <algorithm>
#include <map>

#include "pugixml_loader.h"
#include "AssetImporting.h"
#include "EngineUtility.h"
#include "File.h"

namespace engine
{
    opengl::DRAWING_MODE parse_DrawingMode(
        const char* str)
    {
        if (std::strcmp(str, "TRIANGLES") == 0)
            return opengl::DRAWING_MODE::TRIANGLES;

        if (std::strcmp(str, "TRIANGLE_STRIP") == 0)
            return opengl::DRAWING_MODE::TRIANGLE_STRIP;

        if (std::strcmp(str, "TRIANGLE_FAN") == 0)
            return opengl::DRAWING_MODE::TRIANGLE_FAN;

        if (std::strcmp(str, "TRIANGLES_ADJACENCY") == 0)
            return opengl::DRAWING_MODE::TRIANGLES_ADJACENCY;

        if (std::strcmp(str, "TRIANGLE_STRIP_ADJACENCY") == 0)
            return opengl::DRAWING_MODE::TRIANGLE_STRIP_ADJACENCY;

        if (std::strcmp(str, "LINES") == 0)
            return opengl::DRAWING_MODE::LINES;

        if (std::strcmp(str, "POINTS") == 0)
            return opengl::DRAWING_MODE::POINTS;

        if (std::strcmp(str, "PATCHES") == 0)
            return opengl::DRAWING_MODE::PATCHES;

        if (std::strcmp(str, "LINE_STRIP") == 0)
            return opengl::DRAWING_MODE::LINE_STRIP;

        if (std::strcmp(str, "LINE_LOOP") == 0)
            return opengl::DRAWING_MODE::LINE_LOOP;

        if (std::strcmp(str, "LINE_STRIP_ADJACENCY") == 0)
            return opengl::DRAWING_MODE::LINE_STRIP_ADJACENCY;

        if (std::strcmp(str, "LINES_ADJACENCY") == 0)
            return opengl::DRAWING_MODE::LINES_ADJACENCY;

        throw std::runtime_error("engine::parse_DrawingMode()");
    }

    color_rgba32f parse_Color(
        const pugi::xml_node& node)
    {
        return color_rgba32f
        {
            std::stof(node.attribute("r").value()),
            std::stof(node.attribute("g").value()),
            std::stof(node.attribute("b").value()),
            std::stof(node.attribute("a").value())
        };
    }

    vec2 parse_texcoords(
        const pugi::xml_node& node)
    {
        return vec2
        {
            std::stof(node.attribute("u").value()),
            std::stof(node.attribute("v").value())
        };
    }

    vec3 parse_orientation(
        const pugi::xml_node& node)
    {
        return vec3
        {
            std::stof(node.attribute("yaw").value()),
            std::stof(node.attribute("pitch").value()),
            std::stof(node.attribute("roll").value())
        };
    }

    vec4 parse_direction(
        const pugi::xml_node& node)
    {
        return vec4
        {
            std::stof(node.attribute("x").value()),
            std::stof(node.attribute("y").value()),
            std::stof(node.attribute("z").value()),
            0.0f
        };
    }

    template<typename T>
    T parse_vec(
        const pugi::xml_node& node);

    template<>
    vec3 parse_vec<vec3>(
        const pugi::xml_node& node)
    {
        return vec3
        {
            std::stof(node.attribute("x").value()),
            std::stof(node.attribute("y").value()),
            std::stof(node.attribute("z").value())
        };
    }

    template<>
    vec4 parse_vec<vec4>(
        const pugi::xml_node& node)
    {
        return vec4
        {
            std::stof(node.attribute("x").value()),
            std::stof(node.attribute("y").value()),
            std::stof(node.attribute("z").value()),
            std::stof(node.attribute("w").value()),
        };
    }

    template<typename T>
    T parse_Vertex(
        const pugi::xml_node& node,
        const vec3& scale);

    template<>
    VertexP parse_Vertex<VertexP>(
        const pugi::xml_node& node,
        const vec3& scale)
    {
        return VertexP
        {
            parse_vec<vec3>(node.child("Position")) * scale
        };
    }

    template<>
    VertexPN parse_Vertex<VertexPN>(
        const pugi::xml_node& node,
        const vec3& scale)
    {
        return VertexPN
        {
            parse_vec<vec3>(node.child("Position")) * scale,
            parse_vec<vec3>(node.child("Normal"))
        };
    }

    template<>
    VertexPNT parse_Vertex<VertexPNT>(
        const pugi::xml_node& node,
        const vec3& scale)
    {
        return VertexPNT
        {
            parse_vec<vec3>(node.child("Position")) * scale,
            parse_vec<vec3>(node.child("Normal")),
            parse_texcoords(node.child("TexCoords"))
        };
    }

    template<>
    VertexPNTT parse_Vertex<VertexPNTT>(
        const pugi::xml_node& node,
        const vec3& scale)
    {
        return VertexPNTT
        {
            parse_vec<vec3>(node.child("Position")) * scale,
            parse_vec<vec3>(node.child("Normal")),
            parse_texcoords(node.child("TexCoords")),
            parse_vec<vec4>(node.child("Tangent"))
        };
    }

    template<typename T>
    opengl::VertexBuffer<T> parse_Vertices(
        const pugi::xml_node& vertices,
        const vec3& scale = 1.0f)
    {
        std::vector<T> verts;
        for (auto& vertex : vertices.children("Vertex"))
            verts.push_back(parse_Vertex<T>(vertex, scale));

        return opengl::VertexBuffer<T>(
            verts,
            opengl::Buffer::DRAW_USAGE::STATIC_DRAW);
    }

    std::vector<GLuint> parse_Indices(
        const char* str)
    {
        std::vector<GLuint> vec;
        std::stringstream sstream(str);
        while (sstream)
        {
            GLuint val;
            sstream >> val;
            vec.push_back(val);
        }
        return vec;
    }

    AttenuationStruct parse_Attenuation(
        const pugi::xml_node& node)
    {
        return AttenuationStruct
        {
            std::stof(node.attribute("Range").value()),
            std::stof(node.attribute("Constant").value()),
            std::stof(node.attribute("Linear").value()),
            std::stof(node.attribute("Quadratic").value())
        };
    }

    std::shared_ptr<MetaProgram> load_MetaProgram(
        const pugi::xml_node& meta_program,
        const fs::path& dir)
    {
        auto program_src = meta_program.attribute("src");
        if (!program_src.empty())
            return load_MetaProgram((dir / program_src.value()).string().c_str());

        // === macro devinitions ===
        std::vector<std::string> definitions;
        for (auto& definition : meta_program.child("Definitions").children("Define"))
            definitions.push_back(std::string("#define ") + definition.child_value());

        // === shaders ===
        std::vector< std::unique_ptr<opengl::Shader> > shaders;
        for (auto& shader : meta_program.child("Shaders").children("Shader"))
        {
            std::stringstream sstream;
            sstream << "#version " << shader.attribute("version").value() << std::endl;
            std::copy(definitions.begin(), definitions.end(), std::ostream_iterator<std::string>(sstream, "\n"));

            shaders.push_back(
                std::make_unique<opengl::Shader>(
                    opengl::make_ShaderFromFile(
                        opengl::Shader::parse_SHADER_TYPE(shader.attribute("type").value()),
                        sstream,
                        (dir / shader.attribute("src").value()).string())));
        }

        // === vertex attributes ===
        std::map<VertexAttributeEnum, const char*> vertex_attributes;
        for (auto& vertex_attribute : meta_program.child("VertexAttributes").children("VertexAttribute"))
        {
            vertex_attributes[parse_VertexAttributeEnum(vertex_attribute.attribute("type").value())] =
                vertex_attribute.attribute("name").value();
        }

        // === uniform blocks ===
        std::map<UniformBlockEnum, const char*> uniform_blocks;
        for (auto& uniform_block : meta_program.child("UniformBlocks").children("UniformBlock"))
        {
            uniform_blocks[parse_UniformBlockEnum(uniform_block.attribute("type").value())] =
                uniform_block.attribute("name").value();
        }

        // === textures ===
        std::map<TextureEnum, const char*> textures;
        for (auto& texture : meta_program.child("Textures").children("Texture"))
        {
            textures[parse_TextureEnum(texture.attribute("type").value())] =
                texture.attribute("name").value();
        }

        MetaProgram program(
            /*program*/ opengl::Program(shaders),
            /*attributes*/ vertex_attributes,
            /*uniform_blocks*/ uniform_blocks,
            /*textures*/ textures);

        // === subroutines ===
        for (auto& subrotine_node : meta_program.child("Subroutines").children("Subroutine"))
        {
            auto& subroutine = program.getSubroutine(subrotine_node.attribute("name").value());
            subroutine.SelectFunction(subrotine_node.attribute("value").value());
        }

        return std::make_shared<MetaProgram>(std::move(program));
    }

    static std::map< const std::string, std::shared_ptr<MetaProgram> > program_cache;
    std::shared_ptr<MetaProgram>& load_MetaProgram(
        const char* file_name)
    {
        auto cached_program = program_cache.find(file_name);
        if (cached_program != program_cache.end())
        {
            return cached_program->second;
        }
        else
        {
            pugi::xml_document doc;
            auto result = doc.load_file(file_name);
            if (!result) throw std::exception(result.description());

            return program_cache[file_name] = load_MetaProgram(
                doc.child("MetaProgram"),
                fs::path(file_name).parent_path());
        }
    }

    void clear_ProgramCache() { program_cache.clear(); }

    std::shared_ptr<opengl::Texture> load_Texture(
        const pugi::xml_node& texture,
        const fs::path& dir)
    {
        auto type = texture.attribute("type").value();

        if (std::strcmp(type, "TextureCubemap") == 0)
        {
            return std::make_shared<opengl::TextureCubemap>(
                engine::CreateTextureCubemapFromFiles(
                /*pos_x*/ (dir / texture.attribute("pos_x").value()).string(),
                /*neg_x*/ (dir / texture.attribute("neg_x").value()).string(),
                /*pos_y*/ (dir / texture.attribute("pos_y").value()).string(),
                /*neg_y*/ (dir / texture.attribute("neg_y").value()).string(),
                /*pos_z*/ (dir / texture.attribute("pos_z").value()).string(),
                /*neg_z*/ (dir / texture.attribute("neg_z").value()).string()));
        }
        else if (std::strcmp(type, "Texture2D") == 0)
        {
            return std::make_shared<opengl::Texture2D>(
                engine::CreateTextureFromFile(
                    (dir / texture.attribute("src").value()).string()));
        }
        else if (std::strcmp(type, "Texture2DMonochrome") == 0)
        {
            return std::make_shared<opengl::Texture2D>(
                engine::make_Texture2dMonochrome(parse_Color(texture.child("Color"))));
        }
        else
        {
            pugi::xml_document doc;
            fs::path redirect = dir / texture.attribute("src").value();
            auto result = doc.load_file(redirect.string().c_str());
            if (!result) throw std::exception(result.description());
            return load_Texture(doc.child("Texture"), redirect.parent_path());
        }
    }

    std::shared_ptr<opengl::Texture> load_Texture(
        const char* file_name)
    {
        pugi::xml_document doc;
        auto result = doc.load_file(file_name);
        if (!result) throw std::exception(result.description());
        return load_Texture(doc.child("Texture"), fs::path(file_name).parent_path());
    }

    std::shared_ptr<MeshBase> load_Mesh(
        const pugi::xml_node& mesh,
        const fs::path& dir)
    {
        auto mesh_src = mesh.attribute("src");
        auto mesh_scale = mesh.child("scale");
        auto scale = mesh_scale.empty() ? vec3(1.0f) : parse_vec<vec3>(mesh_scale);
        if (!mesh_src.empty())
            return load_Mesh((dir / mesh_src.value()).string().c_str(), scale);

        auto mesh_type = mesh.attribute("type").value();
        auto vertex_type = mesh.attribute("vertex").value();

        if (std::strcmp(mesh_type, "MeshArray") == 0)
        {
            auto vertices = mesh.child("Vertices");
            auto drawing_mode = parse_DrawingMode(mesh.attribute("drawing_mode").value());
            
            if (std::strcmp(vertex_type, "VertexPN") == 0)
            {
                return std::make_shared<MeshArray>(
                    /*vertex_buffer*/ parse_Vertices<VertexPN>(vertices, scale),
                    /*drawing_mode*/ drawing_mode);
            }
            else if (std::strcmp(vertex_type, "VertexPNT") == 0)
            {
                return std::make_shared<MeshArray>(
                    /*vertex_buffer*/ parse_Vertices<VertexPNT>(vertices, scale),
                    /*drawing_mode*/ drawing_mode);
            }
            else if (std::strcmp(vertex_type, "VertexPNTT") == 0)
            {
                return std::make_shared<MeshArray>(
                    /*vertex_buffer*/ parse_Vertices<VertexPNTT>(vertices, scale),
                    /*drawing_mode*/ drawing_mode);
            }
        }
        else if (std::strcmp(mesh_type, "MeshIndexed") == 0)
        {
            auto vertices = mesh.child("Vertices");
            auto drawing_mode = parse_DrawingMode(mesh.attribute("drawing_mode").value());
            auto indices = mesh.child("Indices").child_value();

            if (std::strcmp(vertex_type, "VertexPN") == 0)
            {
                return std::make_shared<MeshIndexed>(
                    /*vertex_buffer*/ parse_Vertices<VertexPN>(vertices, scale),
                    /*index_buffer*/ parse_Indices(indices),
                    /*drawing_mode*/ drawing_mode);
            }
            else if (std::strcmp(vertex_type, "VertexPNT") == 0)
            {
                return std::make_shared<MeshIndexed>(
                    /*vertex_buffer*/ parse_Vertices<VertexPNT>(vertices, scale),
                    /*index_buffer*/ parse_Indices(indices),
                    /*drawing_mode*/ drawing_mode);
            }
            else if (std::strcmp(vertex_type, "VertexPNTT") == 0)
            {
                return std::make_shared<MeshIndexed>(
                    /*vertex_buffer*/ parse_Vertices<VertexPNTT>(vertices, scale),
                    /*index_buffer*/ parse_Indices(indices),
                    /*drawing_mode*/ drawing_mode);
            }
        }
        else if (std::strcmp(mesh_type, "MeshSphere") == 0)
        {
            auto radius = std::stof(mesh.attribute("radius").value());
            auto angular_resolution = std::stoi(mesh.attribute("angular_resolution").value());

            if (std::strcmp(vertex_type, "VertexPN") == 0)
                return std::make_shared< MeshSphere<VertexPN> >(radius, angular_resolution, scale);
            else if (std::strcmp(vertex_type, "VertexPNT") == 0)
                return std::make_shared< MeshSphere<VertexPNT> >(radius, angular_resolution, scale);
            else if (std::strcmp(vertex_type, "VertexPNTT") == 0)
                return std::make_shared< MeshSphere<VertexPNTT> >(radius, angular_resolution, scale);
        }
        else if (std::strcmp(mesh_type, "MeshCube") == 0)
        {
            auto radius = std::stof(mesh.attribute("radius").value());

            if (std::strcmp(vertex_type, "VertexPN") == 0)
                return std::make_shared<MeshIndexed>(makeCubePN(radius));
            else if (std::strcmp(vertex_type, "VertexPNT") == 0)
                return std::make_shared<MeshIndexed>(makeCubePNT(radius));
            else if (std::strcmp(vertex_type, "VertexPNTT") == 0)
                return std::make_shared<MeshIndexed>(makeCubePNTT(radius));
        }
        else if (std::strcmp(mesh_type, "MeshRectangle") == 0)
        {
            auto width = std::stof(mesh.attribute("width").value());
            auto height = std::stof(mesh.attribute("height").value());

            if (std::strcmp(vertex_type, "VertexPN") == 0)
                return std::make_shared<MeshIndexed>(makeRectanglePN(width, height));
            else if (std::strcmp(vertex_type, "VertexPNT") == 0)
            {
                auto umax = std::stof(mesh.attribute("umax").value());
                auto vmax = std::stof(mesh.attribute("vmax").value());
                return std::make_shared<MeshIndexed>(makeRectanglePNT(width, height, umax, vmax));
            }
            else if (std::strcmp(vertex_type, "VertexPNTT") == 0)
            {
                auto umax = std::stof(mesh.attribute("umax").value());
                auto vmax = std::stof(mesh.attribute("vmax").value());
                return std::make_shared<MeshIndexed>(makeRectanglePNTT(width, height, umax, vmax));
            }
        }

        throw std::runtime_error("engine::load_Mesh()");
    }

    std::shared_ptr<MeshBase> load_Mesh(
        const char* file_name,
        const vec3& scale)
    {
        auto extension = fs::path(file_name).extension();
        if (extension == ".xml")
        {
            pugi::xml_document doc;
            auto result = doc.load_file(file_name);
            if (!result) throw std::exception(result.description());
            return load_Mesh(doc.child("Mesh"), fs::path(file_name).parent_path());
        }
        else
        {
            return std::make_shared<MeshIndexed>(import_mesh(file_name, scale));
        }
    }

    Material load_Material(
        const pugi::xml_node& material,
        const fs::path& dir)
    {
        auto material_src = material.attribute("src");
        if (!material_src.empty())
            return load_Material((dir / material_src.value()).string().c_str());

        return Material
        {
            parse_Color(material.child("EmissiveColor")),
            parse_Color(material.child("DiffuseColor")),
            parse_Color(material.child("SpecularColor")),
            std::stof(material.child("Shininess").child_value())
        };
    }

    Material load_Material(
        const char* file_name)
    {
        pugi::xml_document doc;
        auto result = doc.load_file(file_name);
        if (!result) throw std::exception(result.description());
        return load_Material(doc.child("Material"), fs::path(file_name).parent_path());
    }

    Model3d_Base load_Model(
        const pugi::xml_node& model_node,
        const fs::path& dir)
    {
        auto model_src = model_node.attribute("src");
        if (!model_src.empty())
            return load_Model((dir / model_src.value()).string().c_str());

        auto program = load_MetaProgram(model_node.child("MetaProgram"), dir);
        auto mesh = load_Mesh(model_node.child("Mesh"), dir);
        auto material = load_Material(model_node.child("Material"), dir);

        Model3d_Base model(program, mesh);
        model.getUniformBlock<UniformBlockEnum::Material>().setValue(material);
        for (auto& texture_node : model_node.child("Textures").children("Texture"))
        {
            auto use = parse_TextureEnum(texture_node.attribute("use").value());
            auto texture = load_Texture(texture_node, dir);
            model.setTexture(use, texture);
            auto sampler = std::make_shared<opengl::TextureSampler>();
            sampler->setWrapMode(opengl::Texture::WRAP_MODE::REPEAT);
            model.setTextureSampler(use, sampler);
        }

        return model;
    }

    Model3d_Base load_Model(
        const char* file_name)
    {
        pugi::xml_document doc;
        auto result = doc.load_file(file_name);
        if (!result) throw std::exception(result.description());
        return load_Model(doc.child("Model"), fs::path(file_name).parent_path());
    }

    Scene load_Scene(
        const pugi::xml_node& scene_node,
        const fs::path& dir)
    {
        auto scene_src = scene_node.attribute("src");
        if (!scene_src.empty())
            return load_Scene((dir / scene_src.value()).string().c_str());

        auto ambientLightNode = scene_node.child("AmbientLight");
        auto dirLights = scene_node.child("DirLights").children("DirLight");
        auto pointLights = scene_node.child("PointLights").children("PointLight");
        auto spotLights = scene_node.child("SpotLights").children("SpotLight");

        auto nDirLights = std::distance(dirLights.begin(), dirLights.end());
        auto nPointLights = std::distance(pointLights.begin(), pointLights.end());
        auto nSpotLights = std::distance(spotLights.begin(), spotLights.end());

        Scene scene(nDirLights, nPointLights, nSpotLights);

        scene.getAmbientLight()->setLightColor(parse_Color(ambientLightNode.child("Color")));

        int i = 0;
        for (auto& dirLight : dirLights)
        {
            scene.DirectionalLights()->LightView(i).setWidth(std::stof(dirLight.attribute("width").value()));
            scene.DirectionalLights()->LightView(i).setHeight(std::stof(dirLight.attribute("height").value()));
            scene.DirectionalLights()->setLightColor(i, parse_Color(dirLight.child("Color")));
            scene.DirectionalLights()->setLightDirection(i, parse_direction(dirLight.child("Direction")));
            scene.DirectionalLights()->LightView(i).PositionAndOrientation().setPosition(parse_vec<vec3>(dirLight.child("Position")));
            i++;
        }

        i = 0;
        for (auto& pointLight : pointLights)
        {
            scene.PointLights()->setLightColor(i, parse_Color(pointLight.child("Color")));
            scene.PointLights()->LightView(i).PositionAndOrientation().setPosition(parse_vec<vec3>(pointLight.child("Position")));
            scene.PointLights()->setAttenuation(i, parse_Attenuation(pointLight.child("Attenuation")));
            i++;
        }

        i = 0;
        for (auto& spotLight : spotLights)
        {
            scene.SpotLights()->setLightColor(i, parse_Color(spotLight.child("Color")));
            scene.SpotLights()->LightView(i).PositionAndOrientation().setPosition(parse_vec<vec3>(spotLight.child("Position")));
            scene.SpotLights()->LightView(i).setViewAngle(util::to_radians(std::stof(spotLight.attribute("angle").value())));
            scene.SpotLights()->setLightDirection(i, parse_direction(spotLight.child("Direction")));
            scene.SpotLights()->setAttenuation(i, parse_Attenuation(spotLight.child("Attenuation")));
            i++;
        }

        for (auto& model_node : scene_node.child("Models").children("Model"))
        {
            auto ref = model_node.attribute("ref");
            auto model = ref.empty() ?
                load_Model(model_node, dir) :
                scene.findModel(ref.value()).Clone();

            model.Name() = model_node.attribute("name").value();
            bool shadowcast = std::strcmp(model_node.attribute("shadowcast").value(), "true") == 0;

            auto position_node = model_node.child("Position");
            if (!position_node.empty())
                model.PositionAndOrientation().setPosition(parse_vec<vec3>(position_node));

            auto orientation_node = model_node.child("Orientation");
            if (!orientation_node.empty())
            {
                auto orientation = parse_orientation(orientation_node);
                model.PositionAndOrientation().Yaw(util::to_radians(orientation.X()));
                model.PositionAndOrientation().Pitch(util::to_radians(orientation.Y()));
                model.PositionAndOrientation().Roll(util::to_radians(orientation.Z()));
            }

            scene.setup_Model(std::move(model), shadowcast);
        }

        return scene;
    }

    Scene load_Scene(
        const char* file_name)
    {
        pugi::xml_document doc;
        auto result = doc.load_file(file_name);
        if (!result) throw std::exception(result.description());
        return load_Scene(doc.child("Scene"), fs::path(file_name).parent_path());
    }
}
