/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "UniformBlockBuffer.h"

#include "LightStructs.h"

namespace engine
{
    class UniformBlock_DirectionalLight : public opengl::UniformBlockStructure<DirectionalLightStruct>
    {
        #pragma region Constructors

        public:
            UniformBlock_DirectionalLight() :
                opengl::UniformBlockStructure<DirectionalLightStruct>(
                    DirectionalLightStruct
                    {
                        make_Color_RGBA32F_Gray(1.0f),            // LightColor
                        vec4 { 0.0f, -1.0f, 0.0f, 0.0f },         // LightDirection
                        vmath::make_IdentityMatrix<GLfloat, 4>(), // WorldToLightProjectionPlane
                        vmath::make_IdentityMatrix<GLfloat, 4>()  // WorldToShadowMap
                    },
                    opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(const DirectionalLightStruct::TLightColor& value)
            {
                this->setData(
                    /*offset*/ offsetof(DirectionalLightStruct, DirectionalLightStruct::LightColor),
                    /*size*/ sizeof(DirectionalLightStruct::TLightColor),
                    /*data*/ &value);
            }

            DirectionalLightStruct::TLightColor getLightColor() const
            {
                DirectionalLightStruct::TLightColor value;
                this->getData(
                    /*offset*/ offsetof(DirectionalLightStruct, DirectionalLightStruct::LightColor),
                    /*size*/ sizeof(DirectionalLightStruct::TLightColor),
                    /*data*/ &value);
                return value;
            }

            void setLightDirection(const DirectionalLightStruct::TLightDirection& value)
            {
                this->setData(
                    /*offset*/ offsetof(DirectionalLightStruct, DirectionalLightStruct::LightDirection),
                    /*size*/ sizeof(DirectionalLightStruct::TLightDirection),
                    /*data*/ &value);
            }

            DirectionalLightStruct::TLightDirection getLightDirection() const
            {
                DirectionalLightStruct::TLightDirection value;
                this->getData(
                    /*offset*/ offsetof(DirectionalLightStruct, DirectionalLightStruct::LightDirection),
                    /*size*/ sizeof(DirectionalLightStruct::TLightDirection),
                    /*data*/ &value);
                return value;
            }

            void setWorldToLightProjectionPlane(const DirectionalLightStruct::TWorldToLightProjectionPlane& value)
            {
                this->setData(
                    /*offset*/ offsetof(DirectionalLightStruct, DirectionalLightStruct::WorldToLightProjectionPlane),
                    /*size*/ sizeof(DirectionalLightStruct::TWorldToLightProjectionPlane),
                    /*data*/ &value);

                // The bias is needed to transform NDC coordinates to 2d-texture coordinates, i. e.
                // NDC: -1 ... +1 --> 2d-texture: 0 ... 1
                static const mat4 bias
                {
                    { 0.5f, 0.0f, 0.0f, 0.0f },
                    { 0.0f, 0.5f, 0.0f, 0.0f },
                    { 0.0f, 0.0f, 0.5f, 0.0f },
                    { 0.5f, 0.5f, 0.5f, 1.0f }
                };

                mat4 temp = bias * value;
                this->setData(
                    /*offset*/ offsetof(DirectionalLightStruct, DirectionalLightStruct::WorldToShadowMap),
                    /*size*/ sizeof(DirectionalLightStruct::TWorldToShadowMap),
                    /*data*/ &temp);
            }

            DirectionalLightStruct::TWorldToLightProjectionPlane getWorldToLightProjectionPlane() const
            {
                DirectionalLightStruct::TWorldToLightProjectionPlane value;
                this->getData(
                    /*offset*/ offsetof(DirectionalLightStruct, DirectionalLightStruct::WorldToLightProjectionPlane),
                    /*size*/ sizeof(DirectionalLightStruct::TWorldToLightProjectionPlane),
                    /*data*/ &value);
                return value;
            }

        #pragma endregion
    };

    class UniformBlock_DirectionalLightArray : public opengl::UniformBlockArray<DirectionalLightStruct>
    {
        #pragma region Constructors

        public:
            using UniformBlockArray::UniformBlockArray;

            UniformBlock_DirectionalLightArray(size_t nLights)
                : UniformBlockArray(
                    /*data*/ std::vector<DirectionalLightStruct>(
                        nLights,
                        DirectionalLightStruct
                        {
                            make_Color_RGBA32F_Gray(1.0f),            // LightColor
                            vec4 { 0.0f, -1.0f, 0.0f, 0.0f },         // LightDirection
                            vmath::make_IdentityMatrix<GLfloat, 4>(), // WorldToLightProjectionPlane
                            vmath::make_IdentityMatrix<GLfloat, 4>()  // WorldToShadowMap
                        }),
                    /*usage*/ opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(size_t i, const DirectionalLightStruct::TLightColor& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(DirectionalLightStruct, DirectionalLightStruct::LightColor),
                    /*size*/ sizeof(DirectionalLightStruct::TLightColor),
                    /*data*/ &value);
            }

            DirectionalLightStruct::TLightColor getLightColor(size_t i) const
            {
                DirectionalLightStruct::TLightColor value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(DirectionalLightStruct, DirectionalLightStruct::LightColor),
                    /*size*/ sizeof(DirectionalLightStruct::TLightColor),
                    /*data*/ &value);
                return value;
            }

            void setLightDirection(size_t i, const DirectionalLightStruct::TLightDirection& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(DirectionalLightStruct, DirectionalLightStruct::LightDirection),
                    /*size*/ sizeof(DirectionalLightStruct::TLightDirection),
                    /*data*/ &value);
            }

            DirectionalLightStruct::TLightDirection getLightDirection(size_t i) const
            {
                DirectionalLightStruct::TLightDirection value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(DirectionalLightStruct, DirectionalLightStruct::LightDirection),
                    /*size*/ sizeof(DirectionalLightStruct::TLightDirection),
                    /*data*/ &value);
                return value;
            }

            void setWorldToLightProjectionPlane(size_t i, const DirectionalLightStruct::TWorldToLightProjectionPlane& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(DirectionalLightStruct, DirectionalLightStruct::WorldToLightProjectionPlane),
                    /*size*/ sizeof(DirectionalLightStruct::TWorldToLightProjectionPlane),
                    /*data*/ &value);

                // The bias is needed to transform NDC coordinates to 2d-texture coordinates, i. e.
                // NDC: -1 ... +1 --> 2d-texture: 0 ... 1
                static const mat4 bias
                {
                    { 0.5f, 0.0f, 0.0f, 0.0f },
                    { 0.0f, 0.5f, 0.0f, 0.0f },
                    { 0.0f, 0.0f, 0.5f, 0.0f },
                    { 0.5f, 0.5f, 0.5f, 1.0f }
                };

                mat4 temp = bias * value;
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(DirectionalLightStruct, DirectionalLightStruct::WorldToShadowMap),
                    /*size*/ sizeof(DirectionalLightStruct::TWorldToShadowMap),
                    /*data*/ &temp);
            }

            DirectionalLightStruct::TWorldToLightProjectionPlane getWorldToLightProjectionPlane(size_t i) const
            {
                DirectionalLightStruct::TWorldToLightProjectionPlane value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(DirectionalLightStruct, DirectionalLightStruct::WorldToLightProjectionPlane),
                    /*size*/ sizeof(DirectionalLightStruct::TWorldToLightProjectionPlane),
                    /*data*/ &value);
                return value;
            }

        #pragma endregion
    };
}
