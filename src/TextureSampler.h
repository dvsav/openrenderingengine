/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Texture.h"

namespace opengl
{
    // Encapsulates wrapping and filtering parameters of a texture.
    class TextureSampler final
    {
        #pragma region Fields

        private:
            GLuint m_Handle;

        #pragma endregion

        #pragma region Constructors

        public:
            TextureSampler() :
                m_Handle(0)
            {
                glGenSamplers(1, &m_Handle);
            }

            // Move constructor.
            TextureSampler(TextureSampler&& value) noexcept :
                m_Handle(value.m_Handle)
            {
                value.m_Handle = 0;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            ~TextureSampler()
            {
                glDeleteSamplers(1, &m_Handle);
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            TextureSampler(const TextureSampler&) = delete;
            TextureSampler& operator=(const TextureSampler&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            // Gets the handle of the texture sampler.
            GLuint getHandle() { return m_Handle; }

            // The texture magnification function is used when the pixel being textured maps to an area less than or equal to one texture element.
            void setMagnificationFilter(
                Texture::TEXTURE_FILTER filter)
            {
                glSamplerParameteri(
                    m_Handle,
                    GL_TEXTURE_MAG_FILTER,
                    static_cast<GLint>(filter));
            }

            // The texture minifying function is used whenever the pixel being textured maps to an area greater than one texture element.
            void setMinificationFilter(
                Texture::TEXTURE_FILTER filter)
            {
                glSamplerParameteri(
                    m_Handle,
                    GL_TEXTURE_MIN_FILTER,
                    static_cast<GLint>(filter));
            }

            // Defines what happens when texture coordinates fall outside the range 0...1.
            void setWrapMode(
                Texture::WRAP_MODE mode)
            {
                glSamplerParameteri(m_Handle, GL_TEXTURE_WRAP_S, static_cast<GLint>(mode));
                glSamplerParameteri(m_Handle, GL_TEXTURE_WRAP_T, static_cast<GLint>(mode));
                glSamplerParameteri(m_Handle, GL_TEXTURE_WRAP_R, static_cast<GLint>(mode));
            }

            // Defines what happens when particular texture coordinate falls outside the range 0...1.
            void setWrapMode(
                Texture::TEXTURE_COORDINATE coord,
                Texture::WRAP_MODE mode);

            // When texture is used as depth buffer (for example for shadow mapping) defines depth test behavior.
            void setupDepthTest(
                Texture::COMPARE_MODE compareMode,
                COMPARISON_FUNCTION compareFunc)
            {
                glSamplerParameteri(
                    m_Handle,
                    GL_TEXTURE_COMPARE_MODE,
                    static_cast<GLint>(compareMode));

                glSamplerParameteri(
                    m_Handle,
                    GL_TEXTURE_COMPARE_FUNC,
                    static_cast<GLint>(compareFunc));
            }

            /*
            Sets border color for a texture.
            If the texture contains depth components parameter 'r' is interpreted as a depth value.
            */
            void setBorderColor(
                GLfloat r, GLfloat g, GLfloat b, GLfloat a)
            {
                GLfloat border[]{ r, g, b, a };
                glSamplerParameterfv(
                    m_Handle,
                    GL_TEXTURE_BORDER_COLOR,
                    border);
            }

            /*
            Sets the amount of anisotropy in anisotropy filtering.
            Parameters:
            value - amount of anisotropy.
            1.0f is the minimum allowable value and means no anisotropy (isotropic filtering).
            You can retrieve the maximum allowable value through the method Texture::getMaximumAnisortopy().
            */
            void setAnisotropyAmount(
                GLfloat value)
            {
                util::Requires::That(
                    value >= 1.0f && value <= Texture::getMaximumAnisortopy(),
                    FUNCTION_INFO);

                glSamplerParameterf(
                    m_Handle,
                    GL_TEXTURE_MAX_ANISOTROPY_EXT,
                    value);
            }

        #pragma endregion

        #pragma region Methods

        public:
            // Binds the sampler to a specified texture unit.
            void Bind(GLuint textureUnit)
            {
                util::Requires::That(
                    textureUnit < Texture::getMaxTextureUnits(),
                    FUNCTION_INFO);

                glBindSampler(textureUnit, m_Handle);
            }

        #pragma endregion
    };
}