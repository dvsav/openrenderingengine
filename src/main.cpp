/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include <iostream>
#include "Program.h"

void main()
{
#ifdef _DEBUG
    std::cout << "--- DEBUG VERSION ---" << std::endl;
#else
    std::cout << "--- RELEASE VERSION ---" << std::endl;
#endif

    try
    {
        Program prog;
        prog.Main();
    }
    catch (std::exception ex)
    {
        std::cout << ex.what() << std::endl;
    }

    std::cout << "Press [Enter] to exit ...";
    std::cin.get();
}