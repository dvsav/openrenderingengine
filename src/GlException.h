/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <stdexcept>

namespace opengl
{
    class GlException : public std::exception
    {
        public:
            using std::exception::exception;

            explicit GlException(const std::string& message)
                : exception(message.c_str())
            {}
    };
}