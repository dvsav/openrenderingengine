/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <stdexcept>
#include "GLEWHeaders.h"

namespace opengl
{
    // Specifies the type of data.
    // Packed data types specify packed color layout in a pixel
    // (color components are ordered from MSB to LSB).
    enum class TYPE : GLenum
    {
        BYTE = GL_BYTE,
        UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
        SHORT = GL_SHORT,
        UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
        INT = GL_INT,
        UNSIGNED_INT = GL_UNSIGNED_INT,
        FLOAT = GL_FLOAT,
        HALF_FLOAT = GL_HALF_FLOAT,
        BYTES_2 = GL_2_BYTES,
        BYTES_3 = GL_3_BYTES,
        BYTES_4 = GL_4_BYTES,
        DOUBLE = GL_DOUBLE,
        INT_2_10_10_10_REV = GL_INT_2_10_10_10_REV,
        UNSIGNED_INT_2_10_10_10_REV = GL_UNSIGNED_INT_2_10_10_10_REV,
        UNSIGNED_INT_10F_11F_11F_REV = GL_UNSIGNED_INT_10F_11F_11F_REV,
        FIXED = GL_FIXED,
        UNSIGNED_BYTE_3_3_2 = GL_UNSIGNED_BYTE_3_3_2,
        UNSIGNED_BYTE_2_3_3_REV = GL_UNSIGNED_BYTE_2_3_3_REV,
        UNSIGNED_SHORT_5_6_5 = GL_UNSIGNED_SHORT_5_6_5,
        UNSIGNED_SHORT_5_6_5_REV = GL_UNSIGNED_SHORT_5_6_5_REV,
        UNSIGNED_SHORT_4_4_4_4 = GL_UNSIGNED_SHORT_4_4_4_4,
        UNSIGNED_SHORT_4_4_4_4_REV = GL_UNSIGNED_SHORT_4_4_4_4_REV,
        UNSIGNED_SHORT_5_5_5_1 = GL_UNSIGNED_SHORT_5_5_5_1,
        UNSIGNED_SHORT_1_5_5_5_REV = GL_UNSIGNED_SHORT_1_5_5_5_REV,
        UNSIGNED_INT_8_8_8_8 = GL_UNSIGNED_INT_8_8_8_8,
        UNSIGNED_INT_8_8_8_8_REV = GL_UNSIGNED_INT_8_8_8_8_REV,
        UNSIGNED_INT_10_10_10_2 = GL_UNSIGNED_INT_10_10_10_2
    };

    // OpenGL vector types.
    enum class VECTOR_TYPE : GLenum
    {
        FLOAT = GL_FLOAT,
        FLOAT_VEC2 = GL_FLOAT_VEC2,
        FLOAT_VEC3 = GL_FLOAT_VEC3,
        FLOAT_VEC4 = GL_FLOAT_VEC4,

        INT = GL_INT,
        INT_VEC2 = GL_INT_VEC2,
        INT_VEC3 = GL_INT_VEC3,
        INT_VEC4 = GL_INT_VEC4,

        BOOL = GL_BOOL,
        BOOL_VEC2 = GL_BOOL_VEC2,
        BOOL_VEC3 = GL_BOOL_VEC3,
        BOOL_VEC4 = GL_BOOL_VEC4,

        UNSIGNED_INT = GL_UNSIGNED_INT,
        UNSIGNED_INT_VEC2 = GL_UNSIGNED_INT_VEC2,
        UNSIGNED_INT_VEC3 = GL_UNSIGNED_INT_VEC3,
        UNSIGNED_INT_VEC4 = GL_UNSIGNED_INT_VEC4,

        DOUBLE = GL_DOUBLE,
        DOUBLE_VEC2 = GL_DOUBLE_VEC2,
        DOUBLE_VEC3 = GL_DOUBLE_VEC3,
        DOUBLE_VEC4 = GL_DOUBLE_VEC4
    };

    #pragma region type_traits

    template<typename T>
    struct type_traits
    {};

    template<>
    struct type_traits<GLbyte>
    {
        static constexpr TYPE gl_type = TYPE::BYTE;
    };

    template<>
    struct type_traits<GLubyte>
    {
        static constexpr TYPE gl_type = TYPE::UNSIGNED_BYTE;
    };

    template<>
    struct type_traits<GLshort>
    {
        static constexpr TYPE gl_type = TYPE::SHORT;
    };

    template<>
    struct type_traits<GLushort>
    {
        static constexpr TYPE gl_type = TYPE::UNSIGNED_SHORT;
    };

    template<>
    struct type_traits<GLint>
    {
        static constexpr TYPE gl_type = TYPE::INT;
    };

    template<>
    struct type_traits<GLuint>
    {
        static constexpr TYPE gl_type = TYPE::UNSIGNED_INT;
    };

    template<>
    struct type_traits<GLfloat>
    {
        static constexpr TYPE gl_type = TYPE::FLOAT;
    };

    template<>
    struct type_traits<GLdouble>
    {
        static constexpr TYPE gl_type = TYPE::DOUBLE;
    };

    #pragma endregion

    #pragma region gl_type_traits

    template<TYPE gl_type>
    struct gl_type_traits
    {};

    template<>
    struct gl_type_traits<TYPE::BYTE>
    {
        using value_type = GLbyte;
    };

    template<>
    struct gl_type_traits<TYPE::UNSIGNED_BYTE>
    {
        using value_type = GLubyte;
    };

    template<>
    struct gl_type_traits<TYPE::SHORT>
    {
        using value_type = GLshort;
    };

    template<>
    struct gl_type_traits<TYPE::UNSIGNED_SHORT>
    {
        using value_type = GLushort;
    };

    template<>
    struct gl_type_traits<TYPE::INT>
    {
        using value_type = GLint;
    };

    template<>
    struct gl_type_traits<TYPE::UNSIGNED_INT>
    {
        using value_type = GLuint;
    };

    template<>
    struct gl_type_traits<TYPE::FLOAT>
    {
        using value_type = GLfloat;
    };

    template<>
    struct gl_type_traits<TYPE::DOUBLE>
    {
        using value_type = GLdouble;
    };

    template<>
    struct gl_type_traits<TYPE::BYTES_2>
    {
        using value_type = GLbyte[2];
    };

    template<>
    struct gl_type_traits<TYPE::BYTES_3>
    {
        using value_type = GLbyte[3];
    };

    template<>
    struct gl_type_traits<TYPE::BYTES_4>
    {
        using value_type = GLbyte[4];
    };

    template<>
    struct gl_type_traits<TYPE::FIXED>
    {
        using value_type = GLshort[2];
    };

    #pragma endregion

    template <TYPE gl_type>
    constexpr GLsizei getTypeSize()
    {
        return sizeof(typename gl_type_traits<gl_type>::value_type);
    }

    inline GLsizei getTypeSize(TYPE type)
    {
        switch (type)
        {
            case TYPE::BYTE: return sizeof(gl_type_traits<TYPE::BYTE>::value_type);
            case TYPE::UNSIGNED_BYTE: return sizeof(gl_type_traits<TYPE::UNSIGNED_BYTE>::value_type);
            case TYPE::SHORT: return sizeof(gl_type_traits<TYPE::SHORT>::value_type);
            case TYPE::UNSIGNED_SHORT: return sizeof(gl_type_traits<TYPE::UNSIGNED_SHORT>::value_type);
            case TYPE::INT: return sizeof(gl_type_traits<TYPE::INT>::value_type);
            case TYPE::UNSIGNED_INT: return sizeof(gl_type_traits<TYPE::UNSIGNED_INT>::value_type);
            case TYPE::FLOAT: return sizeof(gl_type_traits<TYPE::FLOAT>::value_type);
            case TYPE::HALF_FLOAT: return sizeof(gl_type_traits<TYPE::FLOAT>::value_type) / 2;
            case TYPE::BYTES_2: return sizeof(gl_type_traits<TYPE::BYTES_2>::value_type);
            case TYPE::BYTES_3: return sizeof(gl_type_traits<TYPE::BYTES_3>::value_type);
            case TYPE::BYTES_4: return sizeof(gl_type_traits<TYPE::BYTES_4>::value_type);
            case TYPE::DOUBLE: return sizeof(gl_type_traits<TYPE::DOUBLE>::value_type);
            case TYPE::INT_2_10_10_10_REV: return sizeof(gl_type_traits<TYPE::INT>::value_type);
            case TYPE::UNSIGNED_INT_2_10_10_10_REV: return sizeof(gl_type_traits<TYPE::UNSIGNED_INT>::value_type);
            case TYPE::UNSIGNED_INT_10F_11F_11F_REV: return sizeof(gl_type_traits<TYPE::UNSIGNED_INT>::value_type);
            case TYPE::FIXED: return sizeof(gl_type_traits<TYPE::FIXED>::value_type);
            default: throw std::invalid_argument("opengl::getTypeSize(type)");
        }
    }

    inline const std::string getTypeName(TYPE type)
    {
        switch (type)
        {
            case TYPE::BYTE: return "byte";
            case TYPE::UNSIGNED_BYTE: "unsigned byte";
            case TYPE::SHORT: "short";
            case TYPE::UNSIGNED_SHORT: "unsigned short";
            case TYPE::INT: return "int";
            case TYPE::UNSIGNED_INT: "unsigned int";
            case TYPE::FLOAT: return "float";
            case TYPE::HALF_FLOAT: return "halffloat";
            case TYPE::BYTES_2: return "byte[2]";
            case TYPE::BYTES_3: return "byte[3]";
            case TYPE::BYTES_4: return "byte[4]";
            case TYPE::DOUBLE: return "double";
            case TYPE::INT_2_10_10_10_REV: "int_2_10_10_10_rev";
            case TYPE::UNSIGNED_INT_2_10_10_10_REV: "unsigned int_2_10_10_10_rev";
            case TYPE::UNSIGNED_INT_10F_11F_11F_REV: "unsigned int_10f_11f_11f_rev";
            case TYPE::FIXED: return "fixed";
            default: return "unknown type";
        }
    }

    inline const std::string getTypeName(VECTOR_TYPE type)
    {
        switch (type)
        {
            case VECTOR_TYPE::FLOAT: return "float";
            case VECTOR_TYPE::FLOAT_VEC2: return "vec2";
            case VECTOR_TYPE::FLOAT_VEC3: return "vec3";
            case VECTOR_TYPE::FLOAT_VEC4: return "vec4";

            case VECTOR_TYPE::INT: return "int";
            case VECTOR_TYPE::INT_VEC2: return "ivec2";
            case VECTOR_TYPE::INT_VEC3: return "ivec3";
            case VECTOR_TYPE::INT_VEC4: return "ivec4";

            case VECTOR_TYPE::BOOL: return "bool";
            case VECTOR_TYPE::BOOL_VEC2: return "bvec2";
            case VECTOR_TYPE::BOOL_VEC3: return "bvec3";
            case VECTOR_TYPE::BOOL_VEC4: return "bvec4";

            case VECTOR_TYPE::UNSIGNED_INT: return "uint";
            case VECTOR_TYPE::UNSIGNED_INT_VEC2: return "uvec2";
            case VECTOR_TYPE::UNSIGNED_INT_VEC3: return "uvec3";
            case VECTOR_TYPE::UNSIGNED_INT_VEC4: return "uvec4";

            case VECTOR_TYPE::DOUBLE: return "double";
            case VECTOR_TYPE::DOUBLE_VEC2: return "dvec2";
            case VECTOR_TYPE::DOUBLE_VEC3: return "dvec3";
            case VECTOR_TYPE::DOUBLE_VEC4: return "dvec4";

            default: return "unknown type";
        }
    }
}