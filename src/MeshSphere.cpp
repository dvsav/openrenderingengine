/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include "MeshSphere.h"
#include "utility.h"

namespace engine
{
    std::vector<VertexPN> MeshSphere<VertexPN>::makeSphereMesh(
        GLfloat radius,
        GLuint angular_resolution,
        const vec3& scale)
    {
        GLfloat step_phi = 2.0f * M_PI / angular_resolution;
        GLfloat step_theta = M_PI / angular_resolution;

        std::vector<VertexPN> vertices;
        vertices.reserve(angular_resolution * (angular_resolution + 1));

        for (GLuint i = 0; i <= angular_resolution; i++)
        {
            GLfloat sin_theta = sinf(i * step_theta);
            GLfloat cos_theta = cosf(i * step_theta);
            for (GLuint j = 0; j < angular_resolution; j++)
            {
                GLfloat sin_phi = sinf(j * step_phi);
                GLfloat cos_phi = cosf(j * step_phi);

                vec3 normal
                {
                    sin_theta * cos_phi,
                    cos_theta,
                    sin_theta * sin_phi
                };

                vertices.emplace_back(
                    /*Position*/ radius * normal * scale,
                    /*Normal*/ normal
                );
            }
        }

        return vertices;
    }

    std::vector<GLuint> MeshSphere<VertexPN>::makeIndices(
        GLuint angular_resolution)
    {
        std::vector<GLuint> indices;
        indices.reserve(2 * angular_resolution + 2);

        indices.push_back(0);

        for (GLuint i = 1; i <= angular_resolution - 1; i++)
        {
            indices.push_back(angular_resolution + i);
            indices.push_back(i);
        }

        indices.push_back(angular_resolution);
        indices.push_back(0);

        indices.push_back(angular_resolution + 1);

        return indices;
    }

    void MeshSphere<VertexPN>::Render()
    {
        for (GLuint i = 0; i < m_AngularResolution; i++)
        {
            glDrawElementsBaseVertex(
                /*mode*/    m_DrawingMode,
                /*count*/   m_IndexBuffer.getIndexCount(),
                /*type*/    m_IndexBuffer.getIndexType(),
                /*indices*/ 0,
                /*basevertex*/ i * m_AngularResolution);
        }
    }

    /*
    Renders insatance_count instances of the mesh.
    Parameters:
    instance_count - the number of instances of the specified range of indices to be rendered.
    base_instance - the base instance for use in fetching instanced vertex attributes.
    */
    void MeshSphere<VertexPN>::Render(
        GLsizei instance_count,
        GLuint base_instance)
    {
        for (GLuint i = 0; i < m_AngularResolution; i++)
        {
            glDrawElementsInstancedBaseVertexBaseInstance(
                /*mode*/    m_DrawingMode,
                /*count*/   m_IndexBuffer.getIndexCount(),
                /*type*/    m_IndexBuffer.getIndexType(),
                /*indices*/ 0,
                /*primcount*/ instance_count,
                /*basevertex*/ i * m_AngularResolution,
                /*baseinstance*/ base_instance);
        }
    }

    std::vector<VertexPNT> MeshSphere<VertexPNT>::makeSphereMesh(
        GLfloat radius,
        GLuint angular_resolution,
        const vec3& scale)
    {
        GLfloat step_phi = 2.0f * M_PI / angular_resolution;
        GLfloat step_theta = M_PI / angular_resolution;

        std::vector<VertexPNT> vertices;
        vertices.reserve((angular_resolution + 1) * (angular_resolution + 1));

        for (GLuint i = 0; i <= angular_resolution; i++)
        {
            GLfloat sin_theta = sinf(i * step_theta);
            GLfloat cos_theta = cosf(i * step_theta);
            for (GLuint j = 0; j <= angular_resolution; j++)
            {
                GLfloat sin_phi = sinf(j * step_phi);
                GLfloat cos_phi = cosf(j * step_phi);

                vec3 normal
                {
                    sin_theta * cos_phi,
                    cos_theta,
                    sin_theta * sin_phi
                };

                vertices.emplace_back(
                    /*Position*/ radius * normal * scale,
                    /*Normal*/ normal,
                    /*TextureCoords*/ vec2{ static_cast<GLfloat>(j) / angular_resolution, static_cast<GLfloat>(i) / angular_resolution }
                );
            }
        }

        return vertices;
    }

    std::vector<VertexPNTT> MeshSphere<VertexPNTT>::makeSphereMesh(
        GLfloat radius,
        GLuint angular_resolution,
        const vec3& scale)
    {
        GLfloat step_phi = 2.0f * M_PI / angular_resolution;
        GLfloat step_theta = M_PI / angular_resolution;

        std::vector<VertexPNTT> vertices;
        vertices.reserve((angular_resolution + 1) * (angular_resolution + 1));

        for (GLuint i = 0; i <= angular_resolution; i++)
        {
            GLfloat sin_theta = sinf(i * step_theta);
            GLfloat cos_theta = cosf(i * step_theta);
            for (GLuint j = 0; j <= angular_resolution; j++)
            {
                GLfloat sin_phi = sinf(j * step_phi);
                GLfloat cos_phi = cosf(j * step_phi);

                vec3 normal
                {
                    sin_theta * cos_phi,
                    cos_theta,
                    sin_theta * sin_phi
                };

                vec4 tangent
                {
                    -sin_phi,
                    0.0f,
                    cos_phi
                };

                vertices.emplace_back(
                    /*Position*/ radius * normal * scale,
                    /*Normal*/ normal,
                    /*TextureCoords*/ vec2{ static_cast<GLfloat>(j) / angular_resolution, static_cast<GLfloat>(i) / angular_resolution },
                    /*Tangent*/ tangent
                );
            }
        }

        return vertices;
    }

    std::vector<GLuint> MeshSphere<VertexPNT>::makeIndices(
        GLuint angular_resolution)
    {
        std::vector<GLuint> indices;
        indices.reserve(2 * angular_resolution + 4);

        indices.push_back(0);

        for (GLuint i = 1; i <= angular_resolution; i++)
        {
            indices.push_back(i + angular_resolution + 1);
            indices.push_back(i);
        }

        indices.push_back(angular_resolution + 1);
        indices.push_back(0);

        indices.push_back(angular_resolution + 2);

        return indices;
    }

    std::vector<GLuint> MeshSphere<VertexPNTT>::makeIndices(
        GLuint angular_resolution)
    {
        return MeshSphere<VertexPNT>::makeIndices(angular_resolution);
    }

    void MeshSphere<VertexPNT>::Render()
    {
        for (GLuint i = 0; i < m_AngularResolution; i++)
        {
            glDrawElementsBaseVertex(
                /*mode*/    m_DrawingMode,
                /*count*/   m_IndexBuffer.getIndexCount(),
                /*type*/    m_IndexBuffer.getIndexType(),
                /*indices*/ 0,
                /*basevertex*/ i * (m_AngularResolution + 1));
        }
    }

    void MeshSphere<VertexPNTT>::Render()
    {
        for (GLuint i = 0; i < m_AngularResolution; i++)
        {
            glDrawElementsBaseVertex(
                /*mode*/    m_DrawingMode,
                /*count*/   m_IndexBuffer.getIndexCount(),
                /*type*/    m_IndexBuffer.getIndexType(),
                /*indices*/ 0,
                /*basevertex*/ i * (m_AngularResolution + 1));
        }
    }

    void MeshSphere<VertexPNT>::Render(
        GLsizei instance_count,
        GLuint base_instance)
    {
        for (GLuint i = 0; i < m_AngularResolution; i++)
        {
            glDrawElementsInstancedBaseVertexBaseInstance(
                /*mode*/    m_DrawingMode,
                /*count*/   m_IndexBuffer.getIndexCount(),
                /*type*/    m_IndexBuffer.getIndexType(),
                /*indices*/ 0,
                /*primcount*/ instance_count,
                /*basevertex*/ i * (m_AngularResolution + 1),
                /*baseinstance*/ base_instance);
        }
    }

    void MeshSphere<VertexPNTT>::Render(
        GLsizei instance_count,
        GLuint base_instance)
    {
        for (GLuint i = 0; i < m_AngularResolution; i++)
        {
            glDrawElementsInstancedBaseVertexBaseInstance(
                /*mode*/    m_DrawingMode,
                /*count*/   m_IndexBuffer.getIndexCount(),
                /*type*/    m_IndexBuffer.getIndexType(),
                /*indices*/ 0,
                /*primcount*/ instance_count,
                /*basevertex*/ i * (m_AngularResolution + 1),
                /*baseinstance*/ base_instance);
        }
    }
}

#undef M_PI