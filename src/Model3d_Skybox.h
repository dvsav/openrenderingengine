/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Model3d_Base.h"

namespace engine
{
    class Model3d_Skybox : public Model3d_Base
    {
        #pragma region Fields

        private:
            mat4 m_WorldToScreen;

        #pragma endregion

        #pragma region Constructors

        public:
            Model3d_Skybox(
                const std::shared_ptr<MetaProgram>& program,
                const std::shared_ptr<MeshBase>& mesh,
                const std::shared_ptr<opengl::Texture>& textureDiffuse) :
                Model3d_Base(program, mesh),
                m_WorldToScreen(vmath::make_IdentityMatrix<GLfloat, 4>())
            {
                setUniformBlock<UniformBlockEnum::ModelViewPerspective>(
                    std::shared_ptr<UniformBlock_ModelViewPerspective>(new UniformBlock_ModelViewPerspective()));

                setTexture(TextureEnum::Diffuse, textureDiffuse);
            }

            // Move constructor.
            Model3d_Skybox(Model3d_Skybox&& value) noexcept :
                Model3d_Base(std::move(value)),
                m_WorldToScreen(std::move(value.m_WorldToScreen))
            { }

        #pragma endregion

        #pragma region Methods

        protected:
            void PrepareForRendering(RenderPassEnum renderPass) override
            {
                if (m_WorldPositionAndOrientation_Invalid)
                {
                    getUniformBlock<UniformBlockEnum::ModelViewPerspective>().setValue(
                        m_WorldToScreen * m_WorldPositionAndOrientation.ModelToWorldMatrix());

                    m_WorldPositionAndOrientation_Invalid = false;
                }
            }

        public:
            // Sets up all state variables dependent on the camera.
            void SetupCamera(const Camera& cam)
            {
                m_WorldToScreen = cam.getCameraToScreenMatrix() * vmath::eliminateTranslation(cam.getWorldToCameraMatrix());
                m_WorldPositionAndOrientation_Invalid = true;
            }

        #pragma endregion
    };
}