/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "GLEWHeaders.h"
#include "GLenums.h"

namespace opengl
{
    namespace Stencil
    {
        enum class OPERATION : GLenum
        {
            KEEP = GL_KEEP,
            ZERO = GL_ZERO,
            REPLACE = GL_REPLACE,
            INCR = GL_INCR,
            INCR_WRAP = GL_INCR_WRAP,
            DECR = GL_DECR,
            DECR_WRAP = GL_DECR_WRAP,
            INVERT = GL_INVERT
        };

        inline void Enable()
        {
            glEnable(GL_STENCIL_TEST);
        }

        inline void Disable()
        {
            glDisable(GL_STENCIL_TEST);
        }

        inline bool IsEnabled()
        {
            return glIsEnabled(GL_STENCIL_TEST);
        }

        inline void setStencilFunc(
            COMPARISON_FUNCTION func,
            GLint ref,
            GLuint mask = 0xFFFFFFFF)
        {
            glStencilFunc(
                /*func*/ static_cast<GLenum>(func),
                /*ref*/ ref,
                /*mask*/ mask);
        }
        
        inline void setStencilFunc(
            FACE face,
            COMPARISON_FUNCTION func,
            GLint ref,
            GLuint mask = 0xFFFFFFFF)
        {
            glStencilFuncSeparate(
                /*face*/ static_cast<GLenum>(face),
                /*func*/ static_cast<GLenum>(func),
                /*ref*/ ref,
                /*mask*/ mask);
        }

        inline COMPARISON_FUNCTION getStencilFunc(
            FACE face)
        {
            COMPARISON_FUNCTION func;

            switch (face)
            {
            case opengl::FACE::BACK:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_BACK_FUNC,
                    /*params*/ reinterpret_cast<GLint*>(&func));
                break;
            default:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_FUNC,
                    /*params*/ reinterpret_cast<GLint*>(&func));
                break;
            }
            return func;
        }

        inline GLuint getStencilValueMask(
            FACE face)
        {
            GLuint mask;
            switch (face)
            {
            case opengl::FACE::BACK:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_BACK_VALUE_MASK,
                    /*params*/ reinterpret_cast<GLint*>(&mask));
                break;
            default:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_VALUE_MASK,
                    /*params*/ reinterpret_cast<GLint*>(&mask));
                break;
            }
            return mask;
        }

        inline GLint getStencilRef(
            FACE face)
        {
            GLint ref;
            switch (face)
            {
            case opengl::FACE::BACK:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_BACK_REF,
                    /*params*/ &ref);
                break;
            default:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_REF,
                    /*params*/ &ref);
                break;
            }
            return ref;
        }

        inline void setStencilOperation(
            OPERATION stencil_fail,
            OPERATION depth_fail,
            OPERATION depth_pass)
        {
            glStencilOp(
                /*sfail*/ static_cast<GLenum>(stencil_fail),
                /*dpfail*/ static_cast<GLenum>(depth_fail),
                /*dppass*/ static_cast<GLenum>(depth_pass));
        }

        inline void setStencilOperation(
            FACE face,
            OPERATION stencil_fail,
            OPERATION depth_fail,
            OPERATION depth_pass)
        {
            glStencilOpSeparate(
                /*face*/ static_cast<GLenum>(face),
                /*sfail*/ static_cast<GLenum>(stencil_fail),
                /*dpfail*/ static_cast<GLenum>(depth_fail),
                /*dppass*/ static_cast<GLenum>(depth_pass));
        }

        inline OPERATION getStencilOperationFail(
            FACE face)
        {
            OPERATION op;
            switch (face)
            {
            case opengl::FACE::BACK:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_BACK_FAIL,
                    /*params*/ reinterpret_cast<GLint*>(&op));
                break;
            default:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_FAIL,
                    /*params*/ reinterpret_cast<GLint*>(&op));
                break;
            }
            return op;
        }

        inline OPERATION getStencilOperationDepthFail(
            FACE face)
        {
            OPERATION op;
            switch (face)
            {
            case opengl::FACE::BACK:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_BACK_PASS_DEPTH_FAIL,
                    /*params*/ reinterpret_cast<GLint*>(&op));
                break;
            default:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_PASS_DEPTH_FAIL,
                    /*params*/ reinterpret_cast<GLint*>(&op));
                break;
            }
            return op;
        }

        inline OPERATION getStencilOperationDepthPass(
            FACE face)
        {
            OPERATION op;
            switch (face)
            {
            case opengl::FACE::BACK:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_BACK_PASS_DEPTH_PASS,
                    /*params*/ reinterpret_cast<GLint*>(&op));
                break;
            default:
                glGetIntegerv(
                    /*pname*/ GL_STENCIL_PASS_DEPTH_PASS,
                    /*params*/ reinterpret_cast<GLint*>(&op));
                break;
            }
            return op;
        }
    };
}