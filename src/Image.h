/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"
#include <string>

namespace util
{
    class Image
    {
    private:
        void* m_Data;
        int m_Width;
        int m_Height;
        int m_BitDepth;

    public:
        Image(
            const std::string& filename,
            unsigned int requiredChannels = 0);

        ~Image() noexcept;

    private:
        Image(const Image&) = delete;
        Image& operator=(const Image&) = delete;

    public:
        const void* data() const { return m_Data; }
        int width() const { return m_Width; }
        int height() const { return m_Height; }
        int bitDepth() const { return m_BitDepth; }
    };
}