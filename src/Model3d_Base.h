/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <memory>
#include <map>
#include <type_traits>

#include "UniformBlockBuffer.h"
#include "Texture.h"
#include "TextureSampler.h"
#include "VertexArrayObject.h"
#include "MeshBase.h"
#include "MetaProgram.h"
#include "Camera.h"

namespace engine
{
    // Class aggregating a minimal set of opengl objects required for rendering (mesh and program).
    class Model3d_Base
    {
        #pragma region Nested Types

        public:
            enum class RenderPassEnum
            {
                RenderScene,
                RecordDepth
            };

        #pragma endregion

        #pragma region Fields

        private:
            std::string m_Name;
            std::shared_ptr<MeshBase> m_Mesh;
            std::map< RenderPassEnum, std::pair< opengl::VertexArrayObject, std::shared_ptr<MetaProgram> > > m_VaosAndMetaPrograms;
            std::map< UniformBlockEnum, std::shared_ptr<opengl::UniformBlockBuffer> > m_UniformBlocks;
            std::map< TextureEnum, std::shared_ptr<opengl::Texture> > m_Textures;
            std::map< TextureEnum, std::shared_ptr<opengl::TextureSampler> > m_TextureSamplers;

        protected:
            WorldPositionAndOrientation m_WorldPositionAndOrientation;
            bool m_WorldPositionAndOrientation_Invalid;

        #pragma endregion

        #pragma region Constructors

        public:
            Model3d_Base(
                const std::shared_ptr<MetaProgram>& program,
                const std::shared_ptr<MeshBase>& mesh) :

                m_Name(),
                m_Mesh(mesh),
                m_VaosAndMetaPrograms(),
                m_UniformBlocks(),
                m_Textures(),
                m_TextureSamplers(),
                m_WorldPositionAndOrientation(),
                m_WorldPositionAndOrientation_Invalid(true)
            {
                setProgram(RenderPassEnum::RenderScene, program);
                setUniformBlock<UniformBlockEnum::ModelToWorld>(std::make_shared<UniformBlock_ModelToWorld>());
                setUniformBlock<UniformBlockEnum::Material>(std::make_shared<UniformBlock_Material>());
            }

            // Move constructor.
            Model3d_Base(Model3d_Base&& value) noexcept :
                m_Name(std::move(value.m_Name)),
                m_Mesh(std::move(value.m_Mesh)),
                m_VaosAndMetaPrograms(std::move(value.m_VaosAndMetaPrograms)),
                m_UniformBlocks(std::move(value.m_UniformBlocks)),
                m_Textures(std::move(value.m_Textures)),
                m_TextureSamplers(std::move(value.m_TextureSamplers)),
                m_WorldPositionAndOrientation(std::move(value.m_WorldPositionAndOrientation)),
                m_WorldPositionAndOrientation_Invalid(value.m_WorldPositionAndOrientation_Invalid)
            { }

            Model3d_Base Clone()
            {
                Model3d_Base clon(
                    getProgram(RenderPassEnum::RenderScene),
                    m_Mesh);

                for (auto& item : m_VaosAndMetaPrograms)
                    clon.setProgram(item.first, item.second.second);

                for (auto& item : m_UniformBlocks)
                {
                    switch (item.first)
                    {
                    case UniformBlockEnum::Material:
                        clon.setUniformBlock<UniformBlockEnum::Material>(
                            std::make_shared<UniformBlock_Material>());

                        clon.getUniformBlock<UniformBlockEnum::Material>().setValue(
                            getUniformBlock<UniformBlockEnum::Material>().getValue());

                        break;

                    case UniformBlockEnum::ModelToWorld:
                        clon.setUniformBlock<UniformBlockEnum::ModelToWorld>(
                            std::make_shared<UniformBlock_ModelToWorld>());

                        clon.getUniformBlock<UniformBlockEnum::ModelToWorld>().setValue(
                            getUniformBlock<UniformBlockEnum::ModelToWorld>().getValue());

                        break;

                    case UniformBlockEnum::ModelViewPerspective:
                        setUniformBlock<UniformBlockEnum::ModelViewPerspective>(
                            std::shared_ptr<UniformBlock_ModelViewPerspective>(
                                new UniformBlock_ModelViewPerspective()));

                        clon.getUniformBlock<UniformBlockEnum::ModelViewPerspective>().setValue(
                            getUniformBlock<UniformBlockEnum::ModelViewPerspective>().getValue());

                        break;

                    default:
                        clon.setUniformBlock(item.first, item.second);
                        break;
                    }
                }

                clon.m_Textures = m_Textures;
                clon.m_TextureSamplers = m_TextureSamplers;

                return clon;
            }

            Model3d_Base(const Model3d_Base& value) = delete;
            Model3d_Base& operator=(const Model3d_Base& value) = delete;

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~Model3d_Base() { }

        #pragma endregion

        #pragma region Properties

        public:
            std::string& Name() { return m_Name; }
            const std::string& Name() const { return m_Name; }

            WorldPositionAndOrientation& PositionAndOrientation()
            {
                m_WorldPositionAndOrientation_Invalid = true;
                return m_WorldPositionAndOrientation;
            }
            const WorldPositionAndOrientation& PositionAndOrientation() const
            {
                return m_WorldPositionAndOrientation;
            }

            const std::shared_ptr<MeshBase>& getMesh() { return m_Mesh; }
            Model3d_Base& setMesh(std::shared_ptr<MeshBase> mesh)
            {
                m_Mesh = mesh;
                BindMeshToPrograms();
                return *this;
            }

            const std::shared_ptr<MetaProgram>& getProgram(RenderPassEnum renderPass = RenderPassEnum::RenderScene) { return m_VaosAndMetaPrograms.at(renderPass).second; }
            Model3d_Base& setProgram(RenderPassEnum renderPass, std::shared_ptr<MetaProgram> program)
            {
                if (program)
                {
                    m_VaosAndMetaPrograms[renderPass] = std::make_pair(opengl::VertexArrayObject(), program);
                    BindMeshToProgram(m_VaosAndMetaPrograms[renderPass]);
                }
                else
                {
                    m_VaosAndMetaPrograms.erase(renderPass);
                }
                return *this;
            }

            std::shared_ptr<opengl::UniformBlockBuffer>& getUniformBlockPtr(UniformBlockEnum which)
            {
                return m_UniformBlocks.at(which);
            }
            opengl::UniformBlockBuffer& getUniformBlock(UniformBlockEnum which)
            {
                return *m_UniformBlocks.at(which);
            }
            Model3d_Base& setUniformBlock(UniformBlockEnum which, std::shared_ptr<opengl::UniformBlockBuffer> value)
            {
                m_UniformBlocks[which] = value;
                return *this;
            }

            template <UniformBlockEnum WHICH>
            std::shared_ptr<typename uniform_block_traits<WHICH>::block_type>& getUniformBlockPtr()
            {
                return std::static_pointer_cast<typename uniform_block_traits<WHICH>::block_type>(
                    m_UniformBlocks.at(WHICH));
            }
            template <UniformBlockEnum WHICH>
            typename uniform_block_traits<WHICH>::block_type& getUniformBlock()
            {
                return *reinterpret_cast<uniform_block_traits<WHICH>::block_type*>(
                    m_UniformBlocks.at(WHICH).get());
            }
            template <UniformBlockEnum WHICH>
            Model3d_Base& setUniformBlock(std::shared_ptr<typename uniform_block_traits<WHICH>::block_type> value)
            {
                m_UniformBlocks[WHICH] = value;
                return *this;
            }

            std::shared_ptr<opengl::Texture>& getTexturePtr(TextureEnum which)
            {
                return m_Textures.at(which);
            }
            opengl::Texture& getTexture(TextureEnum which)
            {
                return *m_Textures.at(which);
            }
            Model3d_Base& setTexture(TextureEnum which, std::shared_ptr<opengl::Texture> value)
            {
                m_Textures[which] = value;
                return *this;
            }

            std::shared_ptr<opengl::TextureSampler>& getTextureSamplerPtr(TextureEnum which)
            {
                return m_TextureSamplers.at(which);
            }
            opengl::TextureSampler& getTextureSampler(TextureEnum which)
            {
                return *m_TextureSamplers.at(which);
            }
            Model3d_Base& setTextureSampler(TextureEnum which, std::shared_ptr<opengl::TextureSampler> value)
            {
                m_TextureSamplers[which] = value;
                return *this;
            }

        #pragma endregion

        #pragma region Methods

        private:
            void BindMeshToPrograms()
            {
                for (auto& item : m_VaosAndMetaPrograms)
                {
                    BindMeshToProgram(item.second);
                }
            }

            void BindMeshToProgram(
                std::pair< opengl::VertexArrayObject, std::shared_ptr<MetaProgram> >& vao_program)
            {
                opengl::VertexArrayObject::Binding bind_vao(vao_program.first);
                m_Mesh->BindIndexBuffer();
                for (auto& attribute : vao_program.second->VertexAttributeLocations())
                    m_Mesh->AttachAttributeToAttribLocation(std::get<0>(attribute), std::get<1>(attribute));
            }

            void PrepareForRendering(
                RenderPassEnum renderPass,
                const MetaProgram& metaprogram)
            {
                for (auto& uniform_block_binding_point : metaprogram.UniformBlocksBindingPoints())
                {
                    m_UniformBlocks.at(uniform_block_binding_point.first)
                        ->BindToUniformBindingPoint(uniform_block_binding_point.second);
                }

                for (auto& texture_unit : metaprogram.TextureUnits())
                {
                    m_Textures.at(texture_unit.first)->BindToTextureUnit(
                        texture_unit.second);

                    auto texture_sampler = m_TextureSamplers.find(texture_unit.first);
                    if (texture_sampler != m_TextureSamplers.end())
                        texture_sampler->second->Bind(texture_unit.second);
                }

                PrepareForRendering(renderPass);
            }

        protected:
            virtual void PrepareForRendering(
                RenderPassEnum renderPass)
            {
                if (m_WorldPositionAndOrientation_Invalid)
                {
                    getUniformBlock<UniformBlockEnum::ModelToWorld>().setModelToWorld(
                        m_WorldPositionAndOrientation.ModelToWorldMatrix());

                    getUniformBlock<UniformBlockEnum::ModelToWorld>().setNormalToWorld(
                        vmath::eliminateTranslation(m_WorldPositionAndOrientation.ModelToWorldMatrix()));

                    m_WorldPositionAndOrientation_Invalid = false;
                }
            }

        public:
            // Renders the object.
            void Render(
                RenderPassEnum renderPass = RenderPassEnum::RenderScene)
            {
                // Search VAO & program combination for the required render pass.
                // If the combination is not found skip to the default combination (RenderScene).
                auto vao_program_iterator = m_VaosAndMetaPrograms.find(renderPass);
                auto& vao_program = vao_program_iterator == m_VaosAndMetaPrograms.end() ?
                    m_VaosAndMetaPrograms.at(renderPass = RenderPassEnum::RenderScene) :
                    vao_program_iterator->second;

                PrepareForRendering(renderPass, *vao_program.second);

                opengl::Program::Using use(
                    vao_program.second->Program(),
                    /*use_for_rendering*/ true);

                opengl::VertexArrayObject::Binding bind_vao(
                    vao_program.first);

                m_Mesh->Render();
            }

            /*
            Renders insatance_count instances of the object.
            Parameters:
            instance_count - the number of instances of the specified range of indices to be rendered.
            base_instance - the base instance for use in fetching instanced vertex attributes.
            */
            void Render(
                GLsizei instance_count,
                GLuint base_instance = 0,
                RenderPassEnum renderPass = RenderPassEnum::RenderScene)
            {
                // Search VAO & program combination for the required render pass.
                // If the combination is not found skip to the default combination (RenderScene).
                auto vao_program_iterator = m_VaosAndMetaPrograms.find(renderPass);
                auto& vao_program = vao_program_iterator == m_VaosAndMetaPrograms.end() ?
                    m_VaosAndMetaPrograms.at(renderPass = RenderPassEnum::RenderScene) :
                    vao_program_iterator->second;

                PrepareForRendering(renderPass, *vao_program.second.get());

                opengl::Program::Using use(
                    vao_program.second->Program(),
                    true);

                opengl::VertexArrayObject::Binding bind_vao(
                    vao_program.first);

                m_Mesh->Render(
                    instance_count,
                    base_instance);
            }

        #pragma endregion
    };
}