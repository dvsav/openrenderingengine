/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

// Link to opengl32.lib
#pragma comment(lib, "opengl32.lib")

// Link to glew32.lib
#pragma comment(lib, "glew32.lib")

// You can also statically link GLEW library as follows:
// #pragma comment(lib, "glew32s.lib")
// #define GLEW_STATIC

#include "GL/glew.h"
#include "GL/wglew.h"
