/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <iostream>

namespace engine
{
    template<typename T>
    struct Color_RGB
    {
        using value_type = T;
        static constexpr size_t components_number = 3;

        T R;
        T G;
        T B;

        Color_RGB() :
            R(), G(), B()
        {}

        Color_RGB(T r, T g, T b) :
            R(r), G(g), B(b)
        {}

        T* data()
        {
            return reinterpret_cast<T*>(this);
        }

        const T* data() const
        {
            return reinterpret_cast<T*>(const_cast<Color_RGB*>(this));
        }
    };

    template<typename T>
    std::ostream& operator<<(std::ostream& os, const Color_RGB<T>& color)
    {
        os << "{ " << color.R << ", " << color.G << ", " << color.B << " }";
        return os;
    }

    template<typename T, typename V>
    const Color_RGB<decltype(std::declval<T>()*std::declval<V>())> operator*(
        const T lhs,
        const Color_RGB<V>& rhs) noexcept
    {
        return make_Color_RGB(lhs * rhs.R, lhs * rhs.G, lhs * rhs.B);
    }

    template<typename T, typename V>
    const Color_RGB<decltype(std::declval<T>()*std::declval<V>())> operator*(
        const Color_RGB<T>& lhs,
        const V rhs) noexcept
    {
        return make_Color_RGB(lhs.R * rhs, lhs.G * rhs, lhs.B * rhs);
    }

    template<typename T>
    Color_RGB<T> make_Color_RGB(T r, T g, T b)
    {
        return Color_RGB<T>{ r, g, b };
    }

    inline Color_RGB<GLubyte> make_Color_RGB_Red()
    {
        return Color_RGB<GLubyte>{ 255, 0, 0 };
    }

    inline Color_RGB<GLubyte> make_Color_RGB_Green()
    {
        return Color_RGB<GLubyte>{ 0, 255, 0 };
    }

    inline Color_RGB<GLubyte> make_Color_RGB_Blue()
    {
        return Color_RGB<GLubyte>{ 0, 0, 255 };
    }

    inline Color_RGB<GLubyte> make_Color_RGB_Yellow()
    {
        return Color_RGB<GLubyte>{ 255, 255, 0 };
    }

    inline Color_RGB<GLfloat> make_Color_RGB32F_Red()
    {
        return Color_RGB<GLfloat>{ 1.0f, 0.0f, 0.0f };
    }

    inline Color_RGB<GLfloat> make_Color_RGB32F_Green()
    {
        return Color_RGB<GLfloat>{ 0.0f, 1.0f, 0.0f };
    }

    inline Color_RGB<GLfloat> make_Color_RGB32F_Blue()
    {
        return Color_RGB<GLfloat>{ 0.0f, 0.0f, 1.0f };
    }

    inline Color_RGB<GLfloat> make_Color_RGB32F_Yellow()
    {
        return Color_RGB<GLfloat>{ 1.0f, 1.0f, 0.0f };
    }

    template<typename T>
    struct Color_RGBA : public Color_RGB<T>
    {
        T A;
        static constexpr size_t components_number = 4;

        Color_RGBA() :
            A()
        {}

        Color_RGBA(T r, T g, T b, T a) :
            Color_RGB(r, g, b),
            A(a)
        {}
    };

    template<typename T>
    std::ostream& operator<<(std::ostream& os, const Color_RGBA<T>& color)
    {
        os << "{ " << color.R << ", " << color.G << ", " << color.B << ", " << color.A << " }";
        return os;
    }

    template<typename T, typename V>
    const Color_RGBA<decltype(std::declval<T>()*std::declval<V>())> operator*(
        const T lhs,
        const Color_RGBA<V>& rhs) noexcept
    {
        return make_Color_RGBA(lhs * rhs.R, lhs * rhs.G, lhs * rhs.B, lhs * rhs.A);
    }

    template<typename T, typename V>
    const Color_RGBA<decltype(std::declval<T>()*std::declval<V>())> operator*(
        const Color_RGBA<T>& lhs,
        const V rhs) noexcept
    {
        return make_Color_RGBA(lhs.R * rhs, lhs.G * rhs, lhs.B * rhs, lhs.A * rhs);
    }

    template<typename T>
    Color_RGBA<T> make_Color_RGBA(T r, T g, T b, T a)
    {
        return Color_RGBA<T>(r, g, b, a);
    };

    inline Color_RGBA<GLubyte> make_Color_RGBA_Red()
    {
        return Color_RGBA<GLubyte>{ 255, 0, 0, 255 };
    }

    inline Color_RGBA<GLubyte> make_Color_RGBA_Green()
    {
        return Color_RGBA<GLubyte>{ 0, 255, 0, 255 };
    }

    inline Color_RGBA<GLubyte> make_Color_RGBA_Blue()
    {
        return Color_RGBA<GLubyte>{ 0, 0, 255, 255 };
    }

    inline Color_RGBA<GLubyte> make_Color_RGBA_Yellow()
    {
        return Color_RGBA<GLubyte>{ 255, 255, 0, 255 };
    }

    inline Color_RGBA<GLubyte> make_Color_RGBA_White()
    {
        return Color_RGBA<GLubyte>{ 255, 255, 255, 255 };
    }

    inline Color_RGBA<GLubyte> make_Color_RGBA_Black()
    {
        return Color_RGBA<GLubyte>{ 0, 0, 0, 255 };
    }

    inline Color_RGBA<GLubyte> make_Color_RGBA_Gray(GLubyte level)
    {
        return Color_RGBA<GLubyte>{ level, level, level, 255 };
    }

    inline Color_RGBA<GLfloat> make_Color_RGBA32F_Red(GLfloat level = 1.0f)
    {
        return Color_RGBA<GLfloat>{ level, 0.0f, 0.0f, 1.0f };
    }

    inline Color_RGBA<GLfloat> make_Color_RGBA32F_Green(GLfloat level = 1.0f)
    {
        return Color_RGBA<GLfloat>{ 0.0f, level, 0.0f, 1.0f };
    }

    inline Color_RGBA<GLfloat> make_Color_RGBA32F_Blue(GLfloat level = 1.0f)
    {
        return Color_RGBA<GLfloat>{ 0.0f, 0.0f, level, 1.0f };
    }

    inline Color_RGBA<GLfloat> make_Color_RGBA32F_Yellow()
    {
        return Color_RGBA<GLfloat>{ 1.0f, 1.0f, 0.0f, 1.0f };
    }

    inline Color_RGBA<GLfloat> make_Color_RGBA32F_White()
    {
        return Color_RGBA<GLfloat>{ 1.0f, 1.0f, 1.0f, 1.0f };
    }

    inline Color_RGBA<GLfloat> make_Color_RGBA32F_Black()
    {
        return Color_RGBA<GLfloat>{ 0.0f, 0.0f, 0.0f, 1.0f };
    }

    inline Color_RGBA<GLfloat> make_Color_RGBA32F_Gray(GLfloat level)
    {
        return Color_RGBA<GLfloat>{ level, level, level, 1.0f };
    }
}