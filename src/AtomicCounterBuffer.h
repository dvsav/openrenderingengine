/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Buffer.h"

namespace opengl
{
    /*
    OpenGL atomic counter buffer.
    The buffer can contain multiple atomic counters.
    Example of several atomic counters definition in a shader:
    layout (binding = 0, offset = 0) uniform atomic_uint my_1st_counter;
    layout (binding = 0, offset = 4) uniform atomic_uint my_2nd_counter;
    layout (binding = 0, offset = 8) uniform atomic_uint my_3rd_counter;
    */
    class AtomicCounterBuffer : public Buffer
    {
        #pragma region Constructors

        public:
            AtomicCounterBuffer(GLsizeiptr numberOfCounters) :
                Buffer(
                    numberOfCounters * sizeof(GLuint),
                    TARGET::ATOMIC_COUNTER,
                    DRAW_USAGE::DYNAMIC_COPY,
                    NULL)
            {
                Clear(0);
            }

            // Move constructor.
            AtomicCounterBuffer(AtomicCounterBuffer&& value) noexcept :
                Buffer(std::move(value))
            {}

        #pragma endregion

        #pragma region Methods

        public:
            void BindToAtomicCounterBindingPoint(
                GLuint index)
            {
                glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, index, getHandle());
            }

            /*
            Gets value of the specified atomic counter.
            Parameters:
            counterIndex - index of the atomic counter whoose value to retrieve.
            */
            GLuint getValue(
                int counterIndex)
            {
                util::Requires::ArgumentNotNegative(counterIndex, "counterIndex", FUNCTION_INFO);

                GLuint value = 0;

                getData(
                    counterIndex * sizeof(GLuint),
                    sizeof(GLuint),
                    &value);
            }

            /*
            Sets value of the specified atomic counter.
            Parameters:
            counterIndex - index of the atomic counter whoose value to set.
            */
            void setValue(
                int counterIndex,
                GLuint value)
            {
                util::Requires::ArgumentNotNegative(counterIndex, "counterIndex", FUNCTION_INFO);

                setData(
                    counterIndex * sizeof(GLuint),
                    sizeof(GLuint),
                    &value);
            }

        #pragma endregion
    };
}