/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Requires.h"
#include "GlException.h"
#include "OpenGLUtility.h"
#include "Event.h"

namespace opengl
{
    class OpenGLWindow final
    {
        #pragma region Typedefs

        public:
            using RENDER_FUNC_DELEGATE = util::Event<OpenGLWindow*>;

        #pragma endregion

        #pragma region Fields

        private:
            HDC m_DeviceContext;
            HGLRC m_hGLRC; // OpenGL rendering context handle
            RENDER_FUNC_DELEGATE m_RenderEvent;

        #pragma endregion

        #pragma region Constructor

        public:
            /*
            Constructor
            Parameters:
            window - window object for which this OpenGL window is giong to be established
            opengl_major_version - OpenGL major version
            opengl_minor_version - OpenGL minor version
            */
            OpenGLWindow(
                HDC deviceContext,
                GLint opengl_major_version,
                GLint opengl_minor_version) :
                m_DeviceContext(deviceContext),
                m_hGLRC(nullptr),
                m_RenderEvent()
            {
                util::Requires::ArgumentNotNegative(opengl_major_version, "opengl_major_version", FUNCTION_INFO);
                util::Requires::ArgumentNotNegative(opengl_minor_version, "opengl_minor_version", FUNCTION_INFO);

                SetPixelFormat(deviceContext);
                CreateRenderingContext(deviceContext, opengl_major_version, opengl_minor_version);
            }

        #pragma endregion

        #pragma region Destructor

        public:
            ~OpenGLWindow()
            {
                // Remove the rendering context from our device context.
                wglMakeCurrent(m_DeviceContext, 0);
                // Delete our rendering context.
                wglDeleteContext(m_hGLRC);
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            OpenGLWindow(const OpenGLWindow&) = delete;
            OpenGLWindow& operator=(const OpenGLWindow&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            // Gets the OpenGL rendering context.
            HGLRC getOpenGLRenderingContext() { return m_hGLRC; }

        #pragma endregion

        #pragma region Events

        public:
            RENDER_FUNC_DELEGATE& RenderEvent() { return m_RenderEvent; }

        #pragma endregion

        #pragma region Methods

        public:
            // Renders window content.
            void Render()
            {
                wglMakeCurrent(m_DeviceContext, m_hGLRC);
                ClearAllBuffers();
                m_RenderEvent(this); // generate RenderEvent
                SwapBuffers(m_DeviceContext);
            }

        private:
            void SetPixelFormat(HDC deviceContext);

            void CreateRenderingContext(
                HDC deviceContext,
                GLint majorVersion,
                GLint minorVersion);

        #pragma endregion
    };
}