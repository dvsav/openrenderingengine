/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"
#include "TextureCubemap.h"

namespace opengl
{
    class TextureCubemapArray : public Texture
    {
        #pragma region Fields

        private:
            GLsizei m_LayersNumber;

        #pragma endregion

        #pragma region Constructors

        public:
            TextureCubemapArray(
                GLint mipmap_levels_number,
                INTERNAL_FORMAT internalFormat,
                GLsizei width,
                GLsizei height,
                PIXEL_FORMAT format,
                TYPE type,
                GLsizei layersNumber) :

                Texture(
                    TARGET::TEXTURE_CUBE_MAP_ARRAY,
                    internalFormat,
                    format,
                    type,
                    mipmap_levels_number,
                    width,
                    height),

                m_LayersNumber(layersNumber)
            {
                util::Requires::ArgumentPositive(layersNumber, "layersNumber", FUNCTION_INFO);

                Binding binding(*this);

#ifdef OPENGL_4
                glTexStorage3D(
                    static_cast<GLenum>(getTarget()),
                    mipmap_levels_number,
                    static_cast<GLint>(internalFormat),
                    width,
                    height,
                    layersNumber * 6);
#else
                for (int mipmap_level = 0; mipmap_level < mipmap_levels_number; mipmap_level++)
                {
                    glTexImage3D(
                        /*target*/ static_cast<GLenum>(getTarget()),
                        /*level*/  mipmap_level,
                        /*internalformat*/ static_cast<GLint>(internalFormat),
                        /*width*/  max(1, width / (1 << mipmap_level)),
                        /*height*/ max(1, height / (1 << mipmap_level)),
                        /*depth*/  layersNumber * 6,
                        /*border*/ 0,
                        /*format*/ static_cast<GLenum>(format),
                        /*type*/   static_cast<GLenum>(type),
                        /*data*/   nullptr);
                }
#endif
            }

            // Move constructor.
            TextureCubemapArray(TextureCubemapArray&& value) noexcept :
                Texture(std::move(value)),
                m_LayersNumber(value.m_LayersNumber)
            {
                value.m_LayersNumber = 0;
            }

        #pragma endregion

        #pragma region Properties

        public:
            GLint getLayersNumber() const { return m_LayersNumber; }

        #pragma endregion

        #pragma region Methods

        public:
            // Sets texture image.
            void setImage(
                TextureCubemap::CUBE_FACE face,
                GLint mipmap_level,
                GLsizei layer,
                const GLvoid* data)
            {
                util::Requires::ArgumentNotNegative(layer, "layer", FUNCTION_INFO);
                util::Requires::That(layer < getLayersNumber(), FUNCTION_INFO);
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);

                Binding binding(*this);

                glTexSubImage3D(
                    /*target*/  static_cast<GLenum>(face),
                    /*level*/   mipmap_level,
                    /*xoffset*/ 0,
                    /*yoffset*/ 0,
                    /*zoffset*/ layer * 6 + ((GLsizei)face - (GLsizei)opengl::TextureCubemap::CUBE_FACE::POSITIVE_X),
                    /*width*/   std::max(1, (getWidth() / (1 << mipmap_level))),
                    /*height*/  std::max(1, (getHeight() / (1 << mipmap_level))),
                    /*depth*/   1,
                    /*format*/  static_cast<GLenum>(getPixelFormat()),
                    /*type*/    static_cast<GLenum>(getType()),
                    /*pixels*/  data);
            }

            // Sets texture subimage.
            void setSubImage(
                TextureCubemap::CUBE_FACE face,
                GLint mipmap_level,
                GLsizei layer,
                GLint xoffset,
                GLint yoffset,
                GLsizei width,
                GLsizei height,
                const GLvoid* data)
            {
                util::Requires::ArgumentNotNegative(layer, "layer", FUNCTION_INFO);
                util::Requires::That(layer < getLayersNumber(), FUNCTION_INFO);
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);
                util::Requires::ArgumentPositive(xoffset, "xoffset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(yoffset, "yoffset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(width, "width", FUNCTION_INFO);
                util::Requires::ArgumentPositive(height, "height", FUNCTION_INFO);
                util::Requires::That(width + xoffset <= std::max(1, (getWidth() / (1 << mipmap_level))), FUNCTION_INFO);
                util::Requires::That(height + yoffset <= std::max(1, (getHeight() / (1 << mipmap_level))), FUNCTION_INFO);

                Binding binding(*this);

                glTexSubImage3D(
                    /*target*/  static_cast<GLenum>(face),
                    /*level*/   mipmap_level,
                    /*xoffset*/ xoffset,
                    /*yoffset*/ yoffset,
                    /*zoffset*/ layer * 6 + ((GLsizei)face - (GLsizei)opengl::TextureCubemap::CUBE_FACE::POSITIVE_X),
                    /*width*/   std::max(1, (getWidth() / (1 << mipmap_level))),
                    /*height*/  std::max(1, (getHeight() / (1 << mipmap_level))),
                    /*depth*/   1,
                    /*format*/  static_cast<GLenum>(getPixelFormat()),
                    /*type*/    static_cast<GLenum>(getType()),
                    /*pixels*/  data);
            }

#ifdef OPENGL_4_5
            // Gets texture image.
            void getSubImage(
                TextureCubemap::CUBE_FACE face,
                GLint mipmapLevel,
                GLsizei layer,
                GLvoid* buffer,
                GLsizei bufSize)
            {
                util::Requires::ArgumentNotNegative(layer, "layer", FUNCTION_INFO);
                util::Requires::That(layer < getLayersNumber(), FUNCTION_INFO);
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);

                glGetTextureSubImage(
                    /*texture*/ getHandle(),
                    /*level*/   mipmapLevel,
                    /*xoffset*/ 0,
                    /*yoffset*/ 0,
                    /*zoffset*/ layer * 6 + ((GLsizei)face - (GLsizei)opengl::TextureCubemap::CUBE_FACE::POSITIVE_X),
                    /*width*/   std::max(1, (getWidth() / (1 << mipmapLevel))),
                    /*height*/  std::max(1, (getHeight() / (1 << mipmapLevel))),
                    /*depth*/   1,
                    /*format*/  static_cast<GLenum>(getPixelFormat()),
                    /*type*/    static_cast<GLenum>(getType()),
                    /*bufSize*/ bufSize,
                    /*pixels*/  buffer);
            }
#endif

        #pragma endregion
    };
}