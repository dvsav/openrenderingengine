/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "MeshIndexed.h"

namespace engine
{
    template<typename TVertex>
    class MeshSphere : public MeshIndexed
    {
        static_assert(std::is_base_of<typename VertexPN, TVertex>::value, "Unsupported vertex type");

        #pragma region Fields

        private:
            GLuint m_AngularResolution;

        #pragma endregion

        #pragma region Constructors

        public:
            /*
            Creates a sphere mesh.
            Parameters:
            [radius] - sphere radius
            [angular_resolution] - the number of points in azimuthal and altitudinal directions. The total number of vertices in the sphere mesh equals [angular_resolution]^2.
            */
            MeshSphere(
                GLfloat radius,
                GLuint angular_resolution) :

                MeshIndexed(
                    /*vertexBuffer*/ opengl::VertexBuffer<TVertex>(makeSphereMesh(radius, angular_resolution, vec3(1.0f)), opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
                    /*indexBuffer*/ makeIndices(angular_resolution),
                    /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLE_STRIP),

                m_AngularResolution(angular_resolution)
            { }

            /*
            Creates a sphere mesh.
            Parameters:
            [radius] - sphere radius
            [angular_resolution] - the number of points in azimuthal and altitudinal directions. The total number of vertices in the sphere mesh equals [angular_resolution]^2.
            [scale] - coefficient all vertices are multiplied by
            */
            MeshSphere(
                GLfloat radius,
                GLuint angular_resolution,
                const vec3& scale) :

                MeshIndexed(
                    /*vertexBuffer*/ opengl::VertexBuffer<TVertex>(makeSphereMesh(radius, angular_resolution, scale), opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
                    /*indexBuffer*/ makeIndices(angular_resolution),
                    /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLE_STRIP),

                m_AngularResolution(angular_resolution)
            { }

            // Move constructor.
            MeshSphere(MeshSphere&& value) noexcept :
                MeshIndexed(std::move(value)),
                m_AngularResolution(value.m_AngularResolution)
            { }

        #pragma endregion

        #pragma region Methods

        public:
            static std::vector<TVertex> makeSphereMesh(
                GLfloat radius,
                GLuint angular_resolution,
                const vec3& scale);

            static std::vector<GLuint> makeIndices(
                GLuint angular_resolution);

        public:
            // Renders the mesh.
            void Render() override;

            /*
            Renders insatance_count instances of the mesh.
            Parameters:
            instance_count - the number of instances of the specified range of indices to be rendered.
            base_instance - the base instance for use in fetching instanced vertex attributes.
            */
            void Render(
                GLsizei instance_count,
                GLuint base_instance = 0) override;

        #pragma endregion
    };
}