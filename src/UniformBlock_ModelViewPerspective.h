/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <malloc.h>

#include "UniformBlockBuffer.h"

#include "EngineTypedefs.h"

namespace engine
{
    /*
    Represents a uniform block storing a single transformation matrix for the whole set of transformations
    (model-to-world transformation, world-to-camera transformation and perspective-projection transformation)
    with the following layout in a shader:
    layout (std140) uniform TransformBlock
    {
        mat4 m4_model_world_camera_projection;
    }
    */
    class UniformBlock_ModelViewPerspective : public opengl::UniformBlockStructure<mat4>
    {
        #pragma region Fields

        private:
            mat4 modelToWorldMatrix;
            mat4 worldToCameraMatrix;
            mat4 perspectiveProjectionMatrix;

        #pragma endregion

        #pragma region Constructors

        public:
            // Creates a uniform block storing single transformation matrix for the whole set of transformations (model->world->camera->screen).
            UniformBlock_ModelViewPerspective() :

                UniformBlockStructure<mat4>(
                    vmath::make_IdentityMatrix<GLfloat, 4>(),
                    opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW),

                modelToWorldMatrix(vmath::make_IdentityMatrix4x4<GLfloat>()),
                worldToCameraMatrix(vmath::make_IdentityMatrix4x4<GLfloat>()),
                perspectiveProjectionMatrix(vmath::make_IdentityMatrix4x4<GLfloat>())
            { }

            // Move constructor.
            UniformBlock_ModelViewPerspective(UniformBlock_ModelViewPerspective&& value) :

                opengl::UniformBlockStructure<mat4>(std::move(value)),

                modelToWorldMatrix(std::move(value.modelToWorldMatrix)),
                worldToCameraMatrix(std::move(value.worldToCameraMatrix)),
                perspectiveProjectionMatrix(std::move(value.perspectiveProjectionMatrix))
            { }

        #pragma endregion

        #pragma region Properties

        public:
            // Sets the model-to-world transformation matrix.
            void setModelToWorld(const engine::mat4& value)
            {
                modelToWorldMatrix = value;
                RefreshBuffer();
            }

            // Gets the model-to-world transformation matrix.
            engine::mat4 getModelToWorld() const
            {
                return modelToWorldMatrix;
            }

            // Sets the world-to-camera transformation matrix.
            void setWorldToCamera(const engine::mat4& value)
            {
                worldToCameraMatrix = value;
                RefreshBuffer();
            }

            // Gets the world-to-camera transformation matrix.
            engine::mat4 getWorldToCamera() const
            {
                return worldToCameraMatrix;
            }

            // Sets the perspective-projection transformation matrix.
            void setPerspectiveProjection(const engine::mat4& value)
            {
                perspectiveProjectionMatrix = value;
                RefreshBuffer();
            }

            // Gets the perspective-projection transformation matrix.
            engine::mat4 getPerspectiveProjection() const
            {
                return perspectiveProjectionMatrix;
            }

        #pragma endregion

        #pragma region Methods

        private:
            void RefreshBuffer()
            {
                this->setData(
                    0,
                    sizeof(value_type),
                    (perspectiveProjectionMatrix * (worldToCameraMatrix * modelToWorldMatrix)).data());
            }

        #pragma endregion
    };
}
