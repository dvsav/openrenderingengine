/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include "TextureSampler.h"

namespace opengl
{
    void TextureSampler::setWrapMode(
        Texture::TEXTURE_COORDINATE coord,
        Texture::WRAP_MODE mode)
    {
        switch (coord)
        {
            case Texture::TEXTURE_COORDINATE::S:
                glSamplerParameteri(m_Handle, GL_TEXTURE_WRAP_S, static_cast<GLint>(mode));
                break;
            case Texture::TEXTURE_COORDINATE::T:
                glSamplerParameteri(m_Handle, GL_TEXTURE_WRAP_T, static_cast<GLint>(mode));
                break;
            case Texture::TEXTURE_COORDINATE::R:
                glSamplerParameteri(m_Handle, GL_TEXTURE_WRAP_R, static_cast<GLint>(mode));
                break;
            default:
                throw std::runtime_error("opengl::TextureSampler::setWrapMode()");
                break;
        }
    }
}