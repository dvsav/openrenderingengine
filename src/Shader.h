/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <string>
#include "GLEWHeaders.h"
#include "TextPreprocessor.h"

namespace opengl
{
    /*
    Shader is part of program executed by the GPU. Shader is written by a programmer in GLSL language.
    There are several types of shaders most important of which are vertex and fragment.
    Several shaders of different types are compiled and linked to make up a program.
    */
    class Shader final
    {
        #pragma region Netsed Types

        public:
            enum class SHADER_TYPE : GLenum
            {
                VERTEX = GL_VERTEX_SHADER,
                FRAGMENT = GL_FRAGMENT_SHADER,
                TESS_CONTROL = GL_TESS_CONTROL_SHADER,
                TESS_EVALUATION = GL_TESS_EVALUATION_SHADER,
                GEOMETRY = GL_GEOMETRY_SHADER,
                COMPUTE = GL_COMPUTE_SHADER
            };

            static SHADER_TYPE parse_SHADER_TYPE(const char* str)
            {
                if (std::strcmp(str, "VERTEX") == 0)
                    return SHADER_TYPE::VERTEX;
                else if (std::strcmp(str, "FRAGMENT") == 0)
                    return SHADER_TYPE::FRAGMENT;
                else if (std::strcmp(str, "TESS_CONTROL") == 0)
                    return SHADER_TYPE::TESS_CONTROL;
                else if (std::strcmp(str, "TESS_EVALUATION") == 0)
                    return SHADER_TYPE::TESS_EVALUATION;
                else if (std::strcmp(str, "GEOMETRY") == 0)
                    return SHADER_TYPE::GEOMETRY;
                else if (std::strcmp(str, "COMPUTE") == 0)
                    return SHADER_TYPE::COMPUTE;
                else
                    throw std::invalid_argument(str);
            }

        #pragma endregion

        #pragma region Fields

        private:
            GLuint m_Handle;
            SHADER_TYPE m_ShaderType;

        #pragma endregion

        #pragma region Constructors

        public:
            Shader(SHADER_TYPE shader_type,
                   const std::string& shaderText);

            // Move constructor.
            Shader(Shader&& shader) noexcept :
                m_Handle(shader.m_Handle),
                m_ShaderType(shader.m_ShaderType)
            {
                shader.m_Handle = 0;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            ~Shader()
            {
                glDeleteShader(m_Handle);
                m_Handle = 0;
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            Shader(const Shader&) = delete;
            Shader& operator=(const Shader&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            const SHADER_TYPE getShaderType() const
            {
                return m_ShaderType;
            }

            const GLuint getHandle() const
            {
                return m_Handle;
            }

        #pragma endregion
    };

    // Makes a shader object out of the specified text file.
    inline Shader make_ShaderFromFile(
        Shader::SHADER_TYPE shader_type,
        const std::string& fileName)
    {
        std::string shaderText = util::TextPreprocessor::default().ReadAllText(fileName);
        return Shader(shader_type, shaderText);
    }

    // Makes a shader object out of the specified text file.
    inline Shader make_ShaderFromFile(
        Shader::SHADER_TYPE shader_type,
        std::stringstream& os,
        const std::string& fileName)
    {
        return Shader(
            shader_type,
            util::TextPreprocessor::default().ReadAllText(os, fileName).str());
    }
}