/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include "Buffer.h"

namespace opengl
{
    GLuint Buffer::getCurrentlyBoundBufferHandle(TARGET target)
    {
        GLint current_buffer;

        switch (target)
        {
            case TARGET::ARRAY:
                glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::ATOMIC_COUNTER:
                glGetIntegerv(GL_ATOMIC_COUNTER_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::DISPATCH_INDIRECT:
                glGetIntegerv(GL_DISPATCH_INDIRECT_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::DRAW_INDIRECT:
                glGetIntegerv(GL_DRAW_INDIRECT_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::QUERY:
                glGetIntegerv(GL_QUERY_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::SHADER_STORAGE:
                glGetIntegerv(GL_SHADER_STORAGE_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::ELEMENT_ARRAY:
                glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::PIXEL_PACK:
                glGetIntegerv(GL_PIXEL_PACK_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::PIXEL_UNPACK:
                glGetIntegerv(GL_PIXEL_UNPACK_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::COPY_READ:
                glGetIntegerv(GL_COPY_READ_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::COPY_WRITE:
                glGetIntegerv(GL_COPY_WRITE_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::TRANSFORM_FEEDBACK:
                glGetIntegerv(GL_TRANSFORM_FEEDBACK_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::UNIFORM:
                glGetIntegerv(GL_UNIFORM_BUFFER_BINDING, &current_buffer);
                break;
            case TARGET::TEXTURE:
                glGetIntegerv(GL_TEXTURE_BUFFER_BINDING, &current_buffer);
                break;
            default:
                throw std::runtime_error("opengl::Buffer.getCurrentlyBoundBufferHandle()");
        }

        return current_buffer;
    }
}