/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include <stdexcept>

#include "Image.h"

#include "Requires.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

namespace util
{
    Image::Image(
        const std::string& filename,
        unsigned int requiredChannels) :
        m_Data(nullptr),
        m_Width(0),
        m_Height(0),
        m_BitDepth(0)
    {
        util::Requires::That(requiredChannels <= 4, FUNCTION_INFO);

        m_Data = ::stbi_load(filename.c_str(), &m_Width, &m_Height, &m_BitDepth, requiredChannels);
        if (!m_Data)
            throw std::runtime_error("Image::Image() -> Cannot read file: " + filename);
    }

    Image::~Image() noexcept
    {
        ::stbi_image_free(m_Data);
    }
}
