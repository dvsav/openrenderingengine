/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <memory>

namespace win
{
    std::unique_ptr<char[]> ReadBMP24File(
        const std::string& filename,
        int& width,
        int& height);

    void WriteBMP24File(
        const std::string& filename,
        int width,
        int height,
        char* pixels);
}