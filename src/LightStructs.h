/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include <type_traits>

#include "EngineTypedefs.h"

namespace engine
{
    struct AmbientLightStruct
    {
        color_rgba32f LightColor;

        using TLightColor = decltype(LightColor);
    };

    struct DirectionalLightStruct
    {
        color_rgba32f LightColor;
        vec4 LightDirection;
        mat4 WorldToLightProjectionPlane;
        mat4 WorldToShadowMap; // bias * WorldToLightProjectionPlane

        using TLightColor = decltype(LightColor);
        using TLightDirection = decltype(LightDirection);
        using TWorldToLightProjectionPlane = decltype(WorldToLightProjectionPlane);
        using TWorldToShadowMap = decltype(WorldToShadowMap);
    };

    struct AttenuationStruct
    {
        GLfloat Range;
        GLfloat Constant;
        GLfloat Linear;
        GLfloat Quadratic;
    };

    inline AttenuationStruct make_AttenuationStruct_Range7() { return AttenuationStruct{ 7.0f, 1.0f, 0.7f, 1.8f }; }
    inline AttenuationStruct make_AttenuationStruct_Range13() { return AttenuationStruct{ 13.0f, 1.0f, 0.35f, 0.44f }; }
    inline AttenuationStruct make_AttenuationStruct_Range20() { return AttenuationStruct{ 20.0f, 1.0f, 0.22f, 0.20f }; }
    inline AttenuationStruct make_AttenuationStruct_Range32() { return AttenuationStruct{ 32.0f, 1.0f, 0.14f, 0.07f }; }
    inline AttenuationStruct make_AttenuationStruct_Range50() { return AttenuationStruct{ 50.0f, 1.0f, 0.09f, 0.032f }; }
    inline AttenuationStruct make_AttenuationStruct_Range65() { return AttenuationStruct{ 65.0f, 1.0f, 0.07f, 0.017f }; }
    inline AttenuationStruct make_AttenuationStruct_Range100() { return AttenuationStruct{ 100.0f, 1.0f, 0.045f, 0.0075f }; }
    inline AttenuationStruct make_AttenuationStruct_Range160() { return AttenuationStruct{ 160.0f, 1.0f, 0.027f, 0.0028f }; }
    inline AttenuationStruct make_AttenuationStruct_Range200() { return AttenuationStruct{ 200.0f, 1.0f, 0.022f, 0.0019f }; }
    inline AttenuationStruct make_AttenuationStruct_Range325() { return AttenuationStruct{ 325.0f, 1.0f, 0.014f, 0.0007f }; }
    inline AttenuationStruct make_AttenuationStruct_Range600() { return AttenuationStruct{ 600.0f, 1.0f, 0.007f, 0.0002f }; }
    inline AttenuationStruct make_AttenuationStruct_Range3250() { return AttenuationStruct{ 3250.0f, 1.0f, 0.0014f, 0.000007f }; }

    struct PointLightStruct
    {
        color_rgba32f LightColor;
        vec4 LightPosition;
        mat4 CameraToScreen;
        /*
        Face Value	Resulting Target
        0 GL_TEXTURE_CUBEMAP_POSITIVE_X
        1 GL_TEXTURE_CUBEMAP_NEGATIVE_X
        2 GL_TEXTURE_CUBEMAP_POSITIVE_Y
        3 GL_TEXTURE_CUBEMAP_NEGATIVE_Y
        4 GL_TEXTURE_CUBEMAP_POSITIVE_Z
        5 GL_TEXTURE_CUBEMAP_NEGATIVE_Z
        */
        mat4 LightMatrices[6];
        AttenuationStruct Attenuation;

        using TLightColor = decltype(LightColor);
        using TLightPosition = decltype(LightPosition);
        using TCameraToScreen = decltype(CameraToScreen);
        using TLightMatrix = std::remove_reference<decltype(LightMatrices[0])>::type;
        using TAttenuation = decltype(Attenuation);
    };

    struct SpotLightStruct
    {
        color_rgba32f LightColor;
        vec4 LightPosition;
        vec4 LightDirection;
        mat4 WorldToLightProjectionPlane;
        mat4 WorldToShadowMap; // bias * WorldToLightProjectionPlane
        AttenuationStruct Attenuation;
        GLfloat EdgeCosine;

        using TLightColor = decltype(LightColor);
        using TLightPosition = decltype(LightPosition);
        using TLightDirection = decltype(LightDirection);
        using TWorldToLightProjectionPlane = decltype(WorldToLightProjectionPlane);
        using TWorldToShadowMap = decltype(WorldToShadowMap);
        using TAttenuation = decltype(Attenuation);
        using TEdgeCosine = decltype(EdgeCosine);
    };
}