/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "UniformBlock_ModelToWorld.h"
#include "UniformBlock_ModelViewPerspective.h"
#include "UniformBlock_Material.h"
#include "UniformBlock_AmbientLight.h"
#include "UniformBlock_DirectionalLight.h"
#include "UniformBlock_PointLight.h"
#include "UniformBlock_SpotLight.h"
#include "UniformBlock_CameraInfo.h"

namespace engine
{
    // Uniform block binding points
    enum class UniformBlockEnum
    {
        ModelToWorld,
        ModelViewPerspective,
        Material,
        AmbientLight,
        CameraInfo,
        PointLightArray,
        DirectionalLightArray,
        SpotLightArray
    };

    inline UniformBlockEnum parse_UniformBlockEnum(const char* str)
    {
        if (std::strcmp(str, "ModelToWorld") == 0)
            return UniformBlockEnum::ModelToWorld;
        else if (std::strcmp(str, "ModelViewPerspective") == 0)
            return UniformBlockEnum::ModelViewPerspective;
        else if (std::strcmp(str, "CameraInfo") == 0)
            return UniformBlockEnum::CameraInfo;
        else if (std::strcmp(str, "Material") == 0)
            return UniformBlockEnum::Material;
        else if (std::strcmp(str, "AmbientLight") == 0)
            return UniformBlockEnum::AmbientLight;
        else if (std::strcmp(str, "PointLightArray") == 0)
            return UniformBlockEnum::PointLightArray;
        else if (std::strcmp(str, "SpotLightArray") == 0)
            return UniformBlockEnum::SpotLightArray;
        else if (std::strcmp(str, "DirectionalLightArray") == 0)
            return UniformBlockEnum::DirectionalLightArray;
        else
            throw std::invalid_argument(str);
    }

    template<UniformBlockEnum U>
    struct uniform_block_traits { };

    template<>
    struct uniform_block_traits<UniformBlockEnum::ModelToWorld>
    {
        using block_type = UniformBlock_ModelToWorld;
    };

    template<>
    struct uniform_block_traits<UniformBlockEnum::ModelViewPerspective>
    {
        using block_type = UniformBlock_ModelViewPerspective;
    };

    template<>
    struct uniform_block_traits<UniformBlockEnum::Material>
    {
        using block_type = UniformBlock_Material;
    };

    template<>
    struct uniform_block_traits<UniformBlockEnum::AmbientLight>
    {
        using block_type = UniformBlock_AmbientLight;
    };

    template<>
    struct uniform_block_traits<UniformBlockEnum::DirectionalLightArray>
    {
        using block_type = UniformBlock_DirectionalLightArray;
    };

    template<>
    struct uniform_block_traits<UniformBlockEnum::PointLightArray>
    {
        using block_type = UniformBlock_PointLightArray;
    };

    template<>
    struct uniform_block_traits<UniformBlockEnum::SpotLightArray>
    {
        using block_type = UniformBlock_SpotLightArray;
    };

    template<>
    struct uniform_block_traits<UniformBlockEnum::CameraInfo>
    {
        using block_type = UniformBlock_CameraInfo;
    };
}
