/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "MeshBase.h"

namespace engine
{
    class MeshArray : public MeshBase
    {
        #pragma region Constructors

        public:
            template<typename TVertex>
            MeshArray(
                opengl::VertexBuffer<TVertex>&& vertexBuffer,
                opengl::DRAWING_MODE drawing_mode) :

                MeshBase(std::move(vertexBuffer), drawing_mode)
            { }

            // Move constructor.
            MeshArray(MeshArray&& value) noexcept :
                MeshBase(std::move(value))
            { }

        #pragma endregion

        #pragma region Methods

        public:
            void Render() override
            {
                glDrawArrays(
                    /*mode*/  m_DrawingMode,
                    /*first*/ 0,
                    /*count*/ m_VertexBuffer.getVertexCount());
            }

            void Render(
                GLsizei instance_count,
                GLuint base_instance = 0) override
            {
                glDrawArraysInstancedBaseInstance(
                    /*mode*/  m_DrawingMode,
                    /*first*/ 0,
                    /*count*/ m_VertexBuffer.getVertexCount(),
                    /*primcount*/ instance_count,
                    /*baseinstance*/ base_instance);
            }

        #pragma endregion
    };

    inline MeshArray make_TriangleMeshPN(
        const vec3& v1,
        const vec3& v2,
        const vec3& v3)
    {
        auto normal = vmath::cross_product(v2 - v1, v3 - v1);

        return MeshArray(
            /*vertexBuffer*/
            opengl::VertexBuffer<VertexPN>(std::array<VertexPN, 3>
                {
                    VertexPN{ v1, normal },
                    VertexPN{ v2, normal },
                    VertexPN{ v3, normal }
                },
                opengl::Buffer::DRAW_USAGE::STATIC_DRAW),

            /*drawing_mode*/
            opengl::DRAWING_MODE::TRIANGLES
        );
    }
}