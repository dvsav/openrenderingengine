/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"
#include "ProgramBase.h"
#include "Game.h"

class Program : public engine::ProgramBase
{
    #pragma region Fields

    private:
        std::unique_ptr<Game> m_Game;

    #pragma endregion

    #pragma region Constructors

    public:
        Program() :
            m_Game(new Game(*this))
        {
            setupCamera(getCamera());
            getWindow().KeyDownEvent() += fastdelegate::MakeDelegate(this, &Program::OnKeyDown);
        }

    #pragma endregion

    #pragma region Methods

    private:
        void OnKeyDown(std::tuple<win::Window*, WPARAM> eventArgs)
        {
            auto key = std::get<1>(eventArgs);

            if (key == VK_F5)
            {
                m_Game.reset();
                engine::clear_ProgramCache();
                m_Game = std::unique_ptr<Game>(new Game(*this));
            }
        }

    protected:
        void UpdateTime() override
        {
            m_Game->UpdateTime();
        }

        void setupCamera(const engine::Camera& cam) override
        {
            m_Game->SetupCamera(cam);
        }

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            m_Game->Render();
        }

    #pragma endregion
};
