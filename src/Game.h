/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"
#include "ProgramBase.h"
#include "pugixml_loader.h"

class Game
{
    #pragma region Fields

    private:
        engine::ProgramBase& m_Program;
        engine::Model3d_Skybox m_Skybox;
        engine::Scene m_Scene;

    #pragma endregion

    #pragma region Constructors

    public:
        Game(engine::ProgramBase& program) :
            m_Program(program),
            m_Skybox(engine::make_Skybox(engine::load_Texture("assets/textures/milkyway/texture_milkyway.xml"))),
            m_Scene(engine::load_Scene("assets/scenes/scene.xml"))
        {
            //::ShowCursor(false);
        }

    #pragma endregion

    #pragma region Methods

    public:
        void UpdateTime()
        {
        }

        void SetupCamera(const engine::Camera& cam)
        {
            auto& orientation = cam.PositionAndOrientation();

            m_Scene.SpotLights()->LightView(0).PositionAndOrientation().Position() =
                 orientation.ModelToWorldMatrix() * engine::vec4{ 0.0f, 0.0f, 0.0f, 1.0f };
            
            m_Scene.SpotLights()->LightView(0).PositionAndOrientation().setForwardDirection(orientation.getForwardDirection().vec3());
            
            m_Skybox.SetupCamera(cam);
            m_Scene.Camera()->SetupCamera(cam);
        }

        void Render()
        {
            m_Scene.Render();
            m_Skybox.Render();
        }

    #pragma endregion
};
