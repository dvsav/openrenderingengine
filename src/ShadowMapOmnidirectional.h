/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ShadowMap.h"
#include "TextureCubemap.h"
#include "TextureCubemapArray.h"

namespace engine
{
    class ShadowMapOmnidirectional : public ShadowMap
    {
        #pragma region Constructors

        public:
            ShadowMapOmnidirectional(
                GLsizei width_height) :
                ShadowMap(
                    width_height,
                    width_height,
                    std::make_shared<opengl::TextureCubemap>(
                        /*mipmap_levels_number*/ 1,
                        /*internalFormat*/ opengl::INTERNAL_FORMAT::DEPTH_COMPONENT24,
                        /*width*/ width_height,
                        /*height*/ width_height,
                        /*format*/ opengl::PIXEL_FORMAT::DEPTH_COMPONENT,
                        /*type*/ opengl::TYPE::FLOAT))
            { }

            ShadowMapOmnidirectional(
                GLsizei width_height,
                std::shared_ptr<opengl::TextureCubemapArray> depthTexture) :
                ShadowMap(
                    width_height,
                    width_height,
                    depthTexture)
            { }

        #pragma endregion
    };
}