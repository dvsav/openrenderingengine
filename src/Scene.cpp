#include "Scene.h"
#include "StandardMetaprograms.h"

namespace engine
{
    Scene::Scene(
        size_t nDirLights,
        size_t nPointLights,
        size_t nSpotLights) :

        m_ShadowRender(standard::ShadowRenderProgram()),
        m_UniformBlock_Camera(std::make_shared<UniformBlock_CameraInfo>()),
        m_AmbientLight(std::make_shared<AmbientLight>()),
        m_DirectionalLights(std::make_shared<DirectionalLightArray>(nDirLights, SHADOW_WIDTH, SHADOW_WIDTH, m_ShadowRender)),
        m_PointLights(std::make_shared<PointLightArray>(nPointLights, SHADOW_WIDTH, m_ShadowRender)),
        m_SpotLights(std::make_shared<SpotLightArray>(nSpotLights, SHADOW_WIDTH, SHADOW_WIDTH, m_ShadowRender)),
        m_ShadowCastingModels(),
        m_NonShadowCastingModels()
    { }

    Model3d_Skybox make_Skybox(
        const std::shared_ptr<opengl::Texture>& texture)
    {
        return engine::Model3d_Skybox(
            /*program*/ standard::SkyboxProgram(),
            /*mesh*/ std::make_shared<engine::MeshIndexed>(engine::makeCubeP_FacingIncide(10.0f)),
            /*texture*/ texture);
    }
}
