#include "ConditionalCompilation.h"

#include "OpenGLProgram.h"

namespace opengl
{
#ifdef OPENGL_4

    std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> Program::getVertexInputAttributes() const
    {
        GLint numAttribs;
        glGetProgramInterfaceiv(
            /*program*/ m_Handle,
            /*programInterface*/ GL_PROGRAM_INPUT,
            /*pname*/ GL_ACTIVE_RESOURCES,
            /*params*/ &numAttribs);

        GLenum properties[]
        {
            GL_LOCATION,
            GL_TYPE,
            GL_NAME_LENGTH
        };

        std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> attribs;

        for (int i = 0; i < numAttribs; ++i)
        {
            GLint results[3];

            glGetProgramResourceiv(
                /*program*/ m_Handle,
                /*programInterface*/ GL_PROGRAM_INPUT,
                /*index*/ i,
                /*propCount*/ sizeof(properties) / sizeof(GLenum),
                /*props*/ properties,
                /*bufSize*/ sizeof(results) / sizeof(GLint),
                /*length*/ NULL,
                /*params*/ results);

            GLint nameBufSize = results[2] + 1;
            char* name = new char[nameBufSize];
            glGetProgramResourceName(
                /*program*/ m_Handle,
                /*programInterface*/ GL_PROGRAM_INPUT,
                /*index*/ i,
                /*bufSize*/ nameBufSize,
                /*length*/ NULL,
                /*name*/ name);

            attribs.emplace_back(
                results[0],
                static_cast<VECTOR_TYPE>(results[1]),
                std::string(name)
            );

            delete name;
        }

        return attribs;
    }

    std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> Program::getUniforms() const
    {
        GLint numUniforms;
        glGetProgramInterfaceiv(
            /*program*/ m_Handle,
            /*programInterface*/ GL_UNIFORM,
            /*pname*/ GL_ACTIVE_RESOURCES,
            /*params*/ &numUniforms);

        GLenum properties[]
        {
            GL_LOCATION,
            GL_TYPE,
            GL_NAME_LENGTH,
            GL_BLOCK_INDEX
        };

        std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> uniforms;

        for (int i = 0; i < numUniforms; ++i)
        {
            GLint results[4];

            glGetProgramResourceiv(
                /*program*/ m_Handle,
                /*programInterface*/ GL_UNIFORM,
                /*index*/ i,
                /*propCount*/ sizeof(properties) / sizeof(GLenum),
                /*props*/ properties,
                /*bufSize*/ sizeof(results) / sizeof(GLint),
                /*length*/ NULL,
                /*params*/ results);

            if (results[3] != -1) // skip uniform blocks
                continue;

            GLint nameBufSize = results[2] + 1;
            char * name = new char[nameBufSize];
            glGetProgramResourceName(
                /*program*/ m_Handle,
                /*programInterface*/ GL_UNIFORM,
                /*index*/ i,
                /*bufSize*/ nameBufSize,
                /*length*/ NULL,
                /*name*/ name);

            uniforms.emplace_back(
                results[0],
                static_cast<VECTOR_TYPE>(results[1]),
                std::string(name)
            );

            delete name;
        }

        return uniforms;
    }

#else

    std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> Program::getVertexInputAttributes()
    {
        GLint numAttribs;
        glGetProgramiv(
            /*program*/ m_Handle,
            /*pname*/ GL_ACTIVE_ATTRIBUTES,
            /*param*/ &numAttribs);

        std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> attribs;

        for (int i = 0; i < numAttribs; ++i)
        {
            GLint nameBufSize;
            glGetProgramiv(
                /*program*/ m_Handle,
                /*pname*/ GL_ACTIVE_ATTRIBUTE_MAX_LENGTH,
                /*param*/ &nameBufSize);

            nameBufSize++;
            char* name = new char[nameBufSize];
            GLint size;
            GLenum type;

            glGetActiveAttrib(
                /*program*/ m_Handle,
                /*index*/ i,
                /*bufSize*/ nameBufSize,
                /*length*/ NULL,
                /*size*/ &size,
                /*type*/ &type,
                /*name*/ name);
            std::string str_name(name);

            attribs.emplace_back(
                getAttributeLocation(str_name),
                static_cast<VECTOR_TYPE>(type),
                str_name
            );

            delete name;
        }

        return attribs;
    }

    std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> Program::getUniforms() const
    {
        GLint numUniforms;
        glGetProgramiv(
            /*program*/ m_Handle,
            /*pname*/ GL_ACTIVE_UNIFORMS,
            /*param*/ &numUniforms);

        std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> uniforms;

        for (int i = 0; i < numUniforms; ++i)
        {
            GLint nameBufSize;
            glGetProgramiv(
                /*program*/ m_Handle,
                /*pname*/ GL_ACTIVE_UNIFORM_MAX_LENGTH,
                /*param*/ &nameBufSize);

            nameBufSize++;
            char* name = new char[nameBufSize];
            GLint size;
            GLenum type;

            glGetActiveUniform(
                /*program*/ m_Handle,
                /*index*/ i,
                /*bufSize*/ nameBufSize,
                /*length*/ NULL,
                /*size*/ &size,
                /*type*/ &type,
                /*name*/ name);
            std::string str_name(name);

            uniforms.emplace_back(
                getUniformLocation(str_name),
                static_cast<VECTOR_TYPE>(type),
                str_name
            );

            delete name;
        }
        return uniforms;
    }

#endif
}
