/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <cmath>

#include "EngineTypedefs.h"

namespace engine
{
    // Represents position and orientation of an object in 3-dimensional world space.
    class WorldPositionAndOrientation
    {
        #pragma region Fields

        private:
            mat4 m_ModelToWorldMatrix;

        #pragma endregion

        #pragma region Constructors

        public:
            WorldPositionAndOrientation() noexcept :
                m_ModelToWorldMatrix
                {
                    { 1.0f, 0.0f, 0.0f, 0.0f }, // right
                    { 0.0f, 1.0f, 0.0f, 0.0f }, // up
                    { 0.0f, 0.0f, 1.0f, 0.0f }, // forward
                    { 0.0f, 0.0f, 0.0f, 1.0f }  // position
                }
            {}

            // copy constructor
            WorldPositionAndOrientation(const WorldPositionAndOrientation& value) noexcept :
                m_ModelToWorldMatrix(value.m_ModelToWorldMatrix)
            { }

            // move constructor
            WorldPositionAndOrientation(WorldPositionAndOrientation&& value) noexcept :
                m_ModelToWorldMatrix(std::move(value.m_ModelToWorldMatrix))
            { }

        #pragma endregion

        #pragma region Properties

        public:
            const mat4& ModelToWorldMatrix() const noexcept { return m_ModelToWorldMatrix; }

            // Gets object's position in world space.
            vec4& Position() noexcept { return m_ModelToWorldMatrix.at(3); }

            // Gets object's position in world space.
            const vec4& Position() const noexcept { return m_ModelToWorldMatrix.at(3); }
            
            // Gets object's forward direction in world space.
            const vec4& getForwardDirection() const noexcept { return m_ModelToWorldMatrix.at(2); }

            // Gets object's up direction in world space.
            const vec4& getUpDirection() const noexcept { return m_ModelToWorldMatrix.at(1); }

            // Gets object's right direction in world space.
            const vec4& getRightDirection() const noexcept { return m_ModelToWorldMatrix.at(0); }

            // Sets object's position in world space.
            WorldPositionAndOrientation& setPosition(const vec3& value) noexcept
            {
                Position().vec3() = value;
                return *this;
            }

            /*
            Sets object's forward direction in world space so that RightDirection stays in the world's XY plane.
            [value] must be a unit length (normalized) vector setting a direction in world space.
            */
            WorldPositionAndOrientation& setForwardDirection(const vec3& dir) noexcept
            {
                ForwardDirection().vec3() = dir;

                auto k = std::sqrtf(1.0f - dir.Y()*dir.Y());
                if (k != 0.0f)
                {
                    UpDirection() = vec4
                    {
                        /*X*/ -dir.X() * dir.Y() / k,
                        /*Y*/ k,
                        /*Z*/ -dir.Z() * dir.Y() / k,
                        /*W*/ 0.0f
                    };
                }
                else
                {
                    UpDirection().Y() = 0.0f;
                    auto len = vmath::length(UpDirection());
                    if (len > 0.0f)
                        UpDirection() = vmath::normalize(UpDirection());
                    else
                        UpDirection() = vec4{ 0.0f, 0.0f, 1.0f, 0.0f };
                }

                RightDirection().vec3() = vmath::cross_product(UpDirection().vec3(), dir);

                return *this;
            }

            WorldPositionAndOrientation& LookAt(const vec3& point) noexcept
            {
                return setForwardDirection(vmath::normalize(point - Position().vec3()));
            }

        private:
            // Gets object's forward direction in world space.
            vec4& ForwardDirection() noexcept { return m_ModelToWorldMatrix.at(2); }

            // Gets object's up direction in world space.
            vec4& UpDirection() noexcept { return m_ModelToWorldMatrix.at(1); }

            // Gets object's right direction in world space.
            vec4& RightDirection() noexcept { return m_ModelToWorldMatrix.at(0); }

        #pragma endregion

        #pragma region Methods

        public:
            const WorldPositionAndOrientation& operator=(const WorldPositionAndOrientation& value) noexcept
            {
                m_ModelToWorldMatrix = value.m_ModelToWorldMatrix;
                return *this;
            }

            // Move assignment operator
            const WorldPositionAndOrientation& operator=(WorldPositionAndOrientation&& value) noexcept
            {
                m_ModelToWorldMatrix = std::move(value.m_ModelToWorldMatrix);
                return *this;
            }

            /*
            Rotates object around specified axis in world space.
            Parameters:
            [rotation_axis_vec] - specifies rotation axis direction (assumed to be normalized i. e. of unit length)
            [angle] - rotation angle in radians
            */
            WorldPositionAndOrientation& RotateWorld(
                const vec3& rotation_axis_vec,
                GLfloat angle)
            {
                m_ModelToWorldMatrix = std::move(vmath::make_RotationMatrix4x4(rotation_axis_vec, angle) * m_ModelToWorldMatrix);
                return *this;
            }

            /*
            Rotates object around world's up axis (Y), not changing its position.
            Parameters:
            [angle] - angle in radians (+ CW, - CCW)
            */
            WorldPositionAndOrientation& YawWorld(GLfloat angle) noexcept
            {
                auto r = vmath::make_YRotationMatrix3x3(angle);
                ForwardDirection().vec3() = r * ForwardDirection().vec3();
                UpDirection().vec3() = r * UpDirection().vec3();
                RightDirection().vec3() = r * RightDirection().vec3();
                return *this;
            }

            /*
            Rotates object around world's right axis (X), not changing its position.
            Parameters:
            [angle] - angle in radians (+ CW, - CCW)
            */
            WorldPositionAndOrientation& PitchWorld(GLfloat angle) noexcept
            {
                auto r = vmath::make_XRotationMatrix3x3(angle);
                ForwardDirection().vec3() = r * ForwardDirection().vec3();
                UpDirection().vec3() = r * UpDirection().vec3();
                RightDirection().vec3() = r * RightDirection().vec3();
                return *this;
            }

            /*
            Rotates object around world's forward axis (Z), not changing its position.
            Parameters:
            [angle] - angle in radians (+ CW, - CCW)
            */
            WorldPositionAndOrientation& RollWorld(GLfloat angle) noexcept
            {
                auto r = vmath::make_ZRotationMatrix3x3(angle);
                ForwardDirection().vec3() = r * ForwardDirection().vec3();
                UpDirection().vec3() = r * UpDirection().vec3();
                RightDirection().vec3() = r * RightDirection().vec3();
                return *this;
            }

            /*
            Rotates object around object's up axis (Y)
            Parameters:
            [angle] - angle in radians (+ CW, - CCW)
            */
            WorldPositionAndOrientation& Yaw(GLfloat angle) noexcept
            {
                auto r = vmath::make_RotationMatrix3x3(
                    UpDirection().vec3(),
                    angle);

                ForwardDirection().vec3() = r * ForwardDirection().vec3();
                RightDirection().vec3() = r * RightDirection().vec3();
                return *this;
            }

            /*
            Rotates object around object's right axis (X)
            Parameters:
            [angle] - angle in radians (+ CW, - CCW)
            */
            WorldPositionAndOrientation& Pitch(GLfloat angle) noexcept
            {
                auto r = vmath::make_RotationMatrix3x3(
                    RightDirection().vec3(),
                    angle);

                ForwardDirection().vec3() = r * ForwardDirection().vec3();
                UpDirection().vec3() = r * UpDirection().vec3();
                return *this;
            }

            /*
            Rotates object around object's forward axis (Z)
            Parameters:
            [angle] - angle in radians (+ CW, - CCW)
            */
            WorldPositionAndOrientation& Roll(GLfloat angle) noexcept
            {
                auto r = vmath::make_RotationMatrix3x3(
                    ForwardDirection().vec3(),
                    angle);

                UpDirection().vec3() = r * UpDirection().vec3();
                RightDirection().vec3() = r * RightDirection().vec3();
                return *this;
            }

            /*
            Moves object position up or down in world space (along world's Y axis)
            Parameters:
            [delta] - translation along world's Y axis (+ up, - down)
            */
            WorldPositionAndOrientation& MoveUpDownWorld(GLfloat delta) noexcept
            {
                Position().Y() += delta;
                return *this;
            }

            /*
            Moves object position right or left in world space (along world's X axis)
            Parameters:
            [delta] - translation along world's X axis (+ right, - left)
            */
            WorldPositionAndOrientation& MoveRightLeftWorld(GLfloat delta) noexcept
            {
                Position().X() += delta;
                return *this;
            }

            /*
            Moves object position forward or backward in world space (along world's Z axis)
            Parameters:
            [delta] - translation along world's Z axis (+ forward, - backward)
            */
            WorldPositionAndOrientation& MoveForwardBackwardWorld(GLfloat delta) noexcept
            {
                Position().Z() += delta;
                return *this;
            }

            /*
            Moves object position up or down (along object's Y axis)
            Parameters:
            [delta] - translation along object's Y axis (+ up, - down)
            */
            WorldPositionAndOrientation& MoveUpDown(GLfloat delta) noexcept
            {
                Position() += (UpDirection() * delta);
                return *this;
            }

            /*
            Moves object position right or left (along object's X axis)
            Parameters:
            [delta] - translation along object's X axis
            */
            WorldPositionAndOrientation& MoveRightLeft(GLfloat delta) noexcept
            {
                Position() += (RightDirection() * delta);
                return *this;
            }

            /*
            Moves object position forward or backward (along object's Z axis)
            Parameters:
            [delta] - translation along object's Z axis (+ forward, - backward)
            */
            WorldPositionAndOrientation& MoveForwardBackward(GLfloat delta) noexcept
            {
                Position() += (ForwardDirection() * delta);
                return *this;
            }

        #pragma endregion
    };
}