/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include <fstream>
#include <Windows.h>

#include "BMP.h"
#include "Requires.h"

namespace win
{
    std::unique_ptr<char[]> ReadBMP24File(
        const std::string& filename,
        int& width,
        int& height)
    {
        std::ifstream fs(filename, std::ios::binary);
        if (!fs)
            throw std::runtime_error("win::ReadBMP24File() -> Cannot open file: " + filename);

        fs.exceptions(std::ifstream::failbit | std::ifstream::badbit);

        BITMAPFILEHEADER fileHeader;
        fs.read(reinterpret_cast<char*>(&fileHeader), sizeof(fileHeader));

        switch (fileHeader.bfType)
        {
            case 0x4d42u:
            case 0x4349u:
            case 0x5450u:
                break;
            default:
                throw std::runtime_error("win::ReadBMP24File() -> Not a BMP file.");
        }

        BITMAPINFOHEADER dibHeader;
        fs.read(reinterpret_cast<char*>(&dibHeader), sizeof(dibHeader));

        if (dibHeader.biBitCount != 24)
            throw std::runtime_error("win::ReadBMP24File() -> Format is not 24bpp.");

        width = dibHeader.biWidth;
        height = dibHeader.biHeight;
        int stride;
        if ((width * 3) % 4 != 0)
            stride = width * 3 + 4 - (width * 3) % 4;
        else
            stride = width * 3;

        std::unique_ptr<char[]> pixels(new char[stride * height]);
        fs.seekg(fileHeader.bfOffBits);
        for (int row = 1; row <= height; row++)
            fs.read(pixels.get() + (height - row) * stride, stride);

        return pixels;
    }

    void WriteBMP24File(
        const std::string& filename,
        int width,
        int height,
        char* pixels)
    {
        util::Requires::That(width % 4 == 0, FUNCTION_INFO);

        std::ofstream fs(filename, std::ios::binary);
        if (!fs)
            throw std::runtime_error("win::WriteBMP24File() -> Cannot open file: " + filename);

        BITMAPINFOHEADER dibHeader{ 0 };
        dibHeader.biSize = sizeof(BITMAPINFOHEADER);
        dibHeader.biWidth = width;
        dibHeader.biHeight = height;
        dibHeader.biPlanes = 1;
        dibHeader.biBitCount = 24;
        dibHeader.biCompression = BI_RGB;
        dibHeader.biSizeImage = width * height * 3;

        BITMAPFILEHEADER fileHeader{0};
        fileHeader.bfType = 0x4d42;
        fileHeader.bfOffBits = sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER);
        fileHeader.bfSize = fileHeader.bfOffBits + dibHeader.biSizeImage;

        fs.write(reinterpret_cast<char*>(&fileHeader), sizeof(BITMAPFILEHEADER));
        fs.write(reinterpret_cast<char*>(&dibHeader), sizeof(BITMAPINFOHEADER));

        fs.write(pixels, dibHeader.biSizeImage);
    }
}