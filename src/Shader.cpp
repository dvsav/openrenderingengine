/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

// Shader.cpp

#include "ConditionalCompilation.h"

#include <string>
#include <sstream>
#include <memory>

#include "Shader.h"
#include "GlException.h"

namespace opengl
{
    const char* ToString(Shader::SHADER_TYPE shader_type)
    {
        switch (shader_type)
        {
            case Shader::SHADER_TYPE::VERTEX:
                return "Vertex";
            case Shader::SHADER_TYPE::FRAGMENT:
                return "Fragment";
            case Shader::SHADER_TYPE::TESS_CONTROL:
                return "Tesselation Control";
            case Shader::SHADER_TYPE::TESS_EVALUATION:
                return "Tesselation Evaluation";
            case Shader::SHADER_TYPE::GEOMETRY:
                return "Geometry";
            default:
                return "Unknown shader type";
        }
    }

    Shader::Shader(
        SHADER_TYPE shader_type,
        const std::string& shaderText) :
        m_Handle(glCreateShader(static_cast<GLenum>(shader_type))),
        m_ShaderType(shader_type)
    {
        const GLchar* text = shaderText.c_str();

        // load text into shader object
        glShaderSource(
            m_Handle, // handle to shader
            1,        // number of �-strings (zero-ended strings) in the text
            &text,    // shader text
            NULL);    // array of C-strings' lengths

        // compile shader
        glCompileShader(m_Handle);

        // get the status of compilation
        GLint status;
        glGetShaderiv(
            m_Handle,
            GL_COMPILE_STATUS,
            &status);

        // if compilation failed generate exception
        if (status == GL_FALSE)
        {
            std::stringstream s(std::ios_base::out);
            s << "opengl::Shader::Shader() -> Compilation failed in " << ToString(shader_type) << " shader" << std::endl;

            GLint infoLogLength;
            glGetShaderiv(
                m_Handle,
                GL_INFO_LOG_LENGTH,
                &infoLogLength);

            std::unique_ptr<GLchar[]> strInfoLog(new GLchar[infoLogLength + 1]);
            glGetShaderInfoLog(
                m_Handle,
                infoLogLength,
                NULL,
                strInfoLog.get());

            std::stringstream text_stream(shaderText, std::ios_base::in);
            int nLine = 1;
            std::string buffer;
            while (std::getline(text_stream, buffer))
                s << nLine++ << ": " << buffer << std::endl;

            s << strInfoLog.get();

            throw GlException(s.str());
        }
    }
}