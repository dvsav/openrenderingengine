/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "UniformBlockBuffer.h"
#include "TextureCubemap.h"

#include "LightStructs.h"

namespace engine
{
    class UniformBlock_PointLight : public opengl::UniformBlockStructure<PointLightStruct>
    {
        #pragma region Constructors

        public:
            UniformBlock_PointLight() :
                opengl::UniformBlockStructure<PointLightStruct>(
                    PointLightStruct
                    {
                        make_Color_RGBA32F_Gray(1.0f),    // LightColor
                        vec4 { 0.0f, 0.0f, 0.0f, 1.0f },  // LightPosition
                        vmath::make_IdentityMatrix<GLfloat, 4>(), // CameraToScreen
                        {                                 // LightMatrices
                            vmath::make_IdentityMatrix<GLfloat, 4>(),
                            vmath::make_IdentityMatrix<GLfloat, 4>(),
                            vmath::make_IdentityMatrix<GLfloat, 4>(),
                            vmath::make_IdentityMatrix<GLfloat, 4>(),
                            vmath::make_IdentityMatrix<GLfloat, 4>(),
                            vmath::make_IdentityMatrix<GLfloat, 4>()
                        },
                        make_AttenuationStruct_Range3250() // Attenuation
                    },
                    opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(const PointLightStruct::TLightColor& value)
            {
                this->setData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::LightColor),
                    /*size*/ sizeof(PointLightStruct::TLightColor),
                    /*data*/ &value);
            }

            PointLightStruct::TLightColor getLightColor() const
            {
                PointLightStruct::TLightColor value;
                this->getData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::LightColor),
                    /*size*/ sizeof(PointLightStruct::TLightColor),
                    /*data*/ &value);
                return value;
            }

            void setLightPosition(const PointLightStruct::TLightPosition& value)
            {
                this->setData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::LightPosition),
                    /*size*/ sizeof(PointLightStruct::TLightPosition),
                    /*data*/ &value);
            }

            PointLightStruct::TLightPosition getLightPosition() const
            {
                PointLightStruct::TLightPosition value;
                this->getData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::LightPosition),
                    /*size*/ sizeof(PointLightStruct::TLightPosition),
                    /*data*/ &value);
                return value;
            }

            void setCameraToScreen(const PointLightStruct::TCameraToScreen& value)
            {
                this->setData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::CameraToScreen),
                    /*size*/ sizeof(PointLightStruct::TCameraToScreen),
                    /*data*/ &value);
            }

            PointLightStruct::TCameraToScreen getCameraToScreen() const
            {
                PointLightStruct::TCameraToScreen value;
                this->getData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::CameraToScreen),
                    /*size*/ sizeof(PointLightStruct::TCameraToScreen),
                    /*data*/ &value);
                return value;
            }

            void setLightMatrix(opengl::TextureCubemap::CUBE_FACE face, const PointLightStruct::TLightMatrix& value)
            {
                this->setData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::LightMatrices) + ((GLint)face - (GLint)opengl::TextureCubemap::CUBE_FACE::POSITIVE_X) * sizeof(PointLightStruct::TLightMatrix),
                    /*size*/ sizeof(PointLightStruct::TLightMatrix),
                    /*data*/ &value);
            }

            PointLightStruct::TLightMatrix getLightMatrix(opengl::TextureCubemap::CUBE_FACE face) const
            {
                PointLightStruct::TLightMatrix value;
                this->getData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::LightMatrices) + ((GLint)face - (GLint)opengl::TextureCubemap::CUBE_FACE::POSITIVE_X) * sizeof(PointLightStruct::TLightMatrix),
                    /*size*/ sizeof(PointLightStruct::TLightMatrix),
                    /*data*/ &value);
                return value;
            }

            void setAttenuation(PointLightStruct::TAttenuation& value)
            {
                this->setData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::Attenuation),
                    /*size*/ sizeof(PointLightStruct::TAttenuation),
                    /*data*/ &value);
            }

            PointLightStruct::TAttenuation getAttenuation() const
            {
                PointLightStruct::TAttenuation value;
                this->getData(
                    /*offset*/ offsetof(PointLightStruct, PointLightStruct::Attenuation),
                    /*size*/ sizeof(PointLightStruct::TAttenuation),
                    /*data*/ &value);
                return value;
            }

        #pragma endregion
    };

    class UniformBlock_PointLightArray : public opengl::UniformBlockArray<PointLightStruct>
    {
        #pragma region Constructors

        public:
            using UniformBlockArray::UniformBlockArray;

            UniformBlock_PointLightArray(size_t nLights)
                : UniformBlockArray(
                    /*data*/ std::vector<PointLightStruct>(
                        nLights,
                        PointLightStruct
                        {
                            make_Color_RGBA32F_Gray(1.0f),    // LightColor
                            vec4 { 0.0f, 0.0f, 0.0f, 1.0f },  // LightPosition
                            vmath::make_IdentityMatrix<GLfloat, 4>(), // CameraToScreen
                            {                                 // LightMatrices
                                vmath::make_IdentityMatrix<GLfloat, 4>(),
                                vmath::make_IdentityMatrix<GLfloat, 4>(),
                                vmath::make_IdentityMatrix<GLfloat, 4>(),
                                vmath::make_IdentityMatrix<GLfloat, 4>(),
                                vmath::make_IdentityMatrix<GLfloat, 4>(),
                                vmath::make_IdentityMatrix<GLfloat, 4>()
                            },
                            make_AttenuationStruct_Range3250() // Attenuation
                        }),
                    /*usage*/ opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(size_t i, const PointLightStruct::TLightColor& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::LightColor),
                    /*size*/ sizeof(PointLightStruct::TLightColor),
                    /*data*/ &value);
            }

            PointLightStruct::TLightColor getLightColor(size_t i) const
            {
                PointLightStruct::TLightColor value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::LightColor),
                    /*size*/ sizeof(PointLightStruct::TLightColor),
                    /*data*/ &value);
                return value;
            }

            void setLightPosition(size_t i, const PointLightStruct::TLightPosition& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::LightPosition),
                    /*size*/ sizeof(PointLightStruct::TLightPosition),
                    /*data*/ &value);
            }

            PointLightStruct::TLightPosition getLightPosition(size_t i) const
            {
                PointLightStruct::TLightPosition value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::LightPosition),
                    /*size*/ sizeof(PointLightStruct::TLightPosition),
                    /*data*/ &value);
                return value;
            }

            void setCameraToScreen(size_t i, const PointLightStruct::TCameraToScreen& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::CameraToScreen),
                    /*size*/ sizeof(PointLightStruct::TCameraToScreen),
                    /*data*/ &value);
            }

            PointLightStruct::TCameraToScreen getCameraToScreen(size_t i) const
            {
                PointLightStruct::TCameraToScreen value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::CameraToScreen),
                    /*size*/ sizeof(PointLightStruct::TCameraToScreen),
                    /*data*/ &value);
                return value;
            }

            void setLightMatrix(size_t i, opengl::TextureCubemap::CUBE_FACE face, const PointLightStruct::TLightMatrix& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::LightMatrices) + ((GLint)face - (GLint)opengl::TextureCubemap::CUBE_FACE::POSITIVE_X) * sizeof(PointLightStruct::TLightMatrix),
                    /*size*/ sizeof(PointLightStruct::TLightMatrix),
                    /*data*/ &value);
            }

            PointLightStruct::TLightMatrix getLightMatrix(size_t i, opengl::TextureCubemap::CUBE_FACE face) const
            {
                PointLightStruct::TLightMatrix value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::LightMatrices) + ((GLint)face - (GLint)opengl::TextureCubemap::CUBE_FACE::POSITIVE_X) * sizeof(PointLightStruct::TLightMatrix),
                    /*size*/ sizeof(PointLightStruct::TLightMatrix),
                    /*data*/ &value);
                return value;
            }

            void setAttenuation(size_t i, const PointLightStruct::TAttenuation& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::Attenuation),
                    /*size*/ sizeof(PointLightStruct::TAttenuation),
                    /*data*/ &value);
            }

            PointLightStruct::TAttenuation getAttenuation(size_t i) const
            {
                PointLightStruct::TAttenuation value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(PointLightStruct, PointLightStruct::Attenuation),
                    /*size*/ sizeof(PointLightStruct::TAttenuation),
                    /*data*/ &value);
                return value;
            }

        #pragma endregion
    };
}
