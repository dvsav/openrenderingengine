/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "UniformBlockBuffer.h"

#include "LightStructs.h"

namespace engine
{
    class UniformBlock_AmbientLight : public opengl::UniformBlockStructure<AmbientLightStruct>
    {
        #pragma region Constructors

        public:
            UniformBlock_AmbientLight() :
                opengl::UniformBlockStructure<AmbientLightStruct>(
                    AmbientLightStruct
                    {
                        make_Color_RGBA32F_Gray(1.0f)
                    },
                    opengl::Buffer::DRAW_USAGE::STATIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(const AmbientLightStruct::TLightColor& value)
            {
                this->setData(
                    /*offset*/ offsetof(AmbientLightStruct, AmbientLightStruct::LightColor),
                    /*size*/ sizeof(AmbientLightStruct::TLightColor),
                    /*data*/ &value);
            }

            AmbientLightStruct::TLightColor getLightColor() const
            {
                AmbientLightStruct::TLightColor value;
                this->getData(
                    /*offset*/ offsetof(AmbientLightStruct, AmbientLightStruct::LightColor),
                    /*size*/ sizeof(AmbientLightStruct::TLightColor),
                    /*data*/ &value);
                return value;
            }

        #pragma endregion
    };
}