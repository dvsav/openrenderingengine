/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <sstream>
#include <map>
#include <vector>
#include <tuple>

#include "GlException.h"
#include "OpenGLTypes.h"
#include "Shader.h"
#include "Requires.h"
#include "Vector.h"

namespace opengl
{
    /*
    Represents a program executed by the graphics processing unit (GPU).
    A program is made out of shaders (small snippets written in GLSL language).
    A program can have input parameters (attributes): vertices and colors for example as well as
    global variables (uniform variables) etc.
    Example:
    // create program
    Program program(shaders);
    // use the program for rendering
    program.Use();
    */
    class Program
    {
        #pragma region Nested Casses

        public:
            // Represents a token identifying the interface within program.
            enum class PROGRAM_INTERFACE : GLenum
            {
                UNIFORM = GL_UNIFORM,
                UNIFORM_BLOCK = GL_UNIFORM_BLOCK,
                PROGRAM_INPUT = GL_PROGRAM_INPUT,
                PROGRAM_OUTPUT = GL_PROGRAM_OUTPUT,
                VERTEX_SUBROUTINE = GL_VERTEX_SUBROUTINE,
                TESS_CONTROL_SUBROUTINE = GL_TESS_CONTROL_SUBROUTINE,
                TESS_EVALUATION_SUBROUTINE = GL_TESS_EVALUATION_SUBROUTINE,
                GEOMETRY_SUBROUTINE = GL_GEOMETRY_SUBROUTINE,
                FRAGMENT_SUBROUTINE = GL_FRAGMENT_SUBROUTINE,
                COMPUTE_SUBROUTINE = GL_COMPUTE_SUBROUTINE,
                VERTEX_SUBROUTINE_UNIFORM = GL_VERTEX_SUBROUTINE_UNIFORM,
                TESS_CONTROL_SUBROUTINE_UNIFORM = GL_TESS_CONTROL_SUBROUTINE_UNIFORM,
                TESS_EVALUATION_SUBROUTINE_UNIFORM = GL_TESS_EVALUATION_SUBROUTINE_UNIFORM,
                GEOMETRY_SUBROUTINE_UNIFORM = GL_GEOMETRY_SUBROUTINE_UNIFORM,
                FRAGMENT_SUBROUTINE_UNIFORM = GL_FRAGMENT_SUBROUTINE_UNIFORM,
                COMPUTE_SUBROUTINE_UNIFORM = GL_COMPUTE_SUBROUTINE_UNIFORM,
                TRANSFORM_FEEDBACK_VARYING = GL_TRANSFORM_FEEDBACK_VARYING,
                TRANSFORM_FEEDBACK_BUFFER = GL_TRANSFORM_FEEDBACK_BUFFER,
                BUFFER_VARIABLE = GL_BUFFER_VARIABLE,
                SHADER_STORAGE_BLOCK = GL_SHADER_STORAGE_BLOCK
            };

            // Used with glGetProgramStageiv() function.
            enum class PROGRAM_STAGE_INFO
            {
                // The number of active subroutine uniforms.
                ACTIVE_SUBROUTINE_UNIFORMS = GL_ACTIVE_SUBROUTINE_UNIFORMS,

                /*
                The number of active subroutine uniform variable locations in a particular shader stage.
                Because subroutine uniforms can be arrays, there can be more subroutine uniform locations
                than there are subroutine uniform indices.
                */
                ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS = GL_ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS,

                // The number of subroutine functions in a particular shader stage.
                ACTIVE_SUBROUTINES = GL_ACTIVE_SUBROUTINES,

                // The length of the longest subroutine uniform name in a particular shader stage.
                ACTIVE_SUBROUTINE_UNIFORM_MAX_LENGTH = GL_ACTIVE_SUBROUTINE_UNIFORM_MAX_LENGTH,

                /*
                The length of the longest subroutine function name (including the null-terminator)
                for a particular shader stage.
                */
                ACTIVE_SUBROUTINE_MAX_LENGTH = GL_ACTIVE_SUBROUTINE_MAX_LENGTH
            };

            /*
            Provides more safe use of a program in RAII fascion. When created the Using object starts using
            the program specified in its constructor. When the Using object is going to be deleted it returns
            to using previous program.
            You are recommended to create instances of this class only as local variables.
            Example:
            {
                Program::Using using(program); // here we start using program
                // do rendering
            } // here we automatically return to using the program which had been used before we created the Using object
            */
            class Using final
            {
                private:
                    GLint m_PreviousProgramHandle;

                private:
                    Using(const Using&) = delete;
                    Using& operator=(const Using&) = delete;

                public:
                    Using(
                        const Program& program,
                        bool useForRendering = false)
                        : m_PreviousProgramHandle(getCurrentProgramHandle())
                    {
                        if (useForRendering)
                        {
#ifdef _DEBUG
                            if (program.Validate() == GL_FALSE)
                            {
                                std::stringstream s;
                                s << "opengl::Program::Use() -> Validation failed:" << std::endl;
                                program.WriteProgramInfoLog(s);
                                throw GlException(s.str());
                            }
#endif
                            program.UseForRendering();
                        }
                        else
                            program.Use();
                    }

                    ~Using()
                    {
                        glUseProgram(m_PreviousProgramHandle);
                    }
            };

        #pragma endregion

        #pragma region Fields

        protected:
            GLuint m_Handle;

#ifdef OPENGL_4
            /*
            The map determines currently active subroutines.
            key (SHADER_TYPE): shader stage
            value (std::vector<GLuint>) - collection of subroutine function indices for a particular shader stage (key - see above):
                                          index into the vector is the subroutine uniform location; value is the selected function index.
            */
            std::map< Shader::SHADER_TYPE, std::vector<GLuint> > m_SubroutinesMap;
#endif

        #pragma endregion

        #pragma region Constructors

        public:
            /*
            Template parameters:
                SHADERS_COLLECTION - a collection of pointers to a Shader (either Shader* or some kind of smart pointer)
            */
            template<typename SHADERS_COLLECTION>
            Program(const SHADERS_COLLECTION& shaders) :
                m_Handle(glCreateProgram())
#ifdef OPENGL_4
                ,
                m_SubroutinesMap()
#endif
            {
                init(shaders);
            }

            // Example: opengl::Program program { &vertex_shader, &fragment_shader };
            Program(std::initializer_list<Shader*> shaders) :
                m_Handle(glCreateProgram())
#ifdef OPENGL_4
                ,
                m_SubroutinesMap()
#endif
            {
                init(shaders);
            }

            // Move constructor.
            Program(Program&& program) noexcept :
                m_Handle(program.m_Handle)
#ifdef OPENGL_4
                ,
                m_SubroutinesMap(std::move(program.m_SubroutinesMap))
#endif
            {
                program.m_Handle = 0;
            }

        protected:
            template<typename SHADERS_COLLECTION>
            void init(
                const SHADERS_COLLECTION& shaders,
                bool is_separable = false)
            {
                // Ensure that [shaders] contain pointers (regular or smart) to Shader
                static_assert(
                    std::is_same< std::remove_reference< decltype(*std::declval<SHADERS_COLLECTION::value_type>()) >::type, Shader>::value,
                    "Wrong type of collection element");

                for (auto& shader : shaders)
                {
                    glAttachShader(
                        m_Handle,
                        shader->getHandle());
                }

                if(is_separable)
                    glProgramParameteri(m_Handle, GL_PROGRAM_SEPARABLE, GL_TRUE);

                glLinkProgram(m_Handle);

                for (auto& shader : shaders)
                {
                    glDetachShader(
                        m_Handle,
                        shader->getHandle());
                }

                // get the status of linking
                GLint status;
                glGetProgramiv(
                    m_Handle,
                    GL_LINK_STATUS,
                    &status);

                // if linking has failed generate exception
                if (status == GL_FALSE)
                {
                    std::stringstream s;
                    s << "opengl::Program::Program() -> Link failed:" << std::endl;
                    WriteProgramInfoLog(s);
                    throw GlException(s.str());
                }

#ifdef OPENGL_4
                // init subroutines map
                for (auto& shader : shaders)
                {
                    auto shader_stage = shader->getShaderType();

                    auto uniform_locations_number = getProgramStageInfo(
                        shader_stage,
                        PROGRAM_STAGE_INFO::ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS);

                    if (uniform_locations_number > 0)
                    {
                        std::vector<GLuint> function_indices(uniform_locations_number);

                        for (int location = 0; location < uniform_locations_number; ++location)
                        {
                            function_indices.at(location) = getSubroutineFunctionIndices(shader_stage, location)[0];
                        }

                        m_SubroutinesMap[shader_stage] = std::move(function_indices);
                    }
                }
#endif
            }

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~Program()
            {
                if (isInUse())
                    UnuseAll();

                glDeleteProgram(m_Handle);
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            Program(const Program&) = delete;
            Program& operator=(const Program&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            // Gets the handle of a program which is currently in use.
            static GLuint getCurrentProgramHandle()
            {
                GLint current_program;
                glGetIntegerv(GL_CURRENT_PROGRAM, &current_program);
                return current_program;
            }

            // Gets program handle.
            GLuint getHandle() { return m_Handle; }

            // Gets whether the program is currently in use.
            bool isInUse() const { return (getCurrentProgramHandle() == m_Handle); }

        #pragma endregion

        #pragma region Methods

        public:
            // Stops using the program.
            static void UnuseAll() { glUseProgram(0); }

            // Use the program.
            void Use() const { glUseProgram(m_Handle); }

            /*
            Use the program for rendering.
            This method not only sets the program as currently used but also
            sets up the subroutines which we need to do every time we switch shader program
            because the information about the active subroutines is not stored by OpenGL anywhere.
            */
            void UseForRendering() const
            {
                Use();
#ifdef OPENGL_4
                SetupSubroutines();
#endif
            }

            /*
            Validates the program.
            Validation checks to see whether the executables contained
            in program can execute given the current OpenGL state.
            Returns GL_FALSE in case of validation failure.
            */
            GLint Validate() const
            {
                GLint status = GL_FALSE;
                glValidateProgram(m_Handle);
                glGetProgramiv(
                    m_Handle,
                    GL_VALIDATE_STATUS,
                    &status);
                return status;
            }

            void WriteProgramInfoLog(std::ostream& os) const
            {
                GLint infoLogLength;
                glGetProgramiv(
                    m_Handle,
                    GL_INFO_LOG_LENGTH,
                    &infoLogLength);

                std::unique_ptr<GLchar[]> strInfoLog(new GLchar[infoLogLength + 1]);
                glGetProgramInfoLog(
                    m_Handle,
                    infoLogLength,
                    NULL,
                    strInfoLog.get());

                os << strInfoLog.get();
            }

#ifdef OPENGL_4
            void SetupSubroutines() const
            {
                for (auto& subroutine : m_SubroutinesMap)
                {
                    glUniformSubroutinesuiv(
                        /*shadertype*/ static_cast<GLenum>(subroutine.first),
                        /*count*/   subroutine.second.size(),
                        /*indices*/ subroutine.second.data());
                }
            }
#endif

            // Gets attribute location by its name in the program.
            GLint getAttributeLocation(const std::string& value_name) const
            {
                return glGetAttribLocation(m_Handle, value_name.c_str());
            }

            // Gets the handle to a single uniform variable by its name.
            GLint getUniformLocation(const std::string& uniform_name) const
            {
                auto value = glGetUniformLocation(m_Handle, uniform_name.c_str());
                util::Requires::That(value != -1, FUNCTION_INFO + " " + uniform_name);
                return value;
            }

            // Gets the index of a named uniform block.
            GLuint getUniformBlockIndex(const std::string& uniform_block_name) const
            {
#ifdef OPENGL_4
                return glGetProgramResourceIndex(
                    m_Handle,
                    GL_UNIFORM_BLOCK,
                    uniform_block_name.c_str());
#else
                return glGetUniformBlockIndex(m_Handle, uniform_block_name.c_str());
#endif
            }

            // Gets the bindings of color numbers to user-defined varying out variables in fragment shader.
            GLint getFragDataLocation(const std::string& frag_out_variable_name) const
            {
                return glGetFragDataLocation(m_Handle, frag_out_variable_name.c_str());
            }

            // Gets the bindings of color indices to user-defined varying out variables in fragment shader.
            GLint getFragDataIndex(const std::string& frag_out_variable_name) const
            {
                return glGetFragDataIndex(m_Handle, frag_out_variable_name.c_str());
            }

            void setUniformBlockBinding(
                const std::string& uniformBlockName,
                GLuint uniformBlockBindingPoint)
            {
                glUniformBlockBinding(
                    m_Handle,
                    getUniformBlockIndex(uniformBlockName),
                    uniformBlockBindingPoint);
            }

            void setUniformBlockBinding(
                GLuint uniformBlockIndex,
                GLuint uniformBlockBindingPoint)
            {
                glUniformBlockBinding(
                    m_Handle,
                    uniformBlockIndex,
                    uniformBlockBindingPoint);
            }

#ifdef OPENGL_4
            // Gets the index of a named shader storage block.
            GLuint getShaderStorageBlockIndex(const std::string& storage_block_name)
            {
                return glGetProgramResourceIndex(
                    m_Handle,
                    GL_SHADER_STORAGE_BLOCK,
                    storage_block_name.c_str());
            }

            void setStorageBlockBinding(
                const std::string& storageBlockName,
                GLuint storageBlockBindingPoint)
            {
                glShaderStorageBlockBinding(
                    m_Handle,
                    getShaderStorageBlockIndex(storageBlockName),
                    storageBlockBindingPoint);
            }
#endif

            /*
            Returns a list of tuples each of which contains
            { location, type, name } of a vertex input attribute.
            Example of use:
            for (auto& attrib : program.getVertexInputAttributes())
                std::cout << std::get<0>(attrib) << " " << opengl::getTypeName(std::get<1>(attrib)) << " " << std::get<2>(attrib) << std::endl;
            */
            std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> getVertexInputAttributes() const;

            /*
            Returns a list of tuples each of which contains
            { location, type, name } of a uniform variable.
            Example of use:
            for (auto& uniform : program.getUniforms())
                std::cout << std::get<0>(uniform) << " " << opengl::getTypeName(std::get<1>(uniform)) << " " << std::get<2>(uniform) << std::endl;
            */
            std::vector<std::tuple<GLint, VECTOR_TYPE, std::string>> getUniforms() const;

            // Assigns a scalar value of type T to a uniform variable
            template<typename T>
            void setUniform(
                GLint uniformLocation,
                T value);

            template<>
            void setUniform<GLint>(
                GLint uniformLocation,
                GLint value)
            {
                Program::Using use(*this);
                glUniform1i(uniformLocation, value);
            }

            template<>
            void setUniform<GLfloat>(
                GLint uniformLocation,
                GLfloat value)
            {
                Program::Using use(*this);
                glUniform1f(uniformLocation, value);
            }

            template<>
            void setUniform<GLuint>(
                GLint uniformLocation,
                GLuint value)
            {
                Program::Using use(*this);
                glUniform1ui(uniformLocation, value);
            }

            // Assigns a 3-dimensional vector to a uniform variable.
            void setUniform3fv(
                GLint uniformLocation,
                const GLfloat* value)
            {
                Program::Using use(*this);
                glUniform3fv(
                    /*location*/ uniformLocation,
                    /*count*/ 1,
                    /*value*/ value);
            }

            // Assigns a 4-dimensional vector to a uniform variable.
            void setUniform4fv(
                GLint uniformLocation,
                const GLfloat* value)
            {
                Program::Using use(*this);
                glUniform4fv(
                    /*location*/ uniformLocation,
                    /*count*/ 1,
                    /*value*/ value);
            }

            // Assigns a 3-dimensional vector to a uniform variable.
            void setUniform3fv(
                const std::string& uniformName,
                const GLfloat* value)
            {
                setUniform3fv(getUniformLocation(uniformName), value);
            }

            // Assigns a 4-dimensional vector to a uniform variable.
            void setUniform4fv(
                const std::string& uniformName,
                const GLfloat* value)
            {
                setUniform4fv(getUniformLocation(uniformName), value);
            }

            template<typename T>
            void setUniform(
                const std::string& uniformName,
                T value)
            {
                setUniform<T>(getUniformLocation(uniformName), value);
            }

            // Assigns a single matrix in column-major order to a uniform variable.
            void setUniformMatrix(
                const std::string& uniformName,
                const GLfloat* matrix)
            {
                Program::Using use(*this);
                glUniformMatrix4fv(
                    /*location*/ getUniformLocation(uniformName),
                    /*count*/ 1,
                    /*transpose*/ GL_FALSE,
                    /*value*/ matrix);
            }

            // Assigns a single matrix in column-major order to a uniform variable.
            void setUniformMatrix(
                GLuint uniformLocation,
                const GLfloat* matrix)
            {
                Program::Using use(*this);
                glUniformMatrix4fv(
                    /*location*/ uniformLocation,
                    /*count*/ 1,
                    /*transpose*/ GL_FALSE,
                    /*value*/ matrix);
            }

#ifdef OPENGL_4
            // Retrieves properties of a program object corresponding to a specified [shader_stage].
            GLint getProgramStageInfo(
                Shader::SHADER_TYPE shader_stage,
                PROGRAM_STAGE_INFO info)
            {
                GLint value;
                glGetProgramStageiv(
                    m_Handle,
                    static_cast<GLenum>(shader_stage),
                    static_cast<GLenum>(info),
                    &value);
                return value;
            }

            // Gets the location of a subroutine uniform of a given shader stage within the program.
            GLint getSubroutineUniformLocation(
                Shader::SHADER_TYPE shader_stage,
                const std::string& subroutine_uniform_name) const
            {
                auto value = glGetSubroutineUniformLocation(
                    m_Handle,
                    static_cast<GLenum>(shader_stage),
                    subroutine_uniform_name.c_str());
                util::Requires::That(value != -1, FUNCTION_INFO);
                return value;
            }

            // Gets the name of a subroutine uniform of a given shader stage within the program..
            std::string getSubroutineUnifromName(
                Shader::SHADER_TYPE shader_stage,
                GLint uniform_location)
            {
                GLint name_length;
                glGetActiveSubroutineUniformiv(
                    m_Handle,
                    static_cast<GLenum>(shader_stage),
                    uniform_location,
                    GL_UNIFORM_NAME_LENGTH,
                    &name_length);

                auto name = std::make_unique<GLchar[]>(name_length);
                GLint charachters_written;
                glGetActiveSubroutineUniformName(
                    m_Handle,
                    static_cast<GLenum>(shader_stage),
                    uniform_location,
                    name_length,
                    &charachters_written,
                    name.get());

                std::string value(name.get(), charachters_written);
                return value;
            }

            // Gets the index of a subroutine function of a given shader stage within the program.
            GLuint getSubroutineFunctionIndex(
                Shader::SHADER_TYPE shader_stage,
                const std::string& function_name) const
            {
                auto value = glGetSubroutineIndex(
                    m_Handle,
                    static_cast<GLenum>(shader_stage),
                    function_name.c_str());
                util::Requires::That(value != GL_INVALID_INDEX, FUNCTION_INFO);
                return value;
            }

            // Gets the name of a subroutine function of a given shader stage within the program.
            std::string getSubroutineFunctionName(
                Shader::SHADER_TYPE shader_stage,
                GLuint function_index)
            {
                GLint name_length = getProgramStageInfo(
                    shader_stage,
                    PROGRAM_STAGE_INFO::ACTIVE_SUBROUTINE_MAX_LENGTH);

                auto name = std::make_unique<GLchar[]>(name_length);
                GLint charachters_written;
                glGetActiveSubroutineName(
                    m_Handle,
                    static_cast<GLenum>(shader_stage),
                    function_index,
                    name_length,
                    &charachters_written,
                    name.get());

                std::string value(name.get(), charachters_written);
                return value;
            }

            /*
            Gets the list of compatible function indices for a subroutine
            specified by [shader_stage] and [subroutine_uniform_location].
            */
            std::vector<GLuint> getSubroutineFunctionIndices(
                Shader::SHADER_TYPE shader_stage,
                GLint subroutine_uniform_location)
            {
                GLint number_of_functions;
                glGetActiveSubroutineUniformiv(
                    m_Handle,
                    static_cast<GLenum>(shader_stage),
                    subroutine_uniform_location,
                    GL_NUM_COMPATIBLE_SUBROUTINES,
                    &number_of_functions);

                std::vector<GLuint> function_indices(number_of_functions, 0);
                glGetActiveSubroutineUniformiv(
                    m_Handle,
                    static_cast<GLenum>(shader_stage),
                    subroutine_uniform_location,
                    GL_COMPATIBLE_SUBROUTINES,
                    reinterpret_cast<GLint*>(function_indices.data()));

                return function_indices;
            }

            // Selects the subroutine for the program to execute.
            void SelectSubroutine(
                Shader::SHADER_TYPE shader_stage,
                GLint subroutineUniformLocation,
                GLuint subroutineFunctionIndex)
            {
                std::vector<GLuint>& indices = m_SubroutinesMap.at(shader_stage);
                indices.at(subroutineUniformLocation) = subroutineFunctionIndex;
            }
#endif

        #pragma endregion
    };
}