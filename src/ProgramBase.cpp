/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include "ProgramBase.h"

namespace engine
{
    void ProgramBase::OnMouseMove(std::tuple<win::Window*, int, int, WPARAM> eventArgs)
    {
        static bool skip_mouse_move = false;
        if (skip_mouse_move)
        {
            skip_mouse_move = false;
            return;
        }

        if (win::IsKeyPressed(VK_LBUTTON))
            return;

        const GLfloat ANGLE_DELTA = 0.1f;

        auto clientRect = m_Window.getClientRect();

        auto centerX = clientRect.right / 2.0f;
        auto centerY = clientRect.bottom / 2.0f;

        auto x = std::get<1>(eventArgs);
        auto y = std::get<2>(eventArgs);

        auto angleX = util::constraint(1.0f - x / centerX, -ANGLE_DELTA, ANGLE_DELTA);
        auto angleY = util::constraint(1.0f - y / centerY, -ANGLE_DELTA, ANGLE_DELTA);

        m_Camera.PositionAndOrientation().YawWorld(angleX);
        m_Camera.PositionAndOrientation().Pitch(angleY);

        m_Window.setCursorPosition(
            (int)centerX,
            (int)centerY);
        skip_mouse_move = true;

        setupCamera(m_Camera);

        UpdateTimeAndRender();
    }

    void ProgramBase::OnKeyDown(std::tuple<win::Window*, WPARAM> eventArgs)
    {
        auto key = std::get<1>(eventArgs);

        m_VirtualKeyCodes.insert(key);

        switch (key)
        {
            case VK_ESCAPE:
                m_Window.Close();
                break;

            case VK_RETURN:
                int width, height;
                win::getClientSize(getWindow(), width, height);
                SaveScreenshot("C:\\DVSAV\\screenshot.bmp", width, height);
                break;

            default:
                return;
        }
    }
}