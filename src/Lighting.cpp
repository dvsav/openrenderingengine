#include "Lighting.h"
#include "StandardMetaprograms.h"

namespace engine
{
    void DirectionalLight::RenderShadows(std::function<void()> renderFunc)
    {
        if (lightViewInvalidated)
            UpdateLightView();

        uniformBlock->BindToUniformBindingPoint(
            shadowRender->UniformBlocksBindingPoints().at(engine::UniformBlockEnum::DirectionalLightArray));

        GLuint old_function = 0;
        auto& subroutine = standard::selectLightEnumSubroutine<standard::LightEnum::Directional>(*shadowRender, old_function);
        {
            shadowMap.Render(renderFunc);
        }
        subroutine.SelectFunction(old_function);
    }

    void PointLight::RenderShadows(std::function<void()> renderFunc)
    {
        UpdateLightView();

        uniformBlock->BindToUniformBindingPoint(
            shadowRender->UniformBlocksBindingPoints().at(engine::UniformBlockEnum::PointLightArray));

        GLuint old_function = 0;
        auto& subroutine = standard::selectLightEnumSubroutine<standard::LightEnum::Point>(*shadowRender, old_function);
        {
            shadowMap.Render(renderFunc);
        }
        subroutine.SelectFunction(old_function);
    }

    void SpotLight::RenderShadows(std::function<void()> renderFunc)
    {
        if (lightViewInvalidated)
            UpdateLightView();

        uniformBlock->BindToUniformBindingPoint(
            shadowRender->UniformBlocksBindingPoints().at(engine::UniformBlockEnum::SpotLightArray));

        GLuint old_function = 0;
        auto& subroutine = standard::selectLightEnumSubroutine<standard::LightEnum::Spot>(*shadowRender, old_function);
        {
            shadowMap.Render(renderFunc);
        }
        subroutine.SelectFunction(old_function);
    }

    void DirectionalLightArray::RenderShadows(std::function<void()> renderFunc)
    {
        if (getCount() == 0) return;

        for (size_t i = 0; i < lightViews.size(); i++)
            UpdateLightView(i);

        uniformBlock->BindToUniformBindingPoint(
            shadowRender->UniformBlocksBindingPoints().at(engine::UniformBlockEnum::DirectionalLightArray));

        GLuint old_function = 0;
        auto& subroutine = standard::selectLightEnumSubroutine<standard::LightEnum::Directional>(*shadowRender, old_function);
        {
            shadowMap.Render(renderFunc);
        }
        subroutine.SelectFunction(old_function);
    }

    void PointLightArray::RenderShadows(std::function<void()> renderFunc)
    {
        if (getCount() == 0) return;

        for (size_t i = 0; i < lightViews.size(); i++)
            UpdateLightView(i);

        uniformBlock->BindToUniformBindingPoint(
            shadowRender->UniformBlocksBindingPoints().at(engine::UniformBlockEnum::PointLightArray));

        GLuint old_function = 0;
        auto& subroutine = standard::selectLightEnumSubroutine<standard::LightEnum::Point>(*shadowRender, old_function);
        {
            shadowMap.Render(renderFunc);
        }
        subroutine.SelectFunction(old_function);
    }

    void SpotLightArray::RenderShadows(std::function<void()> renderFunc)
    {
        if (getCount() == 0) return;

        for (size_t i = 0; i < lightViews.size(); i++)
            UpdateLightView(i);

        uniformBlock->BindToUniformBindingPoint(
            shadowRender->UniformBlocksBindingPoints().at(engine::UniformBlockEnum::SpotLightArray));

        GLuint old_function = 0;
        auto& subroutine = standard::selectLightEnumSubroutine<standard::LightEnum::Spot>(*shadowRender, old_function);
        {
            shadowMap.Render(renderFunc);
        }
        subroutine.SelectFunction(old_function);
    }
}
