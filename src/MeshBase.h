/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <map>
#include <cmath>
#include <type_traits>

#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "GLenums.h"

#include "VertexAttributeEnum.h"
#include "utility.h"

namespace engine
{
    /*
    Abstract base class representing a mesh, i. e. vertex coordinates and the way of connecting them together (drawing mode).
    Mesh does not specify properties like material, texture or orientation in space.
    Thus you can use a single mesh to draw multiple objects with different material, texture, orientation etc.
    Template parameter TVertex must be one of the types defined in the "Vertex.h" header.
    */
    class MeshBase
    {
        #pragma region  Nested Types

        private:
            struct AttributeInfo
            {
                int Offset;
                opengl::TYPE ComponentType;
                int ComponentsNumber;
                GLboolean Normalized;
            };

        #pragma endregion

        #pragma region Fields

        protected:
            opengl::VertexBufferBase m_VertexBuffer;
            const GLenum m_DrawingMode;
            const GLsizei m_Stride;
            std::map<VertexAttributeEnum, AttributeInfo> m_Attributes;

        #pragma endregion

        #pragma region Constructors

// This macro creates the structure [AttributeInfo] which brings together all
// the information required to attach (connect) a specified vertex attribute
// in the vertex buffer to a specified attribute location in shader program.
#define MAKE_ATTRIB_INFO(VERTEX, FIELD, NORMALIZED) \
AttributeInfo \
{ \
    /*Offset*/           offsetof(VERTEX, VERTEX::FIELD), \
    /*ComponentType*/    opengl::type_traits<decltype(VERTEX::FIELD)::value_type>::gl_type, \
    /*ComponentsNumber*/ sizeof(decltype(VERTEX::FIELD)) / sizeof(decltype(VERTEX::FIELD)::value_type), \
    /*Normalized*/       NORMALIZED \
}

        protected:
            template<typename TVertex>
            MeshBase(
                opengl::VertexBuffer<TVertex>&& vertexBuffer,
                opengl::DRAWING_MODE drawing_mode) :

                m_VertexBuffer(std::move(vertexBuffer)),
                m_DrawingMode(static_cast<GLenum>(drawing_mode)),
                m_Stride(sizeof(TVertex)),
                m_Attributes()
            {
                FillAttribInfoMap<TVertex>();
            }

            template<typename TVertex>
            void FillAttribInfoMap()
            { }

            template<>
            void FillAttribInfoMap<VertexP>()
            {
                m_Attributes[VertexAttributeEnum::Position] = MAKE_ATTRIB_INFO(VertexP, Position, GL_FALSE);
            }

            template<>
            void FillAttribInfoMap<VertexPC>()
            {
                m_Attributes[VertexAttributeEnum::Position] = MAKE_ATTRIB_INFO(VertexPC, Position, GL_FALSE);
                m_Attributes[VertexAttributeEnum::Color] = MAKE_ATTRIB_INFO(VertexPC, Color, GL_FALSE);
            }

            template<>
            void FillAttribInfoMap<VertexPN>()
            {
                m_Attributes[VertexAttributeEnum::Position] = MAKE_ATTRIB_INFO(VertexPN, Position, GL_FALSE);
                m_Attributes[VertexAttributeEnum::Normal] = MAKE_ATTRIB_INFO(VertexPN, Normal, GL_FALSE);
            }

            template<>
            void FillAttribInfoMap<VertexPNT>()
            {
                m_Attributes[VertexAttributeEnum::Position] = MAKE_ATTRIB_INFO(VertexPNT, Position, GL_FALSE);
                m_Attributes[VertexAttributeEnum::Normal] = MAKE_ATTRIB_INFO(VertexPNT, Normal, GL_FALSE);
                m_Attributes[VertexAttributeEnum::TexCoords] = MAKE_ATTRIB_INFO(VertexPNT, TextureCoords, GL_FALSE);
            }

            template<>
            void FillAttribInfoMap<VertexPNTT>()
            {
                m_Attributes[VertexAttributeEnum::Position] = MAKE_ATTRIB_INFO(VertexPNTT, Position, GL_FALSE);
                m_Attributes[VertexAttributeEnum::Normal] = MAKE_ATTRIB_INFO(VertexPNTT, Normal, GL_FALSE);
                m_Attributes[VertexAttributeEnum::TexCoords] = MAKE_ATTRIB_INFO(VertexPNTT, TextureCoords, GL_FALSE);
                m_Attributes[VertexAttributeEnum::Tangent] = MAKE_ATTRIB_INFO(VertexPNTT, Tangent, GL_FALSE);
            }

#undef MAKE_ATTRIB_INFO

            // Move constructor.
            MeshBase(MeshBase&& value) noexcept :
                m_VertexBuffer(std::move(value.m_VertexBuffer)),
                m_DrawingMode(value.m_DrawingMode),
                m_Stride(value.m_Stride),
                m_Attributes(std::move(value.m_Attributes))
            { }

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~MeshBase() { }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            MeshBase(const MeshBase&) = delete;
            MeshBase& operator=(const MeshBase&) = delete;

        #pragma endregion

        #pragma region Methods

        private:
            void AttachAttribToAttribLocation(
                AttributeInfo attribInfo,
                GLuint attrib_location)
            {
                opengl::Buffer::Binding bind_buf(m_VertexBuffer, opengl::Buffer::TARGET::ARRAY);

                glVertexAttribPointer(
                    /*index*/      attrib_location,
                    /*size*/       attribInfo.ComponentsNumber,
                    /*type*/       static_cast<GLenum>(attribInfo.ComponentType),
                    /*normalized*/ attribInfo.Normalized,
                    /*stride*/     m_Stride,
                    /*pointer*/    reinterpret_cast<void*>(attribInfo.Offset)
                );

                glEnableVertexAttribArray(attrib_location);
            }

        public:
            virtual void BindIndexBuffer() {}
                
            // Returns true if the mesh has a specified vertex attribute [attrib_type] and false otherwise.
            bool HasAttribute(VertexAttributeEnum attrib_type)
            {
                return m_Attributes.find(attrib_type) != m_Attributes.end();
            }

            /*
            Tells OpenGL to supply a specified attribute values into shader program
            from the mesh. Don't forget to bind a Vertex Array Object before calling this function!
            Parameters:
            [attrib_type] - attribute type.
            [attrib_location] - location of position attribute in shader program.
            Exceptions:
            If the mesh doesn't have the specified attribute the function throws std::out_of_range exception.
            */
            void AttachAttributeToAttribLocation(
                VertexAttributeEnum attrib_type,
                GLuint attrib_location)
            {
                AttachAttribToAttribLocation(
                    /*attribInfo*/ m_Attributes.at(attrib_type),
                    /*attrib_location*/ attrib_location);
            }

            // Renders the mesh.
            virtual void Render() = 0;

            /*
            Renders insatance_count instances of the mesh.
            Parameters:
            instance_count - the number of instances of the specified range of indices to be rendered.
            base_instance - the base instance for use in fetching instanced vertex attributes.
            */
            virtual void Render(
                GLsizei instance_count,
                GLuint base_instance = 0) = 0;

        #pragma endregion
    };
}