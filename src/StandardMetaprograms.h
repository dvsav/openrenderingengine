#pragma once

#include "ConditionalCompilation.h"

#include "MetaProgram.h"
#include "pugixml_loader.h"

namespace engine { namespace standard
{
    enum class LightEnum
    {
        Directional,
        Point,
        Spot
    };

    template <LightEnum SUB>
    inline ShaderSubroutine& selectLightEnumSubroutine(
        MetaProgram& program,
        GLuint& oldFunctionIndex)
    {
        auto& subroutine = program.getSubroutine("EmitShadowMapGeometry");

        switch (SUB)
        {
        case LightEnum::Directional:
            oldFunctionIndex = subroutine.SelectFunction("EmitDirLightShadows");
            break;

        case LightEnum::Point:
            oldFunctionIndex = subroutine.SelectFunction("EmitPointLightShadows");
            break;

        case LightEnum::Spot:
            oldFunctionIndex = subroutine.SelectFunction("EmitSpotLightShadows");
            break;

        default:
            oldFunctionIndex = subroutine.ActiveFunctionIndex();
        }

        return subroutine;
    }

    // ==================================================================

    inline std::shared_ptr<engine::MetaProgram>& SimpleColoredProgram()
    {
        return load_MetaProgram("assets/programs/SimpleColoredProgram.xml");
    }

    inline std::shared_ptr<engine::MetaProgram>& ShadowRenderProgram()
    {
        return load_MetaProgram("assets/programs/ShadowRenderProgram.xml");
    }

    inline std::shared_ptr<engine::MetaProgram>& SkyboxProgram()
    {
        return load_MetaProgram("assets/programs/SkyboxProgram.xml");
    }

    inline std::shared_ptr<engine::MetaProgram>& MonochromeLightsShadowmapsProgram()
    {
        return load_MetaProgram("assets/programs/MonochromeLightsShadowmapsProgram.xml");
    }

    inline std::shared_ptr<engine::MetaProgram>& Texture2dLightsShadowmapsProgram()
    {
        return load_MetaProgram("assets/programs/Texture2dLightsShadowmapsProgram.xml");
    }

    inline std::shared_ptr<engine::MetaProgram>& CubemapLightsShadowmapsProgram()
    {
        return load_MetaProgram("assets/programs/CubemapLightsShadowmapsProgram.xml");
    }
}}