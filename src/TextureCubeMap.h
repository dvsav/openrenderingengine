/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <algorithm>

#include "GLEWHeaders.h"
#include "OpenGLTypes.h"

#include "Requires.h"
#include "Texture.h"

namespace opengl
{
    class TextureCubemap : public Texture
    {
        #pragma region Nested Types

        public:
            enum class CUBE_FACE : GLenum
            {
                POSITIVE_X = GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                NEGATIVE_X = GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                POSITIVE_Y = GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                NEGATIVE_Y = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                POSITIVE_Z = GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                NEGATIVE_Z = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
            };

        #pragma endregion

        #pragma region Constructors

        public:
            TextureCubemap(
                GLint mipmap_levels_number,
                INTERNAL_FORMAT internalFormat,
                GLsizei width,
                GLsizei height,
                PIXEL_FORMAT format,
                TYPE type) :

                Texture(
                    TARGET::TEXTURE_CUBE_MAP,
                    internalFormat,
                    format,
                    type,
                    mipmap_levels_number,
                    width,
                    height)
            {

                Binding binding(*this);

#ifdef OPENGL_4
                glTexStorage2D(
                    static_cast<GLenum>(getTarget()),
                    mipmap_levels_number,
                    static_cast<GLint>(internalFormat),
                    width,
                    height);
#else
                GLenum faces[6] =
                {
                    GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                    GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                    GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
                    GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                    GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
                    GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
                };

                for (GLenum face : faces)
                {
                    for (int mipmap_level = 0; mipmap_level < mipmap_levels_number; mipmap_level++)
                    {
                        glTexImage2D(
                            /*target*/ face,
                            /*level*/ mipmap_level,
                            /*internalFormat*/ static_cast<GLint>(internalFormat),
                            /*width*/ max(1, width / (1 << mipmap_level)),
                            /*height*/ max(1, height / (1 << mipmap_level)),
                            /*border*/ 0,
                            /*format*/ static_cast<GLenum>(format),
                            /*type*/ static_cast<GLenum>(type),
                            /*data*/ nullptr);
                    }
                }
#endif
            }

        #pragma endregion

        #pragma region Methods

        public:
            // Sets texture image.
            void setImage(
                CUBE_FACE face,
                GLint mipmap_level,
                const GLvoid* data)
            {
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);

                Binding binding(*this);

                glTexSubImage2D(
                    /*target*/  static_cast<GLenum>(face),
                    /*level*/   mipmap_level,
                    /*xoffset*/ 0,
                    /*yoffset*/ 0,
                    /*width*/   std::max(1, (getWidth() / (1 << mipmap_level))),
                    /*height*/  std::max(1, (getHeight() / (1 << mipmap_level))),
                    /*format*/  static_cast<GLenum>(getPixelFormat()),
                    /*type*/    static_cast<GLenum>(getType()),
                    /*pixels*/  data);
            }

            // Sets texture subimage.
            void setSubImage(
                CUBE_FACE face,
                GLint mipmap_level,
                GLint xoffset,
                GLint yoffset,
                GLsizei width,
                GLsizei height,
                const GLvoid* data)
            {
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);
                util::Requires::ArgumentPositive(xoffset, "xoffset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(yoffset, "yoffset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(width, "width", FUNCTION_INFO);
                util::Requires::ArgumentPositive(height, "height", FUNCTION_INFO);
                util::Requires::That(width + xoffset <= std::max(1, (getWidth() / (1 << mipmap_level))), FUNCTION_INFO);
                util::Requires::That(height + yoffset <= std::max(1, (getHeight() / (1 << mipmap_level))), FUNCTION_INFO);

                Binding binding(*this);

                glTexSubImage2D(
                    static_cast<GLenum>(face),
                    mipmap_level,
                    xoffset,
                    yoffset,
                    width,
                    height,
                    static_cast<GLenum>(getPixelFormat()),
                    static_cast<GLenum>(getType()),
                    data);
            }

            // Gets texture image.
            void getImage(
                CUBE_FACE cubeFace,
                GLint mipmapLevel,
                GLvoid* buffer) const
            {
                Binding binding(const_cast<TextureCubemap&>(*this));

                glGetTexImage(
                    static_cast<GLenum>(cubeFace),
                    mipmapLevel,
                    static_cast<GLenum>(getPixelFormat()),
                    static_cast<GLenum>(getType()),
                    buffer);
            }

        #pragma endregion
    };

    /*
    Makes TextureCubemap object.
    Texture data are taken from general purpose memory in certain [format] and
    stored in GPU memory in RGBA32F format.
    */
    inline TextureCubemap make_TextureCubemap_RGBA32F(
        GLsizei width,
        GLsizei height,
        PIXEL_FORMAT format,
        const GLubyte* pos_x,
        const GLubyte* neg_x,
        const GLubyte* pos_y,
        const GLubyte* neg_y,
        const GLubyte* pos_z,
        const GLubyte* neg_z)
    {
        TextureCubemap texture(
            static_cast<GLint>(1 + floor(log2((float)std::max(width, height)))),
            INTERNAL_FORMAT::RGBA32F,
            width,
            height,
            format,
            TYPE::UNSIGNED_BYTE);

        texture.setImage(TextureCubemap::CUBE_FACE::POSITIVE_X, 0, pos_x);
        texture.setImage(TextureCubemap::CUBE_FACE::NEGATIVE_X, 0, neg_x);
        texture.setImage(TextureCubemap::CUBE_FACE::POSITIVE_Y, 0, pos_y);
        texture.setImage(TextureCubemap::CUBE_FACE::NEGATIVE_Y, 0, neg_y);
        texture.setImage(TextureCubemap::CUBE_FACE::POSITIVE_Z, 0, pos_z);
        texture.setImage(TextureCubemap::CUBE_FACE::NEGATIVE_Z, 0, neg_z);

        texture.GenerateMipmap();
        return texture;
    }
}