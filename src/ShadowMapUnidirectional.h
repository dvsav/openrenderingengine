/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ShadowMap.h"
#include "Texture2D.h"
#include "Texture2DArray.h"
#include "TextureCubeMapArray.h"

#include "File.h"

namespace engine
{
    class ShadowMapUnidirectional : public ShadowMap
    {
        #pragma region Constructors

        public:
            ShadowMapUnidirectional(
                GLsizei width,
                GLsizei height) :

                ShadowMap(
                    width,
                    height,
                    std::make_shared<opengl::Texture2D>(
                        /*mipmap_levels_number*/ 1,
                        /*internalFormat*/ opengl::INTERNAL_FORMAT::DEPTH_COMPONENT24,
                        /*width*/ width,
                        /*height*/ height,
                        /*format*/ opengl::PIXEL_FORMAT::DEPTH_COMPONENT,
                        /*type*/ opengl::TYPE::FLOAT))
            { }

            ShadowMapUnidirectional(
                GLsizei width,
                GLsizei height,
                std::shared_ptr<opengl::Texture2DArray> depthTexture) :

                ShadowMap(
                    width,
                    height,
                    depthTexture)
            { }

        #pragma endregion
    };

    inline void SaveDepthTexture(
        const opengl::Texture2D& depthTexture,
        const std::string& filename)
    {
        GLsizei width = depthTexture.getWidth();
        GLsizei height = depthTexture.getHeight();

        std::unique_ptr<GLfloat[]> depth(new GLfloat[width * height]);

        depthTexture.getImage(0, depth.get());

        std::unique_ptr<unsigned char[]> pixels(new unsigned char[width * height * 3]);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                unsigned char d = static_cast<unsigned char>(depth[x + y * width] * 255);
                pixels[(x + y * width) * 3 + 0] = d;
                pixels[(x + y * width) * 3 + 1] = d;
                pixels[(x + y * width) * 3 + 2] = d;
            }
        }

        win::WriteBMP24File(
            /*filename*/ filename,
            /*width*/ width,
            /*height*/ height,
            /*pixels*/ reinterpret_cast<char*>(pixels.get()));
    }

    inline void SaveDepthTexture(
        opengl::TextureCubemap::CUBE_FACE cubeFace,
        const opengl::TextureCubemap& depthTexture,
        const std::string& filename)
    {
        GLsizei width = depthTexture.getWidth();
        GLsizei height = depthTexture.getHeight();

        std::unique_ptr<GLfloat[]> depth(new GLfloat[width * height]);

        depthTexture.getImage(cubeFace, 0, depth.get());

        std::unique_ptr<unsigned char[]> pixels(new unsigned char[width * height * 3]);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                unsigned char d = static_cast<unsigned char>(depth[x + y * width] * 255);
                pixels[(x + y * width) * 3 + 0] = d;
                pixels[(x + y * width) * 3 + 1] = d;
                pixels[(x + y * width) * 3 + 2] = d;
            }
        }

        win::WriteBMP24File(
            /*filename*/ filename,
            /*width*/ width,
            /*height*/ height,
            /*pixels*/ reinterpret_cast<char*>(pixels.get()));
    }

    inline void SaveDepthTexture(
        const opengl::Texture2DArray& depthTexture,
        const std::string& filename)
    {
        GLsizei width = depthTexture.getWidth() * depthTexture.getLayersNumber();
        GLsizei height = depthTexture.getHeight();

        std::unique_ptr<GLfloat[]> depth(new GLfloat[width * height]);

        depthTexture.getImage(0, depth.get());

        std::unique_ptr<unsigned char[]> pixels(new unsigned char[width * height * 3]);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                unsigned char d = static_cast<unsigned char>(depth[x + y * width] * 255);
                pixels[(x + y * width) * 3 + 0] = d;
                pixels[(x + y * width) * 3 + 1] = d;
                pixels[(x + y * width) * 3 + 2] = d;
            }
        }

        win::WriteBMP24File(
            /*filename*/ filename,
            /*width*/ width,
            /*height*/ height,
            /*pixels*/ reinterpret_cast<char*>(pixels.get()));
    }

    inline void SaveDepthTexture(
        const opengl::TextureCubemapArray& depthTexture,
        const std::string& filename)
    {
        GLsizei width = depthTexture.getWidth() * depthTexture.getLayersNumber();
        GLsizei height = depthTexture.getHeight() * 6;

        std::unique_ptr<GLfloat[]> depth(new GLfloat[width * height]);

        depthTexture.getImage(0, depth.get());

        std::unique_ptr<unsigned char[]> pixels(new unsigned char[width * height * 3]);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                unsigned char d = static_cast<unsigned char>(depth[x + y * width] * 255);
                pixels[(x + y * width) * 3 + 0] = d;
                pixels[(x + y * width) * 3 + 1] = d;
                pixels[(x + y * width) * 3 + 2] = d;
            }
        }

        win::WriteBMP24File(
            /*filename*/ filename,
            /*width*/ width,
            /*height*/ height,
            /*pixels*/ reinterpret_cast<char*>(pixels.get()));
    }

    inline void SaveShadowmap(
        const opengl::Texture& depthTexture,
        const std::string& filepath)
    {
        switch (depthTexture.getTarget())
        {
            case opengl::Texture::TARGET::TEXTURE_2D:
            {
                SaveDepthTexture((const opengl::Texture2D&)depthTexture, filepath);
                break;
            }
            case opengl::Texture::TARGET::TEXTURE_CUBE_MAP:
            {
                auto filename = util::file::getFileName(filepath);

                SaveDepthTexture(opengl::TextureCubemap::CUBE_FACE::POSITIVE_X, (const opengl::TextureCubemap&)depthTexture, util::file::ReplaceFileName(filepath, "POSITIVE_X_" + filename));
                SaveDepthTexture(opengl::TextureCubemap::CUBE_FACE::NEGATIVE_X, (const opengl::TextureCubemap&)depthTexture, util::file::ReplaceFileName(filepath, "NEGATIVE_X_" + filename));
                SaveDepthTexture(opengl::TextureCubemap::CUBE_FACE::POSITIVE_Y, (const opengl::TextureCubemap&)depthTexture, util::file::ReplaceFileName(filepath, "POSITIVE_Y_" + filename));
                SaveDepthTexture(opengl::TextureCubemap::CUBE_FACE::NEGATIVE_Y, (const opengl::TextureCubemap&)depthTexture, util::file::ReplaceFileName(filepath, "NEGATIVE_Y_" + filename));
                SaveDepthTexture(opengl::TextureCubemap::CUBE_FACE::POSITIVE_Z, (const opengl::TextureCubemap&)depthTexture, util::file::ReplaceFileName(filepath, "POSITIVE_Z_" + filename));
                SaveDepthTexture(opengl::TextureCubemap::CUBE_FACE::NEGATIVE_Z, (const opengl::TextureCubemap&)depthTexture, util::file::ReplaceFileName(filepath, "NEGATIVE_Z_" + filename));
                break;
            }
            case opengl::Texture::TARGET::TEXTURE_2D_ARRAY:
            {
                SaveDepthTexture((const opengl::Texture2DArray&)depthTexture, filepath);
                break;
            }
            case opengl::Texture::TARGET::TEXTURE_CUBE_MAP_ARRAY:
            {
                SaveDepthTexture((const opengl::TextureCubemapArray&)depthTexture, filepath);
                break;
            }
            default:
            {
                SaveDepthTexture((const opengl::Texture2D&)depthTexture, filepath);
                break;
            }
        }
    }
}