/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "UniformBlockBuffer.h"

#include "Camera.h"

namespace engine
{
    struct CameraInfo
    {
        mat4 WorldToScreen;
        vec3 CameraPosition;
    };

    class UniformBlock_CameraInfo : public opengl::UniformBlockStructure<CameraInfo>
    {
        #pragma region Type Aliases

        public:
            using TWorldToScreen = decltype(CameraInfo::WorldToScreen);
            using TCameraPosition = decltype(CameraInfo::CameraPosition);

        #pragma endregion

        #pragma region Constructors

        public:
            UniformBlock_CameraInfo() :
                opengl::UniformBlockStructure<CameraInfo>(
                    {
                        vmath::make_IdentityMatrix<GLfloat, 4>(),
                        vec3(0.0f, 0.0f, 0.0f)
                    },
                    opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void SetupCamera(const Camera& camera)
            {
                setWorldToScreen(
                    camera.getCameraToScreenMatrix() * camera.getWorldToCameraMatrix());

                setCameraPosition(
                    camera.PositionAndOrientation().Position().vec3());
            }

            void setWorldToScreen(const TWorldToScreen& value)
            {
                this->setData(
                    /*offset*/ offsetof(CameraInfo, CameraInfo::WorldToScreen),
                    /*size*/ sizeof(TWorldToScreen),
                    /*data*/ value.data());
            }

            void setCameraPosition(const TCameraPosition& value)
            {
                this->setData(
                    /*offset*/ offsetof(CameraInfo, CameraInfo::CameraPosition),
                    /*size*/ sizeof(TCameraPosition),
                    /*data*/ value.data());
            }

        #pragma endregion
    };
}