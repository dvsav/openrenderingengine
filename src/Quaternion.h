/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Matrix.h"

namespace vmath
{
    template<typename T>
    class Quaternion
    {
    private:
        Vector<T> qv; // vector part of the quaternion
        T qs; // scalar part of the quaternion

    public:
        Quaternion() noexcept
            : qv(), qs()
        {}

        Quaternion(Vector<T> qv, T qs) noexcept
            : qv(qv), qs(qs)
        {}

        Quaternion(T x, T y, T z, T w) noexcept
            : qv(x, y, z), qs(w)
        {}

        const Vector<T>& vector() const noexcept { return qv; }
        Vector<T>& vector() noexcept { return qv; }
        
        T scalar() const noexcept { return qs; }
        T& scalar() noexcept { return qs; }

        Quaternion conjugate() const noexcept
        {
            return Quaternion(-qv, qs);
        }

        T squared_magnitude() const noexcept
        {
            return qv.X() * qv.X() + qv.Y() * qv.Y() + qv.Z() * qv.Z() + qs * qs;
        }

        T length() const noexcept
        {
            return sqrt(squared_magnitude());
        }

        Quaternion inverse() const noexcept
        {
            return conjugate() / squared_magnitude();
        }

        // Transforms quaternion to rotation matrix
        Matrix<T, 3, 3> ToRotationMatrix()
        {
            return Matrix<T, 3, 3>
            {
                { T(1.0) - T(2.0) * (qv.Y() * qv.Y() + qv.Z() * qv.Z()), T(2.0) * (qv.X() * qv.Y() + qv.Z() * qs), T(2.0) * (qv.X() * qv.Z() - qv.Y() * qs) }, // column 1
                { T(2.0) * (qv.X() * qv.Y() - qv.Z() * qs), T(1.0) - T(2.0) * (qv.X() * qv.X() + qv.Z() * qv.Z()), T(2.0) * (qv.Y() * qv.Z() + qv.X() * qs) }, // column 2
                { T(2.0) * (qv.X() * qv.Z() + qv.Y() * qs), T(2.0) * (qv.Y() * qv.Z() - qv.X() * qs), T(1.0) - T(2.0) * (qv.X() * qv.X() + qv.Y() * qv.Y()) }  // column 3
            };
        }
    };

    template<typename T>
    std::ostream& operator<<(
        std::ostream& os,
        const Quaternion<T>& q)
    {
        return os << q.vector() << " " << q.scalar();
    }

    /*
    Makes a rotation quaternion.
    The direction of rotation follows the right-hand rule.
    Parameters:
    [axis] - a vector of unit length representing the axis of rotation.
    [angle] - the angle of rotation in radians.
    */
    template<typename T>
    Quaternion<T> make_RotationQuaternion(
        Vector<T, 3> axis,
        double angle) noexcept
    {
        auto sine = sin(angle / 2);
        return Quaternion<T>(
            axis.X() * sine,
            axis.Y() * sine,
            axis.Z() * sine,
            cos(angle / 2));
    }

    /*
    Calculates Grassman product of two quaternions.
    */
    template<typename T>
    Quaternion<T> operator*(
        const Quaternion<T>& q,
        const Quaternion<T>& p) noexcept
    {
        return Quaternion<T>(
            (q.scalar() * p.vector() + p.scalar() * q.vector() + cross_product(q.vector(), p.vector())) * (q.scalar() * p.scalar() - dot_product(q.vector(), p.vector())));
    }

    template<typename T>
    T dot_product(
        const Quaternion<T>& q,
        const Quaternion<T>& p) noexcept
    {
        return dot_product(q.vector(), p.vector()) + q.scalar() * q.scalar();
    }

    template<typename T, typename V>
    Quaternion<T> operator*(
        const Quaternion<T>& lhs,
        const V rhs) noexcept
    {
        return Quaternion<T>(lhs.vector() * rhs, lhs.scalar() * rhs);
    }

    template<typename T, typename V>
    Quaternion<T> operator*(
        const V lhs,
        const Quaternion<T>& rhs) noexcept
    {
        return Quaternion<T>(lhs * rhs.vector(), lhs * rhs.scalar());
    }

    template<typename T, typename V>
    Quaternion<T> operator/(
        const Quaternion<T>& lhs,
        const V rhs) noexcept
    {
        return Quaternion<T>(lhs.vector() / rhs, lhs.scalar() / rhs);
    }

    template<typename T>
    Quaternion<T> operator+(
        const Quaternion<T>& lhs,
        const Quaternion<T>& rhs) noexcept
    {
        return Quaternion<T>(lhs.vector() + rhs.vector(), lhs.scalar() + rhs.scalar());
    }

    /*
    Rotates vector [v] with quaternion [q].
    Quaternion [q] must be a rotation quaternion (see make_RotationQuaternion).
    */
    template<typename T>
    Vector<T, 3> rotate(
        const Vector<T, 3>& v,
        const Quaternion<T>& q) noexcept
    {
        return (q * Quaternion<T>(v, 0) * q.conjugate()).vector();
    }

    template<typename T>
    Quaternion<T> normalize(const Quaternion<T>& q) noexcept
    {
        return q / q.length();
    }

    /*
    Performs linear interpolation (LERP) between two quaternions: [first] and [second].
    [fraction] must be between 0 and 1.
    */
    template<typename T>
    Quaternion<T> Lerp(
        const Quaternion<T>& first,
        const Quaternion<T>& second,
        double fraction) noexcept
    {
        return (first * fraction + second * (1.0 - fraction)).normalize();
    }

    /*
    Performs spherical interpolation (SLERP) between two quaternions: [first] and [second].
    [fraction] must be between 0 and 1.
    */
    template<typename T>
    Quaternion<T> Slerp(
        const Quaternion<T>& first,
        const Quaternion<T>& second,
        double fraction) noexcept
    {
        auto cosine = dot_product(first, second);
        auto theta = acos(cosine);
        auto sine = sin(theta);
        return (sin((1-fraction) * theta)) / sine) * first + (sin(fraction * theta) / sine) * second;
    }
}

