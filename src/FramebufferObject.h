/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <vector>
#include <functional>

#include "GLEWHeaders.h"
#include "GLenums.h"
#include "OpenGLTypes.h"
#include "Renderbuffer.h"
#include "Requires.h"

namespace opengl
{
    class FramebufferObject final
    {
        #pragma region Nested Types

        public:
            enum class TARGET : GLenum
            {
                DRAW = GL_DRAW_FRAMEBUFFER,
                READ = GL_READ_FRAMEBUFFER,
                READ_WRITE = GL_FRAMEBUFFER
            };

            enum class COLOR_ATTACHMENT : GLenum
            {
                NONE = GL_NONE,
                COLOR0 = GL_COLOR_ATTACHMENT0,
                COLOR1 = GL_COLOR_ATTACHMENT1,
                COLOR2 = GL_COLOR_ATTACHMENT2,
                COLOR3 = GL_COLOR_ATTACHMENT3,
                COLOR4 = GL_COLOR_ATTACHMENT4,
                COLOR5 = GL_COLOR_ATTACHMENT5,
                COLOR6 = GL_COLOR_ATTACHMENT6,
                COLOR7 = GL_COLOR_ATTACHMENT7,
                COLOR8 = GL_COLOR_ATTACHMENT8,
                COLOR9 = GL_COLOR_ATTACHMENT9,
                COLOR10 = GL_COLOR_ATTACHMENT10,
                COLOR11 = GL_COLOR_ATTACHMENT11,
                COLOR12 = GL_COLOR_ATTACHMENT12,
                COLOR13 = GL_COLOR_ATTACHMENT13,
                COLOR14 = GL_COLOR_ATTACHMENT14,
                COLOR15 = GL_COLOR_ATTACHMENT15,
            };

            enum class ATTACHMENT : GLenum
            {
                NONE = GL_NONE,
                COLOR0 = GL_COLOR_ATTACHMENT0,
                COLOR1 = GL_COLOR_ATTACHMENT1,
                COLOR2 = GL_COLOR_ATTACHMENT2,
                COLOR3 = GL_COLOR_ATTACHMENT3,
                COLOR4 = GL_COLOR_ATTACHMENT4,
                COLOR5 = GL_COLOR_ATTACHMENT5,
                COLOR6 = GL_COLOR_ATTACHMENT6,
                COLOR7 = GL_COLOR_ATTACHMENT7,
                COLOR8 = GL_COLOR_ATTACHMENT8,
                COLOR9 = GL_COLOR_ATTACHMENT9,
                COLOR10 = GL_COLOR_ATTACHMENT10,
                COLOR11 = GL_COLOR_ATTACHMENT11,
                COLOR12 = GL_COLOR_ATTACHMENT12,
                COLOR13 = GL_COLOR_ATTACHMENT13,
                COLOR14 = GL_COLOR_ATTACHMENT14,
                COLOR15 = GL_COLOR_ATTACHMENT15,
                DEPTH = GL_DEPTH_ATTACHMENT,
                STENCIL = GL_STENCIL_ATTACHMENT,
                DEPTH_STENCIL = GL_DEPTH_STENCIL_ATTACHMENT
            };

            enum class STATUS : GLenum
            {
                COMPLETE = GL_FRAMEBUFFER_COMPLETE,
                UNDEFINED = GL_FRAMEBUFFER_UNDEFINED,
                INCOMPLETE_ATTACHMENT = GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT,
                INCOMPLETE_MISSING_ATTACHMENT = GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT,
                INCOMPLETE_DRAW_BUFFER = GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER,
                INCOMPLETE_READ_BUFFER = GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER,
                UNSUPPORTED = GL_FRAMEBUFFER_UNSUPPORTED,
                INCOMPLETE_MULTISAMPLE = GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE,
                INCOMPLETE_LAYER_TARGETS = GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS
            };

            enum class FILTER : GLenum
            {
                NEAREST = GL_NEAREST,
                LINEAR = GL_LINEAR
            };

            class Binding final
            {
                private:
                    TARGET m_Target;
                    GLuint m_PreviouslyBoundFBO;

                private:
                    Binding(const Binding&) = delete;
                    Binding& operator=(const Binding&) = delete;

                public:
                    Binding(
                        FramebufferObject& framebuffer,
                        TARGET target) :
                        m_Target(target),
                        m_PreviouslyBoundFBO(FramebufferObject::getCurrentlyBoundFBOHandle(target))
                    {
                        framebuffer.Bind(target);
                    }

                    ~Binding()
                    {
                        glBindFramebuffer(
                            static_cast<GLenum>(m_Target),
                            m_PreviouslyBoundFBO);
                    }

                    TARGET getTarget() const { return m_Target; }
            };

        #pragma endregion

        #pragma region Fields

        private:
            GLuint m_Handle;

        #pragma endregion

        #pragma region Constructors

        public:
            FramebufferObject() :
                m_Handle(0)
            {
#ifdef OPENGL_4_5
                glCreateFramebuffers(1, &m_Handle);
#else
                glGenFramebuffers(1, &m_Handle);
                Binding bind(*this, TARGET::READ_WRITE);
#endif
            }

            // Move constructor.
            FramebufferObject(FramebufferObject&& value) noexcept :
                m_Handle(value.m_Handle)
            {
                value.m_Handle = 0;
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            FramebufferObject(const FramebufferObject& value) = delete;
            FramebufferObject& operator=(const FramebufferObject&) = delete;

        #pragma endregion

        #pragma region Destructor

        public:
            ~FramebufferObject()
            {
                if (m_Handle == 0) return;
                glDeleteFramebuffers(1, &m_Handle);
            }

        #pragma endregion

        #pragma region Properties

        public:
            static GLuint getCurrentlyBoundFBOHandle(TARGET target)
            {
                GLint current_fbo;
                GLenum pname = GL_FRAMEBUFFER_BINDING;

                switch (target)
                {
                    case TARGET::DRAW:
                        pname = GL_DRAW_FRAMEBUFFER_BINDING;
                        break;
                    case TARGET::READ:
                        pname = GL_READ_FRAMEBUFFER_BINDING;
                        break;
                }

                glGetIntegerv(
                    pname,
                    &current_fbo);

                return current_fbo;
            }

            GLuint getHandle() { return m_Handle; }

            void setDefaultWidth(GLint value)
            {
                util::Requires::ArgumentNotNegative(value, "value", FUNCTION_INFO);

#ifdef OPENGL_4_5
                glNamedFramebufferParameteri(
                    m_Handle,
                    GL_FRAMEBUFFER_DEFAULT_WIDTH,
                    value);
#else
                Binding bind(*this, TARGET::READ_WRITE);
                glFramebufferParameteri(
                    static_cast<GLenum>(bind.getTarget()),
                    GL_FRAMEBUFFER_DEFAULT_WIDTH,
                    value);
#endif
            }

            void setDefaultHeight(GLint value)
            {
                util::Requires::ArgumentNotNegative(value, "value", FUNCTION_INFO);

#ifdef OPENGL_4_5
                glNamedFramebufferParameteri(
                    m_Handle,
                    GL_FRAMEBUFFER_DEFAULT_HEIGHT,
                    value);
#else
                Binding bind(*this, TARGET::READ_WRITE);
                glFramebufferParameteri(
                    static_cast<GLenum>(bind.getTarget()),
                    GL_FRAMEBUFFER_DEFAULT_HEIGHT,
                    value);
#endif
            }

            void setDefaultLayers(GLint value)
            {
                util::Requires::ArgumentNotNegative(value, "value", FUNCTION_INFO);

#ifdef OPENGL_4_5
                glNamedFramebufferParameteri(
                    m_Handle,
                    GL_FRAMEBUFFER_DEFAULT_LAYERS,
                    value);
#else
                Binding bind(*this, TARGET::READ_WRITE);
                glFramebufferParameteri(
                    static_cast<GLenum>(bind.getTarget()),
                    GL_FRAMEBUFFER_DEFAULT_LAYERS,
                    value);
#endif
            }

            void setDefaultSamples(GLint value)
            {
                util::Requires::ArgumentNotNegative(value, "value", FUNCTION_INFO);

#ifdef OPENGL_4_5
                glNamedFramebufferParameteri(
                    m_Handle,
                    GL_FRAMEBUFFER_DEFAULT_SAMPLES,
                    value);
#else
                Binding bind(*this, TARGET::READ_WRITE);
                glFramebufferParameteri(
                    static_cast<GLenum>(bind.getTarget()),
                    GL_FRAMEBUFFER_DEFAULT_SAMPLES,
                    value);
#endif
            }

            void setDefaultFixedSampleLocations(bool value)
            {
#ifdef OPENGL_4_5
                glNamedFramebufferParameteri(
                    m_Handle,
                    GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS,
                    value ? 1 : 0);
#else
                Binding bind(*this, TARGET::READ_WRITE);
                glFramebufferParameteri(
                    static_cast<GLenum>(bind.getTarget()),
                    GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS,
                    value ? 1 : 0);
#endif
            }

            STATUS getStatus(TARGET target) const
            {
#ifdef OPENGL_4_5
                return static_cast<STATUS>(glCheckNamedFramebufferStatus(m_Handle, static_cast<GLenum>(target)));
#else
                Binding bind(const_cast<FramebufferObject&>(*this), target);
                return static_cast<STATUS>(glCheckFramebufferStatus(static_cast<GLenum>(target)));
#endif
            }

        #pragma endregion

        #pragma region Methods

        public:
            // Binds framebuffer to a framebuffer target.
            void Bind(TARGET target)
            {
                glBindFramebuffer(
                    static_cast<GLenum>(target),
                    m_Handle);
            }

            void Invalidate(std::vector<ATTACHMENT> attachments)
            {
                Binding bind_framebuffer(*this, TARGET::READ_WRITE);

                glInvalidateFramebuffer(
                    /*target*/ static_cast<GLenum>(bind_framebuffer.getTarget()),
                    /*numAttachments*/ attachments.size(),
                    /*attachments*/ reinterpret_cast<GLenum*>(attachments.data()));
            }

            // Attaches a level of a texture object as a logical buffer of the framebuffer object.
            void AttachTexture(
                ATTACHMENT attachment,
                GLuint textureHandle,
                GLint mipmapLevel = 0)
            {
#ifdef OPENGL_4_5
                glNamedFramebufferTexture(
                    /*framebuffer*/ m_Handle,
                    /*attachment*/ static_cast<GLenum>(attachment),
                    /*texture*/ textureHandle,
                    /*level*/ mipmapLevel);
#else
                Binding bind_framebuffer(*this, TARGET::READ_WRITE);

                glFramebufferTexture(
                    /*target*/ static_cast<GLenum>(bind_framebuffer.getTarget()),
                    /*attachment*/ static_cast<GLenum>(attachment),
                    /*texture*/ textureHandle,
                    /*level*/ mipmapLevel);
#endif

                util::Requires::That(
                    getStatus(bind_framebuffer.getTarget()) == STATUS::COMPLETE,
                    FUNCTION_INFO);
            }

            // Detaches any texture from the specified attachment point of the framebuffer object.
            void DetachTexture(ATTACHMENT attachment)
            {
#ifdef OPENGL_4_5
                glNamedFramebufferTexture(
                    m_Handle,
                    static_cast<GLenum>(attachment),
                    0,
                    0);
#else
                Binding bind_framebuffer(*this, TARGET::READ_WRITE);

                glFramebufferTexture(
                    /*target*/ static_cast<GLenum>(bind_framebuffer.getTarget()),
                    /*attachment*/ static_cast<GLenum>(attachment),
                    /*texture*/ 0,
                    /*level*/ 0);
#endif
            }

            // Attaches a renderbuffer as a logical buffer of a framebuffer object.
            void AttachRenderbuffer(
                Renderbuffer& renderbuffer,
                ATTACHMENT attachment)
            {
#ifdef OPENGL_4_5
                glNamedFramebufferRenderbuffer(GLuint framebuffer,
                    /*attachment*/ static_cast<GLenum>(attachment),
                    /*renderbuffertarget*/ GL_RENDERBUFFER,
                    /*renderbuffer*/ renderbuffer.getHandle());
#else
                Binding bind_framebuffer(*this, TARGET::READ_WRITE);
                glFramebufferRenderbuffer(
                    /*target*/ static_cast<GLenum>(bind_framebuffer.getTarget()),
                    /*attachment*/ static_cast<GLenum>(attachment),
                    /*renderbuffertarget*/ GL_RENDERBUFFER,
                    /*renderbuffer*/ renderbuffer.getHandle());
#endif
                util::Requires::That(
                    getStatus(bind_framebuffer.getTarget()) == STATUS::COMPLETE,
                    FUNCTION_INFO);
            }

            // Detaches any renderbuffer attached to the specified attachment point.
            void DetachRenderbuffer(
                ATTACHMENT attachment)
            {
#ifdef OPENGL_4_5
                glNamedFramebufferRenderbuffer(GLuint framebuffer,
                    /*attachment*/ static_cast<GLenum>(attachment),
                    /*renderbuffertarget*/ GL_RENDERBUFFER,
                    /*renderbuffer*/ 0);
#else
                Binding bind_framebuffer(*this, TARGET::READ_WRITE);
                glFramebufferRenderbuffer(
                    /*target*/ static_cast<GLenum>(bind_framebuffer.getTarget()),
                    /*attachment*/ static_cast<GLenum>(attachment),
                    /*renderbuffertarget*/ GL_RENDERBUFFER,
                    /*renderbuffer*/ 0);
#endif
            }

            /*
            Performs rendering (via function [renderFunc]) into the framebuffer.
            [attachments] is a collection of constants specifying the Renderbuffer objects to which
            fragment shader's output variables are written. This [attachments] collection usually
            contains just one element - opengl::FramebufferObject::COLOR_ATTACHMENT::COLOR0 so as
            to render a scene to the Renderbuffer attached to the COLOR0 attachment point.
            */
            void Render(
                std::vector<COLOR_ATTACHMENT> attachments,
                std::function<void()> renderFunc)
            {
                {
#ifdef OPENGL_4_5
                    glNamedFramebufferDrawBuffers(
                        m_Handle,
                        attachments.size(),
                        reinterpret_cast<GLenum*>(attachments.data()));
#else
                    Binding bind_framebuffer(*this, TARGET::DRAW);

                    glDrawBuffers(
                        attachments.size(),
                        reinterpret_cast<GLenum*>(attachments.data()));
#endif

                    renderFunc();
                }
            }

            /*
            Performs rendering (via function [renderFunc]) into the framebuffer.
            [attachment] is a constant specifying the Renderbuffer object to which
            fragment shader's output variables are written. This [attachment] is usually
            the opengl::FramebufferObject::COLOR_ATTACHMENT::COLOR0 so that the scene is
            rendered to the Renderbuffer attached to the COLOR0 attachment point.
            */
            void Render(
                COLOR_ATTACHMENT attachment,
                std::function<void()> renderFunc)
            {
                {
#ifdef OPENGL_4_5
                    glNamedFramebufferDrawBuffers(
                        m_Handle,
                        1,
                        reinterpret_cast<GLenum*>(&attachment));
#else
                    Binding bind_framebuffer(*this, TARGET::DRAW);

                    glDrawBuffers(
                        1,
                        reinterpret_cast<GLenum*>(&attachment));
#endif

                    renderFunc();
                }
            }

            // Copies a block of pixels from the framebuffer object object to the default framebuffer.
            void CopyImageToDefaultFramebuffer(
                FRAMEBUFFER_MASK mask,
                FILTER filter,
                GLint srcX,
                GLint srcY,
                GLint srcWidth,
                GLint srcHeight,
                GLint dstX,
                GLint dstY,
                GLint dstWidth,
                GLint dstHeight)
            {
#ifdef OPENGL_4_5
                glBlitNamedFramebuffer(
                    /*readFramebuffer*/ m_Handle,
                    /*drawFramebuffer*/ 0,
                    /*srcX0*/ srcX,
                    /*srcY0*/ srcY,
                    /*srcX1*/ srcX + srcWidth,
                    /*srcY1*/ srcY + srcHeight,
                    /*dstX0*/ dstX,
                    /*dstY0*/ dstY,
                    /*dstX1*/ dstX + dstWidth,
                    /*dstY1*/ dstY + dstHeight,
                    /*mask*/ static_cast<GLbitfield>(mask),
                    /*filter*/ static_cast<GLenum>(filter));
#else
                Binding bind_framebuffer(*this, TARGET::READ);
                glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

                glBlitFramebuffer(
                    /*srcX0*/ srcX,
                    /*srcY0*/ srcY,
                    /*srcX1*/ srcX + srcWidth,
                    /*srcY1*/ srcY + srcHeight,
                    /*dstX0*/ dstX,
                    /*dstY0*/ dstY,
                    /*dstX1*/ dstX + dstWidth,
                    /*dstY1*/ dstY + dstHeight,
                    /*mask*/ static_cast<GLbitfield>(mask),
                    /*filter*/ static_cast<GLenum>(filter));
#endif
            }

            /*
            Reads a block of pixels from the frame buffer into memory.
            Parameters:
            color_buffer - the frame buffer from which pixels are read.
            x, y - location is the lower left corner of a rectangular block of pixels.
            width, height - width and height of the rectangular block of pixels.
            format - the format of the pixel data (color layout of a pixel).
            component_type - data type for the store of color components in the pixel.
            data - memory buffer to which pixels are copied from the framebuffer.
            */
            inline void ReadPixels(
                COLOR_ATTACHMENT attachment,
                GLint x,
                GLint y,
                GLsizei width,
                GLsizei height,
                PIXEL_FORMAT format,
                TYPE component_type,
                GLvoid* data)
            {
                {
#ifdef OPENGL_4_5
                    glNamedFramebufferReadBuffer(
                        /*framebuffer*/ m_Handle,
                        /*mode*/ static_cast<GLenum>(attachment));
#else
                    Binding bind_framebuffer(*this, TARGET::READ);
                    glReadBuffer(static_cast<GLenum>(attachment));
#endif

                    glReadPixels(
                        x, y,
                        width, height,
                        static_cast<GLenum>(format),
                        static_cast<GLenum>(component_type),
                        data);
                }
            }

        #pragma endregion
    };
}