/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <sstream>

#include "OpenGLProgram.h"
#include "Requires.h"
#include "Vector.h"

namespace opengl
{
    /*
    Represents a separate program stage intended for use in program pipeline.
    */
    class ProgramStage : public Program
    {
        #pragma region Constructors

        public:
            /*
            Parameters:
            subroutines - std::get<0>() - shader stage; std::get<1>() - subroutine unifrom name; std::get<2>() - subroutine function name;
            */
            template<typename SHADERS_COLLECTION>
            ProgramStage(
                const SHADERS_COLLECTION& shaders,
                const std::vector<std::tuple<Shader::SHADER_TYPE, const char*, const char*>>& subroutines)

            {
                init(shaders, true);
                init_subroutines(subroutines);
            }

            template<typename SHADERS_COLLECTION>
            ProgramStage(const SHADERS_COLLECTION& shaders)
            {
                init(shaders, true);
            }

            // Example: opengl::ProgramStage program { &vertex_shader, &fragment_shader };
            ProgramStage(std::initializer_list<Shader*> shaders)
            {
                init(shaders, true);
            }

            // Move constructor
            ProgramStage(Program&& program) noexcept :
                Program(std::move(program))
            {
            }

        #pragma endregion
    };
}