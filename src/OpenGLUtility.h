/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <tuple>
#include <vector>

#include "OpenGLTypes.h"
#include "GLenums.h"

namespace opengl
{
    inline void ClearAllBuffers()
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    }

    struct DebugMessageStruct
    {
        GLenum source;
        GLenum type;
        GLuint id;
        GLenum severity;
        GLsizei length;
        const GLchar* message;
        const void* userParam;
    };

    using DEBUG_MESSAGE_CALLBACK = void(*)(const DebugMessageStruct& msg);

#ifdef OPENGL_4

    void setDebugMessageCallback(DEBUG_MESSAGE_CALLBACK callback);

    // Defines a barrier ordering memory transactions.
    inline void MemoryBarrier(BARRIERS barriers)
    {
        glMemoryBarrier(static_cast<GLbitfield>(barriers));
    }

#endif

    /*
    Gets OpenGL version supported by the current OpenGL rendering context.
    Returns a tuple where 0th element is the major version
    and 1st element is the minor version.
    */
    inline std::tuple<GLint, GLint> getOpenGLVersion()
    {
        GLint majorVersion, minorVersion;
        glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
        glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
        return std::tuple<GLint, GLint>{ majorVersion, minorVersion };
    }

    // Gets the name of the company responsible for the OpenGL implementation.
    inline std::string getVendor()
    {
        return std::string(reinterpret_cast<const char*>(glGetString(GL_VENDOR)));
    }

    // Gets the name of the hardware platform (video card).
    inline std::string getRenderer()
    {
        return std::string(reinterpret_cast<const char*>(glGetString(GL_RENDERER)));
    }

    // Gets the supported GLSL shader language version.
    inline std::string getShaderVersion()
    {
        return std::string(reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION)));
    }

    // Gets supported OpenGL extensions as an array of strings.
    inline std::vector<const char*> getExtensions()
    {
        GLint nExtensions;
        glGetIntegerv(GL_NUM_EXTENSIONS, &nExtensions);

        std::vector<const char*> extensions(nExtensions);

        for (int i = 0; i < nExtensions; i++)
            extensions[i] = reinterpret_cast<const char*>(glGetStringi(GL_EXTENSIONS, i));

        return extensions;
    }

    /*
    Sets up the depth test.
    Parameters:
    [zNear] - near z value in window space coordinates. Must be between 0 and [zFar].
    [zFar] - far z value in window space coordinates. Must be between [zNear] and 1.
    [testFunction] - one of the following: GL_LESS, GL_EQUAL, GL_LEQUAL, GL_GREATER, GL_NOTEQUAL, GL_GEQUAL, GL_ALWAYS. When the test is True the incoming fragment is written.
    Default values of parameters are arranged in assumption that Z-axis in camera space goes from the viewer deep into the screen.
    */
    inline void SetupDepthTest(
        GLclampd zNear = 0.0f,
        GLclampd zFar = 1.0f,
        COMPARISON_FUNCTION testFunction = COMPARISON_FUNCTION::LEQUAL)
    {
        glClearDepth(zFar);
        glDepthRange(zNear, zFar);
        glEnable(GL_DEPTH_TEST);
        glDepthMask(GL_TRUE);
        glDepthFunc(static_cast<GLenum>(testFunction));
    }

    inline GLdouble getClearDepth()
    {
        GLdouble value = 0.0;
        glGetDoublev(GL_DEPTH_CLEAR_VALUE, &value);
        return value;
    }

    /*
    Sets pixel storage modes.
    Parameters:
    [pname] - specifies the symbolic name of the parameter to be set.
    [param] - specifies the value that pname is set to.
    */
    inline void PixelStore(
        PIXEL_STORE pname,
        GLint param)
    {
        glPixelStorei(
            static_cast<GLenum>(pname),
            param);
    }

    /*
    Reads a block of pixels from the frame buffer into memory.
    Parameters:
    [color_buffer] - the frame buffer from which pixels are read.
    [x], [y] - location is the lower left corner of a rectangular block of pixels.
    [width], [height] - width and height of the rectangular block of pixels.
    [format] - the format of the pixel data (color layout of a pixel).
    [component_type] - data type for the store of color components in the pixel.
    [data] - memory buffer to which pixels are copied from the framebuffer.
    */
    inline void ReadPixels(
        COLOR_BUFFER color_buffer,
        GLint x,
        GLint y,
        GLsizei width,
        GLsizei height,
        PIXEL_FORMAT format,
        TYPE component_type,
        GLvoid* data)
    {
        glReadBuffer(static_cast<GLenum>(color_buffer));

        glReadPixels(
            x, y,
            width, height,
            static_cast<GLenum>(format),
            static_cast<GLenum>(component_type),
            data);
    }

    struct Rect
    {
        GLint x;
        GLint y;
        GLint width;
        GLint height;
    };

    inline Rect getViewportRect()
    {
        Rect viewport;
        glGetIntegerv(GL_VIEWPORT, reinterpret_cast<GLint*>(&viewport));
        return viewport;
    }

    inline CULL_FACE_MODE getCullFaceMode()
    {
        if (glIsEnabled(GL_CULL_FACE))
        {
            GLint mode;
            glGetIntegerv(GL_CULL_FACE_MODE, &mode);
            return static_cast<CULL_FACE_MODE>(mode);
        }
        else
            return CULL_FACE_MODE::DISABLED;
    }

    inline void setCullFaceMode(
        CULL_FACE_MODE mode)
    {
        if (mode == CULL_FACE_MODE::DISABLED)
            glDisable(GL_CULL_FACE);
        else
        {
            glEnable(GL_CULL_FACE);
            glCullFace(static_cast<GLenum>(mode));
        }
    }

/*
A macro, facilitating the invokation of glVertexAttribPointer function.
Parameters:
    [ATTRIB_LOCATION] - GLuint - shader vertex attribute location index (the [index] parameter of glVertexAttribPointer)
    [VERTEX]          - Vertex type (usually - a structure, containg vertex attributes like position, normal vector, etc.)
    [FIELD]           - Vertex field (the name of the field comprising a certain vertex attribute like position or normal, etc.)
    [NORMALIZED]      - GLboolean - the [normalized] parameter of glVertexAttribPointer
*/
#define glVertexAttribPointerWithFun(ATTRIB_LOCATION, VERTEX, FIELD, NORMALIZED) \
    glVertexAttribPointer( \
        /*index*/      (ATTRIB_LOCATION), \
        /*size*/       decltype(VERTEX::FIELD)::components_number, \
        /*type*/       static_cast<GLenum>(opengl::type_traits<decltype(VERTEX::FIELD)::value_type>::gl_type), \
        /*normalized*/ (NORMALIZED), \
        /*stride*/     sizeof(VERTEX), \
        /*pointer*/    (void*)offsetof(VERTEX, FIELD))
}