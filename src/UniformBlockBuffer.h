/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <functional>
#include <algorithm>

#include "Buffer.h"

namespace opengl
{
    /*
    OpenGL uniform block buffer. Uniform block buffers were designed specifically to
    ease the sharing of uniform data between shader programs.
    Shaders can only read data from a uniform block and cannot write to it.
    Example of a uniform block definition in a shader:
    layout (std140) uniform TransformBlock
    {
        mat4 ModelToWorldMatrix;
        mat4 WorldToCameraMatrix;
        mat4 PerspectiveProjectionMatrix;
    }
    Another example:
    layout (std140) uniform Material
    {
        vec3 AmbientColor;
        vec3 DiffuseColor;
        vec3 SpecularColor;
    }
    Note the "layout (std140)" qualifier - it's mandatory!
    */
    class UniformBlockBuffer : public Buffer
    {
        #pragma region Constructors

        public:
            UniformBlockBuffer(
                GLsizeiptr size,
                DRAW_USAGE usage,
                const void* data = nullptr) :

                Buffer(
                    size,
                    TARGET::UNIFORM,
                    usage,
                    data)
            {}

            // Move constructor.
            UniformBlockBuffer(UniformBlockBuffer&& value) noexcept :
                Buffer(std::move(value))
            {}

        #pragma endregion

        #pragma region Methods

        public:
            void BindToUniformBindingPoint(GLuint index)
            {
                glBindBufferBase(GL_UNIFORM_BUFFER, index, getHandle());
            }

        #pragma endregion
    };

    /*
    Represents a typified UniformBlockBuffer object.
    == Template parameters ==
    T - the type of data in the buffer.
    */
    template<typename T>
    class UniformBlockStructure : public UniformBlockBuffer
    {
        public:
            using value_type = T;

        public:
            UniformBlockStructure(
                const value_type& data,
                DRAW_USAGE usage) :
                
                UniformBlockBuffer(
                    /*size*/ sizeof(value_type),
                    /*usage*/ usage,
                    /*data*/ &data
                )
            {}

            // Move constructor.
            UniformBlockStructure(UniformBlockStructure&& value) noexcept :
                UniformBlockBuffer(std::move(value))
            {}

        public:
            value_type getValue() const
            {
                value_type value;
                this->getData(
                    /*offset*/ 0,
                    /*size*/ sizeof(value_type),
                    /*buffer*/ &value
                );
                return value;
            }

            void setValue(const value_type& value)
            {
                this->setData(
                    /*offset*/ 0,
                    /*size*/ sizeof(value_type),
                    /*buffer*/ &value
                );
            }

            void Change(std::function<void(value_type&)> changeFunction)
            {
                Mapping mapping(*this, ACCESS::READ_WRITE);
                changeFunction(*reinterpret_cast<value_type*>(mapping.getMappingPointer()));
            }
    };

    /*
    Represents a typified UniformBlockBuffer object
    that contains an array of structures.
    == Template parameters ==
    T - the type of data element in the array contained in the buffer.
    */
    template<typename T>
    class UniformBlockArray : public UniformBlockBuffer
    {
    public:
        using value_type = T;
        using count_type = GLint;

    public:
        UniformBlockArray(
            const std::vector<value_type>& data,
            DRAW_USAGE usage) :

            UniformBlockBuffer(
                /*size*/ getElementOffset(0) + sizeof(value_type) * data.size(),
                /*usage*/ usage)
        {
            setCount(data.size());
            if (data.size() > 0)
                setData(getElementOffset(0), sizeof(value_type) * data.size(), data.data());
        }

        // Move constructor.
        UniformBlockArray(UniformBlockArray&& value) noexcept :
            UniformBlockBuffer(std::move(value))
        {}

    public:
        count_type MaxCount() const
        {
            return (getSize() - getElementOffset(0)) / sizeof(value_type);
        }

        count_type getCount() const
        {
            count_type value = 0;
            getData(0, sizeof(value), &value);
            return value;
        }

        void setCount(count_type value)
        {
            util::Requires::That(value <= MaxCount(), FUNCTION_INFO);
            setData(0, sizeof(value), &value);
        }

        value_type getValue(count_type i) const
        {
            value_type value;
            this->getData(
                /*offset*/ getElementOffset(i),
                /*size*/ sizeof(value_type),
                /*buffer*/ &value
            );
            return value;
        }

        void setValue(count_type i, const value_type& value)
        {
            this->setData(
                /*offset*/ getElementOffset(i),
                /*size*/ sizeof(value_type),
                /*buffer*/ &value
            );
        }

        void Change(std::function<void(value_type*)> changeFunction)
        {
            Mapping mapping(*this, ACCESS::READ_WRITE);
            changeFunction(reinterpret_cast<value_type*>(mapping.getMappingPointer() + getElementOffset(0)));
        }

        static constexpr GLintptr getElementOffset(count_type i)
        {
            // 16 is the OpenGL's std140 alignment for structs
            return std::max<count_type>(16, sizeof(count_type)) + i * sizeof(value_type);
        }
    };
}