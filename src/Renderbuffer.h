/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "GLEWHeaders.h"
#include "GLenums.h"
#include "Requires.h"

namespace opengl
{
    class Renderbuffer final
    {
        #pragma region Nested Types

        public:
            class Binding final
            {
            private:
                Binding(const Binding&) = delete;
                Binding& operator=(const Binding&) = delete;

            public:
                Binding(Renderbuffer& renderbuffer) { renderbuffer.Bind(); }
                ~Binding() { glBindRenderbuffer(GL_RENDERBUFFER, 0); }
            };

        #pragma endregion

        #pragma region Fields

        private:
            GLuint m_Handle;

        #pragma endregion

        #pragma region Constructors

        public:
            Renderbuffer(
                INTERNAL_FORMAT internalFormat,
                GLsizei width,
                GLsizei height) :
                m_Handle(0)
            {
                util::Requires::ArgumentNotNegative(width, "width", FUNCTION_INFO);
                util::Requires::ArgumentNotNegative(height, "height", FUNCTION_INFO);

#ifdef OPENGL_4_5
                glCreateRenderbuffers(1, &m_Handle);

                glNamedRenderbufferStorage(
                    /*renderbuffer*/ m_Handle,
                    /*internalformat*/ static_cast<GLenum>(internalFormat),
                    /*width*/ width,
                    /*height*/ height);
#else
                glGenRenderbuffers(1, &m_Handle);
                Binding bind(*this);
                glRenderbufferStorage(
                    /*target*/ GL_RENDERBUFFER,
                    /*internalformat*/ static_cast<GLenum>(internalFormat),
                    /*width*/ width,
                    /*height*/ height);
#endif
            }

            // Move counstructor.
            Renderbuffer(Renderbuffer&& value) noexcept :
                m_Handle(value.m_Handle)
            {
                value.m_Handle = 0;
            }

            Renderbuffer(const Renderbuffer& value) = delete;

        #pragma endregion

        #pragma region Destructor

        public:
            ~Renderbuffer()
            {
                glDeleteRenderbuffers(1, &m_Handle);
            }

        #pragma endregion

        #pragma region Properties

        public:
            GLuint getHandle() { return m_Handle; }

        #pragma endregion

        #pragma region Methods

        public:
            Renderbuffer& operator=(const Renderbuffer& value) = delete;

            void Bind()
            {
                glBindRenderbuffer(GL_RENDERBUFFER, m_Handle);
            }

        #pragma endregion
    };
}