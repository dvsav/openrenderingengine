/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <memory>
#include <functional>

#include "OpenGLUtility.h"
#include "FramebufferObject.h"
#include "Texture.h"
#include "TextureSampler.h"
#include "BMP.h"

namespace engine
{
    class ShadowMap
    {
        #pragma region Fields

        private:
            GLint m_Width;
            GLint m_Height;
            opengl::FramebufferObject m_ShadowMapFbo;
            std::shared_ptr<opengl::Texture> m_TextureShadowMapDepth;
            std::shared_ptr<opengl::TextureSampler> m_ShadowTextureSampler;

        #pragma endregion

        #pragma region Constructors

        protected:
            ShadowMap(
                GLint width,
                GLint height,
                std::shared_ptr<opengl::Texture> depthTexture) :

                m_Width(width),
                m_Height(height),
                m_ShadowMapFbo(),
                m_TextureShadowMapDepth(depthTexture),
                m_ShadowTextureSampler(std::make_shared<opengl::TextureSampler>())
            {
                m_ShadowMapFbo.AttachTexture(
                    /*attachment*/ opengl::FramebufferObject::ATTACHMENT::DEPTH,
                    /*textureHandle*/ m_TextureShadowMapDepth->getHandle());

                m_ShadowTextureSampler->setupDepthTest(
                    /*compareMode*/ opengl::Texture::COMPARE_MODE::COMPARE_REF_TO_TEXTURE,
                    /*compareFunc*/ opengl::COMPARISON_FUNCTION::LESS);

                m_ShadowTextureSampler->setMagnificationFilter(
                    /*filter*/ opengl::Texture::TEXTURE_FILTER::LINEAR);

                m_ShadowTextureSampler->setMinificationFilter(
                    /*filter*/ opengl::Texture::TEXTURE_FILTER::LINEAR);

                m_ShadowTextureSampler->setWrapMode(
                    /*mode*/ opengl::Texture::WRAP_MODE::CLAMP_TO_BORDER);

                m_ShadowTextureSampler->setBorderColor(
                    /*rgba*/ 1.0f, 0.0f, 0.0f, 0.0f);
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            ShadowMap(const ShadowMap&) = delete;
            ShadowMap& operator=(const ShadowMap&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            const std::shared_ptr<opengl::Texture>& getDepthTexture() const { return m_TextureShadowMapDepth; }
            std::shared_ptr<opengl::TextureSampler>& ShadowTextureSampler() { return m_ShadowTextureSampler; }

        #pragma endregion

        #pragma region Methods

        public:
            void Render(std::function<void()> renderFunc)
            {
                opengl::Rect oldViewport = opengl::getViewportRect();

                auto cull_face_mode = opengl::getCullFaceMode();

                {
                    engine::setViewport(
                        /*width*/  m_Width,
                        /*height*/ m_Height,
                        /*aspect_ratio*/ m_Width / (GLfloat)m_Height);

                    // prevents shadow acne (see http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-16-shadow-mapping/#shadow-acne)
                    opengl::setCullFaceMode(opengl::CULL_FACE_MODE::FRONT);

                    m_ShadowMapFbo.Render(
                        /*attachment*/
                        opengl::FramebufferObject::COLOR_ATTACHMENT::NONE,
                        
                        /*renderFunc*/
                        [renderFunc]() -> void
                        {
                            opengl::ClearAllBuffers();
                            renderFunc();
                        });
                }

                opengl::setCullFaceMode(cull_face_mode);

                engine::setViewport(oldViewport);
            }

        #pragma endregion
    };
}