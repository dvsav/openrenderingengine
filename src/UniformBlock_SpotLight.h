/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "UniformBlockBuffer.h"

#include "LightStructs.h"

namespace engine
{
    class UniformBlock_SpotLight : public opengl::UniformBlockStructure<SpotLightStruct>
    {
        #pragma region Constructors

        public:
            UniformBlock_SpotLight() :
                opengl::UniformBlockStructure<SpotLightStruct>(
                    SpotLightStruct
                    {
                        make_Color_RGBA32F_Gray(1.0f),            // LightColor
                        vec4 { 0.0f, 0.0f, 0.0f, 1.0f },          // LightPosition
                        vec4 { 0.0f, 0.0f, 1.0f, 0.0f },          // LightDirection
                        vmath::make_IdentityMatrix<GLfloat, 4>(), // WorldToLightProjectionPlane
                        vmath::make_IdentityMatrix<GLfloat, 4>(), // WorldToShadowMap
                        make_AttenuationStruct_Range3250(),       // Attenuation
                        0.5f                                      // EdgeCosine
                    },
                    opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(const SpotLightStruct::TLightColor& value)
            {
                this->setData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::LightColor),
                    /*size*/ sizeof(SpotLightStruct::TLightColor),
                    /*data*/ &value);
            }

            SpotLightStruct::TLightColor getLightColor() const
            {
                SpotLightStruct::TLightColor value;
                this->getData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::LightColor),
                    /*size*/ sizeof(SpotLightStruct::TLightColor),
                    /*data*/ &value);
                return value;
            }

            void setLightPosition(const SpotLightStruct::TLightPosition& value)
            {
                this->setData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::LightPosition),
                    /*size*/ sizeof(SpotLightStruct::TLightPosition),
                    /*data*/ &value);
            }

            SpotLightStruct::TLightPosition getLightPosition() const
            {
                SpotLightStruct::TLightPosition value;
                this->getData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::LightPosition),
                    /*size*/ sizeof(SpotLightStruct::TLightPosition),
                    /*data*/ &value);
                return value;
            }

            void setLightDirection(const SpotLightStruct::TLightDirection& value)
            {
                this->setData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::LightDirection),
                    /*size*/ sizeof(SpotLightStruct::TLightDirection),
                    /*data*/ &value);
            }

            SpotLightStruct::TLightDirection getLightDirection() const
            {
                SpotLightStruct::TLightDirection value;
                this->getData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::LightDirection),
                    /*size*/ sizeof(SpotLightStruct::TLightDirection),
                    /*data*/ &value);
                return value;
            }

            void setWorldToLightProjectionPlane(const SpotLightStruct::TWorldToLightProjectionPlane& value)
            {
                this->setData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::WorldToLightProjectionPlane),
                    /*size*/ sizeof(SpotLightStruct::TWorldToLightProjectionPlane),
                    /*data*/ &value);

                // The bias is needed to transform NDC coordinates to 2d-texture coordinates, i. e.
                // NDC: -1 ... +1 --> 2d-texture: 0 ... 1
                static const mat4 bias
                {
                    { 0.5f, 0.0f, 0.0f, 0.0f },
                    { 0.0f, 0.5f, 0.0f, 0.0f },
                    { 0.0f, 0.0f, 0.5f, 0.0f },
                    { 0.5f, 0.5f, 0.5f, 1.0f }
                };

                mat4 temp = bias * value;
                this->setData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::WorldToShadowMap),
                    /*size*/ sizeof(SpotLightStruct::TWorldToShadowMap),
                    /*data*/ &temp);
            }

            SpotLightStruct::TWorldToLightProjectionPlane getWorldToLightProjectionPlane() const
            {
                SpotLightStruct::TWorldToLightProjectionPlane value;
                this->getData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::WorldToLightProjectionPlane),
                    /*size*/ sizeof(SpotLightStruct::TWorldToLightProjectionPlane),
                    /*data*/ &value);
                return value;
            }

            void setEdgeCosine(SpotLightStruct::TEdgeCosine value)
            {
                this->setData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::EdgeCosine),
                    /*size*/ sizeof(SpotLightStruct::TEdgeCosine),
                    /*data*/ &value);
            }

            SpotLightStruct::TEdgeCosine getEdgeCosine() const
            {
                SpotLightStruct::TEdgeCosine value;
                this->getData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::EdgeCosine),
                    /*size*/ sizeof(SpotLightStruct::TEdgeCosine),
                    /*data*/ &value);
                return value;
            }

            void setAttenuation(SpotLightStruct::TAttenuation& value)
            {
                this->setData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::Attenuation),
                    /*size*/ sizeof(SpotLightStruct::TAttenuation),
                    /*data*/ &value);
            }

            SpotLightStruct::TAttenuation getAttenuation() const
            {
                SpotLightStruct::TAttenuation value;
                this->getData(
                    /*offset*/ offsetof(SpotLightStruct, SpotLightStruct::Attenuation),
                    /*size*/ sizeof(SpotLightStruct::TAttenuation),
                    /*data*/ &value);
                return value;
            }

        #pragma endregion
    };

    class UniformBlock_SpotLightArray : public opengl::UniformBlockArray<SpotLightStruct>
    {
        #pragma region Constructors

        public:
            using UniformBlockArray::UniformBlockArray;

            UniformBlock_SpotLightArray(size_t nLights)
                : UniformBlockArray(
                    /*data*/ std::vector<SpotLightStruct>(
                        nLights,
                        SpotLightStruct
                        {
                            make_Color_RGBA32F_Gray(1.0f),            // LightColor
                            vec4 { 0.0f, 0.0f, 0.0f, 1.0f },          // LightPosition
                            vec4 { 0.0f, 0.0f, 1.0f, 0.0f },          // LightDirection
                            vmath::make_IdentityMatrix<GLfloat, 4>(), // WorldToLightProjectionPlane
                            vmath::make_IdentityMatrix<GLfloat, 4>(), // WorldToShadowMap
                            make_AttenuationStruct_Range3250(),       // Attenuation
                            0.5f                                      // EdgeCosine
                        }),
                    /*usage*/ opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(size_t i, const SpotLightStruct::TLightColor& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::LightColor),
                    /*size*/ sizeof(SpotLightStruct::TLightColor),
                    /*data*/ &value);
            }

            SpotLightStruct::TLightColor getLightColor(size_t i) const
            {
                SpotLightStruct::TLightColor value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::LightColor),
                    /*size*/ sizeof(SpotLightStruct::TLightColor),
                    /*data*/ &value);
                return value;
            }

            void setLightPosition(size_t i, const SpotLightStruct::TLightPosition& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::LightPosition),
                    /*size*/ sizeof(SpotLightStruct::TLightPosition),
                    /*data*/ &value);
            }

            SpotLightStruct::TLightPosition getLightPosition(size_t i) const
            {
                SpotLightStruct::TLightPosition value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::LightPosition),
                    /*size*/ sizeof(SpotLightStruct::TLightPosition),
                    /*data*/ &value);
                return value;
            }

            void setLightDirection(size_t i, const SpotLightStruct::TLightDirection& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::LightDirection),
                    /*size*/ sizeof(SpotLightStruct::TLightDirection),
                    /*data*/ &value);
            }

            SpotLightStruct::TLightDirection getLightDirection(size_t i) const
            {
                SpotLightStruct::TLightDirection value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::LightDirection),
                    /*size*/ sizeof(SpotLightStruct::TLightDirection),
                    /*data*/ &value);
                return value;
            }

            void setWorldToLightProjectionPlane(size_t i, const SpotLightStruct::TWorldToLightProjectionPlane& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::WorldToLightProjectionPlane),
                    /*size*/ sizeof(SpotLightStruct::TWorldToLightProjectionPlane),
                    /*data*/ &value);

                // The bias is needed to transform NDC coordinates to 2d-texture coordinates, i. e.
                // NDC: -1 ... +1 --> 2d-texture: 0 ... 1
                static const mat4 bias
                {
                    { 0.5f, 0.0f, 0.0f, 0.0f },
                    { 0.0f, 0.5f, 0.0f, 0.0f },
                    { 0.0f, 0.0f, 0.5f, 0.0f },
                    { 0.5f, 0.5f, 0.5f, 1.0f }
                };

                mat4 temp = bias * value;
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::WorldToShadowMap),
                    /*size*/ sizeof(SpotLightStruct::TWorldToShadowMap),
                    /*data*/ &temp);
            }

            SpotLightStruct::TWorldToLightProjectionPlane getWorldToLightProjectionPlane(size_t i) const
            {
                SpotLightStruct::TWorldToLightProjectionPlane value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::WorldToLightProjectionPlane),
                    /*size*/ sizeof(SpotLightStruct::TWorldToLightProjectionPlane),
                    /*data*/ &value);
                return value;
            }

            void setEdgeCosine(size_t i, SpotLightStruct::TEdgeCosine value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::EdgeCosine),
                    /*size*/ sizeof(SpotLightStruct::TEdgeCosine),
                    /*data*/ &value);
            }

            SpotLightStruct::TEdgeCosine getEdgeCosine(size_t i) const
            {
                SpotLightStruct::TEdgeCosine value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::EdgeCosine),
                    /*size*/ sizeof(SpotLightStruct::TEdgeCosine),
                    /*data*/ &value);
                return value;
            }

            void setAttenuation(size_t i, const SpotLightStruct::TAttenuation& value)
            {
                this->setData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::Attenuation),
                    /*size*/ sizeof(SpotLightStruct::TAttenuation),
                    /*data*/ &value);
            }

            SpotLightStruct::TAttenuation getAttenuation(size_t i) const
            {
                SpotLightStruct::TAttenuation value;
                this->getData(
                    /*offset*/ getElementOffset(i) + offsetof(SpotLightStruct, SpotLightStruct::Attenuation),
                    /*size*/ sizeof(SpotLightStruct::TAttenuation),
                    /*data*/ &value);
                return value;
            }

        #pragma endregion
    };
}
