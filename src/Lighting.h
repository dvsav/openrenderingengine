/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include <memory>
#include <algorithm>
#include <functional>

#include "UniformBlock_CameraInfo.h"
#include "UniformBlock_DirectionalLight.h"
#include "UniformBlock_PointLight.h"
#include "UniformBlock_SpotLight.h"

#include "Camera.h"
#include "ShadowMapUnidirectional.h"
#include "ShadowMapOmnidirectional.h"

#include "Model3d_Base.h"

namespace engine
{
    inline mat4 make_WorldToLightProjection(
        const Camera& lightView)
    {
        return
            lightView.getCameraToScreenMatrix() *
            lightView.getWorldToCameraMatrix();
    }

    class AmbientLight
    {
        #pragma region Fields

        private:
            std::shared_ptr<UniformBlock_AmbientLight> uniformBlock;

        #pragma endregion

        #pragma region Constructors

        public:
            AmbientLight() :
                uniformBlock(std::make_shared<UniformBlock_AmbientLight>())
            { }

            AmbientLight(const std::shared_ptr<UniformBlock_AmbientLight>& uniformBlock) :
                uniformBlock(uniformBlock)
            { }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            AmbientLight(const AmbientLight&) = delete;
            AmbientLight& operator=(const AmbientLight&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(const AmbientLightStruct::TLightColor& value)
            {
                uniformBlock->setLightColor(value);
            }
            AmbientLightStruct::TLightColor getLightColor() const
            {
                return uniformBlock->getLightColor();
            }

            std::shared_ptr<UniformBlock_AmbientLight>& UniformBlock() { return uniformBlock; };

        #pragma endregion
    };

    class DirectionalLight
    {
        #pragma region Fields

        private:
            OrthographicCamera lightView;
            ShadowMapUnidirectional shadowMap;
            std::shared_ptr<MetaProgram> shadowRender;
            std::shared_ptr<UniformBlock_DirectionalLightArray> uniformBlock;
            size_t arrayIndex;
            bool lightViewInvalidated;

        #pragma endregion

        #pragma region Constructors

        public:
            DirectionalLight(
                GLsizei shadowWidth,
                GLsizei shadowHeight,
                const std::shared_ptr<MetaProgram>& shadowRender) :

                lightView(),
                shadowMap(shadowWidth, shadowHeight),
                shadowRender(shadowRender),
                uniformBlock(std::make_shared<UniformBlock_DirectionalLightArray>(1)),
                arrayIndex(0),
                lightViewInvalidated(true)
            { }

            DirectionalLight(
                std::shared_ptr<UniformBlock_DirectionalLightArray> directionalLightsArray,
                size_t directionalLightsArrayIndex,
                GLsizei shadowWidth,
                GLsizei shadowHeight,
                const std::shared_ptr<MetaProgram>& shadowRender) :

                lightView(),
                shadowMap(shadowWidth, shadowHeight),
                shadowRender(shadowRender),
                uniformBlock(directionalLightsArray),
                arrayIndex(directionalLightsArrayIndex),
                lightViewInvalidated(true)
            {
                util::Requires::ArgumentNotNegative(
                    directionalLightsArrayIndex,
                    "directionalLightsArrayIndex",
                    FUNCTION_INFO);
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            DirectionalLight(const DirectionalLight&) = delete;
            DirectionalLight& operator=(const DirectionalLight&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(const DirectionalLightStruct::TLightColor& value)
            {
                uniformBlock->setLightColor(arrayIndex, value);
            }
            DirectionalLightStruct::TLightColor getLightColor() const
            {
                return uniformBlock->getLightColor(arrayIndex);
            }

            void setLightDirection(const DirectionalLightStruct::TLightDirection& value)
            {
                LightView().PositionAndOrientation().setForwardDirection(value.vec3());
                uniformBlock->setLightDirection(arrayIndex, value);
            }
            DirectionalLightStruct::TLightDirection getLightDirection() const
            {
                return uniformBlock->getLightDirection(arrayIndex);
            }

            const std::shared_ptr<opengl::Texture>& getDepthTexture() const { return shadowMap.getDepthTexture(); }
            std::shared_ptr<opengl::TextureSampler>& ShadowTextureSampler() { return shadowMap.ShadowTextureSampler(); }

            const OrthographicCamera& LightView() const { return lightView; }
            OrthographicCamera& LightView() { lightViewInvalidated = true; return lightView; }

            std::shared_ptr<UniformBlock_DirectionalLightArray>& UniformBlock() { return uniformBlock; };
            std::shared_ptr<MetaProgram>& ShadowRender() { return shadowRender; }

        #pragma endregion

        #pragma region Methods

        private:
            void UpdateLightView()
            {
                uniformBlock->setLightDirection(
                    arrayIndex,
                    lightView.PositionAndOrientation().getForwardDirection());

                uniformBlock->setWorldToLightProjectionPlane(
                    arrayIndex,
                    make_WorldToLightProjection(lightView));

                lightViewInvalidated = false;
            }

        public:
            void RenderShadows(std::function<void()> renderFunc);

        #pragma endregion
    };

    class PointLight
    {
        #pragma region Fields

        private:
            ShadowMapOmnidirectional shadowMap;
            std::shared_ptr<MetaProgram> shadowRender;

            std::shared_ptr<UniformBlock_PointLightArray> uniformBlock;
            size_t arrayIndex;

            PerspectiveCamera lightView;
            bool lightViewInvalidated;

        #pragma endregion

        #pragma region Constructors

        public:
            PointLight(
                GLsizei shadowWidthHeight,
                const std::shared_ptr<MetaProgram>& shadowRender) :

                shadowMap(shadowWidthHeight),
                shadowRender(shadowRender),
                uniformBlock(std::make_shared<UniformBlock_PointLightArray>(1)),
                arrayIndex(0),
                lightView(M_PI / 2.0f, 1.0f),
                lightViewInvalidated(true)
            { }

            PointLight(
                std::shared_ptr<UniformBlock_PointLightArray> pointLightsArray,
                size_t pointLightsArrayIndex,
                GLsizei shadowWidthHeight,
                const std::shared_ptr<MetaProgram>& shadowRender) :

                shadowMap(shadowWidthHeight),
                shadowRender(shadowRender),
                uniformBlock(pointLightsArray),
                arrayIndex(pointLightsArrayIndex),
                lightView(M_PI / 2.0f, 1.0f),
                lightViewInvalidated(true)
            {
                util::Requires::ArgumentNotNegative(
                    pointLightsArrayIndex,
                    "pointLightsArrayIndex",
                    FUNCTION_INFO);
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            PointLight(const PointLight&) = delete;
            PointLight& operator=(const PointLight&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(const PointLightStruct::TLightColor& value)
            {
                uniformBlock->setLightColor(arrayIndex, value);
            }
            PointLightStruct::TLightColor getLightColor() const
            {
                return uniformBlock->getLightColor(arrayIndex);
            }

            void setLightPosition(const PointLightStruct::TLightPosition& value)
            {
                LightView().PositionAndOrientation().Position() = value;
                uniformBlock->setLightPosition(arrayIndex, value);
            }
            PointLightStruct::TLightPosition getLightPosition() const
            {
                return uniformBlock->getLightPosition(arrayIndex);
            }

            void setAttenuation(const PointLightStruct::TAttenuation& value)
            {
                uniformBlock->setAttenuation(arrayIndex, value);
            }
            PointLightStruct::TAttenuation getAttenuation() const
            {
                return uniformBlock->getAttenuation(arrayIndex);
            }

            const std::shared_ptr<opengl::Texture>& getDepthTexture() const { return shadowMap.getDepthTexture(); }
            std::shared_ptr<opengl::TextureSampler>& ShadowTextureSampler() { return shadowMap.ShadowTextureSampler(); }

            const Camera& LightView() const { return lightView; }
            Camera& LightView() { lightViewInvalidated = true; return lightView; }

            std::shared_ptr<UniformBlock_PointLightArray>& UniformBlock() { return uniformBlock; };
            std::shared_ptr<MetaProgram>& ShadowRender() { return shadowRender; }

        #pragma endregion

        #pragma region Methods

        public:
            void UpdateLightView()
            {
                static const mat4 neg_z = vmath::make_YRotationMatrix4x4(util::to_radians(180.0f));
                static const mat4 pos_x = vmath::make_YRotationMatrix4x4(util::to_radians(90.0f));
                static const mat4 neg_x = vmath::make_YRotationMatrix4x4(util::to_radians(-90.0f));
                static const mat4 pos_y = vmath::make_XRotationMatrix4x4(util::to_radians(90.0f));
                static const mat4 neg_y = vmath::make_XRotationMatrix4x4(util::to_radians(-90.0f));

                if (lightViewInvalidated)
                {
                    const mat4 world_to_camera = lightView.getWorldToCameraMatrix();
                    const mat4& camera_to_screen = lightView.getCameraToScreenMatrix();

                    uniformBlock->setLightPosition(
                        arrayIndex,
                        lightView.PositionAndOrientation().Position());

                    uniformBlock->setCameraToScreen(
                        arrayIndex,
                        camera_to_screen);

                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::POSITIVE_Z,
                        camera_to_screen * world_to_camera);
                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::NEGATIVE_Z,
                        camera_to_screen * neg_z * world_to_camera);

                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::POSITIVE_X,
                        camera_to_screen * pos_x * world_to_camera);
                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::NEGATIVE_X,
                        camera_to_screen * neg_x * world_to_camera);

                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::POSITIVE_Y,
                        camera_to_screen * pos_y * world_to_camera);
                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::NEGATIVE_Y,
                        camera_to_screen * neg_y * world_to_camera);

                    lightViewInvalidated = false;
                }
            }

            void RenderShadows(std::function<void()> renderFunc);

        #pragma endregion
    };

    class SpotLight
    {
        #pragma region Fields

        private:
            PerspectiveCamera lightView;
            ShadowMapUnidirectional shadowMap;
            std::shared_ptr<MetaProgram> shadowRender;
            std::shared_ptr<UniformBlock_SpotLightArray> uniformBlock;
            size_t arrayIndex;
            bool lightViewInvalidated;

        #pragma endregion

        #pragma region Constructors

        public:
            SpotLight(
                GLsizei shadowWidth,
                GLsizei shadowHeight,
                const std::shared_ptr<MetaProgram>& shadowRender) :

                lightView(M_PI / 2.0f, shadowWidth / (GLfloat)shadowHeight),
                shadowMap(shadowWidth, shadowHeight),
                shadowRender(shadowRender),
                uniformBlock(std::make_shared<UniformBlock_SpotLightArray>(1)),
                arrayIndex(0),
                lightViewInvalidated(true)
            { }

            SpotLight(
                std::shared_ptr<UniformBlock_SpotLightArray> spotLightsArray,
                size_t spotLightsArrayIndex,
                GLsizei shadowWidth,
                GLsizei shadowHeight,
                const std::shared_ptr<MetaProgram>& shadowRender) :

                lightView(M_PI / 2.0f, shadowWidth / (GLfloat)shadowHeight),
                shadowMap(shadowWidth, shadowHeight),
                shadowRender(shadowRender),
                uniformBlock(spotLightsArray),
                arrayIndex(spotLightsArrayIndex),
                lightViewInvalidated(true)
            {
                util::Requires::ArgumentNotNegative(
                    spotLightsArrayIndex,
                    "spotLightsArrayIndex",
                    FUNCTION_INFO);
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            SpotLight(const SpotLight&) = delete;
            SpotLight& operator=(const SpotLight&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(const SpotLightStruct::TLightColor& value)
            {
                uniformBlock->setLightColor(arrayIndex, value);
            }
            SpotLightStruct::TLightColor getLightColor() const
            {
                return uniformBlock->getLightColor(arrayIndex);
            }

            void setLightPosition(const SpotLightStruct::TLightPosition& value)
            {
                LightView().PositionAndOrientation().Position() = value;
                uniformBlock->setLightPosition(arrayIndex, value);
            }
            SpotLightStruct::TLightPosition getLightPosition() const
            {
                return uniformBlock->getLightPosition(arrayIndex);
            }

            void setLightDirection(const SpotLightStruct::TLightDirection& value)
            {
                LightView().PositionAndOrientation().setForwardDirection(value.vec3());
                uniformBlock->setLightDirection(arrayIndex, value);
            }
            SpotLightStruct::TLightDirection getLightDirection() const
            {
                return uniformBlock->getLightDirection(arrayIndex);
            }

            void setAttenuation(const SpotLightStruct::TAttenuation& value)
            {
                uniformBlock->setAttenuation(arrayIndex, value);
            }
            SpotLightStruct::TAttenuation getAttenuation() const
            {
                return uniformBlock->getAttenuation(arrayIndex);
            }

            const std::shared_ptr<opengl::Texture>& getDepthTexture() const { return shadowMap.getDepthTexture(); }
            std::shared_ptr<opengl::TextureSampler>& ShadowTextureSampler() { return shadowMap.ShadowTextureSampler(); }

            const PerspectiveCamera& LightView() const { return lightView; }
            PerspectiveCamera& LightView() { lightViewInvalidated = true; return lightView; }

            std::shared_ptr<UniformBlock_SpotLightArray>& UniformBlock() { return uniformBlock; };
            std::shared_ptr<MetaProgram>& ShadowRender() { return shadowRender; }

        #pragma endregion

        #pragma region Methods

        private:
            void UpdateLightView()
            {
                uniformBlock->setEdgeCosine(
                    arrayIndex,
                    cosf(lightView.getViewAngle() / 2.0f));

                uniformBlock->setLightDirection(
                    arrayIndex,
                    lightView.PositionAndOrientation().getForwardDirection());

                uniformBlock->setLightPosition(
                    arrayIndex,
                    lightView.PositionAndOrientation().Position());

                uniformBlock->setWorldToLightProjectionPlane(
                    arrayIndex,
                    make_WorldToLightProjection(lightView));

                lightViewInvalidated = false;
            }

        public:
            void RenderShadows(std::function<void()> renderFunc);

        #pragma endregion
    };

    class DirectionalLightArray
    {
        #pragma region Nested Types

        private:
            struct LightViewStruct
            {
                OrthographicCamera camera;
                bool invalidated;
            };

        #pragma endregion

        #pragma region Fields

        private:
            ShadowMapUnidirectional shadowMap;
            std::shared_ptr<MetaProgram> shadowRender;
            std::shared_ptr<UniformBlock_DirectionalLightArray> uniformBlock;
            std::vector<LightViewStruct> lightViews;

        #pragma endregion

        #pragma region Constructors

        public:
            DirectionalLightArray(
                size_t size,
                GLsizei shadowWidth,
                GLsizei shadowHeight,
                const std::shared_ptr<MetaProgram>& shadowRender) :

                shadowMap(
                    shadowWidth, shadowHeight,
                    std::make_shared<opengl::Texture2DArray>(
                        /*mipmap_levels_number*/ 1,
                        /*internalFormat*/ opengl::INTERNAL_FORMAT::DEPTH_COMPONENT24,
                        /*width*/ shadowWidth,
                        /*height*/ shadowHeight,
                        /*format*/ opengl::PIXEL_FORMAT::DEPTH_COMPONENT,
                        /*type*/ opengl::TYPE::FLOAT,
                        /*layersNumber*/ std::max<size_t>(1, size))),
                shadowRender(shadowRender),
                uniformBlock(std::make_shared<UniformBlock_DirectionalLightArray>(size)),
                lightViews()
            {
                for (size_t i = 0; i < size; i++)
                    lightViews.push_back({ OrthographicCamera(), true });
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            DirectionalLightArray(const DirectionalLightArray&) = delete;
            DirectionalLightArray& operator=(const DirectionalLightArray&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(size_t arrayIndex, const DirectionalLightStruct::TLightColor& value)
            {
                uniformBlock->setLightColor(arrayIndex, value);
            }
            DirectionalLightStruct::TLightColor getLightColor(size_t arrayIndex) const
            {
                return uniformBlock->getLightColor(arrayIndex);
            }

            void setLightDirection(size_t arrayIndex, const DirectionalLightStruct::TLightDirection& value)
            {
                LightView(arrayIndex).PositionAndOrientation().setForwardDirection(value.vec3());
                uniformBlock->setLightDirection(arrayIndex, value);
            }
            DirectionalLightStruct::TLightDirection getLightDirection(size_t arrayIndex) const
            {
                return uniformBlock->getLightDirection(arrayIndex);
            }

            const std::shared_ptr<opengl::Texture>& getDepthTexture() const { return shadowMap.getDepthTexture(); }
            std::shared_ptr<opengl::TextureSampler>& ShadowTextureSampler() { return shadowMap.ShadowTextureSampler(); }

            const OrthographicCamera& LightView(size_t arrayIndex) const { return lightViews[arrayIndex].camera; }
            OrthographicCamera& LightView(size_t arrayIndex) { lightViews[arrayIndex].invalidated = true; return lightViews[arrayIndex].camera; }

            std::shared_ptr<UniformBlock_DirectionalLightArray>& UniformBlock() { return uniformBlock; };
            std::shared_ptr<MetaProgram>& ShadowRender() { return shadowRender; }

            const size_t getCount() const { return lightViews.size(); }

        #pragma endregion

        #pragma region Methods

        private:
            void UpdateLightView(size_t arrayIndex)
            {
                auto& lview = lightViews[arrayIndex];

                if (lview.invalidated)
                {
                    uniformBlock->setLightDirection(
                        arrayIndex,
                        lview.camera.PositionAndOrientation().getForwardDirection());

                    uniformBlock->setWorldToLightProjectionPlane(
                        arrayIndex,
                        make_WorldToLightProjection(lview.camera));

                    lview.invalidated = false;
                }
            }

        public:
            void RenderShadows(std::function<void()> renderFunc);

        #pragma endregion
    };

    class PointLightArray
    {
        #pragma region Nested Types

        private:
            struct LightViewStruct
            {
                PerspectiveCamera camera;
                bool invalidated;
            };

        #pragma endregion

        #pragma region Fields

        private:
            ShadowMapOmnidirectional shadowMap;
            std::shared_ptr<MetaProgram> shadowRender;
            std::shared_ptr<UniformBlock_PointLightArray> uniformBlock;
            std::vector<LightViewStruct> lightViews;

        #pragma endregion

        #pragma region Constructors

        public:
            PointLightArray(
                size_t size,
                GLsizei shadowWidthHeight,
                const std::shared_ptr<MetaProgram>& shadowRender) :

                shadowMap(
                    shadowWidthHeight,
                    std::make_shared<opengl::TextureCubemapArray>(
                        /*mipmap_levels_number*/ 1,
                        /*internalFormat*/ opengl::INTERNAL_FORMAT::DEPTH_COMPONENT24,
                        /*width*/ shadowWidthHeight,
                        /*height*/ shadowWidthHeight,
                        /*format*/ opengl::PIXEL_FORMAT::DEPTH_COMPONENT,
                        /*type*/ opengl::TYPE::FLOAT,
                        /*layersNumber*/ std::max<size_t>(1, size))),
                shadowRender(shadowRender),
                uniformBlock(std::make_shared<UniformBlock_PointLightArray>(size)),
                lightViews()
            {
                for (size_t i = 0; i < size; i++)
                    lightViews.push_back({ PerspectiveCamera(M_PI / 2.0f, 1.0f), true });
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            PointLightArray(const PointLightArray&) = delete;
            PointLightArray& operator=(const PointLightArray&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(size_t arrayIndex, const PointLightStruct::TLightColor& value)
            {
                uniformBlock->setLightColor(arrayIndex, value);
            }
            PointLightStruct::TLightColor getLightColor(size_t arrayIndex) const
            {
                return uniformBlock->getLightColor(arrayIndex);
            }

            void setLightPosition(size_t arrayIndex, const PointLightStruct::TLightPosition& value)
            {
                LightView(arrayIndex).PositionAndOrientation().Position() = value;
                uniformBlock->setLightPosition(arrayIndex, value);
            }
            PointLightStruct::TLightPosition getLightPosition(size_t arrayIndex) const
            {
                return uniformBlock->getLightPosition(arrayIndex);
            }

            void setAttenuation(size_t arrayIndex, const PointLightStruct::TAttenuation& value)
            {
                uniformBlock->setAttenuation(arrayIndex, value);
            }
            PointLightStruct::TAttenuation getAttenuation(size_t arrayIndex) const
            {
                return uniformBlock->getAttenuation(arrayIndex);
            }

            const std::shared_ptr<opengl::Texture>& getDepthTexture() const { return shadowMap.getDepthTexture(); }
            std::shared_ptr<opengl::TextureSampler>& ShadowTextureSampler() { return shadowMap.ShadowTextureSampler(); }

            const PerspectiveCamera& LightView(size_t arrayIndex) const { return lightViews[arrayIndex].camera; }
            PerspectiveCamera& LightView(size_t arrayIndex) { lightViews[arrayIndex].invalidated = true; return lightViews[arrayIndex].camera; }

            std::shared_ptr<UniformBlock_PointLightArray>& UniformBlock() { return uniformBlock; };
            std::shared_ptr<MetaProgram>& ShadowRender() { return shadowRender; }

            const size_t getCount() const { return lightViews.size(); }

        #pragma endregion

        #pragma region Methods

        private:
            void UpdateLightView(size_t arrayIndex)
            {
                static const mat4 neg_z = vmath::make_YRotationMatrix4x4(util::to_radians(180.0f));
                static const mat4 pos_x = vmath::make_YRotationMatrix4x4(util::to_radians(90.0f));
                static const mat4 neg_x = vmath::make_YRotationMatrix4x4(util::to_radians(-90.0f));
                static const mat4 pos_y = vmath::make_XRotationMatrix4x4(util::to_radians(90.0f));
                static const mat4 neg_y = vmath::make_XRotationMatrix4x4(util::to_radians(-90.0f));

                auto& lview = lightViews[arrayIndex];

                if (lview.invalidated)
                {
                    const mat4 world_to_camera = lview.camera.getWorldToCameraMatrix();
                    const mat4& camera_to_screen = lview.camera.getCameraToScreenMatrix();

                    uniformBlock->setLightPosition(
                        arrayIndex,
                        lview.camera.PositionAndOrientation().Position());

                    uniformBlock->setCameraToScreen(
                        arrayIndex,
                        camera_to_screen);

                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::POSITIVE_Z,
                        camera_to_screen * world_to_camera);
                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::NEGATIVE_Z,
                        camera_to_screen * neg_z * world_to_camera);

                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::POSITIVE_X,
                        camera_to_screen * pos_x * world_to_camera);
                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::NEGATIVE_X,
                        camera_to_screen * neg_x * world_to_camera);

                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::POSITIVE_Y,
                        camera_to_screen * pos_y * world_to_camera);
                    uniformBlock->setLightMatrix(
                        arrayIndex,
                        opengl::TextureCubemap::CUBE_FACE::NEGATIVE_Y,
                        camera_to_screen * neg_y * world_to_camera);

                    lview.invalidated = false;
                }
            }

        public:
            void RenderShadows(std::function<void()> renderFunc);

        #pragma endregion
    };

    class SpotLightArray
    {
        #pragma region Nested Types

        private:
            struct LightViewStruct
            {
                PerspectiveCamera camera;
                bool invalidated;
            };

        #pragma endregion

        #pragma region Fields

        private:
            ShadowMapUnidirectional shadowMap;
            std::shared_ptr<MetaProgram> shadowRender;
            std::shared_ptr<UniformBlock_SpotLightArray> uniformBlock;
            std::vector<LightViewStruct> lightViews;

        #pragma endregion

        #pragma region Constructors

        public:
            SpotLightArray(
                size_t size,
                GLsizei shadowWidth,
                GLsizei shadowHeight,
                const std::shared_ptr<MetaProgram>& shadowRender) :

                shadowMap(
                    shadowWidth, shadowHeight,
                    std::make_shared<opengl::Texture2DArray>(
                        /*mipmap_levels_number*/ 1,
                        /*internalFormat*/ opengl::INTERNAL_FORMAT::DEPTH_COMPONENT24,
                        /*width*/ shadowWidth,
                        /*height*/ shadowHeight,
                        /*format*/ opengl::PIXEL_FORMAT::DEPTH_COMPONENT,
                        /*type*/ opengl::TYPE::FLOAT,
                        /*layersNumber*/ std::max<size_t>(1, size))),
                shadowRender(shadowRender),
                uniformBlock(std::make_shared<UniformBlock_SpotLightArray>(size)),
                lightViews()
            {
                for (size_t i = 0; i < size; i++)
                    lightViews.push_back({ PerspectiveCamera(M_PI / 2.0f, shadowWidth / (GLfloat)shadowHeight), true });
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            SpotLightArray(const SpotLightArray&) = delete;
            SpotLightArray& operator=(const SpotLightArray&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            void setLightColor(size_t arrayIndex, const SpotLightStruct::TLightColor& value)
            {
                uniformBlock->setLightColor(arrayIndex, value);
            }
            SpotLightStruct::TLightColor getLightColor(size_t arrayIndex) const
            {
                return uniformBlock->getLightColor(arrayIndex);
            }

            void setLightPosition(size_t arrayIndex, const SpotLightStruct::TLightPosition& value)
            {
                LightView(arrayIndex).PositionAndOrientation().Position() = value;
                uniformBlock->setLightPosition(arrayIndex, value);
            }
            SpotLightStruct::TLightPosition getLightPosition(size_t arrayIndex) const
            {
                return uniformBlock->getLightPosition(arrayIndex);
            }

            void setLightDirection(size_t arrayIndex, const SpotLightStruct::TLightDirection& value)
            {
                LightView(arrayIndex).PositionAndOrientation().setForwardDirection(value.vec3());
                uniformBlock->setLightDirection(arrayIndex, value);
            }
            SpotLightStruct::TLightDirection getLightDirection(size_t arrayIndex) const
            {
                return uniformBlock->getLightDirection(arrayIndex);
            }

            void setAttenuation(size_t arrayIndex, const SpotLightStruct::TAttenuation& value)
            {
                uniformBlock->setAttenuation(arrayIndex, value);
            }
            SpotLightStruct::TAttenuation getAttenuation(size_t arrayIndex) const
            {
                return uniformBlock->getAttenuation(arrayIndex);
            }

            const std::shared_ptr<opengl::Texture>& getDepthTexture() const { return shadowMap.getDepthTexture(); }
            std::shared_ptr<opengl::TextureSampler>& ShadowTextureSampler() { return shadowMap.ShadowTextureSampler(); }

            const PerspectiveCamera& LightView(size_t arrayIndex) const { return lightViews[arrayIndex].camera; }
            PerspectiveCamera& LightView(size_t arrayIndex) { lightViews[arrayIndex].invalidated = true; return lightViews[arrayIndex].camera; }

            std::shared_ptr<UniformBlock_SpotLightArray>& UniformBlock() { return uniformBlock; };
            std::shared_ptr<MetaProgram>& ShadowRender() { return shadowRender; }

            const size_t getCount() const { return lightViews.size(); }

        #pragma endregion

        #pragma region Methods

        private:
            void UpdateLightView(size_t arrayIndex)
            {
                auto& lview = lightViews[arrayIndex];

                if (lview.invalidated)
                {
                    uniformBlock->setEdgeCosine(
                        arrayIndex,
                        cosf(lview.camera.getViewAngle() / 2.0f));

                    uniformBlock->setLightDirection(
                        arrayIndex,
                        lview.camera.PositionAndOrientation().getForwardDirection());

                    uniformBlock->setLightPosition(
                        arrayIndex,
                        lview.camera.PositionAndOrientation().Position());

                    uniformBlock->setWorldToLightProjectionPlane(
                        arrayIndex,
                        make_WorldToLightProjection(lview.camera));

                    lview.invalidated = false;
                }
            }

        public:
            void RenderShadows(std::function<void()> renderFunc);

        #pragma endregion
    };
}
