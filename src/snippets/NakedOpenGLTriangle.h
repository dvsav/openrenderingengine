/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "ProgramBase.h"
#include "GLEWHeaders.h"
#include "File.h"

class Program final : public engine::ProgramBase
{
    private:
        GLuint m_Program;
        GLuint m_VertexBufferObject;
        GLuint m_VAO;

    public:
        Program()
        {
            // --- Compile shaders and link program ---
            GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
            std::string vertexShaderText = util::file::ReadAllText("shaders/passthrough.vert");
            const GLchar* text = vertexShaderText.c_str();
            glShaderSource(vertexShader, 1, &text, nullptr);
            glCompileShader(vertexShader);

            GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
            std::string fragmentShaderText = util::file::ReadAllText("shaders/passthrough.frag");
            text = fragmentShaderText.c_str();
            glShaderSource(fragmentShader, 1, &text, nullptr);
            glCompileShader(fragmentShader);

            m_Program = glCreateProgram();
            glAttachShader(m_Program, vertexShader);
            glAttachShader(m_Program, fragmentShader);
            glLinkProgram(m_Program);

            glDetachShader(m_Program, vertexShader);
            glDetachShader(m_Program, fragmentShader);
            glDeleteShader(vertexShader);
            glDeleteShader(fragmentShader);

            // --- Create buffer and populate it with vertex data ---
            GLfloat vertexData[]{
                -0.5f, 0.5f, 0.5f,      // vertex 1 coordinates
                1.0f, 0.0f, 0.0f, 1.0f, // vertex 1 color = red

                0.0f, -0.5f, 0.5f,      // vertex 2 coordinates
                0.0f, 1.0f, 0.0f, 1.0f, // vertex 1 color = green

                0.5f, 0.5f, 0.5f,       // vertex 3 coordinates
                0.0f, 0.0f, 1.0f, 1.0f  // vertex 1 color = blue
            };

            glGenBuffers(1, &m_VertexBufferObject);
            glBindBuffer(GL_ARRAY_BUFFER, m_VertexBufferObject);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

            // --- Create Vertex Array Object (VAO) and store vertex data format in it ---
            glGenVertexArrays(1, &m_VAO);
            glBindVertexArray(m_VAO);

            GLuint positionAttribLocation = glGetAttribLocation(m_Program, "v3_position");
            glVertexAttribPointer(
                /*index*/      positionAttribLocation,
                /*size*/       3,
                /*type*/       GL_FLOAT,
                /*normalized*/ GL_FALSE,
                /*stride*/     sizeof(GLfloat) * 7,
                /*pointer*/    nullptr
            );
            glEnableVertexAttribArray(positionAttribLocation);

            GLuint colorAttribLocation = glGetAttribLocation(m_Program, "v4_color");
            glVertexAttribPointer(
                /*index*/      colorAttribLocation,
                /*size*/       4,
                /*type*/       GL_FLOAT,
                /*normalized*/ GL_FALSE,
                /*stride*/     sizeof(GLfloat) * 7,
                /*pointer*/    (void*)(sizeof(GLfloat)*3)
            );
            glEnableVertexAttribArray(colorAttribLocation);
        }

    protected:
        void UpdateTime() override { }
        void setupCamera(const engine::Camera& cam) override { }

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            glUseProgram(m_Program);
            glBindVertexArray(m_VAO);
            glDrawArrays(GL_TRIANGLES, 0, 3);
        }
};
