/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "ProgramBase.h"
#include "MeshIndexed.h"
#include "MeshSphere.h"
#include "Model3d_Skybox.h"
#include "Model3d_Textured.h"
#include "Lighting.h"

class Program : public engine::ProgramBase
{
    #pragma region Fields

    private:
        const GLuint SPHERE_ANGULAR_RESOLUTION = 20;

        const GLfloat SUN_RADIUS = 5.0f;
        const GLfloat SUN_AXIAL_SPEED = 3.14f / 16.0f;

        const GLfloat EARTH_RADIUS = 2.0f;
        const GLfloat EARTH_ORBIT_RADIUS = 10.0f * EARTH_RADIUS;
        const GLfloat EARTH_AXIAL_SPEED = 3.14f / 8.0f;
        const GLfloat EARTH_ORBITAL_SPEED = 3.14f / 24.0f;

        const GLfloat MOON_RADIUS = 1.0f;
        const GLfloat MOON_ORBIT_RADIUS = EARTH_RADIUS + 2.0f;
        const GLfloat MOON_ORBITAL_SPEED = EARTH_ORBITAL_SPEED * 12.0f;

        engine::DirectionalLight m_DirectionalLight;
        engine::PointLight m_PointLight;

        std::shared_ptr<engine::UniformBlock_AmbientLight> m_UniformBlock_AmbientLight;
        std::shared_ptr<engine::UniformBlock_CameraInfo> m_UniformBlock_Camera;

        engine::Model3d_Skybox<opengl::TextureCubemap> m_Skybox;
        engine::Model3d_Textured<opengl::Texture2D> m_Sun;
        engine::Model3d_Textured<opengl::TextureCubemap> m_Earth;
        engine::Model3d_Textured<opengl::Texture2D> m_Moon;

    #pragma endregion

    #pragma region Constructors

    public:
        Program() :
            m_DirectionalLight(1024, 1024),
            m_PointLight(1280, 720),
            m_UniformBlock_AmbientLight(new engine::UniformBlock_AmbientLight()),
            m_UniformBlock_Camera(new engine::UniformBlock_CameraInfo()),
            m_Skybox(make_Skybox()),
            m_Sun(make_Sun()),
            m_Earth(make_Earth()),
            m_Moon(make_Moon())
        {
            m_Sun.getMaterial().setDiffuseColor(engine::make_Color_RGBA32F_Gray(1.0f));
            m_Earth.PositionAndOrientation().MoveForwardBackward(EARTH_ORBIT_RADIUS);

            // Set point light source.
            m_PointLight.setLightColor(engine::make_Color_RGBA32F_White());
            m_PointLight.LightView().PositionAndOrientation().setPosition(m_Sun.PositionAndOrientation().Position().vec3());

            m_Moon.getProgram()->Program().setUniform("pointLightCount", 1);
            m_Earth.getProgram()->Program().setUniform("pointLightCount", 1);

            setupCamera(getCamera());
        }

    #pragma endregion

    #pragma region Methods

    private:
        engine::Model3d_Skybox<opengl::TextureCubemap> make_Skybox()
        {
            return engine::Model3d_Skybox<opengl::TextureCubemap>(
                /*program*/
                std::make_shared<engine::MetaProgram>(
                    engine::MetaProgram::make_MetaProgram_Skybox(
                        /*program*/
                        opengl::Program
                        {
                            &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::VERTEX,   "shaders/skybox.vert"),
                            &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::FRAGMENT, "shaders/skybox.frag")
                        },
                        /*attribute_position*/ "v3_position",
                        /*uniform_block_mvp*/  "ModelViewPerspective",
                        /*texture_diffuse*/    "TextureCubemap")),

                /*mesh*/
                std::make_shared<engine::MeshIndexed>(engine::makeCubeP_FacingIncide(10.0f)),

                /*texture*/
                std::make_shared<opengl::TextureCubemap>(engine::CreateTextureCubemapFromFiles(
                    /*pos_x*/ "textures/milkyway/pos_x.bmp",
                    /*neg_x*/ "textures/milkyway/neg_x.bmp",
                    /*pos_y*/ "textures/milkyway/pos_y.bmp",
                    /*neg_y*/ "textures/milkyway/neg_y.bmp",
                    /*pos_z*/ "textures/milkyway/pos_z.bmp",
                    /*neg_z*/ "textures/milkyway/neg_z.bmp"))
                );
        }

        engine::Model3d_Textured<opengl::Texture2D> make_Sun()
        {
            return engine::Model3d_Textured<opengl::Texture2D>(
                /*program*/
                std::make_shared<engine::MetaProgram>(
                    engine::MetaProgram::make_MetaProgram_Textured_2d_PointLights(
                        /*program*/
                        opengl::Program
                        {
                            &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::VERTEX,   "shaders/phong_texture2d.vert"),
                            &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::FRAGMENT, "shaders/phong_texture2d_point_lights.frag")
                        },
                        /*attribute_position*/ "v3_position",
                        /*attribute_normal*/   "v3_normal",
                        /*attribute_texture*/  "v2_tex_coords",
                        /*uniform_block_model_to_world*/ "ModelToWorld",
                        /*uniform_block_material*/       "Material",
                        /*uniform_block_ambient_light*/  "AmbientLight",
                        /*uniform_block_point_lights*/   "PointLights",
                        /*uniform_block_camera_info*/    "CameraInfo",
                        /*texture_diffuse*/ "Texture2dDiffuse")),

                /*mesh*/ std::make_shared<engine::MeshSphere<engine::VertexPNT>>(SUN_RADIUS, SPHERE_ANGULAR_RESOLUTION),
                /*uniformBlock_AmbientLight*/ m_UniformBlock_AmbientLight,
                /*uniformBlock_DirectionalLights*/ m_DirectionalLight.DirectionalLightArray(),
                /*uniformBlock_PointLights*/ m_PointLight.PointLightArray(),
                /*uniformBlock_Camera*/ m_UniformBlock_Camera,
                                    
                /*texture*/ std::make_shared<opengl::Texture2D>(
                    engine::CreateTextureFromFile("textures/sun.bmp"))
                );
        }

        engine::Model3d_Textured<opengl::TextureCubemap> make_Earth()
        {
            return engine::Model3d_Textured<opengl::TextureCubemap>(
                /*program*/
                std::make_shared<engine::MetaProgram>(
                    engine::MetaProgram::make_MetaProgram_Textured_Cubemap_PointLights(
                        /*program*/
                        opengl::Program
                        {
                            &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::VERTEX,   "shaders/phong_cubemap.vert"),
                            &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::FRAGMENT, "shaders/phong_cubemap_point_lights.frag")
                        },
                        /*attribute_position*/ "v3_position",
                        /*attribute_normal*/   "v3_normal",
                        /*uniform_block_model_to_world*/ "ModelToWorld",
                        /*uniform_block_material*/       "Material",
                        /*uniform_block_ambient_light*/  "AmbientLight",
                        /*uniform_block_point_lights*/   "PointLights",
                        /*uniform_block_camera_info*/    "CameraInfo",
                        /*texture_diffuse*/ "TextureCubemap")),

                /*mesh*/ std::make_shared<engine::MeshSphere<engine::VertexPN>>(EARTH_RADIUS, SPHERE_ANGULAR_RESOLUTION),
                /*uniformBlock_AmbientLight*/ m_UniformBlock_AmbientLight,
                /*uniformBlock_DirectionalLights*/ m_DirectionalLight.DirectionalLightArray(),
                /*uniformBlock_PointLights*/ m_PointLight.PointLightArray(),
                /*uniformBlock_Camera*/ m_UniformBlock_Camera,
                                    
                /*texture*/
                std::make_shared<opengl::TextureCubemap>(engine::CreateTextureCubemapFromFiles(
                    /*pos_x*/ "textures/earth/pos_x.bmp",
                    /*neg_x*/ "textures/earth/neg_x.bmp",
                    /*pos_y*/ "textures/earth/pos_y.bmp",
                    /*neg_y*/ "textures/earth/neg_y.bmp",
                    /*pos_z*/ "textures/earth/pos_z.bmp",
                    /*neg_z*/ "textures/earth/neg_z.bmp"))
                );
        }

        engine::Model3d_Textured<opengl::Texture2D> make_Moon()
        {
            return engine::Model3d_Textured<opengl::Texture2D>(
                /*program*/ m_Sun.getProgram(),
                /*mesh*/ std::make_shared<engine::MeshSphere<engine::VertexPNT>>(MOON_RADIUS, SPHERE_ANGULAR_RESOLUTION),
                /*uniformBlock_AmbientLight*/ m_UniformBlock_AmbientLight,
                /*uniformBlock_DirectionalLights*/ m_DirectionalLight.DirectionalLightArray(),
                /*uniformBlock_PointLights*/ m_PointLight.PointLightArray(),
                /*uniformBlock_Camera*/ m_UniformBlock_Camera,
                /*texture*/ std::make_shared<opengl::Texture2D>(
                    engine::CreateTextureFromFile("textures/moon.bmp"))
                );
        }

    protected:
        void UpdateTime() override
        {
            m_Sun.PositionAndOrientation()
                .Yaw(SUN_AXIAL_SPEED * TimeSinceLastFrame());

            m_Earth.PositionAndOrientation()
                .Yaw(EARTH_AXIAL_SPEED * TimeSinceLastFrame())
                .RotateWorld(engine::vec3{ 0.0f, 1.0f, 0.0f }, EARTH_ORBITAL_SPEED * TimeSinceLastFrame());

            auto moon_angle = MOON_ORBITAL_SPEED * CurrentTime();
            engine::vec3 moon_direction{ cosf(moon_angle), 0.0f, sinf(moon_angle) };

            m_Moon.PositionAndOrientation().setPosition(
                m_Earth.PositionAndOrientation().Position().vec3() + MOON_ORBIT_RADIUS * moon_direction)
                .setForwardDirection(moon_direction);
        }

        void setupCamera(const engine::Camera& cam) override
        {
            m_Skybox.SetupCamera(cam);
            m_UniformBlock_Camera->SetupCamera(cam);
        }

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            m_Skybox.Render();
            m_Sun.Render();
            m_Earth.Render();
            m_Moon.Render();
        }

    #pragma endregion
};
