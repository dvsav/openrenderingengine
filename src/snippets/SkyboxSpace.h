/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "ProgramBase.h"
#include "MeshIndexed.h"
#include "Model3d_Skybox.h"

class Program : public engine::ProgramBase
{
    #pragma region Fields

    private:
        engine::Model3d_Skybox<opengl::TextureCubemap> m_Model;

    #pragma endregion

    #pragma region Constructors

    public:
        Program() :
            m_Model(
                /*program*/ make_MetaProgram(),
                /*mesh*/ std::make_shared<engine::MeshIndexed>(engine::makeCubeP_FacingIncide(10.0f)),
                /*texture*/ std::make_shared<opengl::TextureCubemap>(engine::CreateTextureCubemapFromFiles(
                    /*pos_x*/ "textures/space/pos_x.bmp",
                    /*neg_x*/ "textures/space/neg_x.bmp",
                    /*pos_y*/ "textures/space/pos_y.bmp",
                    /*neg_y*/ "textures/space/neg_y.bmp",
                    /*pos_z*/ "textures/space/pos_z.bmp",
                    /*neg_z*/ "textures/space/neg_z.bmp")))
        {
            setupCamera(getCamera());
        }

    #pragma endregion

    #pragma region Methods

    private:
        opengl::Program make_Program()
        {
            return opengl::Program(
                std::array<opengl::Shader*, 2>
                {
                    &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::VERTEX,   "shaders/skybox.vert"),
                    &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::FRAGMENT, "shaders/skybox.frag")
                });
        }

        std::shared_ptr<engine::MetaProgram> make_MetaProgram()
        {
            return std::make_shared<engine::MetaProgram>(
                engine::MetaProgram::make_MetaProgram_Skybox(
                    /*program*/ make_Program(),
                    /*attribute_position*/ "v3_position",
                    /*uniform_block_mvp*/ "ModelViewPerspective",
                    /*texture_diffuse*/ "TextureCubemap"));
        }

    protected:
        void UpdateTime() override { }

        void setupCamera(const engine::Camera& cam) override
        {
            m_Model.SetupCamera(cam);
        }

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            m_Model.Render();
        }

    #pragma endregion
};
