/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "ProgramBase.h"
#include "MeshIndexed.h"
#include "Model3d_Textured_ShadowMap.h"
#include "Lighting.h"
#include "StandardMetaprograms.h"

const int MAX_POINT_LIGHTS = 3;

class Program : public engine::ProgramBase
{
    #pragma region Fields

    private:
        engine::AmbientLight m_AmbientLight;
        engine::DirectionalLight m_DirectionalLight;
        engine::SpotLight m_SpotLight;

        engine::Model3d_Textured_ShadowMap_Factory m_ModelFactory;

        engine::Model3d_Textured_ShadowMap m_Wall;
        engine::Model3d_Textured_ShadowMap m_Model1;
        engine::Model3d_Textured_ShadowMap m_Model2;

    #pragma endregion

    #pragma region Constructors

    public:
        Program() :
            m_AmbientLight(),
            m_DirectionalLight(1024, 1024),
            m_SpotLight(1024, 1024),

            m_ModelFactory(
                /*program*/ std::make_shared<engine::MetaProgram>(engine::make_StandardDirectionalTexture2dProgram()),
                /*uniformBlock_AmbientLight*/ m_AmbientLight.Value(),
                /*uniformBlock_DirectionalLights*/ m_DirectionalLight.DirectionalLightArray(),
                /*uniformBlock_PointLights*/ std::make_shared<engine::UniformBlock_PointLightArray>(MAX_POINT_LIGHTS),
                /*uniformBlock_SpotLights*/  m_SpotLight.SpotLightArray(),
                /*uniformBlock_Camera*/ std::make_shared<engine::UniformBlock_CameraInfo>(),
                /*texture_shadowmap*/ m_DirectionalLight.getDepthTexture(),
                /*shadow_texture_sampler*/ m_DirectionalLight.ShadowTextureSampler()),

            m_Wall(m_ModelFactory.make_Model(
                /*mesh*/ std::make_shared<engine::MeshIndexed>(engine::makeRectanglePNT(10.0f, 10.0f)),
                /*texture_diffuse*/ std::make_shared<opengl::Texture2D>(engine::CreateTextureFromFile("textures/brick.png")))),

            m_Model1(m_ModelFactory.make_Model(
                /*mesh*/ std::make_shared<engine::MeshIndexed>(engine::makeCubePNT(2.0f)),
                /*texture_diffuse*/ std::make_shared<opengl::Texture2D>(engine::CreateTextureFromFile("textures/cube_map.bmp")))),

            m_Model2(m_ModelFactory.make_Model(
                /*mesh*/ std::make_shared<engine::MeshIndexed>(engine::makeCubePNT(0.5f)),
                /*texture_diffuse*/ std::make_shared<opengl::Texture2D>(engine::CreateTextureFromFile("textures/cube_map.bmp"))))
        {
            m_DirectionalLight.LightView().setWidth(5.0f);
            m_DirectionalLight.LightView().setHeight(5.0f);

            // Move the model forward so that camera could see it and the point light could illuminate it.
            m_Wall.PositionAndOrientation().MoveForwardBackwardWorld(15.0f);
            m_Model1.PositionAndOrientation().MoveForwardBackwardWorld(10.0f);
            m_Model2.PositionAndOrientation().MoveForwardBackwardWorld(3.0f);

            // Set the material of the model.
            m_Wall.getMaterial().setValue(
                engine::Material
                {
                    /*DiffuseColor*/  engine::Color_RGBA<GLfloat>{ 0.3f, 0.3f, 0.3f, 1.0f },
                    /*SpecularColor*/ engine::make_Color_RGBA32F_Gray(0.3f),
                    /*Shininess*/     100.0f
                });

            m_Model1.getMaterial().setValue(
                engine::Material
                {
                    /*DiffuseColor*/  engine::Color_RGBA<GLfloat>{ 0.2f, 0.2f, 0.2f, 1.0f },
                    /*SpecularColor*/ engine::make_Color_RGBA32F_Gray(0.3f),
                    /*Shininess*/     100.0f
                });

            m_Model2.getMaterial().setValue(
                engine::Material
                {
                    /*DiffuseColor*/  engine::Color_RGBA<GLfloat>{ 0.2f, 0.2f, 0.2f, 1.0f },
                    /*SpecularColor*/ engine::make_Color_RGBA32F_Gray(0.3f),
                    /*Shininess*/     100.0f
                });

            // Set point light source.
            m_ModelFactory.PointLights()->setLightColor(0, engine::make_Color_RGBA32F_Green());
            m_ModelFactory.PointLights()->setLightPosition(0, engine::vec4(-5.0f, 0.0f, 0.0f, 0.0f));
            m_ModelFactory.PointLights()->setAttenuation(0, engine::make_AttenuationStruct_Range3250());

            m_ModelFactory.PointLights()->setLightColor(1, engine::make_Color_RGBA32F_Blue());
            m_ModelFactory.PointLights()->setLightPosition(1, engine::vec4(5.0f, 0.0f, 0.0f, 0.0f));
            m_ModelFactory.PointLights()->setAttenuation(1, engine::make_AttenuationStruct_Range3250());

            m_ModelFactory.PointLights()->setLightColor(2, engine::make_Color_RGBA32F_Red());
            m_ModelFactory.PointLights()->setLightPosition(2, engine::vec4(0.0f, 5.0f, 0.0f, 0.0f));
            m_ModelFactory.PointLights()->setAttenuation(2, engine::make_AttenuationStruct_Range3250());

            m_ModelFactory.Program()->Program().setUniform("pointLightCount", m_ModelFactory.PointLights()->getCount());
            m_ModelFactory.Program()->Program().setUniform("dirLightCount", m_ModelFactory.DirectionalLights()->getCount());

            setupCamera(getCamera());

            //getWindow().KeyDownEvent() += fastdelegate::MakeDelegate(this, &Program::OnKeyDown);
        }

    #pragma endregion

    #pragma region Methods

    private:
        void OnKeyDown(std::tuple<win::Window*, WPARAM> eventArgs)
        {
            auto key = std::get<1>(eventArgs);

            if (key == VK_ESCAPE)
            {
                engine::SaveShadowmap(*m_DirectionalLight.getDepthTexture(), "C:\\DVSAV\\depth.bmp");
            }
        }

    protected:
        void UpdateTime() override
        {
            m_Model1.PositionAndOrientation().YawWorld(3.14f / 5.0f * TimeSinceLastFrame());
        }

        void setupCamera(const engine::Camera& cam) override
        {
            m_ModelFactory.Camera()->SetupCamera(cam);
        }

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            m_ModelFactory.Program()->getSubroutine("RenderPass").SelectFunction("recordDepth");

            m_DirectionalLight.RenderShadows(
                getCamera(),
                *m_ModelFactory.Camera(),
                [this]() -> void
                {
                    m_Model1.Render();
                    m_Model2.Render();
                });

            m_ModelFactory.Program()->getSubroutine("RenderPass").SelectFunction("shadeWithShadow");

            m_Wall.Render();
            m_Model1.Render();
            m_Model2.Render();
        }

    #pragma endregion
};
