/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "ProgramBase.h"
#include "OpenGLProgram.h"
#include "MeshArray.h"
#include "VertexArrayObject.h"

class Program final : public engine::ProgramBase
{
    #pragma region Fields

    private:
        opengl::Program m_Program;
        engine::MeshArray m_Mesh;
        opengl::VertexArrayObject m_VAO;

    #pragma endregion

    #pragma region Constructors

    public:
        Program() :
            m_Program(
                std::vector<opengl::Shader*>
                {
                    &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::VERTEX, "shaders/passthrough.vert"),
                    &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::FRAGMENT, "shaders/passthrough.frag")
                }),

            m_Mesh(
                /*vertexBuffer*/
                opengl::make_VertexBuffer(
                    std::vector<engine::VertexPC>
                    {
                        engine::VertexPC
                        {
                            engine::vec3{ -0.5f, 0.5f, 0.5f },
                            engine::make_Color_RGBA32F_Red()
                        },
                        engine::VertexPC
                        {
                            engine::vec3{ 0.0f, -0.5f, 0.5f },
                            engine::make_Color_RGBA32F_Green()
                        },
                        engine::VertexPC
                        {
                            engine::vec3{ 0.5f, 0.5f, 0.5f },
                            engine::make_Color_RGBA32F_Blue()
                        }
                    },
                    opengl::Buffer::DRAW_USAGE::STATIC_DRAW),

                /*drawing_mode*/
                opengl::DRAWING_MODE::TRIANGLES),

            m_VAO()
        {
            opengl::VertexArrayObject::Binding bind_vao(m_VAO);
            m_Mesh.AttachAttributeToAttribLocation(engine::VertexAttributeEnum::Position, m_Program.getAttributeLocation("v3_position"));
            m_Mesh.AttachAttributeToAttribLocation(engine::VertexAttributeEnum::Color, m_Program.getAttributeLocation("v4_color"));
        }

    #pragma endregion

    #pragma region Methods

    protected:
        void UpdateTime() override { }
        void setupCamera(const engine::Camera& cam) override { }

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            opengl::Program::Using use(m_Program, true);
            opengl::VertexArrayObject::Binding bind_vao(m_VAO);
            m_Mesh.Render();
        }

    #pragma endregion
};
