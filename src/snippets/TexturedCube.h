/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "ProgramBase.h"
#include "MeshIndexed.h"
#include "Model3d_Textured.h"

const int MAX_POINT_LIGHTS = 3;

class Program : public engine::ProgramBase
{
    #pragma region Fields

    private:
        engine::Model3d_Textured<opengl::Texture2D> m_Model;

    #pragma endregion

    #pragma region Constructors

    public:
        Program() :
            m_Model(
                /*program*/ make_MetaProgram(),
                /*mesh*/ std::make_shared<engine::MeshIndexed>(engine::makeCubePNT(2.0f)),
                /*uniformBlock_AmbientLight*/ std::make_shared<engine::UniformBlock_AmbientLight>(),
                /*uniformBlock_DirectionalLights*/ std::make_shared<engine::UniformBlock_DirectionalLightArray>(1),
                /*uniformBlock_PointLights*/ std::make_shared<engine::UniformBlock_PointLightArray>(MAX_POINT_LIGHTS),
                /*uniformBlock_Camera*/ std::make_shared<engine::UniformBlock_CameraInfo>(),
                /*texture*/ std::make_shared<opengl::Texture2D>(engine::CreateTextureFromFile("textures/cube_map.bmp")))
        {
            // Move the model forward so that camera could see it and the point light could illuminate it.
            m_Model.PositionAndOrientation().MoveForwardBackwardWorld(10.0f);

            // Set the material of the model.
            m_Model.getMaterial().setValue(
                engine::Material
                {
                    /*DiffuseColor*/  engine::Color_RGBA<GLfloat>{ 0.2f, 0.2f, 0.2f, 1.0f },
                    /*SpecularColor*/ engine::make_Color_RGBA32F_Gray(0.3f),
                    /*Shininess*/     100.0f
                });

            // Set point light source.
            m_Model.getPointLightArray().setValue(
                0,
                engine::PointLightStruct
                {
                    engine::make_Color_RGBA32F_Green(), // LightColor
                    engine::vec4(-5.0f, 0.0f, 0.0f, 0.0f), // LightPosition
                    engine::mat4(), // WorldToLightProjectionPlane
                    engine::make_AttenuationStruct_Range3250() // Attenuation
                });
            m_Model.getPointLightArray().setValue(
                1,
                engine::PointLightStruct
                {
                    engine::make_Color_RGBA32F_Blue(), // LightColor
                    engine::vec4(5.0f, 0.0f, 0.0f, 0.0f), // LightPosition
                    engine::mat4(), // WorldToLightProjectionPlane
                    engine::make_AttenuationStruct_Range3250() // Attenuation
                });
            m_Model.getPointLightArray().setValue(
                2,
                engine::PointLightStruct
                {
                    engine::make_Color_RGBA32F_Red(), // LightColor
                    engine::vec4(0.0f, 5.0f, 0.0f, 0.0f), // LightPosition
                    engine::mat4(), // WorldToLightProjectionPlane
                    engine::make_AttenuationStruct_Range3250() // Attenuation
                });

            m_Model.getProgram()->Program().setUniform("pointLightCount", MAX_POINT_LIGHTS);

            setupCamera(getCamera());
        }

    #pragma endregion

    #pragma region Methods

    private:
        std::shared_ptr<engine::MetaProgram> make_MetaProgram()
        {
            return std::make_shared<engine::MetaProgram>(
                engine::MetaProgram::make_MetaProgram_Textured_2d_PointLights(
                    /*program*/
                    opengl::Program
                    {
                        &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::VERTEX,   "shaders/phong_texture2d.vert"),
                        &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::FRAGMENT, "shaders/phong_texture2d_point_lights.frag")
                    },
                    /*attribute_position*/ "v3_position",
                    /*attribute_normal*/   "v3_normal",
                    /*attribute_texture*/  "v2_tex_coords",
                    /*uniform_block_model_to_world*/ "ModelToWorld",
                    /*uniform_block_material*/       "Material",
                    /*uniform_block_ambient_light*/  "AmbientLight",
                    /*uniform_block_point_lights*/   "PointLights",
                    /*uniform_block_camera_info*/    "CameraInfo",
                    /*texture_diffuse*/ "Texture2dDiffuse"));
        }

    protected:
        void UpdateTime() override
        {
            m_Model.PositionAndOrientation().YawWorld(3.14f / 2.0f * TimeSinceLastFrame());
        }

        void setupCamera(const engine::Camera& cam) override
        {
            m_Model.getCameraInfo().SetupCamera(cam);
        }

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            m_Model.Render();
        }

    #pragma endregion
};
