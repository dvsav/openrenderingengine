/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "ProgramBase.h"

#include "MeshIndexed.h"
#include "MetaProgram.h"
#include "Model3d_SimpleColored.h"
#include "StandardMetaprograms.h"

class Program final : public engine::ProgramBase
{
    #pragma region Fields

    private:
        engine::Model3d_SimpleColored m_Model;

    #pragma endregion

    #pragma region Constructors

    public:
        Program() :
            m_Model(
                /*program*/ engine::standard::SimpleColoredProgram(),
                /*mesh*/ make_Mesh())
        {
            opengl::setCullFaceMode(opengl::CULL_FACE_MODE::DISABLED);

            getCamera().PositionAndOrientation().Position().Z() = -5.0f;

            setupCamera(getCamera());
        }

    #pragma endregion

    #pragma region Methods

    private:
        std::shared_ptr<engine::MeshBase> make_Mesh()
        {
            std::vector<engine::VertexPC> vertices
            {
                engine::VertexPC{ engine::vec3{ -1.0f, -1.0f, -1.0f }, engine::make_Color_RGBA32F_Red() },
                engine::VertexPC{ engine::vec3{  1.0f, -1.0f, -1.0f }, engine::make_Color_RGBA32F_Red() },
                engine::VertexPC{ engine::vec3{  1.0f,  1.0f, -1.0f }, engine::make_Color_RGBA32F_Red() },
                engine::VertexPC{ engine::vec3{ -1.0f,  1.0f, -1.0f }, engine::make_Color_RGBA32F_Blue() },
                engine::VertexPC{ engine::vec3{ -1.0f, -1.0f,  1.0f }, engine::make_Color_RGBA32F_Blue() },
                engine::VertexPC{ engine::vec3{  1.0f, -1.0f,  1.0f }, engine::make_Color_RGBA32F_Blue() },
                engine::VertexPC{ engine::vec3{  1.0f,  1.0f,  1.0f }, engine::make_Color_RGBA32F_Green() },
                engine::VertexPC{ engine::vec3{ -1.0f,  1.0f,  1.0f }, engine::make_Color_RGBA32F_Green() }
            };

            std::vector<GLuint> indices
            {
                0, 1, 2,
                0, 2, 3,
                1, 5, 6,
                1, 6, 2,
                5, 4, 7,
                5, 7, 6,
                4, 0, 3,
                4, 3, 7
            };
            
            return std::make_shared<engine::MeshIndexed>(
                /*vertexBuffer*/ opengl::VertexBuffer<engine::VertexPC>(vertices, opengl::Buffer::DRAW_USAGE::STATIC_DRAW),
                /*indexBuffer*/ opengl::IndexBuffer(indices),
                /*drawing_mode*/ opengl::DRAWING_MODE::TRIANGLES);
        }

    protected:
        void UpdateTime() override
        {
            m_Model.PositionAndOrientation().YawWorld(3.14f / 2.0f * TimeSinceLastFrame());
        }

        void setupCamera(const engine::Camera& cam) override
        {
            m_Model.SetupCamera(cam);
        }

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            m_Model.Render();
        }

    #pragma endregion
};
