/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ProgramBase.h"
#include "OpenglProgram.h"
#include "VertexArrayObject.h"
#include "Buffer.h"
#include "Vertex.h"
#include "WorldPositionAndOrientation.h"
#include "UniformBlockBuffer.h"

using namespace engine;

class Program final : public ProgramBase
{
    private:
        opengl::Program m_Program;
        std::unique_ptr<opengl::Buffer> m_VertexBufferObject;
        opengl::VertexArrayObject m_VAO;

        WorldPositionAndOrientation m_TrianglePositionAndOrientation;
        opengl::UniformBlockStructure<mat4> m_MVPMatrixUniformBlockBuffer; // the buffer where MVP matrix is going to be stored
        const GLuint m_UniformBlockBindingPointForMVPMatrix = 0;           // the uniform binding point index

    public:
        Program() :
            m_Program(
                std::vector<opengl::Shader*>
        {
            &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::VERTEX, "shaders/simple_colored.vert"),
                & opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::FRAGMENT, "shaders/passthrough.frag")
        }),
            m_VAO(),
                m_TrianglePositionAndOrientation(),
                m_MVPMatrixUniformBlockBuffer(vmath::make_IdentityMatrix4x4<GLfloat>(), opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
        {
            // Move the triangle 10 units forward so that it appears before the camera and we can see it.
            m_TrianglePositionAndOrientation.MoveForwardBackward(10);

            // --- Create buffer and populate it with vertex data ---
            VertexPC vertexData[]
            {
                VertexPC
                {
                    vec3{ -0.5f, 0.5f, 0.5f },
                    color_rgba32f{ 1.0f, 0.0f, 0.0f, 1.0f }
                },
                VertexPC
                {
                    vec3{ 0.0f, -0.5f, 0.5f },
                    color_rgba32f{ 0.0f, 1.0f, 0.0f, 1.0f }
                },
                VertexPC
                {
                    vec3{ 0.5f, 0.5f, 0.5f },
                    color_rgba32f{ 0.0f, 0.0f, 1.0f, 1.0f }
                }
            };

            m_VertexBufferObject = std::make_unique<opengl::Buffer>(
                sizeof(vertexData),
                opengl::Buffer::TARGET::ARRAY,
                opengl::Buffer::DRAW_USAGE::STATIC_DRAW,
                vertexData);

            // --- Create Vertex Array Object (VAO) and store vertex data format in it ---
            opengl::VertexArrayObject::Binding bind_vao(m_VAO);
            opengl::Buffer::Binding binding(*m_VertexBufferObject, opengl::Buffer::TARGET::ARRAY);

            GLuint positionAttribLocation = m_Program.getAttributeLocation("v3_position");
            glVertexAttribPointerWithFun(positionAttribLocation, VertexPC, Position, GL_FALSE);
            glEnableVertexAttribArray(positionAttribLocation);

            GLuint colorAttribLocation = m_Program.getAttributeLocation("v4_color");
            glVertexAttribPointerWithFun(colorAttribLocation, VertexPC, Color, GL_FALSE);
            glEnableVertexAttribArray(colorAttribLocation);

            // Bind together the uniform block in the shader program and the uniform block buffer.
            m_Program.setUniformBlockBinding("ModelViewPerspective", m_UniformBlockBindingPointForMVPMatrix);
            m_MVPMatrixUniformBlockBuffer.BindToUniformBindingPoint(m_UniformBlockBindingPointForMVPMatrix);
        }

    protected:
        void UpdateTime() override
        {
            // Rotate the triangle.
            m_TrianglePositionAndOrientation.RollWorld(TimeSinceLastFrame());

            // Store the new value of the MVP matrix in the uniform block buffer.
            m_MVPMatrixUniformBlockBuffer.setValue(
                getCamera().getCameraToScreenMatrix() * getCamera().getWorldToCameraMatrix() * m_TrianglePositionAndOrientation.ModelToWorldMatrix());
        }

        void setupCamera(const Camera& cam) override {}

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            opengl::Program::Using use(m_Program);
            opengl::VertexArrayObject::Binding bind_vao(m_VAO);
            glDrawArrays(GL_TRIANGLES, 0, 3);
        }
};
