/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"
#include "ProgramBase.h"
#include "pugixml_loader.h"

class Game
{
    #pragma region Fields

    private:
        engine::ProgramBase& m_Program;
        engine::Model3d_Skybox m_Skybox;
        engine::Scene m_Scene;
        engine::Model3d_Base& m_Flashlight;
        engine::Model3d_Base& m_Flame;
        engine::Model3d_Base& m_Flame2;

    #pragma endregion

    #pragma region Constructors

    public:
        Game(engine::ProgramBase& program) :
            m_Program(program),
            m_Skybox(engine::make_Skybox(engine::load_Texture("assets/textures/milkyway/texture_milkyway.xml"))),
            m_Scene(engine::load_Scene("assets/scenes/quixel.xml")),
            m_Flashlight(m_Scene.findModel("flashlight")),
            m_Flame(m_Scene.findModel("flame")),
            m_Flame2(m_Scene.findModel("flame2"))
        {
            m_Program.getWindow().KeyDownEvent() += fastdelegate::MakeDelegate(this, &Game::OnKeyDown);
            ::ShowCursor(false);
        }

    #pragma endregion

    #pragma region Methods

    private:
        void OnKeyDown(std::tuple<win::Window*, WPARAM> eventArgs)
        {
            static bool on = true;
            auto key = std::get<1>(eventArgs);

            if (key == VK_SPACE)
            {
                on = !on;
                m_Scene.SpotLights()->setLightColor(0, on ? engine::make_Color_RGBA32F_Gray(2.0f) : engine::make_Color_RGBA32F_Black());
            }
        }

    public:
        void UpdateTime()
        {
            auto sinus = std::sinf(3.14f * m_Program.CurrentTime());
            engine::color_rgba32f candle_color{ 0.9f + 0.1f * sinus, 0.8, 0.32f, 1.0f };
            engine::color_rgba32f candle_color2{ 0.9f - 0.1f * sinus, 0.8, 0.32f, 1.0f };

            m_Flame.PositionAndOrientation().MoveRightLeft(0.0025f * sinus);
            m_Flame.PositionAndOrientation().Roll(util::to_radians(0.4f * sinus));
            m_Flame.getUniformBlock<engine::UniformBlockEnum::Material>().setEmissiveColor(candle_color);

            m_Flame2.PositionAndOrientation().MoveRightLeft(-0.0025f * sinus);
            m_Flame2.PositionAndOrientation().Roll(util::to_radians(-0.3f * sinus));
            m_Flame2.getUniformBlock<engine::UniformBlockEnum::Material>().setEmissiveColor(candle_color);

            m_Scene.PointLights()->setLightColor(0, candle_color);
            m_Scene.PointLights()->LightView(0).PositionAndOrientation().MoveRightLeft(0.004f * sinus);
            m_Scene.PointLights()->setAttenuation(0, engine::AttenuationStruct{ 30.0f, 1.0f, 0.0f, 0.02f + 0.01f * sinus });

            m_Scene.PointLights()->setLightColor(1, candle_color2);
            m_Scene.PointLights()->LightView(1).PositionAndOrientation().MoveRightLeft(-0.004f * sinus);
            m_Scene.PointLights()->setAttenuation(1, engine::AttenuationStruct{ 30.0f, 1.0f, 0.0f, 0.02f - 0.01f * sinus });
        }

        void SetupCamera(const engine::Camera& cam)
        {
            auto& orientation = cam.PositionAndOrientation();

            m_Flashlight.PositionAndOrientation().Position() =
            m_Scene.SpotLights()->LightView(0).PositionAndOrientation().Position() =
                 orientation.ModelToWorldMatrix() * engine::vec4{ 2.0f, -2.0f, 5.0f, 1.0f };
            
            m_Scene.SpotLights()->LightView(0).PositionAndOrientation().setForwardDirection(orientation.getForwardDirection().vec3());
            m_Flashlight.PositionAndOrientation().setForwardDirection(-orientation.getForwardDirection().vec3());
            
            m_Skybox.SetupCamera(cam);
            m_Scene.Camera()->SetupCamera(cam);
        }

        void Render()
        {
            m_Scene.Render();
            m_Skybox.Render();
        }

    #pragma endregion
};
