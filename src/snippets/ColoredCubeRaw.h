/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ProgramBase.h"
#include "OpenglProgram.h"
#include "VertexArrayObject.h"
#include "Buffer.h"
#include "Vertex.h"
#include "WorldPositionAndOrientation.h"
#include "UniformBlockBuffer.h"

using namespace engine;

class Program final : public ProgramBase
{
    private:
        opengl::Program m_Program;
        std::unique_ptr<opengl::Buffer> m_VertexBufferObject;
        std::unique_ptr<opengl::Buffer> m_IndexBufferObject;
        opengl::VertexArrayObject m_VAO;

        WorldPositionAndOrientation m_PositionAndOrientation;
        opengl::UniformBlockStructure<mat4> m_MVPMatrixUniformBlockBuffer; // the buffer where MVP matrix is going to be stored
        const GLuint m_UniformBlockBindingPointForMVPMatrix = 0;           // the uniform binding point index

    public:
        Program() :
            m_Program(
                std::vector<opengl::Shader*>
                {
                    &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::VERTEX,   "shaders/simple_colored.vert"),
                    &opengl::make_ShaderFromFile(opengl::Shader::SHADER_TYPE::FRAGMENT, "shaders/passthrough.frag")
                }),
            m_VAO(),
            m_PositionAndOrientation(),
            m_MVPMatrixUniformBlockBuffer(vmath::make_IdentityMatrix4x4<GLfloat>(), opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
        {
            opengl::setCullFaceMode(opengl::CULL_FACE_MODE::DISABLED);

            // Move the triangle 10 units forward so that it appears before the camera and we can see it.
            m_PositionAndOrientation.MoveForwardBackward(10);

            // --- Create a buffer and populate it with vertex data ---
            VertexPC vertexData[]
            {
                engine::VertexPC { engine::vec3{ -1.0f, -1.0f, -1.0f }, engine::make_Color_RGBA32F_Red() },
                engine::VertexPC { engine::vec3{  1.0f, -1.0f, -1.0f }, engine::make_Color_RGBA32F_Red() },
                engine::VertexPC { engine::vec3{  1.0f,  1.0f, -1.0f }, engine::make_Color_RGBA32F_Red() },
                engine::VertexPC { engine::vec3{ -1.0f,  1.0f, -1.0f }, engine::make_Color_RGBA32F_Blue() },
                engine::VertexPC { engine::vec3{ -1.0f, -1.0f,  1.0f }, engine::make_Color_RGBA32F_Blue() },
                engine::VertexPC { engine::vec3{  1.0f, -1.0f,  1.0f }, engine::make_Color_RGBA32F_Blue() },
                engine::VertexPC { engine::vec3{  1.0f,  1.0f,  1.0f }, engine::make_Color_RGBA32F_Green() },
                engine::VertexPC { engine::vec3{ -1.0f,  1.0f,  1.0f }, engine::make_Color_RGBA32F_Green() }
            };

            m_VertexBufferObject = std::make_unique<opengl::Buffer>(
                sizeof(vertexData),
                opengl::Buffer::TARGET::ARRAY,
                opengl::Buffer::DRAW_USAGE::STATIC_DRAW,
                vertexData);

            // --- Create a buffer and populate it with index data ---
            GLuint indexData[]
            {
                0, 1, 2,
                0, 2, 3,
                1, 5, 6,
                1, 6, 2,
                5, 4, 7,
                5, 7, 6,
                4, 0, 3,
                4, 3, 7
            };

            m_IndexBufferObject = std::make_unique<opengl::Buffer>(
                sizeof(indexData),
                opengl::Buffer::TARGET::ELEMENT_ARRAY,
                opengl::Buffer::DRAW_USAGE::STATIC_DRAW,
                indexData);

            // --- Create Vertex Array Object (VAO) and store vertex data format in it ---
            {
                opengl::VertexArrayObject::Binding bind_vao(m_VAO);
                opengl::Buffer::Binding binding(*m_VertexBufferObject, opengl::Buffer::TARGET::ARRAY);

                GLuint positionAttribLocation = m_Program.getAttributeLocation("v3_position");
                glVertexAttribPointerWithFun(positionAttribLocation, VertexPC, Position, GL_FALSE);
                glEnableVertexAttribArray(positionAttribLocation);

                GLuint colorAttribLocation = m_Program.getAttributeLocation("v4_color");
                glVertexAttribPointerWithFun(colorAttribLocation, VertexPC, Color, GL_FALSE);
                glEnableVertexAttribArray(colorAttribLocation);

                m_IndexBufferObject->Bind(opengl::Buffer::TARGET::ELEMENT_ARRAY);
            }
            opengl::Buffer::UnbindAll(opengl::Buffer::TARGET::ELEMENT_ARRAY);

            // Bind together the uniform block in the shader program and the uniform block buffer.
            m_Program.setUniformBlockBinding("ModelViewPerspective", m_UniformBlockBindingPointForMVPMatrix);
            m_MVPMatrixUniformBlockBuffer.BindToUniformBindingPoint(m_UniformBlockBindingPointForMVPMatrix);
        }

    protected:
        void UpdateTime() override
        {
            // Rotate the triangle.
            m_PositionAndOrientation.PitchWorld(TimeSinceLastFrame() / 2.0);

            // Store the new value of the MVP matrix in the uniform block buffer.
            m_MVPMatrixUniformBlockBuffer.setValue(
                getCamera().getCameraToScreenMatrix() * getCamera().getWorldToCameraMatrix() * m_PositionAndOrientation.ModelToWorldMatrix());
        }

        void setupCamera(const Camera& cam) override {}

        void OnRender(opengl::OpenGLWindow* sender) override
        {
            opengl::Program::Using use(m_Program);
            opengl::VertexArrayObject::Binding bind_vao(m_VAO);

            glDrawElements(
                /*mode*/    GL_TRIANGLES,
                /*count*/   m_IndexBufferObject->getSize() / sizeof(GLuint),
                /*type*/    GL_UNSIGNED_INT,
                /*indices*/ 0);
        }
};
