#pragma once

#include "ConditionalCompilation.h"

#include "pugixml.hpp"

#include "MetaProgram.h"
#include "TextureCubeMap.h"
#include "MeshArray.h"
#include "MeshIndexed.h"
#include "MeshSphere.h"
#include "Model3d_Base.h"
#include "Scene.h"

namespace engine
{
    std::shared_ptr<MetaProgram>& load_MetaProgram(
        const char* file_name);

    std::shared_ptr<opengl::Texture> load_Texture(
        const char* file_name);

    std::shared_ptr<MeshBase> load_Mesh(
        const char* file_name,
        const vec3& scale);

    Material load_Material(
        const char* file_name);

    Model3d_Base load_Model(
        const char* file_name);

    Scene load_Scene(
        const char* file_name);

    void clear_ProgramCache();
}
