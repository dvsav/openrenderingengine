/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <type_traits>

#include "Buffer.h"
#include "utility.h"

namespace opengl
{
    class VertexBufferBase : public Buffer
    {
        #pragma region Fields

        private:
            const GLsizei m_VertexSize;
            const GLsizei m_VertexCount;

        #pragma endregion

        #pragma region Constructors

        public:
            template<typename _Container>
            VertexBufferBase(
                const _Container& vertices,
                opengl::Buffer::DRAW_USAGE usage) :

                Buffer(make_Buffer<_Container>(vertices, Buffer::TARGET::ARRAY, usage)),

                m_VertexSize(sizeof(typename _Container::value_type)),
                m_VertexCount(vertices.size())
            { }

            // Move constructor.
            VertexBufferBase(VertexBufferBase&& value) noexcept :

                Buffer(std::move(value)),

                m_VertexSize(value.m_VertexSize),
                m_VertexCount(value.m_VertexCount)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            // Gets the vertex size in bytes.
            GLsizei getVertexSize() const { return m_VertexSize; }

            // Gets the number of vertices.
            GLsizei getVertexCount() const { return m_VertexCount; }

        #pragma endregion
    };

    template<typename TVertex>
    class VertexBuffer : public VertexBufferBase
    {
        #pragma region Type Aliases

        public:
            using vertex_type = TVertex;

        #pragma endregion

        #pragma region Constructors

        public:
            template<typename _Container>
            VertexBuffer(
                const _Container& vertices,
                opengl::Buffer::DRAW_USAGE usage) :
                VertexBufferBase(vertices, usage)
            {
                static_assert(std::is_same<typename _Container::value_type, TVertex>::value, "Vertex type mismatch");
            }

            // Move constructor.
            VertexBuffer(VertexBuffer&& value) noexcept :
                VertexBufferBase(std::move(value))
            { }

        #pragma endregion

        #pragma region Properties

        public:
            /*
            Changes a single vertex in the buffer.
            Parameters:
            [index] - index of the vertex to change.
            [vertex] - the new value of the vertex.
            */
            void setVertex(
                int index,
                const TVertex& vertex)
            {
                setData(
                    /*offset*/ index * sizeof(TVertex),
                    /*size*/ sizeof(TVertex),
                    /*data*/ &vertex);
            }

            // Gets a vertex with a specified [index] from the buffer.
            TVertex getVertex(int index)
            {
                TVertex& vertex;

                getData(
                    /*offset*/ index * sizeof(TVertex),
                    /*size*/ sizeof(TVertex),
                    /*buffer*/ &vertex);

                return vertex;
            }

        #pragma endregion
    };

    template<typename _Container>
    auto make_VertexBuffer(
        const _Container& vertices,
        opengl::Buffer::DRAW_USAGE usage) -> VertexBuffer<typename _Container::value_type>
    {
        return VertexBuffer<typename _Container::value_type>(vertices, usage);
    }
}