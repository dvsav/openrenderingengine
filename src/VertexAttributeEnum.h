/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Vertex.h"

namespace engine
{
    // Vertex attribute type.
    enum class VertexAttributeEnum
    {
        Position,
        Color,
        Normal,
        Binormal,
        Tangent,
        TexCoords
    };

    inline VertexAttributeEnum parse_VertexAttributeEnum(const char* str)
    {
        if (std::strcmp(str, "Position") == 0)
            return VertexAttributeEnum::Position;
        else if (std::strcmp(str, "Color") == 0)
            return VertexAttributeEnum::Color;
        else if (std::strcmp(str, "Normal") == 0)
            return VertexAttributeEnum::Normal;
        else if (std::strcmp(str, "TexCoords") == 0)
            return VertexAttributeEnum::TexCoords;
        else if (std::strcmp(str, "Tangent") == 0)
            return VertexAttributeEnum::Tangent;
        else if (std::strcmp(str, "Binormal") == 0)
            return VertexAttributeEnum::Binormal;
        else
            throw std::invalid_argument(str);
    }

    template<typename TVertex, VertexAttributeEnum VertexType>
    struct has_attribute : std::false_type { };

    template<>
    struct has_attribute<VertexP, VertexAttributeEnum::Position> : std::true_type { };

    template<>
    struct has_attribute<VertexPC, VertexAttributeEnum::Position> : std::true_type { };

    template<>
    struct has_attribute<VertexPN, VertexAttributeEnum::Position> : std::true_type { };

    template<>
    struct has_attribute<VertexPT, VertexAttributeEnum::Position> : std::true_type { };

    template<>
    struct has_attribute<VertexPNT, VertexAttributeEnum::Position> : std::true_type { };

    template<>
    struct has_attribute<VertexPNTT, VertexAttributeEnum::Position> : std::true_type { };

    template<>
    struct has_attribute<VertexPC, VertexAttributeEnum::Color> : std::true_type { };

    template<>
    struct has_attribute<VertexPN, VertexAttributeEnum::Normal> : std::true_type { };

    template<>
    struct has_attribute<VertexPNT, VertexAttributeEnum::Normal> : std::true_type { };

    template<>
    struct has_attribute<VertexPNTT, VertexAttributeEnum::Normal> : std::true_type { };

    template<>
    struct has_attribute<VertexPT, VertexAttributeEnum::TexCoords> : std::true_type { };

    template<>
    struct has_attribute<VertexPNT, VertexAttributeEnum::TexCoords> : std::true_type { };

    template<>
    struct has_attribute<VertexPNTT, VertexAttributeEnum::TexCoords> : std::true_type { };

    template<>
    struct has_attribute<VertexPNTT, VertexAttributeEnum::Tangent> : std::true_type { };
}