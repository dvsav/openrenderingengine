/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <type_traits>

#include "Buffer.h"

namespace opengl
{
    class IndexBuffer : public Buffer
    {
        #pragma region Fields

        private:
            GLenum m_IndexType;
            GLsizei m_IndexCount;

        #pragma endregion

        #pragma region Constructors

        public:
            template<typename _Container>
            IndexBuffer(const _Container& indices) :

                Buffer(make_Buffer<_Container>(
                    indices,
                    Buffer::TARGET::ELEMENT_ARRAY,
                    opengl::Buffer::DRAW_USAGE::STATIC_DRAW)),

                m_IndexType(static_cast<GLenum>(opengl::type_traits<typename _Container::value_type>::gl_type)),
                m_IndexCount(indices.size())
            {
                static_assert(std::is_unsigned<typename _Container::value_type>::value, "_Container::value_type must be unsigned");
            }

            // Move constructor.
            IndexBuffer(IndexBuffer&& value) noexcept :
                Buffer(std::move(value)),
                m_IndexType(value.m_IndexType),
                m_IndexCount(value.m_IndexCount)
            {
                value.m_IndexCount = 0;
            }

        #pragma endregion

        #pragma region Properties

        public:
            // Gets index type.
            GLenum getIndexType() const { return m_IndexType; }

            // Gets the number of indices.
            GLsizei getIndexCount() const { return m_IndexCount; }

        #pragma endregion
    };
}