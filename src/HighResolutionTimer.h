/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <cstdint>
#include <Windows.h>
#include "winapi_error.h"

namespace win
{
    namespace HighResolutionTimer
    {
        // Gets the number of ticks passed since the start of the operation system.
        static uint64_t getTicks()
        {
            LARGE_INTEGER ticks;
            if (QueryPerformanceCounter(&ticks))
                return ticks.QuadPart;
            else
                throw make_winapi_error("HighResolutionTimer::getTicks() -> QueryPerformanceCounter()");
        }

        // Gets the number of ticks per second.
        static uint64_t getTicksPerSecond()
        {
            static uint64_t s_TicksPerSecond{0};

            if (s_TicksPerSecond == 0)
            {
                LARGE_INTEGER freq;
                if (QueryPerformanceFrequency(&freq))
                    s_TicksPerSecond = freq.QuadPart;
                else
                    throw make_winapi_error("HighResolutionTimer::getTicksPerSecond() -> QueryPerformanceFrequency()");
            }
            return s_TicksPerSecond;
        }
    };
}