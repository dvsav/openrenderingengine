/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <fstream>

#if !_HAS_CXX17
    #define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    #include <filesystem>
    namespace fs = std::filesystem;
#endif

namespace util
{
    namespace file
    {
        /*
        Returns all text read from specified text file.
        Parameters: filename - path to the file
        */
        inline std::string ReadAllText(const std::string& filename)
        {
            std::ifstream fileStream(filename);

            if (!fileStream)
                throw std::runtime_error("util::File::ReadAllText() -> Cannot open file: " + filename);

            std::string text;
            text.assign(
                std::istreambuf_iterator<char>(fileStream),
                std::istreambuf_iterator<char>());

            return text;
        }

        // Gets file extension. For example getFileExtension("myfile.txt") gives "txt".
        inline std::string getFileExtension(const std::string& filename)
        {
            size_t i = filename.rfind('.', filename.length());
            if (i != std::string::npos)
                return(filename.substr(i + 1, filename.length() - i));
            else
                return("");
        }

        inline std::string getCurrentDirectory()
        {
            return fs::current_path().generic_string();
        }

        inline std::string getFileName(const std::string& filepath)
        {
            return fs::path(filepath).filename().generic_string();
        }

        inline std::string ReplaceFileName(
            const std::string& filepath, const std::string& filename)
        {
            return fs::path(filepath).replace_filename(filename).generic_string();
        }

        inline fs::path getDirectory(const std::string& filepath)
        {
            return fs::path(filepath).parent_path();
        }

        inline bool equivalent(const fs::path& a, const fs::path& b)
        {
#if _HAS_CXX17
            return fs::equivalent(a, b);
#else
            return a == b;
#endif
        }
    }
}