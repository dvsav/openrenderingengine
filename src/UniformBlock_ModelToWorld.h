/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "UniformBlockBuffer.h"

#include "EngineTypedefs.h"

namespace engine
{
    struct ModelToWorldStruct
    {
        mat4 ModelToWorld;
        mat4 NormalToWorld; // Transforms [Vertex Normals] from [model space] to [world space]
    };

    class UniformBlock_ModelToWorld : public opengl::UniformBlockStructure<ModelToWorldStruct>
    {
        #pragma region Type Aliases

        public:
            using TModelToWorld = decltype(ModelToWorldStruct::ModelToWorld);
            using TNormalToWorld = decltype(ModelToWorldStruct::NormalToWorld);

        #pragma endregion

        #pragma region Constructors

        public:
            UniformBlock_ModelToWorld() :
                opengl::UniformBlockStructure<ModelToWorldStruct>(
                    ModelToWorldStruct
                    {
                        vmath::make_IdentityMatrix<GLfloat, 4>(),
                        vmath::make_IdentityMatrix<GLfloat, 4>(),
                    },
                    opengl::Buffer::DRAW_USAGE::DYNAMIC_DRAW)
            { }

        #pragma endregion

        #pragma region Properties

        public:
            void set_ModelAndNormalToWorld(const mat4&& value)
            {
                this->Change([&value](ModelToWorldStruct& data)
                {
                    data.ModelToWorld = value;
                    data.NormalToWorld = vmath::eliminateTranslation(std::move(value));
                });
            }

            void setModelToWorld(const TModelToWorld& value)
            {
                this->setData(
                    /*offset*/ offsetof(ModelToWorldStruct, ModelToWorldStruct::ModelToWorld),
                    /*size*/ sizeof(TModelToWorld),
                    /*data*/ &value);
            }

            TModelToWorld getModelToWorld() const
            {
                TModelToWorld value;
                this->getData(
                    /*offset*/ offsetof(ModelToWorldStruct, ModelToWorldStruct::ModelToWorld),
                    /*size*/ sizeof(TModelToWorld),
                    /*data*/ &value);
                return value;
            }

            void setNormalToWorld(const TNormalToWorld& value)
            {
                this->setData(
                    /*offset*/ offsetof(ModelToWorldStruct, ModelToWorldStruct::NormalToWorld),
                    /*size*/ sizeof(TNormalToWorld),
                    /*data*/ &value);
            }

            TNormalToWorld getNormalToWorld() const
            {
                TNormalToWorld value;
                this->getData(
                    /*offset*/ offsetof(ModelToWorldStruct, ModelToWorldStruct::NormalToWorld),
                    /*size*/ sizeof(TNormalToWorld),
                    /*data*/ &value);
                return value;
            }

        #pragma endregion
    };
}