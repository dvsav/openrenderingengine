/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <map>
#include "OpenGLProgramStage.h"

namespace opengl
{
    class ProgramPipeline
    {
        #pragma region Nested Types

        public:
            enum class SHADER_STAGES : GLbitfield
            {
                VERTEX = GL_VERTEX_SHADER_BIT,
                TESS_CONTROL = GL_TESS_CONTROL_SHADER_BIT,
                TESS_EVALUATION = GL_TESS_EVALUATION_SHADER_BIT,
                GEOMETRY = GL_GEOMETRY_SHADER_BIT,
                FRAGMENT = GL_FRAGMENT_SHADER_BIT,
                COMPUTE = GL_COMPUTE_SHADER_BIT,
                ALL = GL_ALL_SHADER_BITS
            };

            friend ProgramPipeline::SHADER_STAGES operator|(
                ProgramPipeline::SHADER_STAGES lhs,
                ProgramPipeline::SHADER_STAGES rhs);

            friend bool operator!=(ProgramPipeline::SHADER_STAGES lhs, GLbitfield rhs);

            class Binding final
            {
                private:
                    GLint m_PreviousPipelineHandle;

                private:
                    Binding(const Binding&) = delete;
                    Binding& operator=(const Binding&) = delete;

                public:
                    Binding(
                        const ProgramPipeline& pipeline,
                        bool useForRendering = false) :
                        m_PreviousPipelineHandle(ProgramPipeline::getCurrentPipelineHandle())
                    {
                        pipeline.Bind();
                        if (useForRendering)
                        {
                            for (auto& program : pipeline.m_ProgramStages)
                                program.second->SetupSubroutines();
                        }
                    }

                    ~Binding()
                    {
                        glBindProgramPipeline(m_PreviousPipelineHandle);
                    }
            };

        #pragma endregion

        #pragma region Fields

        private:
            GLuint m_Handle;
            std::map<SHADER_STAGES, ProgramStage*> m_ProgramStages;

        #pragma endregion

        #pragma region Constructors

        public:
            ProgramPipeline() :
                m_Handle(0),
                m_ProgramStages()
            {
                glGenProgramPipelines(1, &m_Handle);
            }

        #pragma endregion

        #pragma region Destructor

        public:
            ~ProgramPipeline()
            {
                glDeleteProgramPipelines(1, &m_Handle);
            }

        #pragma endregion

        #pragma region Methods

        public:
            // Unbinds any program pipeline from the current context.
            static void UnbindAll() { glBindProgramPipeline(0); }

            // Gets the handle of a program pipeline which is currently in bound.
            static GLuint getCurrentPipelineHandle()
            {
                GLint current_program_pipeline;
                glGetIntegerv(GL_PROGRAM_PIPELINE_BINDING, &current_program_pipeline);
                return current_program_pipeline;
            }

            // Binds the program pipeline to the current context.
            void Bind() const { glBindProgramPipeline(m_Handle); }

            // Binds stages of a program object to a program pipeline.
            void AttachProgram(
                ProgramPipeline::SHADER_STAGES stages,
                ProgramStage& program)
            {
                glUseProgramStages(
                    m_Handle,
                    static_cast<GLbitfield>(stages),
                    program.getHandle());

                if ((stages | SHADER_STAGES::VERTEX) != 0)
                    m_ProgramStages[SHADER_STAGES::VERTEX] = &program;

                if ((stages | SHADER_STAGES::FRAGMENT) != 0)
                    m_ProgramStages[SHADER_STAGES::FRAGMENT] = &program;

                if ((stages | SHADER_STAGES::GEOMETRY) != 0)
                    m_ProgramStages[SHADER_STAGES::GEOMETRY] = &program;

                if ((stages | SHADER_STAGES::TESS_CONTROL) != 0)
                    m_ProgramStages[SHADER_STAGES::TESS_CONTROL] = &program;

                if ((stages | SHADER_STAGES::TESS_EVALUATION) != 0)
                    m_ProgramStages[SHADER_STAGES::TESS_EVALUATION] = &program;

                if ((stages | SHADER_STAGES::COMPUTE) != 0)
                    m_ProgramStages[SHADER_STAGES::COMPUTE] = &program;
            }

            // Unbinds any program objects from particular stages of the pipeline.
            void DetachProgram(
                ProgramPipeline::SHADER_STAGES stages)
            {
                glUseProgramStages(
                    m_Handle,
                    static_cast<GLbitfield>(stages),
                    0);

                if ((stages | SHADER_STAGES::VERTEX) != 0)
                    m_ProgramStages[SHADER_STAGES::VERTEX] = nullptr;

                if ((stages | SHADER_STAGES::FRAGMENT) != 0)
                    m_ProgramStages[SHADER_STAGES::FRAGMENT] = nullptr;

                if ((stages | SHADER_STAGES::GEOMETRY) != 0)
                    m_ProgramStages[SHADER_STAGES::GEOMETRY] = nullptr;

                if ((stages | SHADER_STAGES::TESS_CONTROL) != 0)
                    m_ProgramStages[SHADER_STAGES::TESS_CONTROL] = nullptr;

                if ((stages | SHADER_STAGES::TESS_EVALUATION) != 0)
                    m_ProgramStages[SHADER_STAGES::TESS_EVALUATION] = nullptr;

                if ((stages | SHADER_STAGES::COMPUTE) != 0)
                    m_ProgramStages[SHADER_STAGES::COMPUTE] = nullptr;
            }

        #pragma endregion
    };

    ProgramPipeline::SHADER_STAGES operator|(
        ProgramPipeline::SHADER_STAGES lhs,
        ProgramPipeline::SHADER_STAGES rhs)
    {
        return static_cast<ProgramPipeline::SHADER_STAGES>(
            static_cast<GLbitfield>(lhs) | static_cast<GLbitfield>(rhs));
    }

    bool operator==(ProgramPipeline::SHADER_STAGES lhs, GLbitfield rhs)
    {
        return static_cast<GLbitfield>(lhs) == rhs;
    }

    bool operator!=(ProgramPipeline::SHADER_STAGES lhs, GLbitfield rhs)
    {
        return !(lhs == rhs);
    }
}