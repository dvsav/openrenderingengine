/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "OpenGLProgram.h"

namespace engine
{
    class ShaderSubroutine
    {
        #pragma region Fields

        private:
            opengl::Program& m_ShaderProgram;
            const opengl::Shader::SHADER_TYPE m_ShaderStage;
            const std::string m_UniformName;
            const GLint m_UniformLocation;
            std::vector<GLuint> m_FunctionIndices;
            GLuint m_ActiveFunctionIndex;

        #pragma endregion

        #pragma region Constructors

        public:
            ShaderSubroutine(
                opengl::Program& shaderProgram,
                opengl::Shader::SHADER_TYPE shaderStage,
                GLint subroutineUniformLocation) :

                m_ShaderProgram(shaderProgram),
                m_ShaderStage(shaderStage),
                m_UniformName(shaderProgram.getSubroutineUnifromName(shaderStage, subroutineUniformLocation)),
                m_UniformLocation(subroutineUniformLocation),
                m_FunctionIndices(shaderProgram.getSubroutineFunctionIndices(m_ShaderStage, m_UniformLocation)),
                m_ActiveFunctionIndex(m_FunctionIndices[0])
            { }

            ShaderSubroutine(
                opengl::Program& shaderProgram,
                opengl::Shader::SHADER_TYPE shaderStage,
                const std::string& subroutineUniformName) :

                m_ShaderProgram(shaderProgram),
                m_ShaderStage(shaderStage),
                m_UniformName(subroutineUniformName),
                m_UniformLocation(shaderProgram.getSubroutineUniformLocation(shaderStage, subroutineUniformName)),
                m_FunctionIndices(shaderProgram.getSubroutineFunctionIndices(m_ShaderStage, m_UniformLocation)),
                m_ActiveFunctionIndex(m_FunctionIndices[0])
            { }

            // Move constructor.
            ShaderSubroutine(ShaderSubroutine&& value) noexcept :
                m_ShaderProgram(value.m_ShaderProgram),
                m_ShaderStage(value.m_ShaderStage),
                m_UniformName(value.m_UniformName),
                m_UniformLocation(value.m_UniformLocation),
                m_FunctionIndices(std::move(value.m_FunctionIndices)),
                m_ActiveFunctionIndex(value.m_ActiveFunctionIndex)
            { }

            ShaderSubroutine(
                opengl::Program& shaderProgram,
                ShaderSubroutine&& value) noexcept :

                m_ShaderProgram(shaderProgram),
                m_ShaderStage(value.m_ShaderStage),
                m_UniformName(value.m_UniformName),
                m_UniformLocation(value.m_UniformLocation),
                m_FunctionIndices(std::move(value.m_FunctionIndices)),
                m_ActiveFunctionIndex(value.m_ActiveFunctionIndex)
            { }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            ShaderSubroutine(const ShaderSubroutine&) = delete;
            ShaderSubroutine& operator=(const ShaderSubroutine&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            opengl::Shader::SHADER_TYPE ShaderStage() const { return m_ShaderStage; }

            const std::string& UniformName() const { return m_UniformName; }

            GLint UniformLocation() const { return  m_UniformLocation; }

            const std::vector<GLuint>& FunctionIndices() const { return m_FunctionIndices; }

            const GLuint ActiveFunctionIndex() const { return m_ActiveFunctionIndex; };

        #pragma endregion

        #pragma region Methods

        public:
            GLuint getFunctionIndex(const std::string& function_name) const
            {
                return m_ShaderProgram.getSubroutineFunctionIndex(
                    m_ShaderStage,
                    function_name);
            }

            GLuint SelectFunction(const std::string& function_name)
            {
                return SelectFunction(getFunctionIndex(function_name));
            }

            GLuint SelectFunction(GLuint function_index)
            {
                util::Requires::That(
                    std::find(m_FunctionIndices.cbegin(), m_FunctionIndices.cend(), function_index) != m_FunctionIndices.cend(),
                    "There's no such function for this subroutine. Check function name for any typos.");

                GLuint old_function_index = m_ActiveFunctionIndex;
                m_ActiveFunctionIndex = function_index;

                m_ShaderProgram.SelectSubroutine(
                    m_ShaderStage,
                    m_UniformLocation,
                    m_ActiveFunctionIndex);

                return old_function_index;
            }

        #pragma endregion
    };
}
