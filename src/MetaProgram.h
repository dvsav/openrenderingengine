/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <map>

#include "OpenGLProgram.h"
#include "VertexAttributeEnum.h"
#include "UniformBlockEnum.h"
#include "TextureEnum.h"
#include "ShaderSubroutine.h"

namespace engine
{
    /*
    Class reperesenting a shader program which has a strictly defined interface (attributes, uniforms, uniform blocks, textures, etc.).
    The class contains properties to retrieve the locations of every input parameter of the shader program.
    */
    class MetaProgram
    {
        #pragma region Fields

        private:
             opengl::Program m_Program;

        protected:
             std::map<VertexAttributeEnum, GLuint> m_VertexAttributeLocations;
             std::map<UniformBlockEnum, GLuint> m_UniformBlocksBindingPoints;
             std::map<TextureEnum, GLint> m_TextureUnits;
             std::vector<ShaderSubroutine> m_Subroutines;

        #pragma endregion

        #pragma region Constructors

        public:
            MetaProgram(
                opengl::Program&& program,
                const std::map<VertexAttributeEnum, const char*>& attributes,
                const std::map<UniformBlockEnum, const char*>& uniform_blocks,
                const std::map<TextureEnum, const char*>& textures) :

                m_Program(std::move(program)),
                m_VertexAttributeLocations(),
                m_UniformBlocksBindingPoints(),
                m_TextureUnits(),
                m_Subroutines()
            {
                for (auto& attrib : attributes)
                {
                    auto attrib_location = m_Program.getAttributeLocation(attrib.second);
                    if (attrib_location >= 0)
                        m_VertexAttributeLocations[attrib.first] = m_Program.getAttributeLocation(attrib.second);
                }

                GLuint uniform_block_binding_point = 0;
                for (auto& uniform : uniform_blocks)
                {
                    init_uniform_block(
                        /*uniform_block_type*/ uniform.first,
                        /*uniform_block_name*/ uniform.second,
                        /*uniform_block_binding_point*/ uniform_block_binding_point++);
                }

                GLint texture_unit = 0;
                for (auto& texture : textures)
                {
                    init_texture(
                        /*texture_type*/ texture.first,
                        /*texture_name*/ texture.second,
                        /*texture_unit*/ texture_unit++);
                }

                init_subroutines(opengl::Shader::SHADER_TYPE::VERTEX);
                init_subroutines(opengl::Shader::SHADER_TYPE::FRAGMENT);
                init_subroutines(opengl::Shader::SHADER_TYPE::GEOMETRY);
                init_subroutines(opengl::Shader::SHADER_TYPE::TESS_CONTROL);
                init_subroutines(opengl::Shader::SHADER_TYPE::TESS_EVALUATION);
                init_subroutines(opengl::Shader::SHADER_TYPE::COMPUTE);
            }

            // Move constructor.
            MetaProgram(MetaProgram&& value) noexcept :
                m_Program(std::move(value.m_Program)),
                m_VertexAttributeLocations(std::move(value.m_VertexAttributeLocations)),
                m_UniformBlocksBindingPoints(std::move(value.m_UniformBlocksBindingPoints)),
                m_TextureUnits(std::move(value.m_TextureUnits)),
                m_Subroutines()
            {
                for (auto& sub : value.m_Subroutines)
                {
                    m_Subroutines.push_back(
                        ShaderSubroutine(
                            /*shaderProgram*/ m_Program,
                            /*shaderStage*/ std::move(sub)));
                }
            }

        #pragma endregion

        #pragma region Deleted Functions

        public:
            MetaProgram(const MetaProgram&) = delete;
            MetaProgram& operator=(const MetaProgram&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            // Gets the underlying opengl shader program.
            const opengl::Program& Program() const { return m_Program; }

            // Gets the underlying opengl shader program.
            opengl::Program& Program() { return m_Program; }

            // Gets the mapping of vertex attribute types to their locations.
            const std::map<VertexAttributeEnum, GLuint>& VertexAttributeLocations() const
            {
                return m_VertexAttributeLocations;
            }

            // Gets the mapping of uniform block types to their binding points.
            const std::map<UniformBlockEnum, GLuint>& UniformBlocksBindingPoints() const
            {
                return m_UniformBlocksBindingPoints;
            }

            // Gets the mapping of texture types to their texture units.
            const std::map<TextureEnum, GLint>& TextureUnits() const
            {
                return m_TextureUnits;
            }

            const std::vector<ShaderSubroutine>& Subroutines() const { return m_Subroutines; }

            ShaderSubroutine& getSubroutine(const std::string& subroutine_name)
            {
                auto iter = std::find_if(
                    m_Subroutines.begin(),
                    m_Subroutines.end(),
                    [&subroutine_name](const ShaderSubroutine& s) { return s.UniformName() == subroutine_name; });

                util::Requires::That(iter != m_Subroutines.end(), FUNCTION_INFO);

                return *iter;
            }

            ShaderSubroutine& getSubroutine(int index) { return m_Subroutines[index]; }

        #pragma endregion

        #pragma region Methods

        private:
            void init_uniform_block(
                UniformBlockEnum uniform_block_type,
                const char* uniform_block_name,
                GLuint uniform_block_binding_point)
            {
                m_Program.setUniformBlockBinding(
                    /*uniformBlockIndex*/ m_Program.getUniformBlockIndex(uniform_block_name),
                    /*uniformBlockBindingPoint*/ uniform_block_binding_point);
                m_UniformBlocksBindingPoints[uniform_block_type] = uniform_block_binding_point;
            }

            void init_texture(
                TextureEnum texture_type,
                const char* texture_name,
                GLint texture_unit)
            {
                m_Program.setUniform(
                    m_Program.getUniformLocation(texture_name),
                    texture_unit);
                m_TextureUnits[texture_type] = texture_unit;
            }

            void init_subroutines(opengl::Shader::SHADER_TYPE shader_stage)
            {
                auto subroutines_number = m_Program.getProgramStageInfo(
                    shader_stage,
                    opengl::Program::PROGRAM_STAGE_INFO::ACTIVE_SUBROUTINE_UNIFORM_LOCATIONS);

                for (GLint uniform_location = 0; uniform_location < subroutines_number; ++uniform_location)
                {
                    m_Subroutines.push_back(
                        ShaderSubroutine(
                            /*shaderProgram*/ m_Program,
                            /*shaderStage*/ shader_stage,
                            /*subroutineUniformLocation*/ uniform_location));
                }
            }

        public:
            void DefineTexture(
                TextureEnum texture_type,
                const char* texture_name)
            {
                auto iter = m_TextureUnits.find(texture_type);
                if(iter != m_TextureUnits.end())
                    init_texture(texture_type, texture_name, iter->second);
                else
                    init_texture(texture_type, texture_name, m_TextureUnits.size());
            }

        #pragma endregion
    };
}