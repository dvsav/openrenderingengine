/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Buffer.h"

namespace opengl
{
    /*
    OpenGL shader storage block buffer.
    Shaders can both read from and write to it.
    Example of a storage block definition in a shader:
    layout (binding = 0, std430) buffer my_storage_block
    {
        vec4 foo;
        vec3 bar;
        int baz[24];
    };
    */
    class StorageBlockBuffer : public Buffer
    {
        #pragma region Constructors

        public:
            StorageBlockBuffer(
                GLsizeiptr size,
                DRAW_USAGE usage,
                const void* data = nullptr) :

                Buffer(
                    size,
                    TARGET::SHADER_STORAGE,
                    usage,
                    data)
            {}

            // Move constructor.
            StorageBlockBuffer(StorageBlockBuffer&& value) noexcept :
                Buffer(std::move(value))
            {}

        #pragma endregion

        #pragma region Methods

        public:
            void BindToStorageBindingPoint(GLuint index)
            {
                glBindBufferBase(GL_SHADER_STORAGE_BUFFER, index, getHandle());
            }

        #pragma endregion
    };
}