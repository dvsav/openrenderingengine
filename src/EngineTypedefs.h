/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "GLEWHeaders.h"
#include "Matrix.h"
#include "Color.h"

namespace engine
{
    using vec2 = vmath::Vector<GLfloat, 2>;
    using vec3 = vmath::Vector<GLfloat, 3>;
    using vec4 = vmath::Vector<GLfloat, 4>;

    using mat2 = vmath::Matrix<GLfloat, 2, 2>;
    using mat3 = vmath::Matrix<GLfloat, 3, 3>;
    using mat4 = vmath::Matrix<GLfloat, 4, 4>;

    using color_rgb32f = Color_RGB<GLclampf>;
    using color_rgba32f = Color_RGBA<GLclampf>;

    using color_rgb = Color_RGB<GLubyte>;
    using color_rgba = Color_RGBA<GLubyte>;
}
