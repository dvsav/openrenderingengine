/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Model3d_Base.h"
#include "Model3d_Skybox.h"
#include "Texture.h"
#include "Lighting.h"

namespace engine
{
    class Scene
    {
        #pragma region Fields

        private:
            static const GLsizei SHADOW_WIDTH = 1024;

            std::shared_ptr<MetaProgram> m_ShadowRender;
            std::shared_ptr<UniformBlock_CameraInfo> m_UniformBlock_Camera;

            std::shared_ptr<AmbientLight> m_AmbientLight;
            std::shared_ptr<DirectionalLightArray> m_DirectionalLights;
            std::shared_ptr<PointLightArray> m_PointLights;
            std::shared_ptr<SpotLightArray> m_SpotLights;

            std::list<Model3d_Base> m_ShadowCastingModels;
            std::list<Model3d_Base> m_NonShadowCastingModels;

        #pragma endregion

        #pragma region Constructors

        public:
            Scene(
                size_t nDirLights,
                size_t nPointLights,
                size_t nSpotLights);

            // move constructor
            Scene(Scene&& value) noexcept :
                m_ShadowRender(std::move(value.m_ShadowRender)),
                m_UniformBlock_Camera(std::move(value.m_UniformBlock_Camera)),
                m_AmbientLight(std::move(value.m_AmbientLight)),
                m_DirectionalLights(std::move(value.m_DirectionalLights)),
                m_PointLights(std::move(value.m_PointLights)),
                m_SpotLights(std::move(value.m_SpotLights)),
                m_ShadowCastingModels(std::move(value.m_ShadowCastingModels)),
                m_NonShadowCastingModels(std::move(value.m_NonShadowCastingModels))
            { }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            Scene(const Scene&) = delete;
            Scene& operator=(const Scene&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            std::shared_ptr<MetaProgram>& ShadowRender() { return m_ShadowRender; }
            std::shared_ptr<UniformBlock_CameraInfo>& Camera() { return m_UniformBlock_Camera; }
            std::shared_ptr<AmbientLight>& getAmbientLight() { return m_AmbientLight; }
            std::shared_ptr<DirectionalLightArray>& DirectionalLights() { return m_DirectionalLights; }
            std::shared_ptr<PointLightArray>& PointLights() { return m_PointLights; }
            std::shared_ptr<SpotLightArray>& SpotLights() { return m_SpotLights; }

            const Model3d_Base& findModel(const std::string& name) const
            {
                auto predicate = [&name](const Model3d_Base& model) { return model.Name() == name; };
                {
                    auto result = std::find_if(
                        m_ShadowCastingModels.cbegin(),
                        m_ShadowCastingModels.cend(),
                        predicate);

                    if (result != m_ShadowCastingModels.cend())
                        return *result;
                }
                {
                    auto result = std::find_if(
                        m_NonShadowCastingModels.cbegin(),
                        m_NonShadowCastingModels.cend(),
                        predicate);

                    return *result;
                }
            }
            Model3d_Base& findModel(const std::string& name)
            {
                auto predicate = [&name](const Model3d_Base& model) { return model.Name() == name; };
                {
                    auto result = std::find_if(
                        m_ShadowCastingModels.begin(),
                        m_ShadowCastingModels.end(),
                        predicate);

                    if (result != m_ShadowCastingModels.cend())
                        return *result;
                }
                {
                    auto result = std::find_if(
                        m_NonShadowCastingModels.begin(),
                        m_NonShadowCastingModels.end(),
                        predicate);

                    return *result;
                }
            }

        #pragma endregion

        #pragma region Methods

        public:
            Model3d_Base& setup_Model(
                Model3d_Base&& md,
                bool isShadowCasting = false)
            {
                auto& models = isShadowCasting ? m_ShadowCastingModels : m_NonShadowCastingModels;
                models.push_back(std::move(md));
                Model3d_Base& model = models.back();
                
                model.setProgram(engine::Model3d_Base::RenderPassEnum::RecordDepth, m_ShadowRender);

                model.setUniformBlock<UniformBlockEnum::CameraInfo>(m_UniformBlock_Camera);
                model.setUniformBlock<UniformBlockEnum::AmbientLight>(m_AmbientLight->UniformBlock());

                if (m_DirectionalLights)
                {
                    model.setUniformBlock<UniformBlockEnum::DirectionalLightArray>(m_DirectionalLights->UniformBlock());
                    model.setTexture(TextureEnum::ShadowMapDirLightArray, m_DirectionalLights->getDepthTexture());
                    model.setTextureSampler(TextureEnum::ShadowMapDirLightArray, m_DirectionalLights->ShadowTextureSampler());
                }

                if (m_PointLights)
                {
                    model.setUniformBlock<UniformBlockEnum::PointLightArray>(m_PointLights->UniformBlock());
                    model.setTexture(TextureEnum::ShadowMapPointLightArray, m_PointLights->getDepthTexture());
                    model.setTextureSampler(TextureEnum::ShadowMapPointLightArray, m_PointLights->ShadowTextureSampler());
                }

                if (m_SpotLights)
                {
                    model.setUniformBlock<UniformBlockEnum::SpotLightArray>(m_SpotLights->UniformBlock());
                    model.setTexture(TextureEnum::ShadowMapSpotLightArray, m_SpotLights->getDepthTexture());
                    model.setTextureSampler(TextureEnum::ShadowMapSpotLightArray, m_SpotLights->ShadowTextureSampler());
                }

                return model;
            }

            void Render()
            {
                std::function<void()> renderFunc(
                    [this]() -> void
                    {
                        for (auto& model : m_ShadowCastingModels)
                            model.Render(engine::Model3d_Base::RenderPassEnum::RecordDepth);
                    });

                if (m_DirectionalLights)
                    m_DirectionalLights->RenderShadows(renderFunc);

                if (m_PointLights)
                    m_PointLights->RenderShadows(renderFunc);

                if (m_SpotLights)
                    m_SpotLights->RenderShadows(renderFunc);

                for (auto& model : m_ShadowCastingModels)
                    model.Render(engine::Model3d_Base::RenderPassEnum::RenderScene);
                for (auto& model : m_NonShadowCastingModels)
                    model.Render(engine::Model3d_Base::RenderPassEnum::RenderScene);
            }

        #pragma endregion
    };

    Model3d_Skybox make_Skybox(
        const std::shared_ptr<opengl::Texture>& texture);
}
