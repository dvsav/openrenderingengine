/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#include "ConditionalCompilation.h"

#include "OpenGLUtility.h"

namespace opengl
{
    DEBUG_MESSAGE_CALLBACK debugMessageCallback = nullptr;

    void APIENTRY debugCallback(
        GLenum source,
        GLenum type,
        GLuint id,
        GLenum severity,
        GLsizei length,
        const GLchar* message,
        const void* userParam)
    {
        if (debugMessageCallback != nullptr)
            debugMessageCallback(DebugMessageStruct{ source, type, id, severity, length, message, userParam });
    }

    /*
    Specifies a callback to receive debugging messages from the GL.
    (since OpenGL Version 4.3)
    */
    void setDebugMessageCallback(DEBUG_MESSAGE_CALLBACK callback)
    {
        debugMessageCallback = callback;

        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(&debugCallback, NULL);
    }
}