/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "GLEWHeaders.h"

namespace opengl
{
    class VertexArrayObject final
    {
        #pragma region Nested Classes

        public:
            /*
            Provides more safe binding of a VertexArrayObject in RAII fashion. When created the Binding object binds
            the VertexArrayObject specified in its constructor. When the Binding object is going to be deleted it
            unbinds that VertexArrayObject.
            You are recommended to create instances of this class only as local variables.
            Example:
            {
                VertexArrayObject::Binding binding(vao); // here we bind vao
                // do some rendering
            } // here vao is unbound automatically
            */
            class Binding final
            {
                private:
                    GLuint m_PreviousVAO;

                private:
                    Binding(const Binding&) = delete;
                    Binding& operator=(const Binding&) = delete;

                public:
                    Binding(VertexArrayObject& vao)
                        : m_PreviousVAO(VertexArrayObject::getCurrentlyBoundVAOHandle())
                    {
                        vao.Bind();
                    }

                    ~Binding()
                    {
                        glBindVertexArray(m_PreviousVAO);
                    }
            };

        #pragma endregion

        #pragma region Fields

        private:
            GLuint m_Handle; // handle to the VAO object (VAO name)

        #pragma endregion

        #pragma region Constructors

        public:
            VertexArrayObject() :
                m_Handle(0)
            {
#ifdef OPENGL_4_5
                glCreateVertexArrays(
                    1,            // requested number of names
                    &m_Handle);   // pointer to an array of names
#else
                // get a free handle to VAO (VAO name)
                glGenVertexArrays(
                    1,          // requested number of names
                    &m_Handle); // pointer to an array of names

                glBindVertexArray(m_Handle); // create VAO
                glBindVertexArray(0); // unbind VAO
#endif
            }

            // Move constructor.
            VertexArrayObject(VertexArrayObject&& value) noexcept :
                m_Handle(value.m_Handle)
            {
                value.m_Handle = 0;
            }

            VertexArrayObject& operator=(VertexArrayObject&& value) noexcept
            {
                m_Handle = value.m_Handle;
                value.m_Handle = 0;
                return *this;
            }

        #pragma endregion

        #pragma region Destructor

        public:
            ~VertexArrayObject()
            {
                glDeleteVertexArrays(
                    1,          // number of names
                    &m_Handle); // pointer to an array of names
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            VertexArrayObject(const VertexArrayObject&) = delete;
            VertexArrayObject& operator=(const VertexArrayObject&) = delete;

        #pragma endregion

        #pragma region Properties

        public:
            // Gets the handle of VAO.
            GLuint getHandle()
            {
                return m_Handle;
            }

            static GLuint getCurrentlyBoundVAOHandle()
            {
                GLint current_vao;
                glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &current_vao);
                return current_vao;
            }

        #pragma endregion

        #pragma region Methods

        public:
            // Binds VAO to OpenGL context.
            void Bind()
            {
                glBindVertexArray(m_Handle);
            }

        #pragma endregion
    };
}