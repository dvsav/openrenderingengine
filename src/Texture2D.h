/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <algorithm>

#include "GLEWHeaders.h"
#include "OpenGLTypes.h"

#include "Requires.h"
#include "Texture.h"

namespace opengl
{
    class Texture2D final : public Texture
    {
        #pragma region Constructors

        public:
            Texture2D(
                GLint mipmap_levels_number,
                INTERNAL_FORMAT internalFormat,
                GLsizei width,
                GLsizei height,
                PIXEL_FORMAT format,
                TYPE type) :

                Texture(
                    TARGET::TEXTURE_2D,
                    internalFormat,
                    format,
                    type,
                    mipmap_levels_number,
                    width,
                    height)
            {
                Binding binding(*this);

#ifdef OPENGL_4
                glTexStorage2D(
                    static_cast<GLenum>(getTarget()),
                    mipmap_levels_number,
                    static_cast<GLint>(internalFormat),
                    width,
                    height);
#else
                for (int mipmap_level = 0; mipmap_level < mipmap_levels_number; mipmap_level++)
                {
                    glTexImage2D(
                        /*target*/ static_cast<GLenum>(getTarget()),
                        /*level*/ mipmap_level,
                        /*internalFormat*/ static_cast<GLint>(internalFormat),
                        /*width*/ std::max(1, width / (1 << mipmap_level)),
                        /*height*/ std::max(1, height / (1 << mipmap_level)),
                        /*border*/ 0,
                        /*format*/ static_cast<GLenum>(format),
                        /*type*/ static_cast<GLenum>(type),
                        /*data*/ nullptr);
                }
#endif
            }

        #pragma endregion

        #pragma region Methods

        public:
            // Sets texture image.
            void setImage(
                GLint mipmap_level,
                const GLvoid* data)
            {
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);

                Binding binding(*this);

                glTexSubImage2D(
                    /*target*/  static_cast<GLenum>(getTarget()),
                    /*level*/   mipmap_level,
                    /*xoffset*/ 0,
                    /*yoffset*/ 0,
                    /*width*/   std::max(1, (getWidth() / (1 << mipmap_level))),
                    /*height*/  std::max(1, (getHeight() / (1 << mipmap_level))),
                    /*format*/  static_cast<GLenum>(getPixelFormat()),
                    /*type*/    static_cast<GLenum>(getType()),
                    /*pixels*/  data);
            }

            // Sets texture subimage.
            void setSubImage(
                GLint mipmap_level,
                GLint xoffset,
                GLint yoffset,
                GLsizei width,
                GLsizei height,
                const GLvoid* data)
            {
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);
                util::Requires::ArgumentPositive(xoffset, "xoffset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(yoffset, "yoffset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(width, "width", FUNCTION_INFO);
                util::Requires::ArgumentPositive(height, "height", FUNCTION_INFO);
                util::Requires::That(width + xoffset <= std::max(1, (getWidth() / (1 << mipmap_level))), FUNCTION_INFO);
                util::Requires::That(height + yoffset <= std::max(1, (getHeight() / (1 << mipmap_level))), FUNCTION_INFO);

                Binding binding(*this);

                glTexSubImage2D(
                    static_cast<GLenum>(getTarget()),
                    mipmap_level,
                    xoffset,
                    yoffset,
                    width,
                    height,
                    static_cast<GLenum>(getPixelFormat()),
                    static_cast<GLenum>(getType()),
                    data);
            }

            // Sets precompressed texture image.
            void setCompressedImage(
                GLint mipmap_level,
                GLsizei imageSize,
                const GLvoid* data)
            {
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);

                glCompressedTexSubImage2D(
                    /*target*/  static_cast<GLenum>(getTarget()),
                    /*level*/   mipmap_level,
                    /*xoffset*/ 0,
                    /*yoffset*/ 0,
                    /*width*/   std::max(1, (getWidth() / (1 << mipmap_level))),
                    /*height*/  std::max(1, (getHeight() / (1 << mipmap_level))),
                    /*format*/  static_cast<GLenum>(getPixelFormat()),
                    /*imageSize*/ imageSize,
                    /*data*/ data);
            }

            /*
            Copies pixels from the framebuffer into the texture.
            Parameters:
            color_buffer - the frame buffer from which pixels are read.
            mipmap_level - level-of-detail number.
            x, y - window coordinates of the lower left corner of the rectangular region of pixels to be copied.
            */
            void CopyImageFromFramebuffer(
                COLOR_BUFFER color_buffer,
                GLint mipmap_level,
                GLint x,
                GLint y)
            {
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);
                util::Requires::ArgumentPositive(x, "x", FUNCTION_INFO);
                util::Requires::ArgumentPositive(y, "y", FUNCTION_INFO);

                glReadBuffer(static_cast<GLenum>(color_buffer));

                Binding binding(*this);

                glCopyTexImage2D(
                    static_cast<GLenum>(getTarget()),
                    mipmap_level,
                    static_cast<GLenum>(getInternalFormat()),
                    x,
                    y,
                    getWidth(),
                    getHeight(),
                    0);
            }

            /*
            Copies pixels from the framebuffer into a part of the texture.
            Parameters:
            color_buffer - the frame buffer from which pixels are read.
            mipmap_level - level-of-detail number.
            x, y - window coordinates of the lower left corner of the rectangular region of pixels to be copied.
            xoffset, yoffset - texel offset in the x and y direction within the texture array.
            width, height - width and height of the texture subimage.
            */
            void CopySubImageFromFramebuffer(
                COLOR_BUFFER color_buffer,
                GLint mipmap_level,
                GLint xoffset,
                GLint yoffset,
                GLint x,
                GLint y,
                GLsizei width,
                GLsizei height)
            {
                util::Requires::ArgumentNotNegative(mipmap_level, "mipmap_level", FUNCTION_INFO);
                util::Requires::That(mipmap_level < getMipmapLevelsNumber(), FUNCTION_INFO);
                util::Requires::ArgumentPositive(xoffset, "xoffset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(yoffset, "yoffset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(width, "width", FUNCTION_INFO);
                util::Requires::ArgumentPositive(height, "height", FUNCTION_INFO);
                util::Requires::That(width + xoffset <= std::max(1, (getWidth() / (1 << mipmap_level))), FUNCTION_INFO);
                util::Requires::That(height + yoffset <= std::max(1, (getHeight() / (1 << mipmap_level))), FUNCTION_INFO);

                glReadBuffer(static_cast<GLenum>(color_buffer));

                Binding binding(*this);

                glCopyTexSubImage2D(
                    static_cast<GLenum>(getTarget()),
                    mipmap_level,
                    xoffset,
                    yoffset,
                    x,
                    y,
                    width,
                    height);
            }

        #pragma endregion
    };

    /*
    Makes Texture2D object.
    Texture data are taken from general purpose memory in certain [format] and
    stored in GPU memory in RGBA32F format.
    */
    inline Texture2D make_Texture2D_RGBA32F(
        GLsizei width,
        GLsizei height,
        PIXEL_FORMAT format,
        const GLubyte* level_zero_data)
    {
        Texture2D texture(
            static_cast<GLint>(1 + floor(log2((float)std::max(width, height)))),
            INTERNAL_FORMAT::RGBA32F,
            width,
            height,
            format,
            TYPE::UNSIGNED_BYTE);

        texture.setImage(0, level_zero_data);
        texture.GenerateMipmap();
        return texture;
    }
}
