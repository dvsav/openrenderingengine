/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Texture.h"

namespace engine
{
    enum class TextureEnum
    {
        Diffuse,
        NormalMap,
        HeightMap,
        ShadowMapDirLight,
        ShadowMapSpotLight,
        ShadowMapPointLight,
        ShadowMapDirLightArray,
        ShadowMapSpotLightArray,
        ShadowMapPointLightArray
    };

    inline TextureEnum parse_TextureEnum(const char* str)
    {
        if (std::strcmp(str, "Diffuse") == 0)
            return TextureEnum::Diffuse;
        else if (std::strcmp(str, "NormalMap") == 0)
            return TextureEnum::NormalMap;
        else if (std::strcmp(str, "HeightMap") == 0)
            return TextureEnum::HeightMap;
        else if (std::strcmp(str, "ShadowMapDirLight") == 0)
            return TextureEnum::ShadowMapDirLight;
        else if (std::strcmp(str, "ShadowMapSpotLight") == 0)
            return TextureEnum::ShadowMapSpotLight;
        else if (std::strcmp(str, "ShadowMapPointLight") == 0)
            return TextureEnum::ShadowMapPointLight;
        else if (std::strcmp(str, "ShadowMapDirLightArray") == 0)
            return TextureEnum::ShadowMapDirLightArray;
        else if (std::strcmp(str, "ShadowMapSpotLightArray") == 0)
            return TextureEnum::ShadowMapSpotLightArray;
        else if (std::strcmp(str, "ShadowMapPointLightArray") == 0)
            return TextureEnum::ShadowMapPointLightArray;
        else
            throw std::invalid_argument(str);
    }
}