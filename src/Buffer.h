/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include <vector>

#include "GLEWHeaders.h"
#include "GLenums.h"
#include "OpenGLTypes.h"

#include "Requires.h"

namespace opengl
{
    // OpenGL buffer object.
    class Buffer
    {
        #pragma region Nested Classes

        public:
            enum class TARGET : GLenum
            {
                // Vertex attributes.
                ARRAY = GL_ARRAY_BUFFER,
                // Atomic counter storage.
                ATOMIC_COUNTER = GL_ATOMIC_COUNTER_BUFFER,
                // Indirect compute dispatch commands.
                DISPATCH_INDIRECT = GL_DISPATCH_INDIRECT_BUFFER,
                // Indirect command arguments
                DRAW_INDIRECT = GL_DRAW_INDIRECT_BUFFER,
                // Query result buffer.
                QUERY = GL_QUERY_BUFFER,
                // Read-write storage for shaders.
                SHADER_STORAGE = GL_SHADER_STORAGE_BUFFER,
                // Vertex array indices.
                ELEMENT_ARRAY = GL_ELEMENT_ARRAY_BUFFER,
                // Pixel read target.
                PIXEL_PACK = GL_PIXEL_PACK_BUFFER,
                // Texture data source.
                PIXEL_UNPACK = GL_PIXEL_UNPACK_BUFFER,
                // Buffer copy source.
                COPY_READ = GL_COPY_READ_BUFFER,
                // Buffer copy destination.
                COPY_WRITE = GL_COPY_WRITE_BUFFER,
                // Transform feedback buffer.
                TRANSFORM_FEEDBACK = GL_TRANSFORM_FEEDBACK_BUFFER,
                // Uniform block storage.
                UNIFORM = GL_UNIFORM_BUFFER,
                // Texture data buffer.
                TEXTURE = GL_TEXTURE_BUFFER
            };

            /*
            This information is used by OpenGL to decide how to best allocate the buffer object�s data store
            STATIC_... - The data store contents will be modified once and used many times
            DYNAMIC_... - The data store contents will be modified repeatedly and used many times
            STREAM_... - The data store contents will be modified once and used at most a few times
            ..._DRAW - The data store contents are modified by the application and used as the source for OpenGL drawing and image specification commands
            ..._READ - The data store contents are modified by reading data from OpenGL and used to return that data when queried by the application
            ..._COPY - The data store contents are modified by reading data from OpenGL and used as the source for OpenGL drawing and image specification commands
            */
            enum class DRAW_USAGE : GLenum
            {
                STREAM_DRAW = GL_STREAM_DRAW,
                STREAM_READ = GL_STREAM_READ,
                STREAM_COPY = GL_STREAM_COPY,
                STATIC_DRAW = GL_STATIC_DRAW,
                STATIC_READ = GL_STATIC_READ,
                STATIC_COPY = GL_STATIC_COPY,
                DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
                DYNAMIC_READ = GL_DYNAMIC_READ,
                DYNAMIC_COPY = GL_DYNAMIC_COPY
            };

            enum class ACCESS : GLenum
            {
                READ_ONLY = GL_READ_ONLY,
                WRITE_ONLY = GL_WRITE_ONLY,
                READ_WRITE = GL_READ_WRITE
            };

            enum class ACCESS_FLAGS : GLbitfield
            {
                READ = GL_MAP_READ_BIT,
                WRITE = GL_MAP_WRITE_BIT,
                PERSISTENT = GL_MAP_PERSISTENT_BIT,
                COHERENT = GL_MAP_COHERENT_BIT,
                INVALIDATE_RANGE = GL_MAP_INVALIDATE_RANGE_BIT,
                INVALIDATE_BUFFER = GL_MAP_INVALIDATE_BUFFER_BIT,
                FLUSH_EXPLICIT = GL_MAP_FLUSH_EXPLICIT_BIT,
                UNSYNCHRONIZED = GL_MAP_UNSYNCHRONIZED_BIT
            };

            enum class FORMAT : GLenum
            {
                DEPTH_COMPONENT = GL_DEPTH_COMPONENT,
                STENCIL_INDEX = GL_STENCIL_INDEX,
                DEPTH_STENCIL = GL_DEPTH_STENCIL,
                RED = GL_RED,
                GREEN = GL_GREEN,
                BLUE = GL_BLUE,
                RG = GL_RG,
                RGB = GL_RGB,
                BGR = GL_BGR,
                RGBA = GL_RGBA,
                BGRA = GL_BGRA,
                RED_INTEGER = GL_RED_INTEGER,
                GREEN_INTEGER = GL_GREEN_INTEGER,
                BLUE_INTEGER = GL_BLUE_INTEGER,
                RG_INTEGER = GL_RG_INTEGER,
                RGB_INTEGER = GL_RGB_INTEGER,
                BGR_INTEGER = GL_BGR_INTEGER,
                RGBA_INTEGER = GL_RGBA_INTEGER,
                BGRA_INTEGER = GL_BGRA_INTEGER
            };

            /*
            Provides more safe binding of a Buffer in RAII fashion.
            Example:
            {
                Buffer::Binding binding(buffer); // here we bind the buffer
                // do some work
            } // here the buffer is unbound automatically
            */
            class Binding final
            {
            private:
                TARGET m_Target;
                GLuint m_PreviousBuffer = 0;

            private:
                Binding(const Binding&) = delete;
                Binding& operator=(const Binding&) = delete;

            public:
                Binding(Buffer& buffer, TARGET target) :
                    m_Target(target),
                    m_PreviousBuffer(Buffer::getCurrentlyBoundBufferHandle(target))
                {
                    buffer.Bind(target);
                }

                ~Binding()
                {
                    glBindBuffer(static_cast<GLenum>(m_Target), m_PreviousBuffer);
                }

                TARGET getTarget() const { return m_Target; }
            };

            /*
            Provides more safe mapping of a Buffer in RAII fashion.
            Example:
            {
            Buffer::Mapping mapping(buffer, access); // here we map buffer
            void* ptr = mapping.getMappingPointer();
            // work with ptr
            } // here the buffer is unmapped automatically
            */
            class Mapping
            {
            private:
                Buffer* m_Buffer;
                void* m_Ptr;

            public:
                Mapping(
                    Buffer& buffer,
                    ACCESS access) :

                    m_Buffer(&buffer),
                    m_Ptr(buffer.Map(access))
                { }

                Mapping(
                    Buffer& buffer,
                    GLintptr offset,
                    GLsizeiptr size,
                    ACCESS_FLAGS accessFlags) :

                    m_Buffer(&buffer),
                    m_Ptr(buffer.Map(offset, size, accessFlags))
                { }

                Mapping(Mapping&& value) noexcept :
                    m_Buffer(value.m_Buffer),
                    m_Ptr(value.m_Ptr)
                {
                    value.m_Buffer = nullptr;
                    m_Ptr = nullptr;
                }

                ~Mapping()
                {
                    if (m_Buffer != nullptr)
                    {
                        m_Buffer->Unmap();
                        m_Ptr = nullptr;
                    }
                }

                Mapping(const Mapping&) = delete;
                Mapping& operator=(const Mapping&) = delete;

                void* getMappingPointer() { return m_Ptr; }
            };

        #pragma endregion

        #pragma region Fields

        private:
            GLuint m_Handle;
            GLsizeiptr m_Size;
            TARGET m_Target;

        #pragma endregion

        #pragma region Constructors

        public:
            Buffer(
                GLsizeiptr size,
                TARGET target,
                DRAW_USAGE usage,
                const void* data = nullptr) :

                m_Handle(0),
                m_Size(size),
                m_Target(target)
            {
                util::Requires::ArgumentPositive(size, "size", FUNCTION_INFO);

                glGenBuffers(1, &m_Handle);

                Binding binding(*this, target);

                glBufferData(
                    static_cast<GLenum>(target),
                    size,
                    data,
                    static_cast<GLenum>(usage));
            }

            // Move constructor.
            Buffer(Buffer&& value) noexcept :
                m_Handle(value.m_Handle),
                m_Size(value.m_Size),
                m_Target(value.m_Target)
            {
                value.m_Handle = 0;
            }

        #pragma endregion

        #pragma region Deleted Functions

        private:
            Buffer(const Buffer&) = delete;
            Buffer& operator=(const Buffer&) = delete;

        #pragma endregion

        #pragma region Destructor

        public:
            virtual ~Buffer()
            {
                glDeleteBuffers(1, &m_Handle);
            }

        #pragma endregion

        #pragma region Properties

        public:
            static GLuint getCurrentlyBoundBufferHandle(TARGET target);

            // Gets buffer size in bytes.
            GLsizeiptr getSize() const { return m_Size; }

            // Gets the handle of the buffer.
            GLuint getHandle() { return m_Handle; }

        #pragma endregion

        #pragma region Methods

        public:
            /*
            Binds the buffer object to the specified buffer binding point.
            Parameters:
            target - the target to which the buffer object will be bound.
            */
            void Bind(TARGET target)
            {
                glBindBuffer(static_cast<GLenum>(target), m_Handle);
            }

            // Unbinds all buffers from the [target] binding point.
            static void UnbindAll(TARGET target)
            {
                glBindBuffer(static_cast<GLenum>(target), 0);
            }

            /*
            Loads data into the buffer.
            Parameters:
            offset - offset into the buffer object's data store where data replacement will begin, measured in bytes.
            size - size in bytes of the data store region being replaced.
            data - a pointer to the new data that will be copied into the data store.
            */
            void setData(
                GLintptr offset,
                GLsizeiptr size,
                const void* data)
            {
                util::Requires::ArgumentNotNegative(offset, "offset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(size, "size", FUNCTION_INFO);
                util::Requires::That(offset + size <= m_Size, FUNCTION_INFO);

                Binding binding(*this, m_Target);
                glBufferSubData(static_cast<GLenum>(m_Target), offset, size, data);
            }

            /*
            Returns some or all of the data contents of the data store of the specified
            buffer object. Data starting at byte offset offset and extending for size
            bytes is copied from the buffer object's data store to the memory pointed to by buffer.
            Parameters:
            offset - the offset into the buffer object's data store from which data will be returned, measured in bytes.
            size - the size in bytes of the data store region being returned.
            buffer - a pointer to the location where buffer object data is returned.
            */
            void getData(
                GLintptr offset,
                GLsizeiptr size,
                void* buffer) const
            {
                util::Requires::ArgumentNotNegative(offset, "offset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(size, "size", FUNCTION_INFO);
                util::Requires::That(offset + size <= m_Size, FUNCTION_INFO);

                Binding binding(*const_cast<Buffer*>(this), m_Target);
                glGetBufferSubData(static_cast<GLenum>(m_Target), offset, size, buffer);
            }

            /*
            Maps the entire data store of a specified buffer object into the client's address space.
            The data can then be directly read and/or written relative to the returned pointer,
            depending on the specified access policy.
            Parameters:
            access - access policy.
            */
            void* Map(ACCESS access)
            {
                Binding binding(*this, m_Target);
                return glMapBuffer(static_cast<GLenum>(m_Target), static_cast<GLenum>(access));
            }

            /*
            Maps all or part of the data store of a specified buffer object into the client's address space.
            Parameters:
            offset - the starting offset within the buffer of the range to be mapped.
            size - the length of the range to be mapped.
            access - a combination of access flags indicating the desired access to the mapped range.
            Returns:
            The pointer to the data. After writing them call FlushMapped().
            */
            void* Map(
                GLintptr offset,
                GLsizeiptr size,
                ACCESS_FLAGS accessFlags)
            {
                util::Requires::ArgumentNotNegative(offset, "offset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(size, "size", FUNCTION_INFO);
                util::Requires::That(offset + size <= m_Size, FUNCTION_INFO);

                Binding binding(*this, m_Target);
                return glMapBufferRange(
                    static_cast<GLenum>(m_Target),
                    offset,
                    size,
                    static_cast<GLbitfield>(accessFlags));
            }

            /*
            Unmaps (releases) any mapping of the buffer object into the client's address space.
            */
            void Unmap()
            {
                Binding binding(*this, m_Target);
                glUnmapBuffer(static_cast<GLenum>(m_Target));
            }

            /*
            Indicates that modifications have been made to a range of a mapped buffer object.
            The buffer object must previously have been mapped with the GL_MAP_FLUSH_EXPLICIT_BIT flag.
            Parameters:
            offset - the start of the buffer subrange, in basic machine units.
            size - the length of the buffer subrange, in basic machine units.
            */
            void FlushMapped(
                GLintptr offset,
                GLsizeiptr size)
            {
                util::Requires::ArgumentNotNegative(offset, "offset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(size, "size", FUNCTION_INFO);
                util::Requires::That(offset + size <= m_Size, FUNCTION_INFO);

                Binding binding(*this, m_Target);
                glFlushMappedBufferRange(static_cast<GLenum>(m_Target), offset, size);
            }

            /*
            Copies part of the data store attached to a source buffer object to the data store attached to a destination buffer object.
            Parameters:
            writeBuffer - destination buffer object.
            readOffset - the offset, in basic machine units, within the data store of the source buffer object at which data will be read.
            writeOffset - the offset, in basic machine units, within the data store of the destination buffer object at which data will be written.
            size - the size, in basic machine units, of the data to be copied from the source buffer object to the destination buffer object.
            */
            void CopyTo(
                Buffer& writeBuffer,
                GLintptr readOffset,
                GLintptr writeOffset,
                GLsizei size)
            {
                util::Requires::ArgumentNotNegative(readOffset, "readOffset", FUNCTION_INFO);
                util::Requires::ArgumentNotNegative(writeOffset, "writeOffset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(size, "size", FUNCTION_INFO);
                util::Requires::That(readOffset + size <= m_Size, FUNCTION_INFO);
                util::Requires::That(writeOffset + size <= writeBuffer.m_Size, FUNCTION_INFO);

                Binding bindingRead(*this, TARGET::COPY_READ);
                Binding bindingWrite(writeBuffer, TARGET::COPY_WRITE);
                glCopyBufferSubData(
                    static_cast<GLenum>(TARGET::COPY_READ),
                    static_cast<GLenum>(TARGET::COPY_WRITE),
                    readOffset,
                    writeOffset,
                    size);
            }

#ifdef OPENGL_4
            /*
            Fills all or part of buffer object's data store with a fixed value
            (since OpenGL Version 4.3)
            Parameters:
            internalformat - the internal format with which the data will be stored in the buffer object.
            offset - the offset in basic machine units into the buffer object's data store at which to start filling.
            size - the size in basic machine units of the range of the data store to fill.
            format - the format of the data in memory addressed by data.
            type - the type of the data in memory addressed by data.
            data - the address of a memory location storing the data to be replicated into the buffer's data store.
            */
            void Clear(
                INTERNAL_FORMAT internalformat,
                GLintptr offset,
                GLsizeiptr size,
                FORMAT format,
                TYPE type,
                const void* data)
            {
                util::Requires::ArgumentNotNegative(offset, "offset", FUNCTION_INFO);
                util::Requires::ArgumentPositive(size, "size", FUNCTION_INFO);
                util::Requires::That(offset + size <= m_Size, FUNCTION_INFO);

                Binding binding(*this, m_Target);

                glClearBufferSubData(
                    static_cast<GLenum>(m_Target),
                    static_cast<GLenum>(internalformat),
                    offset,
                    size,
                    static_cast<GLenum>(format),
                    static_cast<GLenum>(type),
                    data);
            }
#endif

            /*
            Fills the buffer with the specified value.
            Parameters:
            value - a value each byte of the buffer is filled with.
            */
            void Clear(unsigned char value)
            {
                auto map = Mapping(*this, ACCESS::WRITE_ONLY);
                std::memset(
                    map.getMappingPointer(),
                    value,
                    getSize());
            }

        #pragma endregion
    };

    /*
    Loads data into the buffer.
    Parameters:
    buffer - the buffer to which the data are loaded.
    offset - offset into the buffer object's data store where data replacement will begin, measured in bytes.
    data - container of data.
    */
    template<typename _Container>
    inline void setData(
        Buffer& buffer,
        GLintptr offset,
        const _Container& data)
    {
        buffer.setData(
            offset,
            data.size() * sizeof(typename _Container::value_type),
            data.data());
    }

    template<typename _Container>
    inline Buffer make_Buffer(
        const _Container& data,
        Buffer::TARGET target,
        Buffer::DRAW_USAGE usage)
    {
        return Buffer(
            data.size() * sizeof(typename _Container::value_type),
            target,
            usage,
            data.data()
        );
    }
}