/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#pragma once

#include "ConditionalCompilation.h"

#include "Vertex.h"

namespace engine
{
    /*
    A triangle comprised of three vertices
    Template parameters:
    TVertex - vertex type
    */
    template<typename TVertex>
    class Triangle : public vmath::VectorBase<TVertex, 3>
    {
    public:
        using VectorBase::VectorBase;

    public:
        value_type& V1() { return at(0); }
        const value_type& V1() const { return at(0); }

        value_type& V2() { return at(1); }
        const value_type& V2() const { return at(1); }

        value_type& V3() { return at(2); }
        const value_type& V3() const { return at(2); }
    };

    template<typename TVertex>
    Triangle<TVertex> make_Triangle(
        const TVertex& v1,
        const TVertex& v2,
        const TVertex& v3)
    {
        return Triangle<TVertex>{v1, v2, v3};
    }
}