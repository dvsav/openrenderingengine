1. How to organize Visual Studio solution folders.
    http://stackoverflow.com/questions/5321404/vc2010-c-organizing-source-files
    I decided that the VS solution folder would have the following structure:
    RenderingEngine (solution folder)
        include (dependency header files)
        lib (dependency library files)
        src (project's source code, including both header and source files)
        build (project's built binary files)
            Debug (debug build)
            Release (release build)
    
    After that I had to change some project properties
    Project -> Properties -> General -> Output Directory
        $(SolutionDir)\build\$(Configuration)\
    Project -> Properties -> Debugging -> Working Directory
        $(OutDir)
    
    Then I created a main.cpp file in src folder with helloworld code.

2. Next I was going to use GLEW library in my project.
    GLEW is a library that loads OpenGL function packets (called "extensions"). If you don't use GLEW you have to get each function's address manually through WinAPI function wglGetProcAddress (see https://www.khronos.org/opengl/wiki/Load_OpenGL_Functions).
    I downloaded GLEW binaries from http://glew.sourceforge.net/
    I copied  GLEW lib files (Win32 version) to "\lib\GLEW" folder.
    There are two lib files there "glew32.lib" and "glew32s.lib". The former one is for dynamic link to glew32.dll library and the latter one is for static link (in that case you don't need glew32.dll).
    I copied GLEW headers (we need only glew.h and wglew.h because we target Windows platform only) to "\include\GL" folder.
    I copied glew32.dll (Win32 version) to "\build\Debug" and "\build\Release" folders.
    WGL is an API between OpenGL and the windowing system interface of Windows (https://en.wikipedia.org/wiki/WGL_(API)).
    
    After that I had to change some project properties
    Project -> Properties -> Linker -> General -> Additional Library Directories
        $(SolutionDir)\lib\GLEW;%(AdditionalLibraryDirectories)
    Project -> Properties -> C/C++ -> General -> Additional Include Directories
        $(SolutionDir)\include\GL;%(AdditionalIncludeDirectories)
        
    Then I created GLEWHeaders.h header file which included the abovementioned GLEW headers.

3. How to organize namespaces in the project.
    I decided that there would be the following namespaces:
        opengl - everything concerning OpenGL
        win - everything concerning WinAPI (Windows platform specific)
        engine - everything concerning our rendering engine
        util - miscelaneous utility functions
        vmath - vector math (partly Intel platform specific)

4. How to initialize GLEW.
    We need GLEW library to call OpenGL API functions. But to use GLEW we need to "initialize" it.
    To render anything on the screen with OpenGL API we need an entity called "OpenGL rendering context".
    To initialize GLEW we need this OpenGL rendering context as well. If there is no existent OpenGL rendering context which is current then function glewInit() will fail.
    At the same time we cannot create a realy able OpenGL rendering context until GLEW is initialized.
    The solution is to create a "false" rendering context, with which we are not going to render anything. To create that context we have to create a "false" window which we are not going to show and which we destroy once we initialized GLEW.
    See the code in files GlewInitializer.h and GlewInitializer.cpp.
    Why do we need to initialize GLEW to create a really able rendering context? Because to do that we need to call the following GLEW functions: wglChoosePixelFormatARB(), wglCreateContextAttribsARB().

5. In order to perform rendering we have to create a window. This leads us to writing a Window class. Creating and showing a window must be as simple as
    win::Window wnd("My Window", 640, 480);
    wnd.Show();

6. Naming conventions.
    private class fields' names start with "m_";
    static class fields' names start with "s_";
    public and private class methods start with capital letter except those starting with "get" and "set".

7. Assertions.
    I first encountered the need to check function's arguments when writing the Window class's constructor (assert that width and height are greater than zero).
    References:
        Jason Gregory - Game Engine Architecture 2014, p. 149, paragraph 3.3.3 Assertions.
        http://www.cplusplus.com/reference/cassert/assert/
        https://habrahabr.ru/post/141080/
    I had two options when dealing with assertions:
        a) assert(width > 0);
        b) if(width <= 0) throw std::invalid_argument("width must be > 0!");
    The choice depends on considering the error as either a programming error (a) or as a user error (b).
    I chose to consider this particular error as a user error (b).
    On the other hand when I unregister a window in Window::WindowProcedure() -> case WM_DESTROY:
    I do that as follows:
        // remove window descriptor from the registered windows collection
        auto elems_erased = s_RegisteredWindows.erase(m_hWnd);
        assert(elems_erased == 1);
    because if erase fails, this is certainly a programmer's error who forgot to register the window.
    I have encapsulated throwing exceptions on user errors into Requires namespace in Requires.h.

8. Creating OpenGL rendering context
    OpenGL rendering context is "an object that holds all of OpenGL" (https://www.khronos.org/opengl/wiki/OpenGL_Context).
    In order to make any rendering operations through the OpenGL API we first have to create the so-called OpenGL rendering context - the thing which is bound to the ordinary window's device context.
    Read https://msdn.microsoft.com/en-us/library/windows/desktop/dd183553(v=vs.85).aspx about what the device context is.
    To create the OpenGL rendering context we first need to set the so-called pixel format.
    Read more at https://www.opengl.org/wiki/Creating_an_OpenGL_Context_(WGL)
    I have encapsulated all these operations into the constructor of a class named opengl::OpenGLWindow.

9. Keeping different pieces of code decoupled.
    At first I wrote opengl::OpenGLWindow so that it referenced win::Window (and OpenGLWindow's constructor looked like this: OpenGLWindow(win::Window& wnd)).
    But after some time I decided to decouple opengl::OpenGLWindow from win::Window so that each of these two classes could be developed completely independently.
    So now opengl::OpenGLWindow does not reference win::Window and just holds a copy of its device context's handle (so now OpenGLWindow's constructor looks like this: OpenGLWindow(HDC deviceContext)).

10. Handling windows messages.
    First of all we need to handle user input which can appear either from the mouse or from the keyboard. When user moves the mouse or clicks or presses any key on the keyboard Windows sends a message to our window.
    In C# there is a very convenient built-in way for subscribing to these events. You just write for example window.KeyPress += MyKeyPressEventHandler; where MyKeyPressEventHandler is a method of your class. The method is called each time user presses a key on the keyboard.
    At https://www.codeproject.com/Articles/6197/Emulating-C-delegates-in-Standard-C I found the C++ code to emulate this C#-like style. I've changed it a little and finally made util::Event class.
    This class has one peculiarity. In C# only the class that contains the event can invoke (raise) that event. External code can subscribe to the event but cannot raise it.
    util::Event class has public methods for both subscribing and rasing of the event which are both accessible by external code, so the external code can not only subscribe to the event,
    it also can raise the event and this is a problem.
    
11. Measuring time.
    Game engines usually need to measure time somehow. CPU usually has a register which is incremented at each clock cycle (the high resolution timer register).
    Operation system provides an API for reading this register.
    See Jason Gregory - Game Engine Architecture 2014, p. 353, paragraph 7.5.3 Measuring Real Time with a High-Resolution Timer.
    Here we have at least two alternatives:
    a) std::chrono API from C++ standard library;
    b) WinAPI function QueryPerformanceCounter();
    I chose (b) and encapsulated this in a class win::HighResolutionTimer.

12. Vector math API.
    In order to make 3d-objects and view them in 3d we need some kind of vector math library, including at least two classes: Vector and Matrix.
    I placed these classes to namespace vmath.
    I do vector math with SIMD extensions where this gives performance boost (see Jason Gregory - Game Engine Architecture, p. 218, paragraph 4.7 Hardware-Accelerated SIMD Math).
    I've tested vector and matrix multiplications and found that using SSE doubles performance.
    Note that I'm going to store matrix elements in column-major order because OpenGL assumes this order by default.

13. Shaders.
    Since OpenGL 3.0 rendering is only possible with shaders - small programs written in a special programming language GLSL and executed by the GPU (graphics processor unit).
    Several shaders are linked together to make up a program. I've encapsulated working with shaders and programs in two classes: opengl::Shader and opengl::Program.

14. Minimal rendering.
    Minimal OpenGL-based rendering program is comprised of the following parts:
    a) Compile at least two shaders (vertex and fragment) and link them into a program (see opengl::Shader and opengl::Program);
    b) Create a Vertex Array Object (VAO) (see opengl::VertexArrayObject);
    c) Provide vertex shader with required input parameters (see for example glVertexAttrib3f() function);
    d) Submit rendering commad to OpenGL (see for example glDrawArrays() function).
    See Sellers et al - OpenGL SuperBible Comprehensive Tutorial and Reference, 7th Edition - 2015, p. 60, Chapter 2. Our First OpenGL Program.

15. Feeding data from GPU memory buffer.
    Data for shader attributes are usually taken from the so-called 'buffer objects'. To provide shader with some data you have to:
    a) Create a buffer object;
    b) Fill the buffer object with some vertex data (for example, vertex coordinates (x, y, z) and colors (r, g, b));
    c) Specify the format of the data in the buffer (which piece is the coordinates and which is the color for example);
    d) All this information is bound to the VAO object.
    See Sellers et al - OpenGL SuperBible Comprehensive Tutorial and Reference, 7th Edition - 2015, p. 137, Chapter 5. Data. Buffers.
    I've encapsulated a sample code reading a triangle in a class snippets::Triangle.
    
16. Static mesh.
    Generalizing the previous example of rendering a triangle, I've created a class engine::StaticMesh which allows us to specify an arbitrary mesh (mesh is a 3d-object model comprised of vertices).
    Each vertex is comprised of position and color. To render itself a StaticMesh object also needs a shader program and the names of two of its input attributes (for the position and the color of a vertex).

17. Allowing the camera to move and rotate.
    There are several concepts in 3d graphcis: 3d-object (model), world, camera, framebuffer.
    For these concepts there are several respective coordinate spaces: model space, world space, camera space, clip space.
    A 3d-scene is comprised of:
    a) A set of 3d-objects (meshes). Mesh vertices are specified in the model space.
       A 3d-object also has position and orientation in the world space which determin a matrix transforming the mesh from its model space to world space.
    b) A camera (which is specified by two vectors in the world space: view direction and up direction;
    Finally a 3d-object is transformed consequently from one space to another: model->world->camera->clip.
    At each of these steps we use certain transformation matrices which are called accordingly:
    model-to-world matrix, world-to-camera matrix, perspective projection matrix.
    The latter one projects a 3d-scene to the 2d-screen utilizing the so called perspective division ({x, y, z, w} -> {x/w, y/w, z/w, 1}).
    This set of matrices is passed to the vertex shader as input parameter. There are several sorts of input parameters in a shader program.
    Some parameters are taken in per-vertex manner some are in per-object or per-frame or whatever. The former are the so called 'attributes'. the latter are 'uniforms' or 'uniform blocks'.
    So, the set of matrices is the same for the whole 3d-object and therefore it is passed to the shader as a uniform block.
    I've encapsulated all calculations of world-to-camera and perspective projection matrices in a class engine::Camera.
    About spaces and matrices see
    Jason Gregory - Game Engine Architecture 2014, p. 190, paragraph 4.3.9 Coordinate spaces.
    And I've encapsulated passing of the three abovementioned matrices to the shader program as uniform block in a class engine::TransformBlock.
    About uniforms see
    Sellers et al - OpenGL SuperBible Comprehensive Tutorial and Reference, 7th Edition - 2015, p. 155, Chapter 5. Data. Uniforms.
    In the main program I've subscribed to KeyDown and MouseMove enents of the window and preformed camera motion on KeyDown event and camera rotation on MouseMove event.

18. Left-handed and right-handed coordinate systems.
    There's one important thing which is worth mentioning when calculating model-to-world and world-to-camera matrices. When we calculate them we are likely to use cross product of two vectors (forward and up direction of camera or model) to get the third vector (right direction). So, we will get different results depending on which coordinate system we use.
    There are two kinds of coordinate systems: left-handed and right-handed. In physics and mathematics we usually use right-handed system, though in 3d-graphics applications they more often use left-handed one.
    The point is that cross product changes its meaning if you change your choice of coordinate system from right-handed to left-handed and vise-versa. Cross product in right-handed system obey the 'right screw rule', while that in left-handed systems obeys 'left ... rule'.
    I use LEFT-handed coordinate system (Z is directed from user towards the screen, X is directed to the right and Y to the up direction).
    See Jason Gregory - Game Engine Architecture 2014, p. 167, paragraph 4.2.2 Left-Handed versus Right-Handed Coordinate Systems.
    
19. Encapsulation of position and orientation.
    There are two kinds of objects that can move and rotate in the world space: cameras and 3d-models. So they have something in common: their transformation to/from world space. There's also a difference: from camera we want world-to-camera matrix while from model we want model-to-world matrix. Orientation (rotation) parts of both matrices can be calculated from two vectors: model or camera's forward direction and up direction. Translation parts can be easily taken from model or camera's position.
    I've encapsulated this position and orientation data in a class engine::WorldPositionAndOrientation.

20. OpenGL functions in use.
    For now the engine uses the following OpenGL functions:
    Function                    OpenGL Version
    glAttachShader              2.0
    glBindBuffer                2.0
    glBindBufferBase            3.0
    glBindVertexArray           3.0
    glBindVertexBuffer          4.3
    glBufferData                2.0
    glBufferSubData             2.0
    glClearBufferSubData        4.3
    glCompileShader             2.0
    glCopyBufferSubData         3.1
    glDebugMessageCallback      4.3
    glDeleteBuffers             2.0
    glDeleteProgram             2.0
    glDeleteShader              2.0
    glDeleteVertexArrays        3.0
    glDepthFunc                 2.0
    glDepthMask                 2.0
    glDepthRange                2.0
    glDetachShader              2.0
    glDrawArrays                2.0
    glEnable                    2.0
    glEnableVertexAttribArray   2.0
    glFlushMappedBufferRange    3.0
    glGenBuffers                2.0
    glGenVertexArrays           3.0
    glGetAttribLocation         2.0
    glGetBufferSubData          2.0
    glGetIntegerv               2.0
    glGetProgramInfoLog         2.0
    glGetProgramiv              2.0
    glGetProgramResourceIndex   4.3
    glGetShaderInfoLog          2.0
    glGetShaderiv               2.0
    glGetUniformBlockIndex      3.1
    glGetUniformLocation        2.0
    glLinkProgram               2.0
    glMapBuffer                 2.0
    glMapBufferRange            3.0
    glPointSize                 2.0
    glShaderSource              2.0
    glShaderStorageBlockBinding 4.3
    glTexStorage2D              4.2
    glUniformBlockBinding       3.1
    glUnmapBuffer               2.0
    glUseProgram                2.0
    glVertexAttrib3fv           2.0
    glVertexAttribBinding       4.3
    glVertexAttribFormat        4.3
    glVertexAttribPointer       2.0
    glViewport                  2.0

21. About floating-point numbers precision and the scale of the world space https://www.gamedev.net/forums/topic/466331-floating-point-precision-problem-for-3d-universe/
    The point is that we shan't bother of this because the precision of 32-bit float is enough to handle a world as big as San Francisco.

22. Example of opengl::FramebufferObject usage. Here we render the scene to the framebuffer once and save framebuffer
    data to a BMP file.

    opengl::FramebufferObject fbo;

    int width, height;
    win::getClientSize(getWindow(), width, height);

    opengl::Renderbuffer depthBuffer(
        opengl::INTERNAL_FORMAT::DEPTH_COMPONENT32,
        width, height);

    opengl::Renderbuffer colorBuffer(
        opengl::INTERNAL_FORMAT::RGB8,
        width, height);

    fbo.AttachRenderbuffer(
        depthBuffer,
        opengl::FramebufferObject::ATTACHMENT::DEPTH);

    fbo.AttachRenderbuffer(
        colorBuffer,
        opengl::FramebufferObject::ATTACHMENT::COLOR0);

    fbo.Render(
        std::vector<opengl::FramebufferObject::COLOR_ATTACHMENT> { opengl::FramebufferObject::COLOR_ATTACHMENT::COLOR0 },
        [this]() -> void
        {
            opengl::ClearAllBuffers();

            // do some drawing...
            m_Wall.Render();
            m_Model1.Render();
            m_Model2.Render();
        });

    engine::SaveScreenshot(
        fbo,
        opengl::FramebufferObject::COLOR_ATTACHMENT::COLOR0,
        "C:\\screenshot.bmp",
        width, height);

23. Another example of opengl::FramebufferObject usage. The difference from the previous one is that we use a texture as color buffer
    (we write the scene into the texture).
    const GLsizei width = 512;
    const GLsizei height = 512;

    opengl::FramebufferObject fbo;

    opengl::Renderbuffer depthBuffer(
        opengl::INTERNAL_FORMAT::DEPTH_COMPONENT32,
        width,
        height);

    opengl::Texture2D textureColor(
        /*mipmap_levels_number*/ 1,
        /*internalFormat*/ opengl::INTERNAL_FORMAT::RGB8,
        /*width*/ width,
        /*height*/ height,
        /*format*/ opengl::PIXEL_FORMAT::BGR,
        /*type*/ opengl::TYPE::UNSIGNED_BYTE);

    fbo.AttachRenderbuffer(
        depthBuffer,
        opengl::FramebufferObject::ATTACHMENT::DEPTH);

    fbo.AttachTexture(
        /*attachment*/ opengl::FramebufferObject::ATTACHMENT::COLOR0,
        /*textureHandle*/ textureColor.getHandle());

    fbo.Render(
        std::vector<opengl::FramebufferObject::COLOR_ATTACHMENT> { opengl::FramebufferObject::COLOR_ATTACHMENT::COLOR0 },
        [this]() -> void
    {
        opengl::ClearAllBuffers();

        // do some drawing...
        m_Skybox->Render();
        m_Earth->Render();
        m_Moon->Render();
    });

    engine::SaveTextureImage(
        "D:\\framebuf.bmp",
        textureColor);

24. Example of saving depth buffer (texture) in a BMP file:
    const GLsizei width = 512;
    const GLsizei height = 512;

    opengl::FramebufferObject fbo;

    opengl::Texture2D textureDepth(
        /*mipmap_levels_number*/ 1,
        /*internalFormat*/ opengl::INTERNAL_FORMAT::DEPTH_COMPONENT32,
        /*width*/ width,
        /*height*/ height,
        /*format*/ opengl::PIXEL_FORMAT::DEPTH_COMPONENT,
        /*type*/ opengl::TYPE::FLOAT);

    textureDepth.setupDepthTest(
        /*compareMode*/ opengl::Texture2D::COMPARE_MODE::COMPARE_REF_TO_TEXTURE,
        /*compareFunc*/ opengl::COMPARISON_FUNCTION::LESS);

    fbo.AttachTexture(
        /*attachment*/ opengl::FramebufferObject::ATTACHMENT::DEPTH,
        /*textureHandle*/ textureDepth.getHandle());

    fbo.Render(
        std::vector<opengl::FramebufferObject::COLOR_ATTACHMENT> { opengl::FramebufferObject::COLOR_ATTACHMENT::NONE },
        [this]() -> void
        {
            opengl::ClearAllBuffers();

            m_Skybox->Render();
            m_Earth->Render();
            m_Moon->Render();
        });

    std::unique_ptr<GLfloat[]> depth(new GLfloat[width * height]);

    textureDepth.getImage(0, depth.get());

    std::unique_ptr<char[]> pixels(new char[width * height * 3]);

    for (int x = 0; x < width; x++)
    {
        for (int y = 0; y < height; y++)
        {
            char d = static_cast<char>(depth[x + y * width] * 256);
            pixels[(x + y * width) * 3 + 0] = d;
            pixels[(x + y * width) * 3 + 1] = d;
            pixels[(x + y * width) * 3 + 2] = d;
        }
    }

    win::WriteBMP24File(
        /*filename*/ "D:\\depth.bmp",
        /*width*/ width,
        /*height*/ height,
        /*pixels*/ pixels.get());

25. glTexParameter*() function affects only the currently active texture unit (see glActiveTexture() function) and works properly only if there is a texture bound to that texture unit.

26. When making shadow mapping using depth texture your depth texture must have exactly the same size (width and height) as your current viewport (see glViewport).

27. When dealing with shadow mapping you are likely to use subroutines (subroutine uniforms) in your shaders (one subroutine for filling out the shadow map and another one - for rendering).
    You must be aware of the following (https://www.khronos.org/opengl/wiki/Shader_Subroutine):
    "The biggest difference between those state and the subroutine state is this: EVERY TIME you call glUseProgram, glBindProgramPipeline or glUseProgramStages, all of the current subroutine state is completely lost.
    This state is never preserved, so you must reset it every single time you change programs."
    Note that you have to setup subroutines only when you are going to do rendering. This is why I added two methods into opengl::Program class:
    Use() and UseForRendering(). In the latter one subroutines are set up. I also added a public method SetupSubroutines() into opengl::Program class
    so that I could call it when I bind opengl::ProgramPipeline.