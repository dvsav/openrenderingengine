/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#define GEOMETRY

#include "include/lighting_uniforms.glsl"

layout (triangles) in;
layout (triangle_strip, max_vertices = 54) out; // (max number of lights = MAX_LIGHTS = 3) * (6 faces of cube) * (3 vertices of triangle)

subroutine void EmitGeometry(); // function signature
subroutine uniform EmitGeometry EmitShadowMapGeometry; // uniform variable

subroutine (EmitGeometry)
void EmitDirLightShadows()
{
    for (int light = 0; light < directionalLights.count; light++)
    {
        gl_Layer = light;
        
        for (int i = 0; i < gl_in.length(); i++)
        {
            gl_Position = directionalLights.items[light].WorldToLightProjectionPlane * gl_in[i].gl_Position;
            EmitVertex();
        }
        EndPrimitive();
    }
}

subroutine (EmitGeometry)
void EmitPointLightShadows()
{
    for (int light = 0; light < pointLights.count; light++)
    {
        for (int face = 0; face < 6; face++)
        {
            gl_Layer = light * 6 + face;
            
            for (int i = 0; i < gl_in.length(); i++)
            {
                gl_Position = pointLights.items[light].LightMatrices[face] * gl_in[i].gl_Position;
                EmitVertex();
            }
            EndPrimitive();
        }
    }
}

subroutine (EmitGeometry)
void EmitSpotLightShadows()
{
    for (int light = 0; light < spotLights.count; light++)
    {
        gl_Layer = light;
        
        for (int i = 0; i < gl_in.length(); i++)
        {
            gl_Position = spotLights.items[light].WorldToLightProjectionPlane * gl_in[i].gl_Position;
            EmitVertex();
        }
        EndPrimitive();
    }
}

void main()
{
    EmitShadowMapGeometry();
    
    // === somehow geometry shader subroutine doesn't work on nvidia without this: ===
    for (int i = 0; i < gl_in.length(); i++)
        EmitVertex();
    EndPrimitive();
    // ==============================================================================
}
