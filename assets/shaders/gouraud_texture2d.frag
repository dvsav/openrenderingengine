/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

// >>>--- UNIFORMS --->>>
uniform sampler2D Texture2dDiffuse;
// <<<--- UNIFORMS ---<<<

// >>>--- INPUT VARIABLES --->>>
smooth in vec4 vs_diffuse_color;
smooth in vec4 vs_specular_color;
smooth in vec2 vs_tex_coord;
// <<<--- INPUT VARIABLES ---<<<

// >>>--- OUTPUT VARIABLES --->>>
out vec4 color;
// <<<--- OUTPUT VARIABLES ---<<<

void main()
{
    color = vs_diffuse_color * texture(Texture2dDiffuse, vs_tex_coord) + vs_specular_color;
}