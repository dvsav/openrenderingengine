/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#define VERTEX

#include "include/vertex_attributes.glsl"
#include "include/mvp_uniforms.glsl"

void main(void)
{
    gl_Position = VertexToWorld * vec4(v3_position, 1.0f);
}
