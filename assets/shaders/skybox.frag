/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

uniform samplerCube TextureDiffuse;

smooth in vec3 vs_tex_coord;

out vec4 color;

void main()
{
    color = texture(TextureDiffuse, vs_tex_coord);

    if(gl_FrontFacing)
        gl_FragDepth = 0.9999999f; // float32 precision for value 1.0 is 1e-7
    else
        discard;
}