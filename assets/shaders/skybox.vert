/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

layout (location = 0) in vec3 v3_position; // vertex position

layout (std140) uniform ModelViewPerspective
{
    mat4 m4_MVP;
};

smooth out vec3 vs_tex_coord;

void main(void)
{
    vs_tex_coord = v3_position.xyz;
    gl_Position = m4_MVP * vec4(v3_position, 1.0f);
}