/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#define FRAGMENT

#include "include/lighting.glsl"

out vec4 color;

void main()
{
    color = Shade();
}
