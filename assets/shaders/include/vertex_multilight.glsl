#ifdef VERTEX

#include "vertex_attributes.glsl"
#include "lighting_uniforms.glsl"
#include "mvp_uniforms.glsl"
#include "vertex_outputs.glsl"

void vertex_multilight()
{
    #ifdef TEXTURE_2D
        vs_tex2d_coord = v2_tex_coords;
    #endif

    #ifdef TEXTURE_CUBEMAP
        vs_cubemap_coord = v3_position.xyz;
    #endif

    vs_world_position = VertexToWorld * vec4(v3_position, 1.0f);
    vs_world_normal = NormalToWorld * vec4(v3_normal, 0.0f);
    
    #ifdef NORMAL_MAP
        vs_world_tangent = NormalToWorld * v4_tangent;
        vs_world_binormal = vec4( cross(vs_world_normal.xyz, vs_world_tangent.xyz) * v4_tangent.w, 0.0 );
    #endif
    
    for (int i = 0; i < directionalLights.count; i++)
        vs_dir_shadow_coord[i] = directionalLights.items[i].WorldToShadowMap * vs_world_position;
    
    for (int i = 0; i < spotLights.count; i++)
        vs_spot_shadow_coord[i] = spotLights.items[i].WorldToShadowMap * vs_world_position;
    
    for (int i = 0; i < pointLights.count; i++)
    {
        vec4 cam_position = vs_world_position - pointLights.items[i].Position;
        // Y-axis is inverted for cubemaps (image origin is in the upper left instead of the usual lower left)
        // This is done in OpenGL in order to follow the RenderMan specification. So we need to invert Y-axis again.
        vs_point_shadow_coord[i] = vec3(cam_position.x, -cam_position.y, cam_position.z);
    }
        
    gl_Position = WorldToScreen * vs_world_position;
}

#endif
