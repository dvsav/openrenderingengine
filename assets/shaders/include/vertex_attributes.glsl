#ifdef VERTEX

layout (location = 0) in vec3 v3_position;   // vertex position
layout (location = 1) in vec3 v3_normal;     // surface normal (normalized)
layout (location = 2) in vec2 v2_tex_coords; // texture coordinates

#ifdef NORMAL_MAP
    // The 4th component of Tangent represents the
    // handedness of the tangent coordinate system
    // and can be either +1 or -1
    layout (location = 3) in vec4 v4_tangent;
#endif

#endif
