#include "lighting_structs.glsl"

const int MAX_LIGHTS = 3;
const int MAX_POINT_LIGHTS = MAX_LIGHTS;
const int MAX_SPOT_LIGHTS = MAX_LIGHTS;
const int MAX_DIR_LIGHTS = MAX_LIGHTS;

layout (std140) uniform AmbientLight
{
    AmbientLightStruct value;
} ambientLight;

layout (std140) uniform DirectionalLights
{
    int count;
    DirectionalLightStruct items[MAX_DIR_LIGHTS];
} directionalLights;

layout (std140) uniform PointLights
{
    int count;
    PointLightStruct items[MAX_POINT_LIGHTS];
} pointLights;

layout (std140) uniform SpotLights
{
    int count;
    SpotLightStruct items[MAX_SPOT_LIGHTS];
} spotLights;

uniform sampler2DArrayShadow TextureDirShadowMaps;
uniform sampler2DArrayShadow TextureSpotShadowMaps;
uniform samplerCubeArrayShadow TexturePointShadowMaps;

#if defined(TEXTURE_2D)
    uniform sampler2D TextureDiffuse;
#elif defined(TEXTURE_CUBEMAP)
    uniform samplerCube TextureDiffuse;
#endif

#ifdef NORMAL_MAP
    uniform sampler2D TextureNormalMap;
#endif
