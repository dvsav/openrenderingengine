#include "lighting_uniforms.glsl"

#ifdef VERTEX
    #define INOUT out
#else
    #define INOUT in
#endif

smooth INOUT vec4 vs_world_position;
smooth INOUT vec4 vs_world_normal;
#ifdef NORMAL_MAP
    smooth INOUT vec4 vs_world_tangent;
    smooth INOUT vec4 vs_world_binormal;
#endif
smooth INOUT vec2 vs_tex2d_coord;
smooth INOUT vec3 vs_cubemap_coord;

smooth INOUT vec4 vs_dir_shadow_coord[MAX_DIR_LIGHTS];
smooth INOUT vec4 vs_spot_shadow_coord[MAX_SPOT_LIGHTS];
smooth INOUT vec3 vs_point_shadow_coord[MAX_POINT_LIGHTS];
