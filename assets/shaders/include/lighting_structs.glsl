struct AmbientLightStruct
{
    vec4 Color;
};

struct DirectionalLightStruct
{
    vec4 Color;
    vec4 Direction;
    mat4 WorldToLightProjectionPlane;
    mat4 WorldToShadowMap; // bias * WorldToLightProjectionPlane
};

struct AttenuationStruct
{
    float Range;
    float Constant;
    float Linear;
    float Quadratic;
};

const int POS_X = 0;
const int NEG_X = 1;
const int POS_Y = 2;
const int NEG_Y = 3;
const int POS_Z = 4;
const int NEG_Z = 5;

struct PointLightStruct
{
    vec4 Color;
    vec4 Position;
    mat4 CameraToScreen;
    /*
    Face Value    Resulting Target
    0 GL_TEXTURE_CUBEMAP_POSITIVE_X
    1 GL_TEXTURE_CUBEMAP_NEGATIVE_X
    2 GL_TEXTURE_CUBEMAP_POSITIVE_Y
    3 GL_TEXTURE_CUBEMAP_NEGATIVE_Y
    4 GL_TEXTURE_CUBEMAP_POSITIVE_Z
    5 GL_TEXTURE_CUBEMAP_NEGATIVE_Z
    */
    mat4 LightMatrices[6];
    AttenuationStruct Attenuation;
};

struct SpotLightStruct
{
    vec4 Color;
    vec4 Position;
    vec4 Direction;
    mat4 WorldToLightProjectionPlane;
    mat4 WorldToShadowMap; // bias * WorldToLightProjectionPlane
    AttenuationStruct Attenuation;
    float EdgeCosine;
};
