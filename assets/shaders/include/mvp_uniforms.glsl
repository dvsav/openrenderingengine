layout (std140) uniform ModelToWorld
{
    mat4 VertexToWorld; // model-to-world transfomation matrix
    mat4 NormalToWorld; // usually it's the model-to-world matrix with eliminated translation component
};

layout (std140) uniform CameraInfo
{
    mat4 WorldToScreen;  // world space to screen space (view * projection)
    vec3 CameraPosition; // camera position in world space
};
