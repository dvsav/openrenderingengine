#include "lighting_uniforms.glsl"
#include "mvp_uniforms.glsl"
#include "material_uniforms.glsl"
#include "vertex_outputs.glsl"
#include "lighting_functions.glsl"

#ifdef SOFT_SHADOWS
    float texture_pcf(sampler2DArrayShadow sampler, vec4 P)
    {
        float shadow = 0.0f;
        shadow += textureOffset(sampler, P, ivec2(-1, -1));
        shadow += textureOffset(sampler, P, ivec2(-1,  1));
        shadow += textureOffset(sampler, P, ivec2( 1,  1));
        shadow += textureOffset(sampler, P, ivec2( 1, -1));
        return shadow * 0.25f;
    }
#else
    #define texture_pcf texture
#endif

void CalcLights(
    out vec4 diffuse,
    out vec4 specular)
{
    #ifdef NORMAL_MAP
        // normal vector in tangent space
        vec3 ts_normal = 2.0 * texture(TextureNormalMap, vs_tex2d_coord).rgb - 1.0;
        vec3 world_normal =
            +ts_normal.r * vs_world_tangent.xyz
            +ts_normal.g * vs_world_binormal.xyz
            +ts_normal.b * vs_world_normal.xyz;
    #else
        vec3 world_normal = vs_world_normal.xyz; 
    #endif

    diffuse = vec4(0.0f);
    specular = vec4(0.0f);
    
    vec3 eye_direction = normalize(CameraPosition - vs_world_position.xyz);
    
    for (int i = 0; i < directionalLights.count; i++)
    {
        ShadeStruct shade = CalcDirectionalLight(
            /*eyeDirection*/ eye_direction,
            /*lightDirection*/ directionalLights.items[i].Direction.xyz,
            /*surfaceNormal*/ world_normal,
            /*shininess*/ Shininess);

        float shadow = texture_pcf(TextureDirShadowMaps, vec4(vs_dir_shadow_coord[i].xy / vs_dir_shadow_coord[i].w, i, vs_dir_shadow_coord[i].z / vs_dir_shadow_coord[i].w));
        diffuse += shade.Diffuse * directionalLights.items[i].Color * shadow;
        specular += shade.Specular * directionalLights.items[i].Color * shadow;
    }
    
    for (int i = 0; i < spotLights.count; i++)
    {
        ShadeStruct shade = CalcSpotLight(
            /*eyeDirection*/ eye_direction,
            /*fragmentPosition*/ vs_world_position.xyz,
            /*light*/ spotLights.items[i],
            /*surfaceNormal*/ world_normal,
            /*shininess*/ Shininess);
            
        float shadow = texture_pcf(TextureSpotShadowMaps,
            vec4(vs_spot_shadow_coord[i].xy / vs_spot_shadow_coord[i].w,
                 i,
                 vs_spot_shadow_coord[i].z / vs_spot_shadow_coord[i].w));
        diffuse += shade.Diffuse * spotLights.items[i].Color * shadow;
        specular += shade.Specular * spotLights.items[i].Color * shadow;
    }

    for (int i = 0; i < pointLights.count; i++)
    {
        ShadeStruct shade = CalcPointLight(
            /*eyeDirection*/ eye_direction,
            /*fragmentPosition*/ vs_world_position.xyz,
            /*light*/ pointLights.items[i],
            /*surfaceNormal*/ world_normal,
            /*shininess*/ Shininess);
            
        vec4 light_to_frag = vs_world_position - pointLights.items[i].Position;
        float z_cam = max(
            abs(light_to_frag.x),
            max(abs(light_to_frag.y),
                abs(light_to_frag.z))
        );

        vec4 clip_position = pointLights.items[i].CameraToScreen * vec4(0.0f, 0.0f, z_cam, 1.0f);
            
        float shadow = texture(
            TexturePointShadowMaps,
            /*tex_coords*/ vec4(vs_point_shadow_coord[i], i),
            /*compare*/ 0.5 * (1.0 + clip_position.z / clip_position.w)
        );
            
        diffuse += shade.Diffuse * pointLights.items[i].Color * shadow;
        specular += shade.Specular * pointLights.items[i].Color * shadow;
    }
}

vec4 Shade()
{
    vec4 diffuse;
    vec4 specular;
    CalcLights(diffuse, specular);
    
    #if defined(TEXTURE_2D)
        vec4 diffuse_texel = texture(TextureDiffuse, vs_tex2d_coord);
    #elif defined(TEXTURE_CUBEMAP)
        vec4 diffuse_texel = texture(TextureDiffuse, vs_cubemap_coord);
    #else
        vec4 diffuse_texel = vec4(1.0f, 1.0f, 1.0f, 1.0f);
    #endif
    
    return EmissiveColor
            + (ambientLight.value.Color + diffuse) * DiffuseColor * diffuse_texel
            + specular * SpecularColor;
}