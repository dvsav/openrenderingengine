#ifdef FRAGMENT

#include "lighting_structs.glsl"

struct ShadeStruct
{
    float Diffuse;
    float Specular;
};

ShadeStruct CalcDirectionalLight(
    vec3 eyeDirection,    // direction from the fragment towards the eye (in world space)
    vec3 lightDirection,  // light direction (in world space)
    vec3 surfaceNormal,   // surface normal (normalized vector) (in world space)
    float shininess)      // material shininess (metallicity) typically 0...128
{
    ShadeStruct shade;

    float incidence = clamp(-dot(lightDirection, surfaceNormal), 0.0f, 1.0f);
    
    shade.Diffuse = incidence;
    
    if(incidence > 0.0f)
    {
        vec3 reflected_ray = reflect(lightDirection, surfaceNormal);
        shade.Specular = pow(clamp(dot(reflected_ray, eyeDirection), 0.0f, 1.0f), shininess);
    }
    else
        shade.Specular = 0.0f;
    
    return shade;
}

ShadeStruct CalcPointLight(
    vec3 eyeDirection,      // direction from the fragment towards the eye (in world space)
    vec3 fragmentPosition,  // fragment position (in world space)
    PointLightStruct light, // point light
    vec3 surfaceNormal,     // surface normal (normalized vector) (in world space)
    float shininess)        // material shininess (metallicity) typically 0...128
{
    ShadeStruct shade;

    vec3 light_direction = fragmentPosition - light.Position.xyz;
    float light_distance = length(light_direction);
    light_direction = light_direction / light_distance;
    
    if (light_distance < light.Attenuation.Range)
    {
        float attenuation =
            (light.Attenuation.Quadratic * light_distance + light.Attenuation.Linear) * light_distance + light.Attenuation.Constant;

        shade = CalcDirectionalLight(
            /*eyeDirection*/ eyeDirection,
            /*lightDirection*/ light_direction,
            /*surfaceNormal*/ surfaceNormal,
            /*shininess*/ shininess);
            
        shade.Diffuse = shade.Diffuse / attenuation;
        shade.Specular = shade.Specular / attenuation;
    }
    else
    {
        shade.Diffuse = 0.0f;
        shade.Specular = 0.0f;
    }
    
    return shade;
}

ShadeStruct CalcSpotLight(
    vec3 eyeDirection,     // direction from the fragment towards the eye (in world space)
    vec3 fragmentPosition, // fragment position (in world space)
    SpotLightStruct light, // spot light
    vec3 surfaceNormal,    // surface normal (normalized vector) (in world space)
    float shininess)       // material shininess (metallicity) typically 0...128
{
    ShadeStruct shade;

    vec3 light_direction = fragmentPosition - light.Position.xyz;
    float light_distance = length(light_direction);
    light_direction = light_direction / light_distance;
    
    float sl_factor = dot(light_direction, light.Direction.xyz) - light.EdgeCosine;
    
    if (sl_factor > 0.0f && light_distance < light.Attenuation.Range)
    {
        float attenuation =
            (light.Attenuation.Quadratic * light_distance + light.Attenuation.Linear) * light_distance + light.Attenuation.Constant;

        shade = CalcDirectionalLight(
            /*eyeDirection*/ eyeDirection,
            /*lightDirection*/ light_direction,
            /*surfaceNormal*/ surfaceNormal,
            /*shininess*/ shininess);
            
        float coeff = sl_factor / (1.0f - light.EdgeCosine) / attenuation;
           
        shade.Diffuse = shade.Diffuse * coeff;
        shade.Specular = shade.Specular  * coeff;
    }
    else
    {
        shade.Diffuse = 0.0f;
        shade.Specular = 0.0f;
    }
    
    return shade;
}

#endif
