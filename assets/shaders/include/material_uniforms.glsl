layout (std140) uniform Material
{
    vec4 EmissiveColor;
    vec4 DiffuseColor;
    vec4 SpecularColor;
    float Shininess;
};
