/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

#define VERTEX

#include "include/vertex_multilight.glsl"

void main(void)
{
    vertex_multilight();
}
