/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

// >>>--- VERTEX ATTRIBUTES --->>>
layout (location = 0) in vec3 v3_position;   // vertex position
layout (location = 1) in vec3 v3_normal;     // surface normal (normalized)
layout (location = 2) in vec2 v2_tex_coords; // texture coordinates
// <<<--- VERTEX ATTRIBUTES ---<<<

// >>>--- UNIFORMS --->>>
layout (std140) uniform ModelToWorld
{
    mat4 VertexToWorld; // model-to-world transfomation matrix
    mat4 NormalToWorld; // usually it's the model-to-world matrix with eliminated translation component
};

layout (std140) uniform CameraInfo
{
    mat4 WorldToScreen;
    vec3 CameraPosition; // camera position in world space
};

layout (std140) uniform Material
{
    vec4 DiffuseColor;
    vec4 SpecularColor;
    float Shininess;
};

layout (std140) uniform AmbientLight
{
	vec4 AmbientLightColor;
};

struct AttenuationStruct
{
    float Range;
    float Constant;
    float Linear;
    float Quadratic;
};

layout (std140) uniform PointLight
{
	vec4 PointLightColor;
    vec4 LightPosition;
	mat4 WorldToLightProjectionPlane;
    AttenuationStruct Attenuation;
};
// <<<--- UNIFORMS ---<<<

// >>>--- OUTPUT VARIABLES --->>>
smooth out vec4 vs_diffuse_color;
smooth out vec4 vs_specular_color;
smooth out vec2 vs_tex_coord;
// <<<--- OUTPUT VARIABLES ---<<<

void main(void)
{
    vec4 world_position = VertexToWorld * vec4(v3_position, 1.0f);
    vec3 world_normal = (NormalToWorld * vec4(v3_normal, 1.0f)).xyz;
    vec3 light_direction = normalize(LightPosition.xyz - world_position.xyz);

    float incidence = clamp(dot(light_direction, world_normal), 0.0f, 1.0f);

    vs_diffuse_color = (AmbientLightColor  + incidence * PointLightColor) * DiffuseColor;

    if(incidence > 0.0f)
    {
        vec3 reflected_ray = reflect(-light_direction, world_normal);
        vec3 cam_direction = normalize(CameraPosition - world_position.xyz);

        vs_specular_color = PointLightColor * SpecularColor * pow(clamp(dot(reflected_ray, cam_direction), 0.0f, 1.0f), Shininess);
    }
    else
        vs_specular_color = vec4(0.0f);

    gl_Position = WorldToScreen * world_position;
    vs_tex_coord = v2_tex_coords;
}