/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

void main()
{	
    for (int i=0; i<3; i++)
    {    
        gl_Position = gl_in[i].gl_Position;
        EmitVertex();
    }
    EndPrimitive();
}