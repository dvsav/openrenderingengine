/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

layout (location = 0) in vec3 v3_position; // vertex position
layout (location = 1) in vec4 v4_color;    // vertex color

layout (std140) uniform ModelViewPerspective
{
    mat4 m4_MVP;
};

smooth out vec4 vs_color;

void main(void)
{
    gl_Position = m4_MVP * vec4(v3_position, 1.0f);
                    
    vs_color = v4_color;
}