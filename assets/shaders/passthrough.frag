/*
Copyright (c) 2018 Dmitry Savchenkov

This file is part of RenderingEngine project which is released under MIT license.
See file LICENSE.md or go to https://mit-license.org/ for full license details.
*/

in vec4 vs_color; // color produced by the vertex shader

out vec4 color;

void main()
{
    color = vs_color;
}