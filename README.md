# OpenRenderingEngine

OpenRenderingEngine is an amateur OpenGL 4 based API written in C++ for educational purposes.  
For more information see [the project author's blog](https://dvsav.ru) if you can read in Russian.  
If not - you can check out [development_log.txt](development_log.txt).  

## Dependencies

[GLEW](http://glew.sourceforge.net/)  
[stb](https://github.com/nothings/stb)  
[Assimp](https://www.assimp.org)  
[pugixml](https://pugixml.org)  

## Prerequisites

To build the OpenRenderingEngine you will need the following software:  
[Windows 7 or newer](https://www.microsoft.com/windows/) (required)  
[Microsoft Visual Studio 2017](https://visualstudio.microsoft.com/) (required)  
[CMake 3.19 or newer](https://cmake.org) (optional)  
[git](https://git-scm.com/) (optional)  

## Getting Started

Launch git command line interface. In the command prompt go to the folder to which you want to copy the OpenRenderingEngine source code.  
Type: `git clone https://bitbucket.org/dvsav/openrenderingengine.git` to download the source code on your computer.  

Launch Visual Studio 2017, open [RenderingEngine.sln](build/vs2017/RenderingEngine.sln) and build the project from there.  

Alternatilvely you can use CMake to generate project files for any build system supported in Windows - see [CMake script](build/cmake/CMakeLists.txt).  
For example, to generate a VS2017 solution from the Visual Studio Developer Command Prompt go to build/cmake folder and run `cmake -G "Visual Studio 15 2017" -S . -B ./vs2017`  
To generate NMake makefile run `cmake -G "NMake Makefiles" -S . -B ./nmakemakefiles`.  
Once the project files for the target build system are genearted you can either launch the target build system directly or run `cmake --build`.  
For more information on CMake usage see https://cmake.org/cmake/help/latest/manual/cmake.1.html.  

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
